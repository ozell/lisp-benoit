# lisp-benoit

;;; ".emacs" de defaut pour ma config du Cerca.
;;; Copyright (C) Benoît Ozell, benoit.ozell@polymtl.ca.

;; trouver le répertoire lisp-benoit et l'ajouter au load-path.
(let ((dir nil)
      (tail '(
              "${HOME}/git/lisp-benoit"
              "${HOME}/lisp-benoit"
              )))
  (while (and tail (not dir))
    (if (file-readable-p (substitute-in-file-name (concat (car tail) "/cerca-config.el")))
        (setq dir (substitute-in-file-name (car tail)))
      (setq tail (cdr tail))))
  (if dir (add-to-list 'load-path dir)))

;; charger la config
(setq-default config-saveur 'benoit)
(load "cerca-config" nil t)
(define-key global-map [kp-f1] GOLD-map)

;; * le fichier ~/.emacs-user.el, si present, est lu à la fin de cerca-config.

;; * pour avoir le gold-key sur NumLock, ajouter ces lignes dans "~/.xsession":
;;      xmodmap -e "remove mod2 = Num_Lock"
;;      xmodmap -e "remove mod5 = Num_Lock"
;;      xmodmap -e "keysym Num_Lock = KP_F1"
