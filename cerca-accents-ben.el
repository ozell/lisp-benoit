;;; Redefinition des cles pour entrer des accents francais.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

;; needed for compatibility with XEmacs 19.14
;(if (fboundp 'character-to-event)
;    (defun iso-char-to-event (ch)
;      (interactive "P")
;      "returns an event containing the given character"
;      (character-to-event (list ch)))
;  (defun iso-char-to-event (ch)
;    "returns the character itself"
;    ch))
;;(define-key accents-mode-map "'a"  (vector (iso-char-to-event ?�)))
;;(define-key accents-mode-map "'/"  (vector (iso-char-to-event ?/)))
;;(define-key accents-mode-map "'/"  '(lambda () (interactive) (insert ?/)))
;;(define-key accents-mode-map "'\""  '(lambda () (interactive) (insert ?\")))

;;picture-self-insert
;;  Function: Insert this character in place of character previously at the cursor.
;;self-insert-command
;;  Function: Insert the character you type.

;;(defun accents-insert-string (arg) ""
;;  (interactive "*P")
;;  (message "" (prefix-numeric-value arg))
;;  (prefix-arg-internal [48] nil nil))
;;  (self-insert-command (prefix-numeric-value arg))
;;  )

(defvar accents-mode nil
  "Non-nil if using Accents mode.")
(or (assq 'accents-mode minor-mode-alist)
    (setq minor-mode-alist (append minor-mode-alist (list '(accents-mode " Accents")))))

(defvar accents-mode-map nil
  "Keymap for Accents mode.")
(or (assq 'accents-mode minor-mode-map-alist)
    (setq minor-mode-map-alist (cons (cons 'accents-mode accents-mode-map) minor-mode-map-alist)))

(if accents-mode-map
    nil
  (setq accents-mode-map (make-sparse-keymap))
  (set-keymap-name accents-mode-map 'accents-mode-map)

  (define-key accents-mode-map "'A"  '[�])
  (define-key accents-mode-map "'E"  '[�])
  (define-key accents-mode-map "'I"  '[�])
  (define-key accents-mode-map "'O"  '(lambda () (interactive) (insert "\\OE{}")))
  (define-key accents-mode-map "'U"  '[�])
  (define-key accents-mode-map "'a"  '[�])
  (define-key accents-mode-map "'e"  '[�])
  (define-key accents-mode-map "'i"  '[�])
  (define-key accents-mode-map "'o"  '(lambda () (interactive) (insert "\\oe{}")))
  (define-key accents-mode-map "'u"  '[�])
  (define-key accents-mode-map "'/"  '(lambda () (interactive) (insert ?/)))
  (define-key accents-mode-map "'?"  '(lambda () (interactive) (insert ??)))
  (define-key accents-mode-map "'C"  '[�])
  (define-key accents-mode-map "'c"  '[�])
  (define-key accents-mode-map "' "  '(lambda () (interactive) (insert ?`)))
  (define-key accents-mode-map "''"  '(lambda () (interactive) (insert ?')))
  (define-key accents-mode-map "'`"  '(lambda () (interactive) (insert ?`)))
  (define-key accents-mode-map "'\""  '(lambda () (interactive) (insert ?\")))

  (define-key accents-mode-map "`A"  '[�])
  (define-key accents-mode-map "`E"  '[�])
  (define-key accents-mode-map "`I"  '[�])
  (define-key accents-mode-map "`O"  '[�])
  (define-key accents-mode-map "`U"  '[�])
  (define-key accents-mode-map "`a"  '[�])
  (define-key accents-mode-map "`e"  '[�])
  (define-key accents-mode-map "`i"  '[�])
  (define-key accents-mode-map "`o"  '[�])
  (define-key accents-mode-map "`u"  '[�])
  (define-key accents-mode-map "`/"  '(lambda () (interactive) (insert ?/)))
  (define-key accents-mode-map "`?"  '(lambda () (interactive) (insert ??)))
  (define-key accents-mode-map "`C"  '[�])
  (define-key accents-mode-map "`c"  '[�])
  (define-key accents-mode-map "` "  'tex-insert-quote)
  (define-key accents-mode-map "`'"  '(lambda () (interactive) (insert ?')))
  (define-key accents-mode-map "``"  '(lambda () (interactive) (insert ?`)))
  (define-key accents-mode-map "`\""  '(lambda () (interactive) (insert ?\")))

  (define-key accents-mode-map "\"A"  '[�])
  (define-key accents-mode-map "\"E"  '[�])
  (define-key accents-mode-map "\"I"  '[�])
  (define-key accents-mode-map "\"O"  '[�])
  (define-key accents-mode-map "\"U"  '[�])
  (define-key accents-mode-map "\"a"  '[�])
  (define-key accents-mode-map "\"e"  '[�])
  (define-key accents-mode-map "\"i"  '[�])
  (define-key accents-mode-map "\"o"  '[�])
  (define-key accents-mode-map "\"u"  '[�])
  (define-key accents-mode-map "\"/"  '(lambda () (interactive) (insert ?/)))
  (define-key accents-mode-map "\"?"  '(lambda () (interactive) (insert ??)))
  (define-key accents-mode-map "\"C"  '[�])
  (define-key accents-mode-map "\"c"  '[�])
  (define-key accents-mode-map "\"M"  '(lambda () (interactive) (insert "M�me")))
  (define-key accents-mode-map "\"m"  '(lambda () (interactive) (insert "m�me")))
  (define-key accents-mode-map "\" "  '[^])
  (define-key accents-mode-map "\"'"  '(lambda () (interactive) (insert ?')))
  (define-key accents-mode-map "\"`"  '(lambda () (interactive) (insert ?`)))
  (define-key accents-mode-map "\"\""  '(lambda () (interactive) (insert ?\")))

  (define-key accents-mode-map "/"  '[�])
  (define-key accents-mode-map "?"  '[�])
  )

;;;###autoload
(defun accents-mode (&optional arg)
  "Toggle Accents mode.
With arg, turn Accents mode on if arg is positive, off otherwise.
   Modifie le keymap courant pour entrer des caracteres avec accents ISO (ANSI).
Trois touches sont redefinies:
   celle avec ' et \", celle avec / et ?, celle avec `.
Les touches ' \" et ` ...
   suivies d'une voyelle, inserent la voyelle grave, circonflexe ou trema,
   suivies de c, inserent c-cedille,
   suivies d'un espace, inserent un grave, un circonflexe ou `tex-insert-quote.
   suivies de / ou ?, inserent / ou ?,
   suivies d'un accent, inserent ce dernier
      (deux fois la meme touche insere cet accent).
Les touches / et ? inserent un e aigu et E aigu."
  (interactive "P")
  (make-variable-buffer-local 'accents-mode)
  (put 'accents-mode 'permanent-local t)
  (setq accents-mode
        (if (null arg) (not accents-mode)
          (> (prefix-numeric-value arg) 0))))

(require 'cerca-accents-cvt) ; au lieu de iso-cvt
;;(require 'iso-insert) ; (insert-... )
;;(require 'iso-syntax) ; (set-case-syntax ... standard-case-table)
;;(require 'iso8859-1)  ; (modify-syntax-entry ... standard-syntax-table)

(provide 'accents-mode)
