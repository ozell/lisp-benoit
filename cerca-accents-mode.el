;;; Utilisation de iso-acc pour entrer des accents francais.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

(require 'iso-acc)
(require 'cerca-iso-acc)

;;(if (not (assoc "francais" iso-languages))
;;    (setq iso-languages
;;          (append iso-languages
;;                  '(("francais"
;;                     (?' (?A . ?\300) (?E . ?\310)
;;                         (?a . ?\340) (?e . ?\350)
;;                         (?< . ?\253) (?> . ?\273) (?C . ?\307) (?c . ?\347)
;;                         (?/ . ?/) (?\? . ?\?) (space . ?'))
;;                     (?` (?A . ?\304) (?E . ?\313) (?I . ?\317) (?O . ?\326) (?U . ?\334)
;;                         (?a . ?\344) (?e . ?\353) (?i . ?\357) (?o . ?\366) (?u . ?\374)
;;                         (?< . ?\253) (?> . ?\273) (?C . ?\307) (?c . ?\347)
;;                         (?2 . ?\275) (?3 . ?\276) (?4 . ?\273)
;;                         (?/ . ?/) (?\? . ?\?) (space . ?`))
;;                     (?\" (?A . ?\302) (?E . ?\312) (?I . ?\316) (?O . ?\324) (?U . ?\333)
;;                          (?a . ?\342) (?e . ?\352) (?i . ?\356) (?o . ?\364) (?u . ?\373)
;;                          (?2 . ?\275) (?3 . ?\276) (?4 . ?\273)
;;                          (?/ . ?/) (?\? . ?\?) (space . ?\"))
;;                     (?/ . ?\351)
;;                     (?\? . ?\311)
;;                     )))))

(add-minor-mode 'iso-accents-mode " Accents")
;(or (assq 'iso-accents-mode minor-mode-alist)
;    (setq minor-mode-alist (append minor-mode-alist '((iso-accents-mode " ISO-Acc")))))

(defun iso-acc-minibuf-setup ())

(defun iso-accents-accent-key (prompt)
  "Modify the following character by adding an accent to it."
  ;; Pick up the accent character.
  (if (and iso-accents-mode
           (<= (length (this-single-command-keys)) 1)
           (memq last-input-event iso-accents-enable))
      (iso-accents-compose prompt)
    (char-to-string last-input-event)))

(defun iso-accents-compose (prompt)
  (let* ((first-char last-input-event)
         (list (assq first-char iso-accents-list))
         (second-char nil)
         (entry (cond ((listp (cdr list))
                       ;; Wait for the second key and look up the combination.
                       (setq second-char (read-event))
                       (cdr (assq second-char list)))
                      (t (cdr list)))))
    (if entry
        ;; Found it: return the mapped char
        (vector (iso-char-to-event entry))
      ;; Otherwise, advance and schedule the second key for execution.
      (setq unread-command-events (cons (iso-char-to-event second-char)
                                        unread-command-events))
      (vector (iso-char-to-event first-char)))))

(defun accents-mode (&optional arg)
  "Toggle Accents mode."
  (interactive "P")
  (iso-accents-mode arg)
  (iso-accents-customize "francais")
  (setq iso-accents-enable '(?' ?` ?\" ?/ ?\?))
  )

(require 'cerca-accents-cvt) ; au lieu de iso-cvt
;;(require 'iso-syntax) ; (set-case-syntax ... standard-case-table)
;;(require 'iso8859-1)  ; (modify-syntax-entry ... standard-syntax-table)

(provide 'accents-mode)
