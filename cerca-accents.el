;;; Redefinition des cles pour entrer des accents francais.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

(defvar accents-aigu-map)
(defvar accents-circ-map)
(defvar accents-grav-map)
(defvar accents-trem-map)
(defvar accents-type 'ansi "type d'accents.")

;;picture-self-insert
;;  Function: Insert this character in place of character previously at the cursor.
;;self-insert-command
;;  Function: Insert the character you type.

;;(defun accents-insert-string (arg) ""
;;  (interactive "*P")
;;  (message "" (prefix-numeric-value arg))
;;  (prefix-arg-internal [48] nil nil))
;;  (self-insert-command (prefix-numeric-value arg))
;;  )

;;; accents-mode DEB

(defvar accents-mode nil
  "Non-nil if using Accents mode.")
(make-variable-buffer-local 'accents-mode)
(put 'accents-mode 'permanent-local t)
(or (assq 'accents-mode minor-mode-alist)
    (setq minor-mode-alist (append minor-mode-alist (list '(accents-mode " Accents")))))

(defvar accents-mode-map nil
  "Keymap for accents mode.")
(if accents-mode-map
    nil
  (setq accents-mode-map (make-sparse-keymap))
  (set-keymap-name accents-mode-map 'accents-mode-map)
  ;;(define-key accents-mode-map '(meta o) 'find-file)
  ;;(define-key accents-mode-map '(meta o) 'find-file)
  ;;(define-key accents-mode-map '(meta O) 'revert-buffer)
  )
(or (assq 'accents-mode minor-mode-map-alist)
    (setq minor-mode-map-alist (cons (cons 'accents-mode accents-mode-map) minor-mode-map-alist)))

;;;###autoload
(defun accents-mode-old (&optional arg)
  "Toggle Accents mode.
With arg, turn Accents mode on if arg is positive, off otherwise.
Les arguments possible sont 'ansi, 'ibm, t, nil."
  (interactive "P")
  (setq accents-mode
        (if (null arg) (not accents-mode)
          (> (prefix-numeric-value arg) 0))))

;;; accents-mode FIN

(defun accents-on ()
  "Set le keymap avec accents."
  (interactive)
  (local-set-key "'" accents-grav-map)
  (local-set-key "\"" accents-circ-map)
  (local-set-key "`" accents-trem-map)
  (local-set-key "/" (lookup-key accents-aigu-map "e"))
  (local-set-key "?" (lookup-key accents-aigu-map "E"))
  (message "Accents on.")
  )

(defun accents-off ()
  "Reset le keymap sans accents."
  (interactive)
  (local-unset-key "`")
  (local-unset-key "'")
  (local-unset-key "\"")
  (local-unset-key "/")
  (local-unset-key "?")
  (local-unset-key "^")
  (message "Accents off.")
  )

(defun accents-init-keymap ()
  "Set les keymaps pour recevoir les accents."
  (setq accents-aigu-map (make-sparse-keymap))
  (setq accents-circ-map (make-sparse-keymap))
  (setq accents-grav-map (make-sparse-keymap))
  (setq accents-trem-map (make-sparse-keymap))
  )

(defun accents-finish-keymap ()
  "Termine l'initialisation."
  (define-key accents-grav-map "o"  '(lambda () (interactive) (insert-string "\\oe{}")))
  (define-key accents-grav-map "O"  '(lambda () (interactive) (insert-string "\\OE{}")))

  (define-key accents-grav-map " "  '(lambda () (interactive) (insert ?`))) ; suivi d'un espace
  (define-key accents-grav-map "`"  '(lambda () (interactive) (insert ?`))) ; pourquoi pas !
  (define-key accents-grav-map "'"  '(lambda () (interactive) (insert ?'))) ; l'original
  (define-key accents-grav-map "\"" '(lambda () (interactive) (insert ?\"))) ; pourquoi pas !
  (define-key accents-grav-map "/"  '(lambda () (interactive) (insert ?/))) ; le /
  (define-key accents-grav-map "?"  '(lambda () (interactive) (insert ??))) ; le ?
  (define-key accents-grav-map "\C-g" 'keyboard-quit)

  (define-key accents-circ-map " "  '(lambda () (interactive) (insert ?^))) ; suivi d'un espace
  (define-key accents-circ-map "`"  '(lambda () (interactive) (insert ?`))) ; pourquoi pas !
  (define-key accents-circ-map "'"  '(lambda () (interactive) (insert ?'))) ; pourquoi pas !
  (define-key accents-circ-map "\"" '(lambda () (interactive) (insert ?\"))) ; l'original
  (define-key accents-circ-map "/"  '(lambda () (interactive) (insert ?/))) ; le /
  (define-key accents-circ-map "?"  '(lambda () (interactive) (insert ??))) ; le ?
  (define-key accents-circ-map "\C-g" 'keyboard-quit)

  (define-key accents-trem-map " "  'tex-insert-quote) ; suivi d'un espace
  (define-key accents-trem-map "`"  '(lambda () (interactive) (insert ?`))) ; l'orignal
  (define-key accents-trem-map "'"  '(lambda () (interactive) (insert ?'))) ; pourquoi pas !
  (define-key accents-trem-map "\"" '(lambda () (interactive) (insert ?\"))) ; pourquoi pas !
  (define-key accents-trem-map "/"  '(lambda () (interactive) (insert ?/))) ; le /
  (define-key accents-trem-map "?"  '(lambda () (interactive) (insert ??))) ; le ?
  (define-key accents-trem-map "\C-g" 'keyboard-quit)

  (accents-on)
  )

(defun accents-ansi ()
  "Modifie le keymap courant pour entrer des caracteres avec accents ANSI.
Trois touches sont redefinies:
   celle avec ' et \", celle avec / et ?, celle avec `.
Les touches ' \" et ` ...
   suivies d'une voyelle, inserent la voyelle grave, circonflexe ou trema,
   suivies de c, inserent c-cedille,
   suivies d'un espace, inserent un grave, un circonflexe ou `tex-insert-quote.
   suivies de / ou ?, inserent / ou ?,
   suivies d'un accent, inserent ce dernier
      (deux fois la meme touche insere cet accent).
Les touches / et ? inserent un e aigu et E aigu.
Faire \\[accents-off] pour revenir sans accent."
  (interactive)
  (accents-init-keymap)
  (setq accents-type 'ansi)
  (require 'iso-insert) ; (insert-... )
  (require 'iso-syntax) ; (set-case-syntax ... standard-case-table)
  (require 'iso8859-1)  ; (modify-syntax-entry ... standard-syntax-table)

;;; ... trema
  (define-key accents-trem-map "a"  'insert-a-umlaut)
  (define-key accents-trem-map "e"  'insert-e-umlaut)
  (define-key accents-trem-map "i"  'insert-i-umlaut)
  (define-key accents-trem-map "o"  'insert-o-umlaut)
  (define-key accents-trem-map "u"  'insert-u-umlaut)
  (define-key accents-trem-map "A"  'insert-A-umlaut)
  (define-key accents-trem-map "E"  'insert-E-umlaut)
  (define-key accents-trem-map "I"  'insert-I-umlaut)
  (define-key accents-trem-map "O"  'insert-O-umlaut)
  (define-key accents-trem-map "U"  'insert-U-umlaut)
  (define-key accents-trem-map "c"  'insert-c-cedilla)
  (define-key accents-trem-map "C"  'insert-C-cedilla)

;;; ... aigu
  (define-key accents-aigu-map "a"  'insert-a-acute)
  (define-key accents-aigu-map "e"  'insert-e-acute)
  (define-key accents-aigu-map "i"  'insert-i-acute)
  (define-key accents-aigu-map "o"  'insert-o-acute)
  (define-key accents-aigu-map "u"  'insert-u-acute)
  (define-key accents-aigu-map "A"  'insert-A-acute)
  (define-key accents-aigu-map "E"  'insert-E-acute)
  (define-key accents-aigu-map "I"  'insert-I-acute)
  (define-key accents-aigu-map "O"  'insert-O-acute)
  (define-key accents-aigu-map "U"  'insert-U-acute)
  (define-key accents-aigu-map "c"  'insert-c-cedilla)
  (define-key accents-aigu-map "C"  'insert-C-cedilla)

;;; ... grave
  (define-key accents-grav-map "a"  'insert-a-grave)
  (define-key accents-grav-map "e"  'insert-e-grave)
  (define-key accents-grav-map "i"  'insert-i-grave)
  (define-key accents-grav-map "o"  'insert-o-grave)
  (define-key accents-grav-map "u"  'insert-u-grave)
  (define-key accents-grav-map "A"  'insert-A-grave)
  (define-key accents-grav-map "E"  'insert-E-grave)
  (define-key accents-grav-map "I"  'insert-I-grave)
  (define-key accents-grav-map "O"  'insert-O-grave)
  (define-key accents-grav-map "U"  'insert-U-grave)
  (define-key accents-grav-map "c"  'insert-c-cedilla)
  (define-key accents-grav-map "C"  'insert-C-cedilla)

;;; ... circonflexe
  (define-key accents-circ-map "a"  'insert-a-circumflex)
  (define-key accents-circ-map "e"  'insert-e-circumflex)
  (define-key accents-circ-map "i"  'insert-i-circumflex)
  (define-key accents-circ-map "o"  'insert-o-circumflex)
  (define-key accents-circ-map "u"  'insert-u-circumflex)
  (define-key accents-circ-map "A"  'insert-A-circumflex)
  (define-key accents-circ-map "E"  'insert-E-circumflex)
  (define-key accents-circ-map "I"  'insert-I-circumflex)
  (define-key accents-circ-map "O"  'insert-O-circumflex)
  (define-key accents-circ-map "U"  'insert-U-circumflex)
  (define-key accents-circ-map "c"  'insert-c-cedilla)
  (define-key accents-circ-map "C"  'insert-C-cedilla)
  (define-key accents-circ-map "m"  '(lambda () (interactive) (insert-string "même")))
  (define-key accents-circ-map "M"  '(lambda () (interactive) (insert-string "Même")))

  (accents-finish-keymap)
  (message "Accents ANSI."))

(defun accents-ibm ()
  "Modifie le keymap courant pour entrer des caracteres avec accents sur IBM/RISC.
Trois touches sont redefinies:
   celle avec ' et \", celle avec / et ?, celle avec `.
Les touches ' \" et ` ...
   suivies d'une voyelle, inserent la voyelle grave, circonflexe ou trema,
   suivies de c, inserent c-cedille,
   suivies d'un espace, inserent un grave, un circonflexe ou `tex-insert-quote.
   suivies de / ou ?, inserent / ou ?,
   suivies d'un accent, inserent ce dernier
      (deux fois la meme touche insere cet accent).
Les touches / et ? inserent un e aigu et E aigu.
Faire \\[accents-off] pour revenir sans accent."
  (interactive)
  (accents-init-keymap)
  (setq accents-type 'ibm)

;;; ... trema
  (define-key accents-trem-map "a"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-trem-map "e"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-trem-map "i"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-trem-map "o"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-trem-map "u"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-trem-map "A"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-trem-map "E"  '(lambda () (interactive) (insert-string "Ó")))
  (define-key accents-trem-map "I"  '(lambda () (interactive) (insert-string "Ø")))
  (define-key accents-trem-map "O"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-trem-map "U"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-trem-map "c"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-trem-map "C"  '(lambda () (interactive) (insert-string "")))

;;; ... aigu
  (define-key accents-aigu-map "a"  '(lambda () (interactive) (insert-string " ")))
  (define-key accents-aigu-map "e"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-aigu-map "i"  '(lambda () (interactive) (insert-string "¡")))
  (define-key accents-aigu-map "o"  '(lambda () (interactive) (insert-string "¢")))
  (define-key accents-aigu-map "u"  '(lambda () (interactive) (insert-string "£")))
  (define-key accents-aigu-map "A"  '(lambda () (interactive) (insert-string "µ")))
  (define-key accents-aigu-map "E"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-aigu-map "I"  '(lambda () (interactive) (insert-string "Ö")))
  (define-key accents-aigu-map "O"  '(lambda () (interactive) (insert-string "à")))
  (define-key accents-aigu-map "U"  '(lambda () (interactive) (insert-string "é")))
  (define-key accents-aigu-map "c"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-aigu-map "C"  '(lambda () (interactive) (insert-string "")))

;;; ... grave
  (define-key accents-grav-map "a"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-grav-map "e"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-grav-map "i"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-grav-map "o"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-grav-map "u"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-grav-map "A"  '(lambda () (interactive) (insert-string "·")))
  (define-key accents-grav-map "E"  '(lambda () (interactive) (insert-string "Ô")))
  (define-key accents-grav-map "I"  '(lambda () (interactive) (insert-string "Þ")))
  (define-key accents-grav-map "O"  '(lambda () (interactive) (insert-string "ã")))
  (define-key accents-grav-map "U"  '(lambda () (interactive) (insert-string "ë")))
  (define-key accents-grav-map "c"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-grav-map "C"  '(lambda () (interactive) (insert-string "")))

;;; ... circonflexe
  (define-key accents-circ-map "a"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-circ-map "e"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-circ-map "i"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-circ-map "o"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-circ-map "u"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-circ-map "A"  '(lambda () (interactive) (insert-string "¶")))
  (define-key accents-circ-map "E"  '(lambda () (interactive) (insert-string "Ò")))
  (define-key accents-circ-map "I"  '(lambda () (interactive) (insert-string "×")))
  (define-key accents-circ-map "O"  '(lambda () (interactive) (insert-string "â")))
  (define-key accents-circ-map "U"  '(lambda () (interactive) (insert-string "ê")))
  (define-key accents-circ-map "c"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-circ-map "C"  '(lambda () (interactive) (insert-string "")))
  (define-key accents-circ-map "m"  '(lambda () (interactive) (insert-string "mme")))
  (define-key accents-circ-map "M"  '(lambda () (interactive) (insert-string "Mme")))

  (accents-finish-keymap)
  (message "Accents IBM."))

(require 'cerca-accents-cvt)
