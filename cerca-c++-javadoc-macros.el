;;; Quelques macros utiles pour faire des commentaires suivant le standard
;;; Javadoc.
;;;
;;; Authors:
;;  Paul Labb�( paul@cerca.umontreal.ca )
;;  Francois Guibault( francois@cerca.umontreal.ca )
;;  Benoit Ozell( benoit.ozell@polymtl.ca )
;;
;;; Permission is granted to modify these defun provided that one of the
;;; authors is informed by email using the above address.

(defvar cerca-c++-extension ".cpp"
  "*Extension des fichiers C++.")

(defun cerca-javadoc-group ()
  "Insere les chaines //@{ et //@} autour de la region courante."
  (interactive)
  (let ((start (region-beginning))
        (end (region-end))
        (chaine1 "   //@{\n")
        (chaine2 "   //@}\n"))
    (goto-char end)
    (insert chaine2)
    (goto-char start)
    (insert chaine1)
    (backward-char)
    ))

(defun cerca-c++-insert-javadoc-class-comment ()
  "Insert a Javadoc class comment at point."
  (interactive "*")
  (save-excursion
    (goto-char (point-min))
    (if (re-search-forward "^class " nil t)
        (let ((classebeg (match-beginning 0)) (classe ""))
          (re-search-forward "[^:{\n]*" nil t)
          (setq classe (buffer-substring (match-beginning 0) (match-end 0)))
          (goto-char classebeg )
          (insert "/**
 * interface for the " classe " class
 *
 */\n")
          )
      )
    )
  )

(defun cerca-c++-move-defun-to-src-and-javadocify ()
  "Deplace le code qu'une methode fait (son implantation) dans le fichier
  `cerca-c++-extension' et insere le block descriptif standard de JavaDoc en avant."
  (interactive)
  (save-excursion
    (beginning-of-line)
    (let ((beg (point))
          (decl "")
          (body nil)
          (buf (buffer-file-name))
          bound)
      (search-forward ";")
      (setq bound (match-beginning 0))
      (goto-char beg)
      (if (search-forward "{" bound t)
          (progn
            ;; decl
            (setq decl (buffer-substring beg (match-beginning 0)))
            ;; body
            (goto-char (match-beginning 0))
            (forward-sexp)
            (setq body (buffer-substring (match-end 0) (- (point) 1)))
            (kill-region (match-beginning 0) (point)))
        ;; decl
        (setq decl (buffer-substring beg bound))
        )
      ;; classe
      (save-excursion
        (beginning-of-defun)
        (if (re-search-backward "^class" (point-min) t)
            (let (classe)
              (forward-word 1)
              (mark-sexp 1)
              (setq classe (buffer-substring (region-beginning) (region-end)))
              (setq decl (replace-in-string decl " \\([^ ]*\\)(" (concat classe "::\\1(")))
              (setq decl (replace-in-string decl "virtual " ""))
              )))
      ;; inserer les resultats
      (find-file-other-window (concat (substring buf 0 -2) cerca-c++-extension))
      (goto-char (point-max))
      (let ((declbeg (point)))
        (insert "
" decl "
{
}
" )
        (goto-char (+ declbeg 1)))
      (c-indent-command)
      (cerca-c++-insert-javadoc-function-description)
      (if body
          (progn
            (goto-char (- (point-max) 3))
            (insert "
" body )
            (previous-line 1)
            (c-mark-function)
            (indent-region (region-beginning) (region-end) nil)
            )
        )
      (find-file-other-window buf)
      )))

(defun cerca-c++-insert-javadoc-function-description ()
  "Insere un bloc commentaire suivant le standard JavaDoc. Il faut se placer
sur la premiere ligne de la declaration. Le type de retour n'est pas bon
pour les constructeurs et les destructeurs."
  (interactive)
  (save-excursion
    (beginning-of-line)
    (let (args
          retour
          fnprototype
          (dpbound (point))
          )
      ;; le prototype de la fonction va du debut de la ligne jusqu'a la
      ;; parenthese fermente.
      (setq fnprototype (replace-in-string (buffer-substring dpbound (search-forward ")")) "[\n ] *" " "))
      ;; y a-t-il un const apres la parenthese, mais avant l'accolade, ie:
      ;; la methode est-elle const
      (let ((opt (point)))
        (search-forward "{")
        (if (re-search-backward "const" opt t) (setq fnprototype (concat fnprototype " const"))))
      ;; la valeur de retour est ce qui se trouve avant le ::
      ;; et dans le cas d'un template, il faut enlever le template avant, et a la fin
      (setq retour (replace-in-string fnprototype "::.*" "::"))
      (if (not (posix-string-match "template" retour))
          ;; c'est une fct normale, on vire le mot qui precede le ::
          (setq retour (replace-in-string retour "[\n ]*[a-zA-Z0-9_]*[\n ]*::" ""))
        (setq retour (replace-in-string retour "template[ \n]*<[^>]*>[\n ]*" ""))
        ;; enleve le template qui postcede(une chance qu'on a laisse le :: !!!
        (setq retour (replace-in-string retour "[\n ]*[a-zA-Z0-9_]*[\n ]*<[^>]*>[\n ]*::" "")))
      ;; creer la liste d'arguments
      (save-excursion
        (goto-char dpbound)
        (let (args-beg args-end args-liste)
          (search-forward "(") (setq args-beg (match-beginning 0))
          (search-forward ")") (setq args-end (match-end 0))
          (setq args-liste (replace-in-string (buffer-substring args-beg args-end) "[\n ] *" " "))
          (if (not (string-match "( *void *)" args-liste))
              (setq args (substring (replace-in-string args-liste " *[(,)] *" " Description\n * @param ") 16 -11))
            )))
      ;; inserer les resultats en format JavaDoc
      (goto-char dpbound)
      (insert (concat "/**
 *
 * Description Longue sur plusieurs lignes
 *
 * " args "
 * @return " retour " description
 */
"))
      )))

(defun cerca-c++-convert-class-description-to-javadoc ()
  "Convertit le commentaire des debuts de fichiers entete pour qu'il suive le
standard Javadoc et met la sentinelle au tout debut"
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (if (re-search-forward "/\\*[-=]+" nil t)
        (replace-match "/**" nil t))
    (if (re-search-forward "CLASS[E :]*" nil t)
        (replace-match "@class " t t))
    (if (re-search-forward "[=-]+\\*/" nil t)
        (replace-match "*/" nil t))
    (goto-char (point-min))
    (if (re-search-forward "\\*\\*/" nil t)
        (replace-match "*/" nil t))
    (goto-char (point-min))
    (if (re-search-forward "#ifndef [A-Za-z_0-9]+\n#define [A-Za-z_0-9]+[\n ]*" nil t)
        (let (sentinel)
          (setq sentinel (buffer-substring (match-beginning 0) (match-end 0)))
          (replace-match "" nil t)
          (goto-char (point-min))
          (insert sentinel))
      )
    )
  )

(defun cerca-c++-convert-functions-in-buffer-to-javadoc ()
  "Convertit tous les blocs de description cerca du buffer courant dans le standard Javadoc"
  (interactive)
  (save-excursion
    (while (re-search-forward "[ ]*\\*[ ]*FONCTION[ ]*:" nil t)
      (cerca-c++-convert-function-description-to-javadoc))
    )
  )

(defun cerca-c++-convert-function-description-to-javadoc ()
  "Convertit un bloc de description cerca dans le standard Javadoc"
  (interactive)
  (save-excursion
    (search-backward "/*" (point-min) t)
    (let ((begOld (match-beginning 0))
          endOld
          olddesc
          DescLong
          args
          retvaldesc
          retourdesc
          fnprototype
          notedesc
          exceptdesc
          )
      ;; le vieux commentaire va de /* a */
      (search-forward "*/")
      (setq  endOld (match-end 0))
      (setq olddesc (buffer-substring begOld endOld))
      ;; le prototype va de */ a {
      (search-forward "{")
      (setq fnprototype (buffer-substring endOld (match-beginning 0)))
      (setq fnprototype (replace-in-string fnprototype "[\n ] *" " "))
      (setq fnprototype (replace-in-string fnprototype ")[ ]*:.*$" ")"))
      (if (posix-string-match "#ifndef[ ]*M__NO_OVERLOADABLE_HIERARCHY[\n ]*" fnprototype)
          (progn (setq fnprototype (replace-in-string fnprototype "#ifndef[ ]*M__NO_OVERLOADABLE_HIERARCHY[\n ]*" " "))
                 (setq fnprototype (replace-in-string fnprototype "#else.*#endif" "")))
        )
      ;; assure un espace au debut de chaque ligne
      (setq olddesc (replace-in-string olddesc "\n\\*" "\n *"))
      ;; enleve la premiere ligne
      (setq olddesc (replace-in-string olddesc "/\\*[-]+[ ]*\n" ""))
      ;; enleve la derniere ligne
      (setq olddesc (replace-in-string olddesc "[ *]*[-]+\\*/" ""))
      ;; enleve FONCTION et CLASSE, le prototype est pris verbatim du prototype
      (setq olddesc (replace-in-string olddesc "[ ]*\\*[ ]+FONCTION[ ]*:.*\n" ""))
      (setq olddesc (replace-in-string olddesc "[ ]*\\*[ ]+CLASSE[ ]*:.*\n" ""))
      ;; part de la fin et enleve la NOTE, le EXCEPT, le RETOUR, le SORTIE, puis le ENTREE. Ce qui reste sera le commentaire
      (if (not (posix-string-match "^[ ]*\\*[ ]*NOTE[S: ]*" olddesc))
          (setq notedesc "")
        (setq notedesc (concat "@note " (substring olddesc (match-end 0))))
        (setq olddesc (substring olddesc 0 (match-beginning 0)))
        (setq notedesc (replace-in-string notedesc "\n" " "))
        (setq notedesc (concat notedesc "\n * "))
        )
      (if (not (posix-string-match "^[ ]*\\*[ ]*EXCEPT[\.: ]*" olddesc))
          (setq exceptdesc "")
        (setq exceptdesc (concat "@exception " (substring olddesc (match-end 0))))
        (setq olddesc (substring olddesc 0 (match-beginning 0)))
        (if (not (posix-string-match "[a-zA-Z]+" exceptdesc))
            ;; la description ne contient pas un foutu charactere, on met rien!
            (setq exceptdesc "")
          (setq exceptdesc (replace-in-string exceptdesc "\n" " "))
          (setq exceptdesc (concat exceptdesc "\n * "))
          )
        )

      (if (not (posix-string-match "^[ ]*\\*[ ]*RETOUR[S: ]*" olddesc))
          (setq retourdesc "@return void \n * ")
        (setq retourdesc (concat "@return " (substring olddesc (match-end 0))))
        (setq olddesc (substring olddesc 0 (match-beginning 0)))
        (if (posix-string-match "[ ]*AUCUN[ES ]*" retourdesc)
            (setq retourdesc "@return void \n * ")
          (setq retourdesc (replace-in-string retourdesc "\n" " "))
          (setq retourdesc (concat retourdesc "\n * "))
          )
        )
      (if (not (posix-string-match "^[ ]*\\*[ ]*SORTIE[S: ]*" olddesc))
          (setq retvaldesc "")
        (setq retvaldesc (concat "@retval " (substring olddesc (match-end 0))))
        (setq olddesc (substring olddesc 0 (match-beginning 0)))
        (setq retvaldesc (replace-in-string retvaldesc "\n" " "))
        (setq retvaldesc (concat retvaldesc "\n *"))
        (if (posix-string-match "[ ]*AUCUN[ES ]*" retvaldesc)
            (setq retvaldesc "")
          (if (not (posix-string-match "[a-zA-Z]+" retvaldesc))
              ;; la description ne contient pas un foutu charactere, on met rien!
              (setq retvaldesc "")
            ;; il peut y avoir plusieurs sorties; pour chaque, ca prend un retval
            (setq retvaldesc (replace-in-string retvaldesc "\n" ""))
            (setq retvaldesc (concat (substring (replace-in-string retvaldesc "\\([^\\*]*\\):" "\n * @retval \\1: ") 4 nil) "\n * "))
            (setq retvaldesc (replace-in-string retvaldesc "[ ]+" " "))
            (setq retvaldesc (replace-in-string retvaldesc "\\*\n" "\n"))
            )
          )
        )
      ;;      (setq retvaldesc (replace-in-string retvaldesc "\n" " "))
      (if (not (posix-string-match "^[ ]*\\*[ ]*ENTREE[S: ]*" olddesc))
          (setq args "")
        (setq args (substring olddesc (match-end 0)))
        (setq olddesc (substring olddesc 0 (match-beginning 0)))
        (if (posix-string-match "[ ]*AUCUN[ES ]*" args)
            (setq args "")
          (if (not (posix-string-match "[a-zA-Z]+" args))
              ;; la description ne contient pas un foutu charactere, on met rien!
              (setq args "")
            ;; il peut y avoir plusieurs entrees; ca prnd un param pour chaque
            ;; met tout sur une ligne
            (setq args (replace-in-string args "\n" ""))
            ;; precede chaque argument par un 'param'
            (setq args (replace-in-string args "\\([^\\*]*\\):" "\n * @param \\1: "))
            ;; a-t-on trouve au moins un ':'?
            (if (not (posix-string-match "param" args))
                (setq args (concat "@param " args "\n * "))
              (setq args (concat (substring args 4 nil) "\n * "))
              ;; enleve les multi-espace blancs
              (setq args (replace-in-string args "[ ]+" " "))
              (setq args (replace-in-string args "\\*\n" "\n"))
              )
            )
          )
        )
      (setq DescLong (replace-in-string olddesc "\n$" " "))

      (goto-char endOld)
      (insert (concat "/**
" DescLong "
 * " args retvaldesc retourdesc notedesc exceptdesc "
 */"))
      (kill-region begOld endOld)

      )))


(defun cerca-c++-inline-function-in-header ()
  "Definie la methode courante comme etant 'inline' et deplace le code a la
fin du fichier courant, juste avant le #endif. On s'attend a etre dans la
definition d'une classe, a ce que le buffer se termine avec un #endif
provenant d'une sentinelle."
  (interactive)
  (save-excursion
    (beginning-of-line)
    (let ((beg (point))
          (decl "")
          (body nil)
          bound)
      (search-forward ";")
      (setq bound (match-beginning 0))
      (goto-char beg)
      (if (search-forward "{" bound t)
          (progn
            ;; decl
            (setq decl (buffer-substring beg (match-beginning 0)))
            ;; body
            (goto-char (match-beginning 0))
            (forward-sexp)
            (setq body (buffer-substring (match-end 0) (- (point) 1)))
            (kill-region (match-beginning 0) (point))
            (setq body (replace-in-string body "^[ \n]+" ""))
            (setq body (replace-in-string body "[ \n]+$" ""))
            (goto-char beg) (forward-word 1) (backward-word 1) (insert "inline ")
            ;; enleve le \n entre la declaration et le ;
            (goto-char beg) (search-forward ";")
            (let ((pvbound (point)))
              (goto-char beg)
              (if (re-search-forward "[ \n]*;" (+ pvbound 1) t)
                  (replace-match ";"))
              )
            )
        ;; decl
        (setq decl (buffer-substring beg bound))
        )
      ;; classe
      (save-excursion
        (beginning-of-defun)
        (if (re-search-backward "^class" (point-min) t)
            (let (classe)
              (forward-word 1)
              (mark-sexp 1)
              (setq classe (buffer-substring (region-beginning) (region-end)))
              (setq decl (replace-in-string decl " \\([^ ]*\\)(" (concat classe "::\\1(")))
              (setq decl (replace-in-string decl "[ ]+" " "))
              (setq decl (replace-in-string decl "^[ ]+" ""))
              (setq decl (replace-in-string decl "[ \n]+$" ""))
              (setq decl (replace-in-string decl "static " ""))
              (setq decl (replace-in-string decl "virtual " ""))
              )))
      ;; inserer les resultats
      (search-forward "#endif")
      (beginning-of-line)
      (let ((declbeg (point)))
        (insert decl "
{
};

" )
        (goto-char (+ declbeg 1))
        (c-indent-command)
        (cerca-c++-insert-javadoc-function-description)
        (if body
            (progn
              (search-forward "#endif")
              (beginning-of-line)
              (goto-char (- (point) 4))
              (insert body "
")
              (search-forward "#endif")
              (beginning-of-line)
              (indent-region declbeg (point) nil)
              )
          )
        )
      )
    )
  )

(defun cerca-c++-insert-javadoc-file-comment ()
  "Insert a Javadoc file comment at point."
  (interactive "*")
  (insert
   (format
    (concat "/**\n"
            " * @file   %s\n"
            " * @brief  \n"
            " *\n"
            " * <long description>\n"
            " *\n"
            " * @author %s\n"
            " * @date   %s\n"
            " */\n")
    (buffer-file-name)
    (user-full-name)
    (current-time-string)
    )
   )
  )

(provide 'c++-javadoc-macros)
