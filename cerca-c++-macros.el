(defun cerca-c-implante ()
  (interactive)
  (save-excursion
    (beginning-of-line)
    (let ((beg (point))
          (decl "")
          (body nil)
          (buf (buffer-file-name))
          bound)
      (search-forward ";")
      (setq bound (match-beginning 0))
      (goto-char beg)
      (if (search-forward "{" bound t)
          (progn
            ;; decl
            (setq decl (buffer-substring beg (match-beginning 0)))
            ;; body
            (goto-char (match-beginning 0))
            (forward-sexp)
            (setq body (buffer-substring (match-end 0) (- (point) 1)))
            (kill-region (match-beginning 0) (point)))
        ;; decl
        (setq decl (buffer-substring beg bound))
        )
      ;; classe
      (save-excursion
        (beginning-of-defun)
        (if (re-search-backward "^class" (point-min) t)
            (let (classe)
              (forward-word 1)
              (mark-sexp 1)
              (setq classe (buffer-substring (region-beginning) (region-end)))
              (setq decl (replace-in-string decl " \\([^ ]*\\)(" (concat classe "::\\1(")))
              (setq decl (replace-in-string decl "virtual " ""))
              )))
      ;; inserer les resultats
      (find-file-other-window (concat (substring buf 0 -2) ".C"))
      (goto-char (point-max))
      (let ((declbeg (point)))
      (insert "
" decl "
{
}
" )
      (goto-char (+ declbeg 1)))
      (c-indent-command)
      (cerca-c-insert-header)
      (if body
          (progn
            (goto-char (- (point-max) 3))
            (insert "
" body )
            (previous-line 1)
            (c-mark-function)
            (indent-region (region-beginning) (region-end) nil)
            )
        )
      (find-file-other-window buf)
      )))

(defun cerca-c-insert-header ()
  (interactive)
  (fume-rescan-buffer)
  (save-excursion
    (beginning-of-line)
    (let ((args "AUCUNES")
          (retour "AUCUN")
          (fonction (fume-function-before-point))
          classe)
      (if (and fonction (string-match "::" fonction))
          (progn
            (setq classe   (substring fonction 0 (match-beginning 0)))
            (setq fonction (substring fonction (match-end 0)))))
      ;; creer la liste d'arguments
      (save-excursion
        (let (args-beg args-end args-liste)
          (search-forward "(") (setq args-beg (match-beginning 0))
          (search-forward ")") (setq args-end (match-end 0))
          (setq args-liste (replace-in-string (buffer-substring args-beg args-end) "[\n ] *" " "))
          (if (not (string-match "( *void *)" args-liste))
              (setq args (substring (replace-in-string args-liste " *[(,)] *" " :\n *           ") 16 -14)))))
      ;; inserer les resultats
      (if classe
          (insert (concat "/*---------------------------------------------------------------------------
 * FONCTION: " fonction "
 * CLASSE  : " classe "
 *
 *
 *
 * ENTREES : " args "
 * SORTIES : AUCUNES
 * RETOUR  : " retour "
 * EXCEPT. :
 *---------------------------------------------------------------------------*/
"))
        (insert (concat "/*---------------------------------------------------------------------------
 * FONCTION: " fonction "
 *
 *
 *
 * ENTREES : " args "
 * SORTIES : AUCUNES
 * RETOUR  : " retour "
 *---------------------------------------------------------------------------*/
")))
        )))

(provide 'cerca-c++-macros)
