;;; Fichier principal pour obtenir la configuration cerca.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

;; var 'special-display-...
;; 'shell-strip-ctrl-m
;; (add-hook `comint-output-filter-functions `comint-watch-for-password-prompt)
;; (setq-default save-place t)

;;(defmacro defstyle (name &rest body)
;;  (` (progn (defvar (, name) nil)
;;            (if (, name)
;;                ()
;;              (let ((style (make-style)))
;;                (setq (, name) style)
;;                (,@ body))))))

(if (not (getenv "TMPDIR"))
    (setenv "TMPDIR" "/tmp"))

(defmacro GNUEmacs (&rest body)
  "Execute any number of forms if running under GNU Emacs."
  (list 'if (not (featurep 'xemacs)) (cons 'progn body)))

(defmacro XEmacs (&rest body)
  "Execute any number of forms if running under XEmacs."
  (list 'if (featurep 'xemacs) (cons 'progn body)))

;(GNUEmacs
;    (defadvice load (before debug-log activate)
;      (message "Loading %s..." (locate-library (ad-get-arg 0)))))

;(defun enable-flow-control-memstr= (e s)
;  (cond ((null s) nil)
;       ((string= e (car s)) t)
;       (t (enable-flow-control-memstr= e (cdr s)))))

;(defun TeX-insert-punctuation ()
;  "Insert point or comma, cleaning up preceding space."
;  (interactive)
;  (if (TeX-looking-at-backward "\\\\/\\(}+\\)" 50)
;      (replace-match "\\1" t))
;  (call-interactively 'self-insert-command))

;    (and (enable-flow-control-memstr= term losing-terminal-types)
;        (enable-flow-control))))

;(GNUEmacs
; ;; load *.el and *.elc in /usr/share/emacs/site-lisp/site-start.d on startup
; (mapc
;  'load
;  (delete-dups
;   (mapcar 'file-name-sans-extension
;           (directory-files
;            "/usr/share/emacs/site-lisp/site-start.d" t "\\.elc?\\'")))))

;;(setq-default c++-access-specifier-offset -3
;;              c++-empty-arglist-indent 3
;;              c++-friend-offset 0
;;              c++-relative-offset-p nil
;;              c++-untame-characters nil
;;              c-argdecl-indent 0
;;              c-brace-offset -3
;;              c-continued-statement-offset 3
;;              c-indent-level 3
;;              c-indent-offset -3
;;              c-label-offset -3
;;              ) ; pour emacs
(require 'cc-mode)
(setq-default c-site-default-style "cerca"
              winmgr-basic-offset 3)
;; voir variable c-style-alist
(c-add-style "cerca"
             '(
               (c-basic-offset . 3)
               (c-comment-only-line-offset . (0 . 0))
               (c-offsets-alist . ((statement-block-intro . +)
                                   (knr-argdecl-intro . 5)
                                   (substatement-open . 0)
                                   (label . 0)
                                   (statement-case-open . +)
                                   (statement-cont . 0)
                                   (inline-open . 0)
                                   (arglist-intro . c-lineup-arglist-intro-after-paren)
                                   (arglist-close . c-lineup-arglist)
                                   ))
               (c-special-indent-hook . c-gnu-impose-minimum)
               ))
(c-add-style "inf2705"
             '(
               (c-basic-offset . 4)
               (c-comment-only-line-offset . (0 . 0))
               (c-offsets-alist . ((statement-block-intro . +)
                                   (knr-argdecl-intro . 5)
                                   (substatement-open . 0)
                                   (label . 0)
                                   (statement-case-open . +)
                                   (statement-cont . 0)
                                   (inline-open . 0)
                                   (arglist-intro . c-lineup-arglist-intro-after-paren)
                                   (arglist-close . c-lineup-arglist)
                                   ))
               (c-special-indent-hook . c-gnu-impose-minimum)
               ))

(setq-default ;; display-time-day-and-date t
              display-time-24hr-format t
              display-time-mail-file (getenv "MAIL")
              display-time-mode t
              display-battery-mode nil
              display-time-use-mail-icon t
              display-time-mail-face 'match
              ;; display-time-interval 10
              ;; display-time-echo-area t
              )
;; les variables 'customizable'
(setq-default ;;grep-command "grep -nH "
              inhibit-startup-screen t
              pop-up-windows t
              doc-view-continuous t
              transient-mark-mode t
              indicate-buffer-boundaries 'left
              vc-make-backup-files t
              ;;mouse-wheel-scroll-amount '(1 ((shift) . 5) ((control)))
              ;;mouse-wheel-scroll-amount '(1 ((shift) . 0.2) ((control)))
              ;;mouse-wheel-progressive-speed t
              mouse-buffer-menu-mode-mult 30
              org-startup-folded nil
              org-export-backends (quote (ascii beamer html icalendar latex md odt org))
              split-width-threshold nil ;; ==> `split-window-sensibly' will not split a window horizontally.
              imenu-auto-rescan t
              backup-by-copying t
              version-control t  ; use versioned backups
              delete-old-versions t
              kept-new-versions 6
              kept-old-versions 4
              server-raise-frame nil
              ispell-program-name "aspell"
              ;;ansi-color-faces-vector [default default default italic underline success warning error]
              ;;ansi-color-names-vector ["#2d3743" "#ff4242" "#74af68" "#dbdb95" "#34cae2" "#008b8b" "#00ede1" "#e1e1e0"]
              ;;custom-enabled-themes '(tsdh-light)
              )
;;(load-theme 'tsdh-light)
(setq-default abbrev-all-caps t
              abbrev-file-name (expand-file-name "~/.emacs_abbr")
              dabbrev-backward-only nil
              save-abbrevs t
              )
(setq-default tex-default-mode 'latex-mode
              tex-run-command "latex"
              tex-dvi-view-command "xdvi"
              tex-dvi-print-command "dvips -f * | lpr; lpq"
              tex-alt-dvi-print-command "sh -c 'N=\"*\" && dvips $N && ghostview `echo $N | sed 's/dvi/ps/'`'"
              ;; tex-alt-dvi-print-command '(format "dvips -f -o \\!lpr -d%s" (read-string "Imprimante: "))
              tex-show-queue-command "lpq"
              ;; ps-postscript-command "ghostview -"
              fill-column 76
              ;!!ctl-arrow 32
              font-latex-title-fontify 'color
              font-latex-user-keyword-classes '(("exam1" ("question" "soln") font-latex-title-3-face noarg) ("exam2" ("sousquestion") font-latex-title-4-face noarg) ("examPoint1" ("PointDeb" "PointFin" "MinuteDeb" "MinuteFin") font-lock-reference-face noarg) ("examPoint2" ("PointAdd" "PointSet" "MinuteAdd" "MinuteSet") font-latex-warning-face (command 1)) ("examPoint3" ("PointMinAdd" "PointAddMin" "PointMinSet" "PointSetMin") font-latex-warning-face (command 2)))
              )
(if (featurep 'xemacs)
    (setq-default ps-postscript-code-directory (locate-data-directory "ps-print")))
(setq-default html-helper-timestamp-hook 'cerca-html-helper-insert-timestamp
              html-helper-timestamp-start "<!-- hhmts start -->\n"
              html-helper-timestamp-end "<!-- hhmts end -->"
              html-helper-new-buffer-template
              '(html-helper-htmldtd-version
                "<html>\n"
                "<head>\n"
                "<META http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\n"
                "<title>" (p "Document Title: " title) "</title>\n"
                "</head>\n\n"
                "<body>\n"
                "<h1>" (s title) "</h1>\n\n"
                p
                "\n\n<hr>\n"
                "<address>" html-helper-address-string "</address>\n"
                (html-helper-return-created-string)
                html-helper-timestamp-start
                html-helper-timestamp-end
                "\n</body>\n</html>\n")
              )
;;(setq-default gnus-nntp-server "news.umontreal.ca"
;;              gnus-local-organization "Cerca"
;;              gnus-nntp-service 119
;;              gnus-use-generic-from t
;;              ;; gopher-support-bookmarks t
;;              gopher-root-node [?1 "root" "" "gopher.micro.umn.edu" 70]
;;              )
;; (setq webster-host "mintaka.lcs.mit.edu")
;; (setq webster-port "103")
(setq-default ;; crypt-auto-decode-buffer t
              ;; crypt-auto-decode-insert t
              crypt-confirm-password t
              crypt-encoded-disable-auto-save nil
              ;; crypt-encrypted-disable-auto-save t
              crypt-encryption-type 'pgp
              crypt-freeze-vs-fortran nil
              ;; crypt-never-ever-decrypt t
              ;; crypt-auto-decode-buffer nil
              )

(defun cerca-ps-format-date (time)
  "Formats a date 'Sun Sep 16 01:03:52 1973' to become '1973/Sep/16 01:03:52'."
  (let* ((dayname (cdr (assoc (substring time 0 3)
                              '(("Sun" . "dim") ("Mon" . "lun") ("Tue" . "mar") ("Wed" . "mer")
                                ("Thu" . "jeu") ("Fri" . "ven") ("Sat" . "sam")))))
         (monthname (cdr (assoc (substring time 4 7)
                                '(("Jan" . "jan") ("Feb" . "fév") ("Mar" . "mar")
                                  ("Apr" . "avr") ("May" . "mai") ("Jun" . "jun")
                                  ("Jul" . "jui") ("Aug" . "aoû") ("Sep" . "sep")
                                  ("Oct" . "oct") ("Nov" . "nov") ("Dec" . "déc")))))
         (day (substring time 8 10))
         (hms (substring time 11 19)))
    (concat dayname " " day " " monthname " " hms)))
(defun cerca-ps-right-header2 ()
  (concat (user-full-name) ", " (cerca-ps-format-date (current-time-string (current-time)))))
(defun cerca-ps-left-header1 ()
  (concat (ps-header-dirpart) (ps-get-buffer-name)))
(defun cerca-ps-left-header2 ()
  (if (and buffer-file-name (file-exists-p buffer-file-name))
      (cerca-ps-format-date (current-time-string (nth 5 (file-attributes (file-chase-links buffer-file-name)))))
    ""))
(setq-default ps-right-header (list "/pagenumberstring load" 'cerca-ps-right-header2)
              ps-left-header (list 'cerca-ps-left-header1 'cerca-ps-left-header2)
              ps-bold-faces '(
                              font-lock-function-name-face
                              font-lock-keyword-face
                              font-lock-type-face
                              font-lock-preprocessor-face
                              )
              ps-italic-faces '(font-lock-comment-face
                                font-lock-doc-string-face
                                font-lock-string-face
                                font-lock-preprocessor-face
                                ;; font-lock-reference-face
                                ;; font-lock-variable-name-face
                                )
              ps-underlined-faces '(
                                    font-lock-function-name-face
                                    )
              ps-print-color-p nil
              )
;; (setq-default tags-auto-read-changed-tag-files t
;;               tags-build-completion-table t
;;               make-tags-files-invisible t
;;               tags-always-exact t
;;               )
;; (setq-default efs-default-user (user-login-name)   ; id to use for /host:/remote/path
;;               ;; efs-generate-anonymous-password t ; use $USER@`hostname`
;;               ;; efs-binary-file-name-regexp "."   ; always transfer in binary mode
;;               )
;; (if (string-match "/usr/kerberos/bin" (getenv "PATH"))
;;     (setq efs-ftp-program-name "/usr/bin/ftp"))

(require 'font-lock)
(defun font-lock-pre-idle-hook ()
  (condition-case font-lock-error
      (if (> (hash-table-count font-lock-pending-buffer-table) 0)
          (font-lock-fontify-pending-extents))
    ))

(if (require 'color-theme "color-theme" t)
    (require 'color-theme-solarized))

(require 'cerca-vm)

;; semble y avoir conflit entre ça et les frame-parameters: on l'initialise donc plutôt dans bemacs!
;(setq-default frame-title-format (list "%65b %f"))
(setq-default frame-title-format (list
                                  "%b "
                                  (concat
                                   "[" (user-login-name) "@"
                                   (or (and (string-match (regexp-quote ".") (system-name))
                                            (substring (system-name) 0 (match-beginning 0)))
                                       (system-name))
                                   "] ")
                                  "%f"))

(setq-default initial-major-mode 'text-mode
              default-major-mode 'text-mode
              icon-title-format (list "%b")
              large-file-warning-threshold nil
              initial-buffer-choice t
              column-number-mode t
              ;; motion-keys-for-shifted-motion nil
              save-buffer-context t
              ;; save-context-version (user-login-name)
              ;; auto-save-and-recover-context nil
              delete-auto-save-files t
              ;;auto-save-directory (format "%s/gsrvdir%d/" (or (getenv "TMPDIR") "/tmp") (user-uid))
              ;;auto-save-list-file-prefix (format "%s/gsrvdir%d/" (or (getenv "TMPDIR") "/tmp") (user-uid))
              ;;auto-save-directory (concat "/tmp/" (user-login-name) "-autosave/")
              ;;auto-save-directory (expand-file-name "~/autosave/")
              ;;auto-save-directory-fallback
              next-line-add-newlines nil
              require-final-newline t
              ;; inhibit-startup-message t
              enable-local-variables t ;;; 'query
              compile-command "make "
              compilation-ask-about-save t
              compilation-mouse-motion-initiate-parsing nil
              compilation-scroll-output t ;; Non-nil to scroll the *compilation* buffer window as output appears
              uniquify-buffer-name-style 'post-forward-angle-brackets
              sentence-end "[.?!][]\"')}]*\\($\\|       \\| \\)[ ]*"
              scroll-step 1
              split-window-keep-point t
              apropos-do-all t
              ;;gnuserv-frame (window-frame)
              ;; cvs-diff-flags nil
              font-lock-maximum-decoration t
              font-lock-maximum-size '((vu-mode . 100000))
              ;; font-lock-maximum-size '((vu-mode . 100000) (t . 512000))
                                        ; (html-mode . 100000)
              options-save-faces t
              whitespace-visual-chars 'tabs
              whitespace-style (quote (tabs spaces trailing space-before-tab newline indentation empty space-after-tab space-mark tab-mark newline-mark))
              ;;whitespace-chars 'tabs
              ;;whitespace-install-submenu t
              )

;; This regexp MUST match all the way to first character of the filename.
;; You can loosen it to taste, but then you might bomb on filenames starting
;; with a space. This will have to be modified for non-english month names.
(defvar dired-re-month-and-time
  (concat
   " \\(Jan\\|Feb\\|Mar\\|Apr\\|May\\|June?\\|July?\\|Aug\\|Sep\\|Oct\\|Nov\\|Dec"
   "\\|jan ?\\|fév ?\\|mar ?\\|avr ?\\|mai ?\\|jui?n\\|jui?l\\|août?\\|sept?\\|oct ?\\|nov ?\\|déc ?\\)"
   " [ 0-3][0-9]\\("
   " [012][0-9]:[0-6][0-9] \\|" ; time
   "  [12][90][0-9][0-9] \\|"   ; year on IRIX, NeXT, SunOS, ULTRIX, Apollo, HP-UX, A/UX
   " [12][90][0-9][0-9]  \\)"   ; year on AIX
   ))

(setq compilation-buffer-name-function
      (function
       (lambda (mode)
         (concat "*" mode ":" default-directory "*"))))

;; enlever certaines cles fatiguantes!
(global-unset-key "\C-xj")    ;skk-auto-fill-mode
(global-unset-key "\C-xt")    ;skk-tutorial
(global-unset-key "\C-x\t")   ;skk-mode
(global-unset-key "\C-x\C-j") ;skk-mode

;; la config !
(require 'cerca-fcts)
(require 'cerca-tpu)
(load "cerca-hooks" nil t)
(if (not (and (boundp 'emacs-major-version) (<= emacs-major-version 21)))
    (load "cerca-lmenu" nil t))
(load "cerca-keys" nil t)

(if (require 'langtool "langtool.el" t) ; http://www.languagetool.org/
    (progn
      (setq langtool-language-tool-jar "/opt/LanguageTool/LanguageTool.jar")
      (global-set-key "\C-x4w" 'langtool-check-buffer)
      (global-set-key "\C-x4W" 'langtool-check-done)
      (global-set-key "\C-x4n" 'langtool-goto-next-error)
      (global-set-key "\C-x4p" 'langtool-goto-previous-error)
      (global-set-key "\C-x44" 'langtool-show-message-at-point)
      ))

(line-number-mode 1)
(column-number-mode 1)
(font-lock-mode 1)
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Tab-Line.html
;;(setq-default tab-line-tabs-function 'tab-line-tabs-mode-buffers)
(setq-default tab-line-tabs-function 'tab-line-tabs-buffer-groups)
(global-tab-line-mode)

;; (if (featurep 'xemacs)
;;     (progn
;;       (require 'crypt)
;;       (defun crypt-build-encoding-alist ()
;;         (list
;;          (list 'compress "\037\235" nil "\\(\\.Z\\)$" "compress" "uncompress" nil nil "Compress" nil t)
;;          (list 'gzip "\037\213" nil "\\.\\(tgz\\|g?z\\)$" "gzip" "gzip" "--quiet" "--decompress --quiet" "Zip" nil t)
;;          (list 'bzip2 "BZh" nil "\\(\\.bz2\\)$" "bzip2" "bzip2" "" "--decompress" "Bzip2" nil t)
;;          (list 'freeze "\037\236\\|\037\237" nil "\\(\\.F\\)$" "freeze" "freeze" "" "-d" "Freeze" nil crypt-freeze-vs-fortran)
;;          (list 'compact "\377\037" nil "\\(\\.C\\)$" "compact" "uncompact" nil nil "Compact" "^Compression *:.*\n" crypt-compact-vs-C++)
;;          ;;(list 'dos "[^\n\r]*\r$" nil "\\(\\.DOS\\)$" 'crypt-unix-to-dos-region 'crypt-dos-to-unix-region nil nil "Dos" nil nil)
;;          ;; Add new elements here ...
;;          ))
;;       (crypt-rebuild-tables)
;;       ))

;BO;(require 'compile)
(if (featurep 'xemacs)
    (require 'completer))
(display-time)
;;(require 'uniquify)
(require 'ffap)
;BO;(if (not (and (boundp 'emacs-major-version) (<= emacs-major-version 21)))
;BO;    (require 'tex-site nil t))
;BO;(if (not (featurep 'xemacs))
;BO;    (require 'imenu nil t))

;BO;(require 'cerca-x11)

(setq ffap-c-path
      (append ffap-c-path
              '(
                "/usr/X11R6/include"
                "/Developer/GPU Computing/C/common/inc"
                "/usr/local/cuda/include"
                "/data/536/stk_bonne_fedora/packages/usr/include"
                )))

;;(add-hook 'shell-mode-hook 'install-shell-fonts)
(GNUEmacs
 (setq-default recentf-auto-cleanup 'never
               recentf-max-menu-items 30
               recentf-max-saved-items 70
               recentf-menu-append-commands-flag nil
               recentf-exclude (quote ("~$" "TAGS" (substitute-in-file-name "${HOME}/Mail/")))
               )
 (recentf-mode t)
 )
;BO;(require 'facemenu)
(require 'paren)
(if (featurep 'xemacs)
    (if (not (equal (device-type) 'tty))
        (progn
          (paren-set-mode 'paren)
          (require 'speedbar)
          ))
  (show-paren-mode t)
  )
(if (not (and (boundp 'emacs-major-version) (<= emacs-major-version 21)))
    (require 'tramp))
;;(setq tramp-default-method "scp")

(setq completion-ignored-extensions
      (append completion-ignored-extensions (list ".exe")))
(if (file-readable-p abbrev-file-name)
    (quietly-read-abbrev-file abbrev-file-name))
(put 'eval-expression 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)

(defvar iso-accents-mode nil
  "*Non-nil enables ISO Accents mode.
Setting this variable makes it local to the current buffer.
See the function `cerca-iso-acc'.")
(make-variable-buffer-local 'iso-accents-mode)

;; (if (and (string-match "XEmacs" emacs-version)
;;          (boundp 'emacs-major-version)
;;          (<= emacs-major-version 21))
;;     (progn
;;       (autoload 'mwheel-install "mwheel" "Enable mouse wheel support.")
;;       (mwheel-install)
;;       ))

;;; les autoload
(autoload 'icon-mode                "icon" nil t)
(autoload 'save-context             "saveconf" nil t)
(autoload 'recover-context          "saveconf" nil t)
(autoload 'accents-mode             "cerca-iso-acc" nil t)
(autoload 'accents-iso              "cerca-iso-acc" nil t)
(autoload 'accents-ibm              "cerca-accents" nil t)
(autoload 'accents-ansi             "cerca-accents" nil t)
(autoload 'accents-get-tex-from     "cerca-accents" nil t)
(autoload 'accents-get-from-tex     "cerca-accents" nil t)
(autoload 'layout-latex             "cerca-layout" "Description des cles en format .tex" t)
(autoload 'ivd-insert-in-variable-list        "ivd" nil t)
(autoload 'ivd-insert-string-in-variable-list "ivd" nil t)
(autoload 'ivd-insert-variable-dump           "ivd" nil t)
(autoload 'ivd-reset-variable-list            "ivd" nil t)
(autoload 'ivd-select-variable-list           "ivd" nil t)
(autoload 'vu-mode                     "vu-mode" "Vu mode." t)
(autoload 'inferior-vu                 "vu-mode" "Run inferior Vu process." t)
(autoload 'vu-help-on-word             "vu-mode" "Help on Vu commands" t)
(autoload 'vu-auto-fill-mode           "vu-mode" "Vu auto fill." t)
(autoload 'vu-hashify-buffer           "vu-mode" "Vu hashify buffer." t)
(autoload 'pie-mode                    "pie-mode" "Pie mode." t)
(autoload 'inferior-pie                "pie-mode" "Run inferior Pie process." t)
(autoload 'pie-help-on-word            "pie-mode" "Help on Pie commands" t)
(autoload 'pie-auto-fill-mode          "pie-mode" "Pie auto fill." t)
(autoload 'pie-hashify-buffer          "pie-mode" "Pie hashify buffer." t)
(autoload 'html-helper-mode            "html-helper-mode" "Yay HTML" t)
(autoload 'php-mode                    "php-mode" "PHP mode" t)
(autoload 'css-mode                    "css-mode" "CSS mode." t)
(autoload 'crontab-edit                "crontab" "Function to allow the easy editing of crontab files." t)

;; (autoload 'calc-dispatch            "calc/calc" "Calculator Options" t)
;; (autoload 'full-calc                "calc/calc" "Full-screen Calculator" t)
;; (autoload 'full-calc-keypad         "calc/calc" "Full-screen X Calculator" t)
;; (autoload 'calc-eval                "calc/calc" "Use Calculator from Lisp")
;; (autoload 'defmath                  "calc/calc" nil t t)
;; (autoload 'calc                     "calc/calc" "Calculator Mode" t)
;; (autoload 'quick-calc               "calc/calc" "Quick Calculator" t)
;; (autoload 'calc-keypad              "calc/calc" "X windows Calculator" t)
;; (autoload 'calc-embedded            "calc/calc" "Use Calc inside any buffer" t)
;; (autoload 'calc-embedded-activate   "calc/calc" "Activate =>'s in buffer" t)
;; (autoload 'calc-grab-region         "calc/calc" "Grab region of Calc data" t)
;; (autoload 'calc-grab-rectangle      "calc/calc" "Grab rectangle of data" t)
(autoload 'mapserver-mode          "mapserver-mode"   "Mode for editing UMN MapServer files" t)
;;(autoload 'xrdb-mode "xrdb-mode"   "Mode for editing X resource files" t)

(autoload 'cerca-c++-move-defun-to-src-and-javadocify         "cerca-c++-javadoc-macros" nil t)
(autoload 'cerca-c++-insert-javadoc-function-description      "cerca-c++-javadoc-macros" nil t)
(autoload 'cerca-c++-convert-class-description-to-javadoc     "cerca-c++-javadoc-macros" nil t)
(autoload 'cerca-c++-convert-functions-in-buffer-to-javadoc   "cerca-c++-javadoc-macros" nil t)
(autoload 'cerca-c++-convert-function-description-to-javadoc  "cerca-c++-javadoc-macros" nil t)
(autoload 'cerca-c++-inline-function-in-header                "cerca-c++-javadoc-macros" nil t)
(autoload 'cerca-c++-insert-javadoc-file-comment              "cerca-c++-javadoc-macros" nil t)
(autoload 'cerca-c++-insert-javadoc-class-comment             "cerca-c++-javadoc-macros" nil t)

;;; auto-mode-alist
(setq auto-mode-alist
      (append
       '(
         ;;("\\.[k]?sh$" . ksh-mode)
         ;;("\\.bash$" . ksh-mode)
         ;;("\\.kshrc$" . ksh-mode)
         ;;("\\.profile$" . ksh-mode)
         ;;("\\.t?csh$" . sh-mode)
         ;;("\\.cshrc$" . sh-mode)
         ;;("\\.login$" . sh-mode)
         ("\\.[lyYhc]$" . c++-mode)
         ("\\.inl$" . c++-mode)
         ;;("\\.uil$" . c++-mode)
         ("\\.glsl$" . c++-mode)
         ("\\.cglsl$" . c++-mode)
         ("\\.cg$" . c++-mode)
         ("\\.cu$" . c++-mode)
         ("\\.mm$" . objc-mode)
         ("\\.[Vv][Uu]$" . vu-mode)
         ("\\.[Pp][Ii][Ee]$" . pie-mode)
         ("\\.tex$" . latex-mode)
         ("\\.aux$" . latex-mode)
         ("\\.vm$" . emacs-lisp-mode)
         )
       auto-mode-alist
       '(
         ("\\.e?ps$" . postscript-mode)
         ("\\.[Ii][Nn][Cc]$" . fortran-mode)
         ("\\.ftn$" . fortran-mode)
         ("\\.cdk$" . fortran-mode)
         ("\\.cgi$" . perl-mode)
         ("\\.md$" . markdown-mode)
         ;;("\\.php$" . php-mode)
         ;;("\\.xsd$" . xml-mode)
         ;;("\\.xsl$" . xml-mode)
         ("\\.jconf$" . xml-mode)
         ("\\.map$" . mapserver-mode)
         ("\\.compil$" . compilation-mode)
         ("\\.applescript$" . applescript-mode)
         )
       ))

(if (not (featurep 'xemacs))
    (setq auto-mode-alist
          (append
           auto-mode-alist
           '(
             ("\\.fvwm$" . conf-mode)
             ("ACL_entrepot*" . conf-mode)
             ("app-defaults/" . conf-mode)
             ("X11/.*\\.conf$" . conf-mode)
             ("httpd/.*\\.conf$" . conf-mode)
             ("\\.htaccess$" . conf-mode)
             ("\\.Xdefaults$" . conf-xdefaults-mode)
             ("\\.Xenvironment$" . conf-xdefaults-mode)
             ("\\.Xresources$" . conf-xdefaults-mode)
             ("\\.ad$" . conf-xdefaults-mode)
             ("\\.dot$" . sh-mode)
             ))))


;;; le serveur
(if (not (or (getenv "SVNINXEMACS") (getenv "SVNINEMACS")))
    (if (featurep 'xemacs)
        (gnuserv-start)
      (server-start)))
;;      (server-mode 1)))

;; * le fichier ~/.emacs-user.el, si present, est lu a la fin de cerca-config.
(load (expand-file-name "~/.emacs-user") t t)
