;;; ".emacs" de defaut pour ma config du Cerca.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

;; guess du directory lisp et ajout au load-path.
(let ((dir nil)
      (tail '(
              "${HOME}/lisp-benoit"
              "/usr/local/lisp-benoit"
              )))
  (while (and tail (not dir))
    (if (file-readable-p (substitute-in-file-name (concat (car tail) "/cerca-config.el")))
        (setq dir (substitute-in-file-name (car tail)))
      (setq tail (cdr tail))))
  (if dir (add-to-list 'load-path dir)))

(load "cerca-config" nil t)

;; * le fichier ~/.emacs-user.el, si present, est lu a la fin de cerca-config.

;; * pour avoir le gold-key sur NumLock, ajouter ces lignes dans "~/.xsession":
;;      xmodmap -e "remove mod2 = Num_Lock"
;;      xmodmap -e "remove mod5 = Num_Lock"
;;      xmodmap -e "keysym Num_Lock = KP_F1"
