;;; Fonctions diverses.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

(defvar grep-history nil)
(if (featurep 'xemacs)
    ;; version xemacs
    (defun cerca-grep (command-args)
      "Version de grep avec valeur de défaut:

   Run grep, with user-specified args, and collect output in a buffer.
While grep runs asynchronously, you can use the \\[next-error] command
to find the text that grep hits refer to.

This command uses a special history list for its arguments, so you can
easily repeat a grep command."
      ;;(require 'compile)
      (interactive
       (list (let* ((default (or (symbol-near-point)
                                 (and grep-history
                                      (car grep-history))))
                    (minibuffer-history-minimum-string-length 0)
                    (input
                     (if default
                         ;; rewritten for I18N3 snarfing
                         (read-from-minibuffer "Run grep (like this): grep -n " (format "'%s' " default) nil nil 'grep-history)
                       (read-from-minibuffer "Run grep (like this): grep -n " " " nil nil nil 'grep-history))))
               (if (and (equal input "") default)
                   (progn
                     (setq input default)
                     (setcar grep-history default)))
               ;; clear extra entries
               (setcdr grep-history (delete (car grep-history)
                                            (cdr grep-history)))
               (setq command-args (concat "grep -n " input))
               )))
      (let ((buf (compile-internal (concat command-args " " grep-null-device)
                                   "No more grep hits" "grep"
                                   ;; Give it a simpler regexp to match.
                                   nil grep-regexp-alist)))
        (save-excursion
          (set-buffer buf)
          (set (make-local-variable 'compilation-exit-message-function)
               ;; XEmacs change
               (lambda (proc msg)
                 (let ((code (process-exit-status proc)))
                   (if (eq (process-status proc) 'exit)
                       (cond ((zerop code)
                              '("finished (matches found)\n" . "matched"))
                             ((= code 1)
                              '("finished with no matches found\n" . "no match"))
                             (t
                              (cons msg code)))
                     (cons msg code))))))))
    ;; version emacs
  (defun grep (command-args)
    "Run grep, with user-specified args, and collect output in a buffer.
While grep runs asynchronously, you can use \\[next-error] (M-x next-error),
or \\<grep-mode-map>\\[compile-goto-error] in the grep \
output buffer, to go to the lines where grep
found matches.

For doing a recursive `grep', see the `rgrep' command.  For running
`grep' in a specific directory, see `lgrep'.

This command uses a special history list for its COMMAND-ARGS, so you
can easily repeat a grep command.

A prefix argument says to default the argument based upon the current
tag the cursor is over, substituting it into the last grep command
in the grep command history (or into `grep-command' if that history
list is empty)."
    (interactive
     (progn
       (grep-compute-defaults)
       (let ((default (grep-default-command)))
         (list (read-shell-command "Run grep (like this): "
                                   (if current-prefix-arg default grep-command)
                                   'grep-history
                                   (if current-prefix-arg nil default))))))

    ;; Setting process-setup-function makes exit-message-function work
    ;; even when async processes aren't supported.
    (compilation-start (if (and grep-use-null-device null-device)
                           (concat command-args " " null-device)
                         command-args)
                       'grep-mode)))

(defun cerca-cpp-if (&optional DEBUT FIN)
  "Insere les chaines DEBUT et FIN autour de la region courante.
Les valeurs de defaut sont \"#if 0\\n\" et \"#endif\\n\"."
  (interactive)
  (let ((start (region-beginning))
        (end (region-end))
        (chaine1 (if DEBUT DEBUT "#if 0\n"))
        (chaine2 (if FIN FIN "#endif\n")))
    (goto-char end)
    (insert chaine2)
    (goto-char start)
    (insert chaine1)
    (backward-char)
    ))

(defun webster-w3-define ()
  "Trouve la definition du mot pointe."
  (interactive)
  (require 'w3)
  (save-excursion
    (let ((word (current-word)))
      (message (format "Define %s ..." word))
      (w3-fetch "http://c.gp.cs.cmu.edu:5103/prog/webster%3F")
      )
    ))

(defun epelle-latex-buffer ()
  "Execute 'epelle' sur le buffer."
  (interactive)
  (save-excursion
    (accents-get-tex-from)
    (mark-whole-buffer)
    (message "Running 'epelle -latex' buffer...")
    (shell-command-on-region (region-beginning) (region-end)
                             "sed 's/\\\\^\\\\i{}/\\^i/g' | epelle -latex")))
(defun epelle-latex-region ()
  "Execute 'epelle' sur la region."
  (interactive)
  (save-excursion
    (accents-get-tex-from)
    (message "Running 'epelle -latex' region...")
    (shell-command-on-region (region-beginning) (region-end)
                             "sed 's/\\\\^\\\\i{}/\\^i/g' | epelle -latex")))
(defun epelle-latex-paragraph ()
  "Execute 'epelle' sur le paragraphe."
  (interactive)
  (save-excursion
    (accents-get-tex-from)
    (mark-paragraph)
    (message "Running 'epelle -latex' paragraph...")
    (shell-command-on-region (region-beginning) (region-end)
                             "sed 's/\\\\^\\\\i{}/\\^i/g' | epelle -latex")))
(defun epelle-latex-word ()
  "Execute 'epelle' sur le mot precedent."
  (interactive)
  (save-excursion
    (message "Running 'epelle -latex' word...")
    (skip-chars-backward "A-Za-z")
    (char-after 1)
    (if (looking-at "[A-Za-z]*")
        (shell-command-on-region (match-beginning 0) (match-end 0)
                                 "sed 's/\\\\^\\\\i{}/\\^i/g' | epelle -latex"))))

(defun epelle-8bit-buffer ()
  "Execute 'epelle' sur le buffer."
  (interactive)
  (save-excursion
    (mark-whole-buffer)
    (message "Running 'epelle -8bit' buffer...")
    (shell-command-on-region (region-beginning) (region-end)
                             "epelle -8bit")))
(defun epelle-8bit-region ()
  "Execute 'epelle' sur la region."
  (interactive)
  (save-excursion
    (message "Running 'epelle -8bit' region...")
    (shell-command-on-region (region-beginning) (region-end)
                             "epelle -8bit")))
(defun epelle-8bit-paragraph ()
  "Execute 'epelle' sur le paragraphe."
  (interactive)
  (save-excursion
    (mark-paragraph)
    (message "Running 'epelle -8bit' paragraph...")
    (shell-command-on-region (region-beginning) (region-end)
                             "epelle -8bit")))
(defun epelle-8bit-word ()
  "Execute 'epelle' sur le mot precedent."
  (interactive)
  (save-excursion
    (message "Running 'epelle -8bit' word...")
    (skip-chars-backward "A-Za-z")
    (char-after 1)
    (if (looking-at "[A-Za-z]*")
        (shell-command-on-region (match-beginning 0) (match-end 0)
                                 "epelle -8bit"))))

(defun epelle-latex-8bit-buffer ()
  "Execute 'epelle' sur le buffer."
  (interactive)
  (save-excursion
    (mark-whole-buffer)
    (message "Running 'epelle -latex -8bit' buffer...")
    (shell-command-on-region (region-beginning) (region-end)
                             "epelle -latex -8bit")))
(defun epelle-latex-8bit-region ()
  "Execute 'epelle' sur la region."
  (interactive)
  (save-excursion
    (message "Running 'epelle -latex -8bit' region...")
    (shell-command-on-region (region-beginning) (region-end)
                             "epelle -latex -8bit")))
(defun epelle-latex-8bit-paragraph ()
  "Execute 'epelle' sur le paragraphe."
  (interactive)
  (save-excursion
    (mark-paragraph)
    (message "Running 'epelle -latex -8bit' paragraph...")
    (shell-command-on-region (region-beginning) (region-end)
                             "epelle -latex -8bit")))
(defun epelle-latex-8bit-word ()
  "Execute 'epelle' sur le mot precedent."
  (interactive)
  (save-excursion
    (message "Running 'epelle -latex -8bit' word...")
    (skip-chars-backward "A-Za-z")
    (char-after 1)
    (if (looking-at "[A-Za-z]*")
        (shell-command-on-region (match-beginning 0) (match-end 0)
                                 "epelle -latex -8bit"))))

;; "echo epelle -8bit"))))

(defun spell-buffer ()
  "Execute 'spell' sur le buffer."
  (interactive)
  (save-excursion
    (mark-whole-buffer)
    (message "Running 'spell' buffer...")
    (shell-command-on-region (region-beginning) (region-end)
                             "detex | spell")))
(defun spell-region ()
  "Execute 'spell' sur la region."
  (interactive)
  (save-excursion
    (message "Running 'spell' region...")
    (shell-command-on-region (region-beginning) (region-end)
                             "detex | spell")))
(defun spell-paragraph ()
  "Execute 'spell' sur le paragraphe."
  (interactive)
  (save-excursion
    (mark-paragraph)
    (message "Running 'spell' paragraph...")
    (shell-command-on-region (region-beginning) (region-end)
                             "detex | spell")))
(defun spell-word ()
  "Execute 'spell' sur le mot precedent."
  (interactive)
  (save-excursion
    (message "Running 'spell' word...")
    (skip-chars-backward "A-Za-z")
    (char-after 1)
    (if (looking-at "[A-Za-z]*")
        (shell-command-on-region (match-beginning 0) (match-end 0)
                                 "detex | spell"))))

(defun flyspell-francais ()
  "Passer en flyspell-mode avec un dictionnaire francais."
  (interactive)
  (flyspell-mode t)
  (ispell-change-dictionary "francais"))

(defun flyspell-english ()
  "Passer en flyspell-mode avec un dictionnaire francais."
  (interactive)
  (flyspell-mode t)
  (ispell-change-dictionary "english"))

(defun flyspell-espanol ()
  "Passer en flyspell-mode avec un dictionnaire espagnol."
  (interactive)
  (flyspell-mode t)
  (ispell-change-dictionary "espanol"))

(defun view-all-registers () "Affiche le contenu de tous les registres."
  (interactive)
  (with-output-to-temp-buffer "*Output*"
    (let ((liste register-alist))
      (while liste
        (let ((val (get-register (car (car liste)))))
          (princ (format "Register %s contains " (single-key-description (car (car liste)))))
          (cond
           ((integerp val)
            (princ val))
           ((markerp val)
            (let ((buf (marker-buffer val)))
              (if (null buf)
                  (princ "a marker in no buffer")
                (princ (format
                        "a buffer position:\nbuff %s, position %s"
                        (buffer-name (marker-buffer val))
                        (marker-position val))))))

           ((window-configuration-p val)
            (princ "a window configuration."))
           ((and (consp val) (eq (car val) 'file))
            (princ "the file ")
            (prin1 (cdr val))
            (princ "."))
           ((consp val)
            (princ "the rectangle:\n")
            (while val
              (princ (car val))
              (terpri)
              (setq val (cdr val))))
           ((stringp val)
            (princ "the text:\n")
            (princ val))
           (t
            (princ "Garbage:\n")
            (prin1 val))))
        (princ "\n---\n")
        (setq liste (cdr liste))
        ))
    ))

(defun save-registers () "Affiche le contenu des registres (de textes) dans un format de sauvegarde."
  (interactive)
  (with-output-to-temp-buffer "*Output*"
    (let ((liste register-alist))
      (princ "Ajouter ces lignes au fichier ~/.emacs pour les conserver!\n")
      (while liste
        (let ((val (get-register (car (car liste)))))
          (cond
           ((integerp val))
           ((markerp val))
           ((window-configuration-p val))
           ((and (consp val) (eq (car val) 'file)))
           ((consp val)
            (princ (format "(set-register ?%s '" (single-key-description (car (car liste)))))
            (prin1 val)
            (princ ")\n"))
           ((stringp val)
            (princ (format "(set-register ?%s " (single-key-description (car (car liste)))))
            (prin1 val)
            (princ ")\n"))
           ))
        (setq liste (cdr liste))
        ))
    ))

(defun insert-register (register &optional arg)
  "Insert contents of register REGISTER.  (REGISTER is a character.)
Normally puts point before and mark after the inserted text.
If optional second arg is non-nil, puts mark before and point after.
Interactively, second arg is non-nil if prefix arg is supplied."
  (interactive "*cInsert register: \nP")
  (push-mark)
  (let ((val (get-register register)))
    (cond
     ((consp val)
      (insert-rectangle val))
     ((stringp val)
      (insert val))
     ((integerp val)
      (princ val (current-buffer)))
     ((and (markerp val) (marker-position val))
      (princ (marker-position val) (current-buffer)))
     (t
      (error "Register does not contain text"))))
  ;; XEmacs: don't activate the region.  It's annoying.
  ;;(if (not arg) (exchange-point-and-mark t)))
  ;; ma version:
  (if arg (exchange-point-and-mark)))

(defun where-are-other-keys (key)
  "Print message listing other key sequences that invoke the same command
as this key.  Argument is a key definition."
  (interactive "kWhere are other keys: ")
  (let* ((definition (lookup-key global-map key))
         (keys (where-is-internal definition nil nil nil nil)))
    (message "%s is on %s" definition
             (mapcar 'key-description keys))))

(defun interpole (id i if ad a af)
  "Interpolation lineaire."
  (interactive
   (let (id i if ad a af)
     (setq id (read-from-minibuffer "id: " nil nil nil 'query-replace-history))
     (setq i  (read-from-minibuffer "i : " nil nil nil 'query-replace-history))
     (setq if (read-from-minibuffer "if: " nil nil nil 'query-replace-history))
     (setq ad (read-from-minibuffer "ad: " nil nil nil 'query-replace-history))
     (setq a  (read-from-minibuffer "a : " nil nil nil 'query-replace-history))
     (setq af (read-from-minibuffer "af: " nil nil nil 'query-replace-history))
     (list id i if ad a af)))
  (save-excursion
    (insert
     (concat a " = " ad " + ( " af " - " ad " ) * ( " i " - " id " ) / ( " if " - " id " )"))))

(defun interpole2 (id i if ad a af)
  "Interpolation lineaire."
  (interactive
   (let (id i if ad a af)
     (setq id (read-from-minibuffer "id: " nil nil nil 'query-replace-history))
     (setq i  (read-from-minibuffer "i : " nil nil nil 'query-replace-history))
     (setq if (read-from-minibuffer "if: " nil nil nil 'query-replace-history))
     (setq ad (read-from-minibuffer "ad: " nil nil nil 'query-replace-history))
     (setq a  (read-from-minibuffer "a : " nil nil nil 'query-replace-history))
     (setq af (read-from-minibuffer "af: " nil nil nil 'query-replace-history))
     (list id i if ad a af)))
  (save-excursion
    (insert
     (concat a " = " ad " + ( 1 - (( " i " - " id " ) / ( " if " - " id " )) ) + " af " * (( " i " - " id " ) / ( " if " - " id " ))"))))


;(defun cerca-toggle-screen (&optional arg)
;  "Toggle between 80 and 132 character screen width.
;With arg, also changes the fill-column."
;  (interactive "P")
;  (set-frame-width (selected-frame) (- 212 (frame-width)))
;  (if arg (setq fill-column (- (frame-width) 4)))
;  (send-string-to-terminal (if (> (frame-width) 80) "\e[?3h" "\e[?3l")))

(defun elimine-whitespaces ()
  "Enleve les \"whitespaces\" et ^M en fin de lignes.
  (voir aussi delete-trailing-whitespace.)"
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "[ \t ]+$" nil t)
      (delete-region (match-beginning 0) (point)))))

(defun sauver-contexte ()
  "Sauver le contexte dans .emacs_loginame."
  (interactive)
  (save-context)
  (let (file-name)
    (setq file-name (concat (original-working-directory) ".emacs_" (user-login-name)))
    (message "Contexte dans \"%s\"" file-name)))

(defun delete-this-line ()
  "Enlever cette ligne."
  (interactive)
  (beginning-of-line)
;;  (kill-line 1))
  (delete-current-line 1))

(if (featurep 'xemacs)
    (require 'view-less))
(defun toggle-truncate-lines (&optional p)
  "Toggles the values of truncate-lines.
Positive prefix arg sets, negative disables."
  (interactive "P")
  (setq truncate-lines (if p
                           (> (prefix-numeric-value p) 0)
                         (not truncate-lines)))
  ;;(recenter))
  (redraw-display))

;(require 'scroll-in-place)
(defun scroll-down-in-place (n)
  "Defiler la fenetre vers le bas de ARG lignes."
  (interactive "p")
  (scroll-down n)
  (previous-line n))

(defun scroll-up-in-place (n)
  "Defiler la fenetre vers le haut de ARG lignes."
  (interactive "p")
  (scroll-up n)
  (next-line n))

(defun scroll-down-one ()
  "Defiler la fenetre vers le bas de 1 ligne."
  (interactive)
  (scroll-down 1))

(defun scroll-up-one ()
  "Defiler la fenetre vers le haut de 1 ligne."
  (interactive)
  (scroll-up 1))

(defun scroll-down-one-other-window ()
  "Defiler l'autre fenetre vers le bas de 1 ligne."
  (interactive)
  (scroll-other-window -1))

(defun scroll-up-one-other-window ()
  "Defiler l'autre fenetre vers le haut de 1 ligne."
  (interactive)
  (scroll-other-window 1))

(defun scroll-down-onethird ()
  "Defiler la fenetre vers le haut bas 1/3 d'ecran."
  (interactive)
  (scroll-down (/ (window-height) 3)))

(defun scroll-up-onethird ()
  "Defiler la fenetre vers le haut de 1/3 d'ecran."
  (interactive)
  (scroll-up (/ (window-height) 3)))

(defun see-chars ()
  "Afficher les caracteres entres, suivi d'une pause de 2 secondes."
  (interactive)
  (let ((chars "")
        (inhibit-quit t))
    (message "Enter characters, terminated by 2-second timeout.")
    (while (not (sit-for 2))
      (setq chars (concat chars (list (read-char)))
            quit-flag nil))		; quit-flag maybe set by C-g
    (message "Characters entered: %s" (key-description chars))))

(defun indent-buffer ()
  "Indenter tout le buffer."
  (interactive)
  (indent-region (point-min) (point-max) nil)
  (delete-trailing-whitespace))

(defun open-new-line (arg)
  "Ouvrir une nouvelle ligne sous la ligne courante et indent."
  (interactive "p")
  (if (not (looking-at "$")) (end-of-line))
  (if (> arg 1) (open-line (- arg 1)))
  (newline-and-indent))

;(defun choose-completion ()
;  "Choose the completion that point is in or next to."
;  (interactive)
;  (let (beg end)
;    (skip-chars-forward "^ \t\n")
;    (while (looking-at " [^ \n\t]")
;      (forward-char 1)
;      (skip-chars-forward "^ \t\n"))
;    (setq end (point))
;    (skip-chars-backward "^ \t\n")
;    (while (and (= (preceding-char) ?\ )
;		(not (and (> (point) (1+ (point-min)))
;			  (= (char-after (- (point) 2)) ?\ ))))
;      (backward-char 1)
;      (skip-chars-backward "^ \t\n"))
;    (setq beg (point))
;    (choose-completion-string (buffer-substring beg end))))

;; Delete the longest partial match for STRING
;; that can be found before POINT.
;(defun choose-completion-delete-max-match (string)
;  (let ((len (min (length string)
;		  (- (point) (point-min)))))
;    (goto-char (- (point) (length string)))
;    (if completion-ignore-case
;	(setq string (downcase string)))
;    (while (and (> len 0)
;		(let ((tail (buffer-substring (point)
;					      (+ (point) len))))
;		  (if completion-ignore-case
;		      (setq tail (downcase tail)))
;		  (not (string= tail (substring string 0 len)))))
;      (setq len (1- len))
;      (forward-char 1))
;    (delete-char len)))

;(defun choose-completion-string (choice &optional buffer)
;  (let ((buffer (or buffer completion-reference-buffer)))
;    ;; If BUFFER is a minibuffer, barf unless it's the currently
;    ;; active minibuffer.
;    (if (and (string-match "\\` \\*Minibuf-[0-9]+\\*\\'" (buffer-name buffer))
;	     (or (not (minibuffer-window-active-p (minibuffer-window)))
;		 (not (equal buffer (window-buffer (minibuffer-window))))))
;	(error "Minibuffer is not active for completion")
;      ;; Insert the completion into the buffer where completion was requested.
;      (set-buffer buffer)
;      (choose-completion-delete-max-match choice)
;      (insert choice)
;      (remove-text-properties (- (point) (length choice)) (point)
;			      '(mouse-face nil))
;      ;; Update point in the window that BUFFER is showing in.
;      (let ((window (get-buffer-window buffer t)))
;	(set-window-point window (point)))
;      ;; If completing for the minibuffer, exit it with this choice.
;      (and (equal buffer (window-buffer (minibuffer-window)))
;	   (minibuffer-complete-and-exit)))))

(defun cerca-show-keys ()
  "Affiche la liste de cles."
  (interactive)
  (with-output-to-temp-buffer "*cles*"
    (set-buffer standard-output)
    (princ (format "*** Cles PF: (setq config-saveur '%s)\n\n" config-saveur))
    (let ((ff "%s\t%-30s%-30s%-30s\n"))
      (princ (format ff " " "(cle)" "(shift cle)" "(control cle)\n"))
      (princ (format ff "f1"   (key-binding 'f1) (key-binding [(shift f1)]) (key-binding [(control f1)])))
      (princ (format ff "f2"   (key-binding 'f2) (key-binding [(shift f2)]) (key-binding [(control f2)])))
      (princ (format ff "f3"   (key-binding 'f3) (key-binding [(shift f3)]) (key-binding [(control f3)])))
      (princ (format ff "f4"   (key-binding 'f4) (key-binding [(shift f4)]) (key-binding [(control f4)])))
      (princ (format ff "f5"   (key-binding 'f5) (key-binding [(shift f5)]) (key-binding [(control f5)])))
      (princ (format ff "f6"   (key-binding 'f6) (key-binding [(shift f6)]) (key-binding [(control f6)])))
      (princ (format ff "f7"   (key-binding 'f7) (key-binding [(shift f7)]) (key-binding [(control f7)])))
      (princ (format ff "f8"   (key-binding 'f8) (key-binding [(shift f8)]) (key-binding [(control f8)])))
      (princ (format ff "f9"   (key-binding 'f9) (key-binding [(shift f9)]) (key-binding [(control f9)])))
      (princ (format ff "f10"  (key-binding 'f10) (key-binding [(shift f10)]) (key-binding [(control f10)])))
      (princ (format ff "f11"  (key-binding 'f11) (key-binding [(shift f11)]) (key-binding [(control f11)])))
      (princ (format ff "f12"  (key-binding 'f12) (key-binding [(shift f12)]) (key-binding [(control f12)])))
      )
    (princ (format "\n\n*** Cles speciales:\n\n"))
    (let ((ff "%-15s%-30s%-30s\n"))
      (princ (format ff " " "(cle)" "(Gold cle)\n"))
      (princ (format ff "cancel"     (key-binding 'cancel    ) (key-binding [kp_f1 (cancel)    ])))
      (princ (format ff "ctrl/act"   (key-binding 'execute   ) (key-binding [kp_f1 (execute)   ])))
      (princ (format ff "delete"     (key-binding 'delete    ) (key-binding [kp_f1 (delete)    ])))
      (princ (format ff "deletechar" (key-binding 'deletechar) (key-binding [kp_f1 (deletechar)])))
      (princ (format ff "down"       (key-binding 'down      ) (key-binding [kp_f1 (down)      ])))
      (princ (format ff "end"        (key-binding 'end       ) (key-binding [kp_f1 (end)       ])))
      (princ (format ff "execute"    (key-binding 'execute   ) (key-binding [kp_f1 (execute)   ])))
      (princ (format ff "find"       (key-binding 'find      ) (key-binding [kp_f1 (find)      ])))
      (princ (format ff "home"       (key-binding 'home      ) (key-binding [kp_f1 (home)      ])))
      (princ (format ff "insert"     (key-binding 'insert    ) (key-binding [kp_f1 (insert)    ])))
      (princ (format ff "insertchar" (key-binding 'insertchar) (key-binding [kp_f1 (insertchar)])))
      (princ (format ff "left"       (key-binding 'left      ) (key-binding [kp_f1 (left)      ])))
      (princ (format ff "linefeed"   (key-binding 'linefeed  ) (key-binding [kp_f1 (linefeed)  ])))
      (princ (format ff "next"       (key-binding 'next      ) (key-binding [kp_f1 (next)      ])))
      (princ (format ff "pause"      (key-binding 'pause     ) (key-binding [kp_f1 (pause)     ])))
      (princ (format ff "print"      "(voir plus bas)"         (key-binding [kp_f1 (print)     ])))
      (princ (format ff "prior"      (key-binding 'prior     ) (key-binding [kp_f1 (prior)     ])))
      (princ (format ff "prsc"       "(voir plus bas)"         (key-binding [kp_f1 (prsc)      ])))
      (princ (format ff "return"     (key-binding 'return    ) (key-binding [kp_f1 (return)    ])))
      (princ (format ff "right"      (key-binding 'right     ) (key-binding [kp_f1 (right)     ])))
      (princ (format ff "scroll"     (key-binding 'scroll    ) (key-binding [kp_f1 (scroll)    ])))
      (princ (format ff "select"     (key-binding 'select    ) (key-binding [kp_f1 (select)    ])))
      (princ (format ff "tab"        (key-binding 'tab       ) (key-binding [kp_f1 (tab)       ])))
      (princ (format ff "up"         (key-binding 'up        ) (key-binding [kp_f1 (up)        ])))
      )
    (princ (format "\n\n*** Clavier numerique:\n\n"))
    (let ((ff "%-15s%-30s%-30s\n"))
      (princ (format ff " " "(cle)" "(Gold cle)\n"))
      (princ (format ff "."     (key-binding 'kp_decimal)      (key-binding [kp_f1 kp_decimal])))
      (princ (format ff "0"     (key-binding 'kp_0      )      (key-binding [kp_f1 kp_0      ])))
      (princ (format ff "1"     (key-binding 'kp_enter  )      (key-binding [kp_f1 kp_enter  ])))
      (princ (format ff "2"     (key-binding 'kp_2      )      (key-binding [kp_f1 kp_2      ])))
      (princ (format ff "3"     (key-binding 'kp_3      )      (key-binding [kp_f1 kp_3      ])))
      (princ (format ff "4"     (key-binding 'kp_4      )      (key-binding [kp_f1 kp_4      ])))
      (princ (format ff "5"     (key-binding 'kp_5      )      (key-binding [kp_f1 kp_5      ])))
      (princ (format ff "6"     (key-binding 'kp_6      )      (key-binding [kp_f1 kp_6      ])))
      (princ (format ff "7"     (key-binding 'kp_7      )      (key-binding [kp_f1 kp_7      ])))
      (princ (format ff "8"     (key-binding 'kp_8      )      (key-binding [kp_f1 kp_8      ])))
      (princ (format ff "9"     (key-binding 'kp_9      )      (key-binding [kp_f1 kp_9      ])))
      (princ (format ff "GOLD"  "(voir plus bas)"                (key-binding [kp_f1 kp_f1])))
      (princ (format ff "/"     (key-binding 'kp_divide)       (key-binding [kp_f1 kp_divide])))
      (princ (format ff "*"     (key-binding 'kp_multiply)     (key-binding [kp_f1 kp_multiply])))
      (princ (format ff "-"     (key-binding 'kp_subtract)     (key-binding [kp_f1 kp_subtract])))
      (princ (format ff "(shift)+" (key-binding [(shift kp_add)]) (key-binding [kp_f1 (shift kp_add)])))
      (princ (format ff "+"     (key-binding 'kp_add    )      (key-binding [kp_f1 kp_add    ])))
      (princ (format ff "."     (key-binding 'kp_decimal)      (key-binding [kp_f1 kp_decimal])))
      (princ (format ff "enter" (key-binding 'kp_enter)        (key-binding [kp_f1 kp_enter])))
      )
    (princ (format "\n\n*** Gold-Key suivi de:\n\n"))
    (let ((ff "%s   %-30s%s   %-30s%s   %-30s\n"))
      (princ (format ff "a" (lookup-key GOLD-map 'a) "A" (lookup-key GOLD-map 'A) "ctrl-a" (lookup-key GOLD-map [(control a)])))
      (princ (format ff "b" (lookup-key GOLD-map 'b) "B" (lookup-key GOLD-map 'B) "ctrl-b" (lookup-key GOLD-map [(control b)])))
      (princ (format ff "c" (lookup-key GOLD-map 'c) "C" (lookup-key GOLD-map 'C) "ctrl-c" (lookup-key GOLD-map [(control c)])))
      (princ (format ff "d" (lookup-key GOLD-map 'd) "D" (lookup-key GOLD-map 'D) "ctrl-d" (lookup-key GOLD-map [(control d)])))
      (princ (format ff "e" (lookup-key GOLD-map 'e) "E" (lookup-key GOLD-map 'E) "ctrl-e" (lookup-key GOLD-map [(control e)])))
      (princ (format ff "f" (lookup-key GOLD-map 'f) "F" (lookup-key GOLD-map 'F) "ctrl-f" (lookup-key GOLD-map [(control f)])))
      (princ (format ff "g" (lookup-key GOLD-map 'g) "G" (lookup-key GOLD-map 'G) "ctrl-g" (lookup-key GOLD-map [(control g)])))
      (princ (format ff "h" (lookup-key GOLD-map 'h) "H" (lookup-key GOLD-map 'H) "ctrl-h" (lookup-key GOLD-map [(control h)])))
      (princ (format ff "i" (lookup-key GOLD-map 'i) "I" (lookup-key GOLD-map 'I) "ctrl-i" (lookup-key GOLD-map [(control i)])))
      (princ (format ff "j" (lookup-key GOLD-map 'j) "J" (lookup-key GOLD-map 'J) "ctrl-j" (lookup-key GOLD-map [(control j)])))
      (princ (format ff "k" (lookup-key GOLD-map 'k) "K" (lookup-key GOLD-map 'K) "ctrl-k" (lookup-key GOLD-map [(control k)])))
      (princ (format ff "l" (lookup-key GOLD-map 'l) "L" (lookup-key GOLD-map 'L) "ctrl-l" (lookup-key GOLD-map [(control l)])))
      (princ (format ff "m" (lookup-key GOLD-map 'm) "M" (lookup-key GOLD-map 'M) "ctrl-m" (lookup-key GOLD-map [(control m)])))
      (princ (format ff "n" (lookup-key GOLD-map 'n) "N" (lookup-key GOLD-map 'N) "ctrl-n" (lookup-key GOLD-map [(control n)])))
      (princ (format ff "o" (lookup-key GOLD-map 'o) "O" (lookup-key GOLD-map 'O) "ctrl-o" (lookup-key GOLD-map [(control o)])))
      (princ (format ff "p" (lookup-key GOLD-map 'p) "P" (lookup-key GOLD-map 'P) "ctrl-p" (lookup-key GOLD-map [(control p)])))
      (princ (format ff "q" (lookup-key GOLD-map 'q) "Q" (lookup-key GOLD-map 'Q) "ctrl-q" (lookup-key GOLD-map [(control q)])))
      (princ (format ff "r" (lookup-key GOLD-map 'r) "R" (lookup-key GOLD-map 'R) "ctrl-r" (lookup-key GOLD-map [(control r)])))
      (princ (format ff "s" (lookup-key GOLD-map 's) "S" (lookup-key GOLD-map 'S) "ctrl-s" (lookup-key GOLD-map [(control s)])))
      (princ (format ff "t" (lookup-key GOLD-map 't) "T" (lookup-key GOLD-map 'T) "ctrl-t" (lookup-key GOLD-map [(control t)])))
      (princ (format ff "u" (lookup-key GOLD-map 'u) "U" (lookup-key GOLD-map 'U) "ctrl-u" (lookup-key GOLD-map [(control u)])))
      (princ (format ff "v" (lookup-key GOLD-map 'v) "V" (lookup-key GOLD-map 'V) "ctrl-v" (lookup-key GOLD-map [(control v)])))
      (princ (format ff "w" (lookup-key GOLD-map 'w) "W" (lookup-key GOLD-map 'W) "ctrl-w" (lookup-key GOLD-map [(control w)])))
      (princ (format ff "x" (lookup-key GOLD-map 'x) "X" (lookup-key GOLD-map 'X) "ctrl-x" (lookup-key GOLD-map [(control x)])))
      (princ (format ff "y" (lookup-key GOLD-map 'y) "Y" (lookup-key GOLD-map 'Y) "ctrl-y" (lookup-key GOLD-map [(control y)])))
      (princ (format ff "z" (lookup-key GOLD-map 'z) "Z" (lookup-key GOLD-map 'Z) "ctrl-z" (lookup-key GOLD-map [(control z)])))
      )
    (princ (format "\n\n"))
    (let ((ff "%-7s%-27s%-7s%-27s\n"))
      (princ (format ff "space" (lookup-key GOLD-map " ") " " " "))
      (princ (format ff "," (lookup-key GOLD-map ",") "<" (lookup-key GOLD-map "<")))
      (princ (format ff "." (lookup-key GOLD-map ".") ">" (lookup-key GOLD-map ">")))
      (princ (format ff "/" (lookup-key GOLD-map "/") "?" (lookup-key GOLD-map "?")))
      (princ (format ff ";" (lookup-key GOLD-map ";") ":" (lookup-key GOLD-map ":")))
      (princ (format ff "'" (lookup-key GOLD-map "'") "\"" (lookup-key GOLD-map "\"")))
      (princ (format ff "[" (lookup-key GOLD-map "[") "{" (lookup-key GOLD-map "{")))
      (princ (format ff "]" (lookup-key GOLD-map "]") "}" (lookup-key GOLD-map "}")))
      (princ (format ff "\\" (lookup-key GOLD-map "\\") "|" (lookup-key GOLD-map "|")))
      (princ (format ff "`" (lookup-key GOLD-map "`") "~" (lookup-key GOLD-map "~")))
      (princ (format ff "1" (lookup-key GOLD-map "1") "!" (lookup-key GOLD-map "!")))
      (princ (format ff "2" (lookup-key GOLD-map "2") "@" (lookup-key GOLD-map "@")))
      (princ (format ff "3" (lookup-key GOLD-map "3") "#" (lookup-key GOLD-map "#")))
      (princ (format ff "4" (lookup-key GOLD-map "4") "$" (lookup-key GOLD-map "$")))
      (princ (format ff "5" (lookup-key GOLD-map "5") "%" (lookup-key GOLD-map "%")))
      (princ (format ff "6" (lookup-key GOLD-map "6") "^" (lookup-key GOLD-map "^")))
      (princ (format ff "7" (lookup-key GOLD-map "7") "&" (lookup-key GOLD-map "&")))
      (princ (format ff "8" (lookup-key GOLD-map "8") "*" (lookup-key GOLD-map "*")))
      (princ (format ff "9" (lookup-key GOLD-map "9") "(" (lookup-key GOLD-map "(")))
      (princ (format ff "0" (lookup-key GOLD-map "0") ")" (lookup-key GOLD-map ")")))
      (princ (format ff "-" (lookup-key GOLD-map "-") "_" (lookup-key GOLD-map "_")))
      ;;(princ (format ff "=" (lookup-key GOLD-map "=") "+" (lookup-key GOLD-map "+")))
      )
    (princ (format "\n\n*** Souris:\n\n"))
    (let ((ff "%s\t%-33s%-33s%-33s\n"))
      (princ (format ff "                  " "button1" "button2" "button3\n"))
      (princ (format ff "                  "
                     (lookup-key global-map 'button1)
                     (lookup-key global-map 'button2)
                     (lookup-key global-map 'button3)))
      (princ (format ff "shift             "
                     (lookup-key global-map '(shift button1))
                     (lookup-key global-map '(shift button2))
                     (lookup-key global-map '(shift button3))))
      (princ (format ff "      control     "
                     (lookup-key global-map '(control button1))
                     (lookup-key global-map '(control button2))
                     (lookup-key global-map '(control button3))))
      (princ (format ff "shift control     "
                     (lookup-key global-map '(shift control button1))
                     (lookup-key global-map '(shift control button2))
                     (lookup-key global-map '(shift control button3))))
      (princ (format "\n"))
      (princ (format ff "              meta"
                     (lookup-key global-map '(meta button1))
                     (lookup-key global-map '(meta button2))
                     (lookup-key global-map '(meta button3))))
      (princ (format ff "shift         meta"
                     (lookup-key global-map '(shift meta button1))
                     (lookup-key global-map '(shift meta button2))
                     (lookup-key global-map '(shift meta button3))))
      (princ (format ff "      control meta"
                     (lookup-key global-map '(control meta button1))
                     (lookup-key global-map '(control meta button2))
                     (lookup-key global-map '(control meta button3))))
      (princ (format ff "shift control meta"
                     (lookup-key global-map '(shift control meta button1))
                     (lookup-key global-map '(shift control meta button2))
                     (lookup-key global-map '(shift control meta button3))))
      )
    ;;(describe-bindings-internal (current-global-map) nil '() [kp_f1] t)
    ))

(provide 'cerca-fcts)
