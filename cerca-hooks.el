;;; Definition des "hooks" pour les differents modes.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

;  (popup-dialog-box
;   `(,(concat "Compile:\n        " compile-command)
;     ["Compile" (compile compile-command) t]
;     ["Edit command" compile t]
;     nil
;     ["Cancel" (message "Quit") t])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun cerca-hooks-ivd () "Initialise les cl�s pour ivd"
  (local-set-key "\C-ci" 'ivd-insert-in-variable-list)
  (local-set-key "\C-cp" 'ivd-insert-variable-dump)
  ;;(local-set-key "\C-cq" 'ivd-insert-variable-dump)
  (local-set-key "\C-cr" 'ivd-reset-variable-list)
  (local-set-key "\C-cv" 'ivd-select-variable-list)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq c-mode-menu
      '(["Comment Out Region" comment-region (mark)]
        ["Macro Expand Region" c-macro-expand (mark)]
        ["Backslashify" c-backslash-region (mark)]
        ["Indent Expression" c-indent-exp (memq (following-char) (quote (40 91 123)))]
        ["Indent Line" c-indent-command t]
        ["Indent function" c-indent-defun t]
        ["Fill Comment Paragraph" c-fill-paragraph t]
        ["---"                 nil t]
        ["Mark function" c-mark-function t]
        ["Beginning of function" c++-beginning-of-defun t]
        ["End of function" c++-end-of-defun t]
        ["Backward Statement" c-beginning-of-statement t]
        ["Forward Statement" c-end-of-statement t]
        ["----"                 nil t]
        ["Backward Conditional" c-backward-conditional t]
        ["Forward Conditional" c-forward-conditional t]
        ["Up Conditional" c-up-conditional t]
        ["-----"                 nil t]
        ["Implante" cerca-c-implante t]
        ["Insert header" cerca-c-insert-header t]
        ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; (sans auctex) Entering Latex mode runs the hook `text-mode-hook', then
;;; `tex-mode-hook', and finally `latex-mode-hook'.  When the special
;;; subshell is initiated, `tex-shell-hook' is run.

;;; (avec auctex) Entering LaTeX mode calls the value of text-mode-hook,
;;; then the value of TeX-mode-hook, and then the value
;;; of LaTeX-mode-hook.

;;; Entering plain-TeX mode calls the value of text-mode-hook,
;;; then the value of TeX-mode-hook, and then the value
;;; of plain-TeX-mode-hook.

(add-hook
 'text-mode-hook
 (function
  (lambda()
    (accents-iso 1)
    (setq abbrev-mode t)
    (setq word-wrap t)
    (require 'cerca-tpu)
    (tpu-add-word-separators "[]{}()=/,.`':&;")
    (auto-fill-mode 1)
    (setq truncate-lines nil)
    (setq indent-tabs-mode t)
    (modify-syntax-entry ?\" "\"") ;; pour le font-lock
    (if (string-match
         "ChangeLog"
         (file-name-nondirectory (format "%s" buffer-file-name)))
        (progn
          (setq left-margin 8)
          (setq fill-column 74)))
    )))

(defun cerca-tex-mode-hook ()
  ;; (accents-iso 1)
  (setq abbrev-mode t)
  (setq indent-tabs-mode nil)
  (require 'cerca-tex-fcts)
  ;; (local-set-key "\C-cc"    'TeX-chapter)
  ;; (local-set-key "\C-cS"    'TeX-section)
  ;; (local-set-key "\C-cs"    'TeX-subsection)
  ;; (local-set-key "\C-c\C-e" 'TeX-insert-environment)
  ;; (local-set-key "\C-c\C-b" 'TeX-bracket-region-be)
  (local-set-key "`"        'TeX-insert-quote)
  (local-set-key "\C-ci"    'TeX-italicize-word)
  (local-set-key "\C-cb"    'TeX-bold-word)
  (local-set-key "\C-cc"    'TeX-code-word)
  (local-set-key "\C-cd"    'TeX-listing-word)
  (local-set-key "\C-co"    'TeX-overline-word)
  (local-set-key "\C-cu"    'TeX-underline-word)
  (local-set-key "\C-ct"    'TeX-typewrite-word)
  (local-set-key "\C-ck"    'TeX-kill-construct)
  (local-set-key "\C-ce"    'TeX-emphasize-word)
  (local-set-key "\C-cl"    'TeX-slant-word)
  ;;(local-set-key "\C-cr"    'tex-validate-region)
  ;;(local-set-key "\C-cv"    'validate-tex-buffer)
  ;;(local-set-key "\C-csW"    'epelle-latex-word)
  ;;(local-set-key "\C-csR"    'epelle-latex-region)
  ;;(local-set-key "\C-csB"    'epelle-latex-buffer)
  ;;(local-set-key "\C-csw"    'epelle-8bit-word)
  ;;(local-set-key "\C-csr"    'epelle-8bit-region)
  ;;(local-set-key "\C-csb"    'epelle-8bit-buffer)
  ;;(local-set-key "\C-cs\C-w" 'spell-word)
  ;;(local-set-key "\C-cs\C-r" 'spell-region)
  ;;(local-set-key "\C-cs\C-b" 'spell-buffer)
  (local-set-key "\C-c\C-v"  'tex-view)
  (local-set-key "\C-c\C-t"  'tex-alt-print)
  (local-set-key "\C-c\C-a"  'TeX-autodvi)
  (local-set-key "$"         'self-insert-command)
  ;;    (if (and (string-match "XEmacs" emacs-version)
  ;;             (boundp 'emacs-major-version)
  ;;             (= emacs-major-version 19)
  ;;             (<= emacs-minor-version 14))
  ;;        (progn
  ;;          (defvar tex-mode-menu
  ;;            '("LaTeX Command Menu"
  ;;              ["Comment Out Region"   comment-region (mark)]
  ;;              ["Uncomment Region"     uncomment-region (mark)]
  ;;              ["---"                  nil t]
  ;;              ["Bold Word"            TeX-bold-word t]
  ;;              ["Emph Word"            TeX-emphasize-word t]
  ;;              ["Slant Word"           TeX-slant-word t]
  ;;              ["Ital Word"            TeX-italicize-word t]
  ;;              ["Underline Word"       TeX-underline-word t]
  ;;              ["Overline Word"        TeX-overline-word t]
  ;;              ["Typewrite Word"       TeX-typewrite-word t]
  ;;              ["Kill Construct"       TeX-kill-construct t]
  ;;              ["Insert Braces"        tex-insert-braces t]
  ;;              ["Close Latex Block"    tex-close-latex-block t]
  ;;              ["----"                 nil t]
  ;;              ["Validate Buffer"      validate-tex-buffer t]
  ;;              ["Validate Region"      tex-validate-region (mark)]
  ;;              ["latex File"           tex-file t]
  ;;              ["latex Buffer"         tex-buffer t]
  ;;              ["latex Region"         tex-region (mark)]
  ;;              ["latex Block"          tex-latex-block t]
  ;;              ["bibtex File"          tex-bibtex-file t]
  ;;              ["View (xdvi)"          tex-view t]
  ;;              ["View (ghostview)"     tex-alt-print t]
  ;;              ["Print (lpr)"          tex-print t]
  ;;              ["Recenter Output"      tex-recenter-output-buffer t]
  ;;              ["Show Print queue"     tex-show-print-queue t]
  ;;              ["Kill Job"             tex-kill-job t]
  ;;              )
  ;;            "Menu pour Tex mode.")
  ;;          (setq mode-popup-menu tex-mode-menu)
  ;;          (if (and (boundp 'current-menubar)
  ;;                   current-menubar
  ;;                   (not (assoc mode-name current-menubar))
  ;;                   (progn
  ;;                     (set-buffer-menubar (copy-sequence current-menubar))
  ;;                     (add-menu nil mode-name tex-mode-menu))))
  ;;          ))
  (imenu-add-menubar-index)
  )

(add-hook 'doc-view-mode-hook 'auto-revert-mode)
(add-hook 'TeX-mode-hook 'cerca-tex-mode-hook)
(add-hook 'tex-mode-hook 'cerca-tex-mode-hook)


(add-hook
 'bibtex-mode-hook
 (function
  (lambda()
    (accents-iso 1)
    (setq fill-column 99999)
    (setq abbrev-mode t)
    (require 'cerca-tex-fcts)
    (local-set-key "\C-cb" 'tex-bibtex-file)
    (local-set-key "\C-cd" 'find-bibtex-duplicates)
    (local-set-key "\C-cf" 'find-bibtex-entry-location)
    (local-set-key "\C-ch" 'hide-bibtex-entry-bodies)
    (local-set-key "\C-ck" 'bibtex-current-entry-label)
    (local-set-key "\C-cn" 'bibtex-narrow-to-entry)
    (local-set-key "\C-cs" 'bibtex-sort-busort)
    (local-set-key "\C-cv" 'bibtex-validate)
    (setq ;;bibtex-include-OPTannote nil
          ;;bibtex-include-OPTcrossref nil
          bibtex-include-OPTkey nil
          bibtex-maintain-sorted-entries t
          ;; bibtex-sort-ignore-string-entries nil
          )
    )))

(add-hook
 'vm-presentation-mode-hook
 (function
  (lambda()
    (setq word-wrap t)
    )))

(add-hook
 'vm-mail-mode-hook
 (function
  (lambda()
    ;;(accents-iso 1)
    (setq abbrev-mode t)
    (setq word-wrap t)
    (font-lock-mode 1)
    ;NOT(local-set-key '(control tab) 'eudc-expand-inline)
    )))

(add-hook
 'vm-mode-hook
 (function
  (lambda()
    (setq word-wrap t)
    ;(standard-display-ascii ?\200 "e")
    ;;!!(standard-display-ascii ?\221 "\'")  ; =91
    (standard-display-ascii ?\222 "\'")  ; =92
    ;(standard-display-ascii ?\223 "\"")
    ;(standard-display-ascii ?\204 "\"")
    ;(standard-display-ascii ?\224 "\"")
    ;(standard-display-ascii ?\213 " -- ")
    ;(standard-display-ascii ?\205 "--")
    ;(standard-display-ascii ?\226 "--")
    ;(standard-display-ascii ?\227 "\-")
    (local-set-key "#"     'vm-expunge-folder)
    (local-set-key [(f2)]  'vm-visit-folder)
    (local-set-key [(f3)]  'vm-save-and-expunge-folder)
    (local-set-key [(f4)]  'vm-quit)
    (local-set-key [(f5)]  'vm-expunge-folder)
    (local-set-key [(f12)] 'vm-revisit-folder)
    (define-key vm-edit-message-map [(f3)] 'vm-edit-message-end)
    (define-key vm-edit-message-map [(f4)] 'vm-edit-message-abort)
    (font-lock-fontify-buffer)
    )))

;; pour voir les part html!
;(add-hook
; 'vm-menu-setup-hook
; (function
;  (lambda()
;    (defun vm-mime-display-internal-multipart/alternative (layout)
;      (vm-mime-display-internal-multipart/mixed layout))
;    )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-hook
 'php-mode-hook
 (function
  (lambda()
    (interactive)
    (setq abbrev-mode t)
    (setq indent-tabs-mode nil)
    (imenu-add-menubar-index)
    )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-hook
 'c-mode-common-hook
 (function
  (lambda()
    (interactive)
    (setq abbrev-mode t)
    (require 'cerca-tpu)
    (tpu-add-word-separators "<>[]{}()=+-*|/,'\":&;")
    (require 'cerca-c++-macros)
    (local-set-key "\C-c0" 'cerca-cpp-if)
    (cerca-hooks-ivd)
    (local-set-key "\C-cf" 'ivd-insert-float-variable-list)
    (local-set-key "\C-cs" 'ivd-insert-string-in-variable-list)
    (local-set-key "\C-cp" 'ivd-insert-variable-dump)
    (local-set-key "\C-cq" '(lambda nil (interactive) (ivd-insert-variable-dump 1)))
    (local-set-key "\C-cd" '(lambda nil (interactive) (ivd-insert-variable-dump 2)))
    (local-set-key "\C-ch" 'cerca-c-insert-header)
    (local-set-key "\C-cm" 'cerca-c-implante)
    (local-set-key "\C-c\C-b" 'c++-browse)
    (local-set-key "\e\C-a" 'c++-beginning-of-defun)
    (local-set-key "\e\C-e" 'c++-end-of-defun)
    (local-set-key "\e\C-h" 'undo)
    (define-key GOLD-map "/" 'c++-comment-region)
    (define-key GOLD-map "\\" 'c++-uncomment-region)
    (setq indent-tabs-mode nil)
    (c-set-style c-site-default-style)
    ;;(c-set-offset 'substatement-open 0 nil)
    ;;(c-set-offset 'statement-cont 0 nil)
    ;;(c-set-offset 'inline-open 0 nil)
    (let ((term (getenv "TERM")))
      (cond ((string-match "vt100" term))
            ((string-match "vt220" term))
            ((string-match "hft"   term))
            (t (setq delete-key-deletes-forward t))
            ))
    (imenu-add-menubar-index)
    )))

(add-hook
 'c++-mode-hook
 (function
  (lambda()
    (let ((term (getenv "TERM")))
      (cond ((string-match "vt100" term))
            ((string-match "vt220" term))
            ((string-match "hft"   term))
            (t (setq c-delete-function 'delete-char))
            ))
    (local-set-key "\C-c1" 'cerca-c++-convert-function-description-to-javadoc)
    (local-set-key "\C-c2" 'cerca-c++-convert-functions-in-buffer-to-javadoc)
    (local-set-key "\C-c3" 'cerca-c++-convert-class-description-to-javadoc)
    (local-set-key "\C-cC" 'cerca-c++-insert-javadoc-class-comment)
    (local-set-key "\C-cF" 'cerca-c++-insert-javadoc-file-comment)
    (local-set-key "\C-cH" 'cerca-c++-insert-javadoc-function-description)
    (local-set-key "\C-cI" 'cerca-c++-inline-function-in-header)
    (local-set-key "\C-cM" 'cerca-c++-move-defun-to-src-and-javadocify)
    (modify-syntax-entry ?\: "_" c++-mode-syntax-table)
    )))

(add-hook
 'ada-mode-hook
 (function
  (lambda()
    (setq abbrev-mode t)
    (define-key ada-mode-map [(delete)] 'delete-char)
    ;; ? (substitute-key-definition 'delete-char 'delete-backward-char lisp-interaction-mode-map)
    (local-set-key "\e\C-h" 'undo)
    )))

(add-hook
 'fortran-mode-hook
 (function
  (lambda()
    (setq abbrev-mode t)
    (require 'cerca-tpu)
    (tpu-add-word-separators "()=+-*/,'")
    (cerca-hooks-ivd)
    (local-set-key "\C-c\C-m" 'mark-fortran-subprogram)
    (local-set-key "\e\C-h" 'undo)
    ;; (modify-syntax-entry ?\n "> 1" fortran-mode-syntax-table)
    ;; (modify-syntax-entry ?c  "w 2" fortran-mode-syntax-table)
    ;; (modify-syntax-entry ?*  "w 2" fortran-mode-syntax-table)
    ;; (modify-syntax-entry ?C  "w 2" fortran-mode-syntax-table)
    (setq indent-tabs-mode nil)
    )))

(add-hook
 'f90-mode-hook
 (function
  (lambda()
    (setq abbrev-mode t)
    (require 'cerca-tpu)
    (tpu-add-word-separators "()=+-*/,'")
    (cerca-hooks-ivd)
    (local-set-key "\C-c\C-m" 'mark-fortran-subprogram)
    (local-set-key "\e\C-h" 'undo)
    ;; (modify-syntax-entry ?\n "> 1" fortran-mode-syntax-table)
    ;; (modify-syntax-entry ?c  "w 2" fortran-mode-syntax-table)
    ;; (modify-syntax-entry ?*  "w 2" fortran-mode-syntax-table)
    ;; (modify-syntax-entry ?C  "w 2" fortran-mode-syntax-table)
    (setq indent-tabs-mode nil)
    )))

(add-hook
 'python-mode-hook
 (function
  (lambda()
    (setq abbrev-mode t)
    (require 'cerca-tpu)
    (tpu-add-word-separators "()=+-*/,'")
    (cerca-hooks-ivd)
    (setq indent-tabs-mode nil)
    )))

(add-hook
 'emacs-lisp-mode-hook
 (function
  (lambda()
    (setq abbrev-mode t)
    (require 'cerca-tpu)
    (tpu-add-word-separators "()-.'\"/")
    (cerca-hooks-ivd)
    (local-set-key "\C-c\C-b" 'eval-buffer)
    (local-set-key "\C-c\C-f" 'eval-defun)
    (local-set-key "\C-c\C-l" 'load-file)
    (local-set-key "\C-c\C-r" 'eval-region)
    (local-set-key "\C-c\t"   'byte-compile-file)
    (local-set-key "\C-c\C-m" 'byte-recompile-directory)
    (setq indent-tabs-mode nil)
    (imenu-add-menubar-index)
    )))

(add-hook
 'vu-mode-hook
 (function
  (lambda()
    (setq abbrev-mode t)
    )))

(add-hook
 'makefile-mode-hook
 (function
  (lambda()
    (local-set-key "$" 'self-insert-command)
    (cerca-hooks-ivd)
    )))

(add-hook
 'ksh-mode-hook
 (function
  (lambda()
    (interactive)
    (cerca-hooks-ivd)
    (setq indent-tabs-mode nil)
    )))

(add-hook
 'sh-mode-hook
 (function
  (lambda()
    (interactive)
    (cerca-hooks-ivd)
    (setq indent-tabs-mode nil)
    )))

(add-hook
 'awk-mode-hook
 (function
  (lambda()
    (interactive)
    (cerca-hooks-ivd)
    (setq indent-tabs-mode nil)
    (c-set-style c-site-default-style)
    (put 'awk-mode 'font-lock-defaults 'c-mode)
    )))

(add-hook
 'javascript-mode-hook
 (function
  (lambda()
    (interactive)
    (local-unset-key [(meta backspace)])
    (local-unset-key [(meta backward)])
    (local-unset-key [(meta delete)])
    (local-unset-key [(meta control h)])
    (cerca-hooks-ivd)
    )))

(add-hook
 'js-mode-hook
 (function
  (lambda()
    (interactive)
    ;;(local-unset-key [(meta backspace)])
    ;;(local-unset-key [(meta backward)])
    ;;(local-unset-key [(meta delete)])
    ;;(local-unset-key [(meta control h)])
    (cerca-hooks-ivd)
    )))

;;(add-hook
;; 'change-log-mode-hook
;; (function (lambda() (accents-iso 1))))

(add-hook
 'speedbar-load-hook  ; would be too late in antlr-mode.el
 (lambda () (speedbar-add-supported-extension ".g")))

(add-hook
 'compilation-mode-hook
 (lambda() (setq truncate-lines t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-hook
 'html-mode-hook
 (function
  (lambda()
    ;;(accents-iso 1)
    (setq abbrev-mode t)
    (auto-fill-mode 0)
    ;(require 'iso-sgml)
    (remove-hook 'find-file-hooks 'fix-sgml2iso)  ;; de iso-sgm.el
    (remove-hook 'write-file-hooks 'fix-iso2sgml) ;; de iso-sgm.el
    (remove-hook 'after-save-hook 'fix-sgml2iso)  ;; de iso-sgm.el
    (require 'cerca-html-fcts)
    (require 'cerca-tpu)
    (tpu-add-word-separators "<>=/,.`':&;")
    (local-set-key "\C-cd"    'cerca-html-insert-date-tmpl)
    (local-set-key "\C-cu"    'cerca-html-insert-url-tmpl)
    (local-set-key "\C-cv"    'cerca-html-insert-validate-tmpl)
    (local-set-key "\C-ca"    'cerca-html-anchor-word)
    (local-set-key "\C-cb"    'cerca-html-bold-word)
    (local-set-key "\C-ce"    'cerca-html-fontblue-word)
    (local-set-key "\C-cH"    'cerca-html-header1-word)
    (local-set-key "\C-ch"    'cerca-html-header2-word)
    (local-set-key "\C-ci"    'cerca-html-italicize-word)
    (local-set-key "\C-cl"    'cerca-html-fontlinethrough-word)
    (local-set-key "\C-ct"    'cerca-html-typewrite-word)
    (local-set-key "\C-ck"    'cerca-html-kill-construct)
    (local-set-key "\C-cr"    'cerca-html-fontred-word)
    (local-set-key "\C-cg"    'cerca-html-fontgreen-word)
    (local-set-key "\C-cf"    'cerca-html-font-word)
    (local-set-key "\C-cp"    'cerca-html-make-paragraph)
    (add-hook 'local-write-file-hooks 'cerca-html-mise-a-jour-date)
    (add-hook 'local-write-file-hooks 'cerca-html-mise-a-jour-url)
    ;;(add-hook 'after-save-file-hooks 'cerca-html-netscape-reload)
    ;;(add-hook 'after-save-file-hooks 'cerca-html-netscape-openFile)
    ;;(add-hook 'after-save-hook 'cerca-html-netscape-openURL)
    (if (featurep 'xemacs)
        (progn
          (add-menu-button '("Modify") ["Normalize fichier Word" cerca-html-normalize-microsoft t] "Normalize")
          ;;(add-menu-button '("HTML") ["View in W3m" w3m-preview-this-buffer t] "View in W3")
          (add-menu-button '("HTML") ["View in Lynx" sgml-html-lynx-file t] "View in W3")
          (add-menu-button '("HTML") ["View in Mosaic" sgml-html-mosaic-file t] "View in W3")
          ))
    )))

;; (defun w3m-preview-this-buffer ()
;;   "See what this buffer will look like when its formatted as HTML.
;; HTML is the HyperText Markup Language used by the World Wide Web to
;; specify formatting for text using w3m"
;;   (interactive)
;;   (w3m-region (point-min) (point-max)))

(defun sgml-html-lynx-file ()
  "Preview the file for the current buffer in Lynx."
  (interactive)
  (highlight-headers-follow-url-lynx
   (concat "file:" (buffer-file-name (current-buffer)))))

(defun sgml-html-mosaic-file ()
  "Preview the file for the current buffer in Mosaic."
  (interactive)
  (highlight-headers-follow-url-mosaic
   (concat "file:" (buffer-file-name (current-buffer)))))

(defun highlight-headers-follow-url-lynx (url)
  (message "Sending URL to Lynx...")
  (save-excursion
    (set-buffer (get-buffer-create "*Shell Command Output*"))
    (erase-buffer)
    (call-process "xterm" nil 0 nil "-e" "lynx" url))
  (message "Sending URL to Lynx... done"))

;; (defun highlight-headers-follow-url-w3m (url)
;;   (message "Sending URL to W3m...")
;;   (save-excursion
;;     (set-buffer (get-buffer-create "*Shell Command Output*"))
;;     (erase-buffer)
;;     (call-process "xterm" nil 0 nil "-e" "w3m" url))
;;   (message "Sending URL to W3m... done"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun cerca-perl-hook-init nil
  "Initialise le mode perl et cperl"
  (local-set-key [delete] 'delete-char)
  (cerca-hooks-ivd)
  ;;  (if (string-match "\\.cgi$" buffer-file-name)
  ;;      (progn
  ;;        (accents-iso 1)
  ;;        (setq abbrev-mode t)
  ;;        (require 'iso-sgml)
  ;;        (if (not (member major-mode isosgml-modes-list))
  ;;            (setq isosgml-modes-list (append isosgml-modes-list '(perl-mode))))))
  )

(add-hook 'perl-mode-hook 'cerca-perl-hook-init)
(add-hook 'cperl-mode-hook 'cerca-perl-hook-init)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-hook
 'term-setup-hook
 (function
  (lambda()
    (let ((term (or (getenv "TERM") "")))
      ;; mettre le keypad en application mode
      (if (or (string-match "vt100" term)
              (string-match "vt220" term))
          (send-string-to-terminal "="))
      ;;  le function-key-map
      (if (string-match "vt220" term)
          (progn
            (define-key function-key-map "\eOP" [kp-f1       ])
            (define-key function-key-map "\eOQ" [kp-f2       ])
            (define-key function-key-map "\eOR" [kp-f3       ])
            (define-key function-key-map "\eOS" [kp-f4       ])
            (define-key function-key-map "\eOm" [kp-vt-subtract ]) ;; `-'
            (define-key function-key-map "\eOl" [kp-separator]) ;; `,'
            (define-key function-key-map "\eOM" [kp-enter    ])
            (define-key function-key-map "\eOn" [kp-decimal  ]) ;; `.'
            (define-key function-key-map "\eOp" [kp-0        ])
            (define-key function-key-map "\eOq" [kp-1        ])
            (define-key function-key-map "\eOr" [kp-2        ])
            (define-key function-key-map "\eOs" [kp-3        ])
            (define-key function-key-map "\eOt" [kp-4        ])
            (define-key function-key-map "\eOu" [kp-5        ])
            (define-key function-key-map "\eOv" [kp-6        ])
            (define-key function-key-map "\eOw" [kp-7        ])
            (define-key function-key-map "\eOx" [kp-8        ])
            (define-key function-key-map "\eOy" [kp-9        ])
            ))
      ;;(define-key function-key-map "\e[011q" [  f11])
      ;;(define-key function-key-map "\e[012q" [  f12])
      ;;(define-key function-key-map "\e[013q" [S-f1 ])
      ;;(define-key function-key-map "\e[014q" [S-f2 ])
      ;;(define-key function-key-map "\e[015q" [S-f3 ])
      ;;(define-key function-key-map "\e[016q" [S-f4 ])
      ;;(define-key function-key-map "\e[017q" [S-f5 ])
      ;;(define-key function-key-map "\e[018q" [S-f6 ])
      ;;(define-key function-key-map "\e[019q" [S-f7 ])
      ;;(define-key function-key-map "\e[020q" [S-f8 ])
      ;;(define-key function-key-map "\e[021q" [S-f9 ])
      ;;(define-key function-key-map "\e[022q" [S-f10])
      ;;(define-key function-key-map "\e[023q" [S-f11])
      ;;(define-key function-key-map "\e[024q" [S-f12])
      ;;(define-key function-key-map "\e[025q" [C-f1 ])
      ;;(define-key function-key-map "\e[026q" [C-f2 ])
      ;;(define-key function-key-map "\e[027q" [C-f3 ])
      ;;(define-key function-key-map "\e[028q" [C-f4 ])
      ;;(define-key function-key-map "\e[029q" [C-f5 ])
      ;;(define-key function-key-map "\e[030q" [C-f6 ])
      ;;(define-key function-key-map "\e[031q" [C-f7 ])
      ;;(define-key function-key-map "\e[032q" [C-f8 ])
      ;;(define-key function-key-map "\e[033q" [C-f9 ])
      ;;(define-key function-key-map "\e[034q" [C-f10])
      ;;(define-key function-key-map "\e[035q" [C-f11])
      ;;(define-key function-key-map "\e[036q" [C-f12])
      ))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;(add-hook
; 'server-switch-hook
; (function
;  (lambda()
;    (make-frame-visible (selected-frame)) ; always raise?
;    (server-done)
;    )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(if (string-match "^fr" (or (getenv "LC_TIME") ""))
    (add-hook
     'display-time-hook
     (function
      (lambda()
        (if display-time-day-and-date
            (let* ((now (current-time))
                   (time (current-time-string now))
                   (dayname (cdr (assoc (substring time 0 3)
                                        '(("Sun" . "dim") ("Mon" . "lun") ("Tue" . "mar") ("Wed" . "mer")
                                          ("Thu" . "jeu") ("Fri" . "ven") ("Sat" . "sam")))))
                   (monthname (cdr (assoc (substring time 4 7)
                                          '(("Jan" . "jan") ("Feb" . "f�v") ("Mar" . "mar")
                                            ("Apr" . "avr") ("May" . "mai") ("Jun" . "jun")
                                            ("Jul" . "jui") ("Aug" . "ao�") ("Sep" . "sep")
                                            ("Oct" . "oct") ("Nov" . "nov") ("Dec" . "d�c")))))
                   (day (substring time 8 10))
                   )
              (setq display-time-string
                    (append (list (concat dayname " " day " " monthname " ")) (cdr display-time-string)))
              ))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-hook
 'find-file-hooks
 (function
  (lambda()
    ;; (if (not (eq major-mode 'latex-mode))
    (if (featurep 'xemacs)
        (fume-add-menubar-entry))
    ;; )
    )))

(add-hook
 'org-mode-hook
 (function
  (lambda()
    (local-unset-key [(meta up)])    ;; laisser 'scroll-up-in-place
    (local-unset-key [(meta down)])  ;; laisser 'scroll-down-in-place
    (local-unset-key [(meta left)])  ;; laisser 'backward-word
    (local-unset-key [(meta right)]) ;; laisser 'forward-word
    (local-set-key [(control meta up)] 'org-metaup)
    (local-set-key [(control meta down)] 'org-metadown)
    (local-set-key [(control meta left)] 'org-metaleft)
    (local-set-key [(control meta right)] 'org-metaright)
    )))

