;;; Fonctions diverses pour le mode HTML.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

(defvar cerca-html-home "www" "*Le nom du serveur utilise pour envoyer le URL a Netscape.")

(setq sgml2iso-trans-tab
  '(
    ("&AElig\;"  "�")
    ("&Aacute\;"  "�")
    ("&Acirc\;"  "�")
    ("&Agrave\;"  "�")
    ("&Atilde\;"  "�")
    ("&Ccedil\;"  "�")
    ("&Eacute\;"  "�")
    ("&Egrave\;"  "�")
    ("&Euml\;"  "�")
    ("&Iacute\;"  "�")
    ("&Icirc\;"  "�")
    ("&Igrave\;"  "�")
    ("&Iuml\;"  "�")
    ("&Ntilde\;"  "�")
    ("&Oacute\;"  "�")
    ("&Ocirc\;"  "�")
    ("&Ograve\;"  "�")
    ("&Oslash\;"  "�")
    ("&Uacute\;"  "�")
    ("&Ugrave\;"  "�")
    ("&Yacute\;"  "�")
    ("&aacute\;"  "�")
    ("&acirc\;"  "�")
    ("&acute\;" "�")
    ("&aelig\;"  "�")
    ("&agrave\;"  "�")
    ("&aring\;"  "�")
    ("&atilde\;"  "�")
    ("&brvbar\;" "�")
    ("&ccedil\;"  "�")
    ("&cedil\;" "�")
    ("&cent\;" "�")
    ("&copy\;" "�")
    ("&curren\;" "�")
    ("&deg\;" "�")
    ("&eacute\;"  "�")
    ("&ecirc\;"  "�")
    ("&egrave\;"  "�")
    ("&euml\;"  "�")
    ("&frac12\;" "�")
    ("&frac14\;" "�")
    ("&frac34\;" "�")
    ("&iacute\;"  "�")
    ("&icirc\;"  "�")
    ("&igrave\;"  "�")
    ("&iexcl\;" "�")
    ("&iquest\;" "�")
    ("&iuml\;"  "�")
    ("&laquo\;" "�")
    ("&macr\;" "�")
    ("&micro\;" "�")
    ("&middot\;" "�")
    ("&nbsp\;" "�")
    ("&not\;" "�")
    ("&ntilde\;"  "�")
    ("&oacute\;"  "�")
    ("&ocirc\;"  "�")
    ("&ograve\;"  "�")
    ("&ordf\;" "�")
    ("&ordm\;" "�")
    ("&oslash\;"  "�")
    ("&otilde\;"  "�")
    ("&para\;" "�")
    ("&pound\;" "�")
    ("&plusmn\;" "�")
    ("&raquo\;" "�")
    ("&reg\;" "�")
    ("&sect\;" "�")
    ("&shy\;" "�")
    ("&sup1\;" "�")
    ("&sup2\;" "�")
    ("&sup2\;" "�")
    ("&uacute\;"  "�")
    ("&ucirc\;"  "�")
    ("&ugrave\;"  "�")
    ("&uml\;" "�")
    ("&yacute\;"  "�")
    ("&yen\;" "�")
    ("&Auml\;"  "�")
    ("&auml\;"  "�")
    ("&Ouml\;"  "�")
    ("&ouml\;"  "�")
    ("&Uuml\;"  "�")
    ("&uuml\;"  "�")
    ("&szlig\;"  "�")
    ("&sect\;"  "�")
    ("&para\;"  "�")
    ("&copy\;"  "�")
    ("&iexcl\;"  "�")
    ("&iquest\;"  "�")
    ("&cent\;"  "�")
    ("&pound\;"  "�")
    ("&times\;"  "�")
    ("&plusmn\;"  "�")
    ("&divide\;"  "�")
    ("&not\;"  "�")
    ("&mu\;"  "�")
    ("&Ae\;"  "�")
    ("&ae\;"  "�")
    ("&Oe\;"  "�")
    ("&oe\;"  "�")
    ("&Ue\;"  "�")
    ("&ue\;"  "�")
    ("&sz\;"  "�")
   ))

(setq iso2sgml-trans-tab
      (mapcar (function (lambda (entity-char) ; (ENTITY CHAR)
                          ;; Return (CHAR ENTITY)
                          (list (car (cdr entity-char))
                                (car entity-char))))
              sgml2iso-trans-tab))

;(defun fix-sgml2iso ()
;  "Replace SGML entity references with ISO 8859-1 (aka Latin-1) characters."
;  (interactive)
;  (if (member major-mode isosgml-modes-list)
;      (let ((buffer-modified-p (buffer-modified-p)))
;        (unwind-protect
;            (isosgml-translate-conventions sgml2iso-trans-tab)
;          (set-buffer-modified-p buffer-modified-p))
;        (font-lock-mode 1)
;        )))

;(defun fix-iso2sgml ()
;  "Replace ISO 8859-1 (aka Latin-1) characters with SGML entity references."
;  (interactive)
;  (if (member major-mode isosgml-modes-list)
;      (let ((buffer-modified-p (buffer-modified-p)))
;        (font-lock-mode 0)
;        (unwind-protect
;            (isosgml-translate-conventions iso2sgml-trans-tab)
;          (set-buffer-modified-p buffer-modified-p))
;        )))

;(defun isosgml-translate-conventions (trans-tab)
;  "Use the translation table argument to translate the current buffer."
;  (save-excursion
;    (let ((beg (point-min-marker))    ; see the `(elisp)Narrowing' Info node
;	  (end (point-max-marker))
;          (font-lock-fontified-p font-lock-fontified))
;      (unwind-protect
;	  (progn
;	    (widen)
;	    (goto-char (point-min))
;            (font-lock-mode 0)
;	    (let ((buffer-read-only nil) ; (inhibit-read-only t)?
;		  (case-fold-search nil))
;	      (while trans-tab
;		(save-excursion
;		  (let ((trans-this (car trans-tab)))
;		    (while (search-forward (car trans-this) nil t)
;		      (replace-match (car (cdr trans-this)) t t)))
;		  (setq trans-tab (cdr trans-tab))))))
;	(progn
;          (narrow-to-region beg end)
;          (if font-lock-fontified-p (font-lock-mode 1)))
;        ))))

;;; fonctions pour la mise a jour de la date

(defun cerca-html-normalize-microsoft ()
  (interactive)
  "Normalize un document produit par Microsoft Word."
  (save-excursion
    (let ((case-fold-search nil)
          (case-replace t))
      (iso-microsoft)
      (goto-char (point-min)) (replace-regexp "style='[^']+'" "")
      (goto-char (point-min)) (replace-regexp "style=\"[^\"]+\"" "")
      ;;(goto-char (point-min)) (sgml-normalize "html")
      (goto-char (point-min)) (replace-string "<span>" "")
      (goto-char (point-min)) (replace-string "</span>" "")
      (goto-char (point-min)) (replace-regexp "> *<" "><")
      (goto-char (point-min)) (replace-string "<i></i>" "")
      (goto-char (point-min)) (replace-string "<b></b>" "")
      (goto-char (point-min)) (replace-string "<p></p>" "")
      (goto-char (point-min)) (replace-regexp "\n\n+" "\n\n")
      (goto-char (point-min))
      )))

(defun cerca-html-insert-date-tmpl ()
  (interactive)
  "Insert un template de `mise a jour... (date)'."
  (insert
   (concat
    html-helper-timestamp-start
    "Mis � jour le <!--MAJ-->jj mmm aaaa, hh:mm GMT<!--MAJ--><BR>\nLast updated on: <!--UPD-->mmm dd, yyyy, hh:mm GMT<!--UPD--><BR>\n"
    html-helper-timestamp-end "\n")))

(defun cerca-html-helper-insert-timestamp ()
  "Timestamp insertion function (between `html-helper-timestamp-start' and `html-helper-timestamp-end')."
  (insert "Mis � jour le : " (cerca-html-maj-date) "<BR>\n"))

(defun cerca-html-maj-date ()
  "Returns the current date in the format \"day-month-year\"."
  (let* ((time-string (current-time-string))
         (hour (substring time-string 11 13))
         (min (substring time-string 14 16))
         (day (substring time-string 8 10))
         (month (substring time-string 4 7))
         (mois (cond
                ((string-equal month "Jan") "janvier")
                ((string-equal month "Feb") "f�vrier")
                ((string-equal month "Mar") "mars")
                ((string-equal month "Apr") "avril")
                ((string-equal month "May") "mai")
                ((string-equal month "Jun") "juin")
                ((string-equal month "Jul") "juillet")
                ((string-equal month "Aug") "ao�t")
                ((string-equal month "Sep") "septembre")
                ((string-equal month "Oct") "octobre")
                ((string-equal month "Nov") "novembre")
                ((string-equal month "Dec") "d�cembre")))
         (year (substring time-string 20 24)))
    (concat day " " mois " " year ", " hour "h" min " " (nth 1 (current-time-zone)))))

(defun cerca-html-upd-date ()
  "Returns the current date in the format \"day-month-year\"."
  (let* ((time-string (current-time-string))
         (hour (substring time-string 11 13))
         (min (substring time-string 14 16))
         (day (substring time-string 8 10))
         (month (substring time-string 4 7))
         (mois (cond
                ((string-equal month "Jan") "January")
                ((string-equal month "Feb") "February")
                ((string-equal month "Mar") "March")
                ((string-equal month "Apr") "April")
                ((string-equal month "May") "May")
                ((string-equal month "Jun") "June")
                ((string-equal month "Jul") "July")
                ((string-equal month "Aug") "August")
                ((string-equal month "Sep") "September")
                ((string-equal month "Oct") "October")
                ((string-equal month "Nov") "November")
                ((string-equal month "Dec") "December")))
         (year (substring time-string 20 24)))
    (concat mois " " day ", " year ", " hour ":" min " " (nth 1 (current-time-zone)))))

(defun cerca-html-mise-a-jour-date ()
  "The function sets the date between the comment pair <!--MAJ-->."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (let ((case-fold-search t))
      (goto-char (point-min))
      (if (re-search-forward "\\(<!--MAJ-->\\)\\(.*\\)\\(<!--MAJ-->\\)" (point-max) t)
	  (progn
	    (delete-region (match-beginning 2) (match-end 2))
	    (goto-char (match-beginning 2))
	    (insert (cerca-html-maj-date))))
      (goto-char (point-min))
      (if (re-search-forward "\\(<!--UPD-->\\)\\(.*\\)\\(<!--UPD-->\\)" (point-max) t)
	  (progn
	    (delete-region (match-beginning 2) (match-end 2))
	    (goto-char (match-beginning 2))
	    (insert (cerca-html-upd-date))))
      )))

;;; fonctions pour la mise a jour du nom de fichier

(defun cerca-html-get-url-name ()
  "Obtains the URL name of the current file."
  (let ((nom buffer-file-truename))
    (setq nom (replace-in-string nom ".*/\\([a-z]*\\)/HTML/vis/" "/vis/"))
    (setq nom (replace-in-string nom ".*/\\([a-z]*\\)/HTML/" "/~\\1/"))
    (setq nom (replace-in-string nom "^/tmp_mnt/" "/"))
    (setq nom (replace-in-string nom "^/var/www/docs/" "/"))
    (setq nom (replace-in-string nom "^/var/www/html/" "/"))
    (setq nom (replace-in-string nom "^/var/www/projets/" "/"))
    (setq nom (replace-in-string nom "^/net/WWW/docs/" "/"))
    (setq nom (replace-in-string nom "^/net/WWW/projets/" "/"))
    ))

(defun cerca-html-insert-url-tmpl ()
  (interactive)
  "Insert un template `URL : http://.../nom.html'."
  (insert (concat
           "URL : http://" (replace-in-string (system-name) "^[a-zA-Z]*\\." "www.")
           "<!--URL-->" (cerca-html-get-url-name) "<!--URL--><BR>\n")))

(defun cerca-html-insert-validate-tmpl ()
  (interactive)
  "Insert un validate ` : http://.../nom.html'."
  (insert "<p><a href=\"http://validator.w3.org/check/referer\"><img border=\"0\" src=\"http://www.w3.org/Icons/valid-html401\" alt=\"Valid HTML 4.01!\" height=\"31\" width=\"88\"></a></p>\n"))
;;  (insert (concat 
;;           "<A HREF=\"http://validator.w3.org/check?uri=http://" 
;;           (replace-in-string (system-name) "^[a-zA-Z]*\\." "www.")
;;           (cerca-html-get-url-name) "\">(validate)</a><BR>\n")))

(defun cerca-html-mise-a-jour-url ()
  "The function sets the URL name between the comment pair <!--URL-->."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (let ((case-fold-search t))
      (goto-char (point-min))
      (if (re-search-forward "<!--URL--.*>\\(.*\\)<!--URL-->" (point-max) t)
          (progn
            (delete-region (match-beginning 1) (match-end 1))
	    (goto-char (match-beginning 1))
	    (insert (cerca-html-get-url-name))))
      )))

;;; fonctions pour l'envoi a Netscape

(defun cerca-html-netscape-reload ()
  "Updates this web page in netscape using \"reload\"."
  (interactive)
  (if (eq major-mode 'html-mode)
      (shell-command (concat "netscape -noraise -remote 'reload'")))
  nil
  )

(defun cerca-html-netscape-openFile ()
  "Updates this web page in netscape using \"netscape -noraise -remote openFile()\"."
  (interactive)
  (if (eq major-mode 'html-mode)
      (shell-command (concat "netscape -noraise -remote 'openFile(" buffer-file-name ")'")))
  nil
  )

(defun cerca-html-netscape-openURL ()
  "Updates this web page in netscape using \"netscape -noraise -remote openURL()\"."
  (interactive)
  (if (eq major-mode 'html-mode)
      (shell-command (concat "netscape -noraise -remote 'openURL(http://" cerca-html-home "/" (cerca-html-get-url-name) ")' -remote 'reload'")))
  nil
  )

(defconst html-open-parentheses "<"
  "Open parenthesis characters for html.")

(defconst html-close-parentheses ">"
  "Close parenthesis characters for html.  These should match up with
open-parenthesis.")

(defun cerca-html-envelop-word (string1 string2 count &optional surround &optional endpos)
  "Surround current word with html construct {\\STRING ...}.  COUNT
specifies how many words to surround.  A negative count means to skip
backward."
  (let ((spos (point))
        (epos (point))
        (ccoun 0))
    (if (not (zerop count))
        (progn (if (= (char-syntax (preceding-char)) ?w)
                   (forward-sexp (min -1 count)))
               (setq spos (point))
               (if (looking-at (concat "{\\w" html-open-parentheses ">"))
                   (forward-char 2)
                 (goto-char epos)
                 (skip-chars-backward "\\W")
                 (forward-char -1))
               (forward-sexp (max count 1))
               (setq epos (point))))
    (goto-char spos)
    (while (and (< ccoun (length html-open-parentheses))
                (save-excursion
                  (or (search-forward (char-to-string
                                       (aref html-open-parentheses ccoun))
                                      epos t)
                      (search-forward (char-to-string
                                       (aref html-close-parentheses ccoun))
                                      epos t)))
                (setq ccoun (1+ ccoun))))
    (if (>= ccoun (length html-open-parentheses))
        (progn (goto-char epos)
               (insert "@end(" string1 ")")
               (goto-char spos)
               (insert "@begin(" string2 ")"))
      (if surround
          (progn
            (goto-char epos)
            (insert string2)))
      (goto-char spos)
      (insert string1)
      (if endpos
          (goto-char (+ spos endpos))
        (forward-sexp 1))
      )))

(defun cerca-html-kill-construct nil
  "Kills html construct <X> ... </X>"
  (interactive)
  (save-excursion
    (re-search-backward "<.*>")
    (goto-char (match-beginning 0))
    (delete-region (match-beginning 0) (match-end 0))
    (re-search-forward "</.*>")
    (delete-region (match-beginning 0) (match-end 0))
    ))

(defun cerca-html-anchor-word (count)
  "Anchor COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word "<a href=\"\">" "</a>" count t 9))

(defun cerca-html-bold-word (count)
  "Boldface COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word "<b>" "</b>" count t))

(defun cerca-html-italicize-word (count)
  "Italicize COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word "<i>" "</i>" count t))

(defun cerca-html-typewrite-word (count)
  "typewrite COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word "<tt>" "</tt>" count t))

(defun cerca-html-header1-word (count)
  "H1 COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word "<h1>" "</h1>" count t))

(defun cerca-html-header2-word (count)
  "H2 COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word "<h2>" "</h2>" count t))

(defun cerca-html-fontlinethrough-word (count)
  "font color=red COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word "<font style=\"text-decoration:line-through;\">" "</font>" count t))

(defun cerca-html-fontred-word (count)
  "font color=red COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word "<font color=\"red\">" "</font>" count t))

(defun cerca-html-fontgreen-word (count)
  "font color=green COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word "<font color=\"green\">" "</font>" count t))

(defun cerca-html-fontblue-word (count)
  "font color=blue COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word "<font color=\"blue\">" "</font>" count t))

(defun cerca-html-font-word (count &optional color)
  "font color=... COUNT words around point by means of html constructs."
  (interactive "p")
  (cerca-html-envelop-word (concat "<font color=\"" color "\">") "</font>" count t))

(defun cerca-html-make-paragraph nil
  (interactive)
  (mark-paragraph)
  (goto-char (+ (region-beginning) 1))
  (insert "<p>")
  (goto-char (- (region-end) 1))
  (insert "</p>"))



(defun html-bracket-region-be (env min max)
  (interactive "sEnvironment: \nr")
  (save-excursion
    (goto-char max)
    (insert "@end(" env ")\n")
    (goto-char min)
    (insert "@begin(" env ")\n")))

(defun html-insert-environment (env)
  (interactive "sEnvironment: ")
  (html-bracket-region-be env (point) (point))
  (forward-line 1)
  (insert ?\n)
  (forward-char -1))

(defvar html-look-for-section-regexp
   "^\\\\\\(chapter\\|\\(sub\\)*section\\)+")
(defun html-look-for-section (&optional arg)
  "Cherche le prochain chapter, section ou subsection.  Si un argument est
donne, ne pas initialise la direction de recherche."
  (interactive)
  (if (not arg) (setq searching-forward (if tpu-advance t nil)))
  (cond (searching-forward
         (re-search-forward html-look-for-section-regexp))
        (t
         (re-search-backward html-look-for-section-regexp))
        ))

(provide 'cerca-html-fcts)
