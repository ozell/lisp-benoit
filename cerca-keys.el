;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.
;;;
;; Make F6 be `save-file' followed by `delete-window'.
;; (global-set-key 'f6 "\C-x\C-s\C-x0")

(defvar config-saveur 'nouveau-gout "Saveur selon laquelle les PFs sont definies ('cerca ou 'nouveau-gout ou 'nouveau-gout-sans-keypad ou 'benoit).")
(defvar cles-pfs (if (string-match "vt220" (or (getenv "TERM") "xterm")) 'vt 'x)
  "*Cles PF comme 'x ou 'vt.")

;; --------------------
;;  les keymaps
;; --------------------
(defvar GOLD-map (make-keymap) "Les fonctions du GOLD map!")

;; --------------------
;;  help keymap
;; --------------------
(let ((help-map (lookup-key global-map "\C-h")))
  (if (keymapp help-map)
      (progn
        ;;(define-key global-map "\C-d" help-map)
        (define-key help-map "\C-b"  'cerca-show-keys)
        (define-key help-map "\C-c"  'describe-buffer-case-table)
        (define-key help-map "\C-d"  'describe-current-display-table)
        (define-key help-map "\C-l"  'list-command-history)
        (define-key help-map "\C-m"  'manual-entry)
        (define-key help-map "\C-t"  'tags-apropos)
        (define-key help-map "\C-v"  'set-variable)
        (define-key help-map "\C-w"  'webster-www-dictionary)
        (define-key help-map "\C-x"  'calendar)
        (define-key help-map "x"     'diary)
        (define-key help-map "\C-z"  'see-chars)
        )))

;; --------------------
;;  les fleches
;; --------------------
(define-key global-map [(meta up)]       'scroll-down-in-place)
(define-key global-map [(meta down)]     'scroll-up-in-place)
(define-key global-map [(meta right)]    'forward-word)
(define-key global-map [(meta left)]     'backward-word)
(define-key global-map [(super up)]       'scroll-down-in-place)
(define-key global-map [(super down)]     'scroll-up-in-place)
(define-key global-map [(super right)]    'forward-word)
(define-key global-map [(super left)]     'backward-word)
(define-key global-map [(control up)]    'beginning-of-buffer)
(define-key global-map [(control down)]  'end-of-buffer)
(define-key global-map [(control right)] 'end-of-line)
(define-key global-map [(control left)]  'beginning-of-line)
;; (define-key global-map [(meta control right)] 'unpop-window-configuration)
;; (define-key global-map [(meta control left)]  'pop-window-configuration)
;; (define-key global-map [(meta control up)]    'delete-other-windows)
;; (define-key global-map [(meta control down)]  'delete-other-windows)
(define-key GOLD-map [(up)]    'beginning-of-buffer)
(define-key GOLD-map [(down)]  'end-of-buffer)
(define-key GOLD-map [(right)] 'end-of-line)
(define-key GOLD-map [(left)]  'beginning-of-line)
(global-set-key [(control +)] 'text-scale-increase)
(global-set-key [(control =)] 'text-scale-increase)
(global-set-key [(control -)] 'text-scale-decrease)

;; --------------------
;;  6 pack
;; --------------------
(define-key global-map                  [(delete)] 'delete-char)
(if (boundp 'c-mode-map)
    (define-key c-mode-map              [(delete)] 'delete-char))
(define-key lisp-mode-map               [(delete)] 'delete-char)
(define-key emacs-lisp-mode-map         [(delete)] 'delete-char)
(define-key lisp-interaction-mode-map   [(delete)] 'delete-char)
(define-key global-map                  [(deletechar)] 'delete-char)
(if (boundp 'c-mode-map)
    (define-key c-mode-map              [(deletechar)] 'delete-char))
(define-key lisp-mode-map               [(deletechar)] 'delete-char)
(define-key emacs-lisp-mode-map         [(deletechar)] 'delete-char)
(define-key lisp-interaction-mode-map   [(deletechar)] 'delete-char)
(define-key global-map [(prior)]           'scroll-down-onethird)
(define-key global-map [(next)]            'scroll-up-onethird)
(define-key global-map [(shift prior)]    'scroll-down-onethird)
(define-key global-map [(shift next)]     'scroll-up-onethird)
(define-key global-map [(control prior)]  'scroll-down)
(define-key global-map [(control next)]   'scroll-up)
(define-key global-map [(meta prior)]     'enlarge-window)
(define-key global-map [(meta next)]      'shrink-window)
(define-key global-map [(super prior)]     'enlarge-window)
(define-key global-map [(super next)]      'shrink-window)
(define-key global-map [(control home)]   'beginning-of-buffer)
(define-key global-map [(control end)]    'end-of-buffer)
;;(define-key global-map [(meta home)]      'scroll-right)
;;(define-key global-map [(meta end)]       'scroll-left)
(define-key global-map [(insert)]         'overwrite-mode)
(define-key global-map [(shift insert)]   'overwrite-mode)
(define-key global-map [(control insert)] 'overwrite-mode)
;;(define-key global-map [(meta insert)]    'x-insert-selection)
(define-key global-map [(meta insert)]    'x-clipboard-yank)

(define-key GOLD-map [(find)]         'tpu-substitute)
(define-key GOLD-map [(insert)]       'make-frame)
(define-key GOLD-map [(insertchar)]   'make-frame)
(define-key GOLD-map [(delete)]       'delete-frame)
(define-key GOLD-map [(deletechar)]   'delete-frame)
(define-key GOLD-map [(select)]       'tpu-append-region)
(define-key GOLD-map [(prior)]        'tpu-previous-window)
(define-key GOLD-map [(next)]         'tpu-next-window)
(define-key GOLD-map [(home)]         'back-to-indentation)
(define-key GOLD-map [(end)]          'end-of-line)

;; --------------------
;;  3 pack
;; --------------------
(define-key global-map [(print)]          GOLD-map)
(define-key global-map [(prsc)]           GOLD-map)
;;(define-key global-map [(scroll)]         GOLD-map)
;;(define-key global-map [(scroll_lock)]    GOLD-map)
;;(define-key global-map [(scroll-lock)]    GOLD-map)
(define-key global-map [(pause)]          GOLD-map)
;; (define-key global-map [(shift print)]    'ps-print-region-with-faces)
;; (define-key global-map [(shift prsc)]     'ps-print-region-with-faces)
(define-key global-map [(shift cancel)]   'ps-spool-region-with-faces)
(define-key global-map [(shift scroll)]   'ps-spool-region-with-faces)
(define-key global-map [(shift scroll_lock)] 'ps-spool-region-with-faces)
(define-key global-map [(shift scroll-lock)] 'ps-spool-region-with-faces)
(define-key global-map [(shift pause)]    'next-error)
;; (define-key global-map [(control print)]  'ps-print-buffer-with-faces)
;; (define-key global-map [(control prsc)]   'ps-print-buffer-with-faces)
(define-key global-map [(control cancel)] 'ps-spool-buffer-with-faces)
(define-key global-map [(control scroll)] 'ps-spool-buffer-with-faces)
(define-key global-map [(control scroll_lock)] 'ps-spool-buffer-with-faces)
(define-key global-map [(control scroll-lock)] 'ps-spool-buffer-with-faces)
(define-key global-map [(control pause)]  'execute-extended-command)
(define-key global-map [(meta print)]     'ps-despool)
(define-key global-map [(meta prsc)]      'ps-despool)
(define-key global-map [(meta cancel)]    'ps-despool)
(define-key global-map [(meta scroll)]    'ps-despool)
(define-key global-map [(meta scroll_lock)]    'ps-despool)
(define-key global-map [(meta scroll-lock)]    'ps-despool)
(define-key global-map [(meta pause)]     nil)
(define-key GOLD-map   [(print)]          'universal-argument)
(define-key GOLD-map   [(prsc)]           'universal-argument)
(define-key GOLD-map   [(cancel)]         'undo)
;;(define-key GOLD-map   [(scroll)]         'undo)
;;(define-key GOLD-map   [(scroll_lock)]    'undo)
;;(define-key GOLD-map   [(scroll-lock)]    'undo)
(define-key GOLD-map   [(pause)]          'universal-argument)

;; --------------------
;;  numeric keypad
;; --------------------

(cond
 ((equal config-saveur 'nouveau-gout-sans-keypad)
  )
 (t
  (mapcar
   '(lambda (liste) nil
      (let ((fonction-base (nth 0 liste))
            (fonction-gold (nth 1 liste)))
        (if (featurep 'xemacs)
            (progn
              (define-key view-minor-mode-map (nth 2 liste) fonction-base)
              (define-key view-minor-mode-map (nth 3 liste) fonction-base)
              ))
        (define-key global-map (nth 2 liste) fonction-base)
        (define-key global-map (nth 3 liste) fonction-base)
        (define-key GOLD-map   (nth 2 liste) fonction-gold)
        (define-key GOLD-map   (nth 3 liste) fonction-gold)
        ))
   ;;base                     shift                    2                3
   '(
     (nil                     universal-argument       [kp-f1         ] [kp-f1       ])
     (delete-this-line        undelete-lines           [kp-divide     ] [kp-f2       ])
     (delete-current-word     undelete-words           [kp-multiply   ] [kp-f3       ])
     (delete-current-char     undelete-char            [kp-subtract   ] [kp-f4       ])
     (downcase-word           capitalize-word          [kp-add        ] [kp-separator])
     (upcase-word             tpu-change-case          [kp-vt-subtract] [kp-vt-subtract])
     (tpu-search-again        transpose-chars          [kp-enter      ] [kp-enter      ])
     (accents-iso             accents-get-from-tex     [kp-delete     ] [kp-decimal  ])
     (tpu-line                open-new-line            [kp-insert     ] [kp-0        ])
     (tpu-word                tpu-paragraph            [kp-end        ] [kp-1        ])
     (scroll-down-one         "\eq"                    [kp-down       ] [kp-2        ])
     (scroll-up-one           quoted-insert            [kp-next       ] [kp-3        ])
     (advance-direction       beginning-of-window      [kp-left       ] [kp-4        ])
     (backup-direction        end-of-window            [kp-begin      ] [kp-5        ])
     (point-to-register       jump-to-register         [kp-right      ] [kp-6        ])
     (tpu-sentence            execute-extended-command [kp-home       ] [kp-7        ])
     (delete-to-eol           undelete-lines           [kp-up         ] [kp-8        ])
     (picture-duplicate-line  undelete-lines           [kp-prior      ] [kp-9        ])
     ))
  (if (featurep 'xemacs)
      (define-key view-minor-mode-map [kp-f1] GOLD-map))
  (define-key global-map [kp-f1] GOLD-map)
  ))
(define-key global-map [(control kp_down)] 'scroll-down-one-other-window)
(define-key global-map [(control kp_2)]    'scroll-down-one-other-window)
(define-key global-map [(control kp_up)]   'scroll-up-one-other-window)
(define-key global-map [(control kp_3)]    'scroll-up-one-other-window)

;; --------------------
;;  function keys
;; --------------------

(if (string-match "darwin" system-configuration)
    (define-key global-map [(meta f1)] GOLD-map)
  )

(cond
 ((equal cles-pfs 'x)
  (defun cerca-map-keys (liste)
    (let ((fonction-base  (nth 0 liste))
          (fonction-shift (nth 1 liste))
          (fonction-ctrl  (nth 2 liste))
          (fonction-gold  (nth 3 liste)))
      (define-key global-map (nth 4 liste) fonction-base)  ;; f1
      (define-key global-map (nth 5 liste) fonction-shift) ;; shift f1
      (define-key global-map (nth 6 liste) fonction-ctrl)  ;; ctrl f1
      (define-key GOLD-map   (nth 4 liste) fonction-gold)  ;; gold f1
      ))
  (cond
   ((equal config-saveur 'benoit)
    (mapcar
     'cerca-map-keys
     ;;base                     shift                      ctrl                    gold                          4     5           6
     '(
       (next-file-buffer        cerca-show-keys            next-buffer             next-buffer                   [(f1 )] [(shift f1 )] [(control f1 )])
       (find-file               insert-file                find-file-at-point      find-file-at-point            [(f2 )] [(shift f2 )] [(control f2 )])
       (save-buffer             revert-buffer              save-some-buffers       set-buffer-file-coding-system [(f3 )] [(shift f3 )] [(control f3 )])
       (kill-buffer             speedbar                   delete-frame            delete-frame                  [(f4 )] [(shift f4 )] [(control f4 )])
       (point-to-register       point-to-register          kill-rectangle          kill-rectangle                [(f5 )] [(shift f5 )] [(control f5 )])
       (jump-to-register        jump-to-register           string-rectangle        string-rectangle              [(f6 )] [(shift f6 )] [(control f6 )])
       (copy-to-register        open-rectangle             copy-rectangle-to-register copy-rectangle-to-register [(f7 )] [(shift f7 )] [(control f7 )])
       (yank                    insert-register            yank-rectangle          yank-rectangle                [(f8 )] [(shift f8 )] [(control f8 )])
       (query-replace           replace-string             search-forward          search-forward                [(f9 )] [(shift f9 )] [(control f9 )])
       (query-replace-regexp    replace-regexp             search-forward-regexp   search-forward-regexp         [(f10)] [(shift f10)] [(control f10)])
       (next-error              add-change-log-entry-other-window set-selective-display set-selective-display    [(f11)] [(shift f11)] [(control f11)])
       (compile                 repeat-complex-command     indent-region           indent-region                 [(f12)] [(shift f12)] [(control f12)])
       )))
   ((or (equal config-saveur 'nouveau-gout) (equal config-saveur 'nouveau-gout-sans-keypad))
    (mapcar
     'cerca-map-keys
     ;;base                     shift                      ctrl                    gold                          4     5           6
     '(
       (next-file-buffer        next-buffer                cerca-show-keys         next-buffer                   [(f1 )] [(shift f1 )] [(control f1 )])
       (find-file               find-file-at-point         insert-file             find-file-at-point            [(f2 )] [(shift f2 )] [(control f2 )])
       (save-buffer             save-some-buffers          revert-buffer           save-some-buffers             [(f3 )] [(shift f3 )] [(control f3 )])
       (kill-buffer             delete-frame               speedbar                delete-frame                  [(f4 )] [(shift f4 )] [(control f4 )])
       (kill-region             kill-rectangle             clear-rectangle         kill-rectangle                [(f5 )] [(shift f5 )] [(control f5 )])
       (copy-region-as-kill     CLE-A-DEFINIR              append-to-register      CLE-A-DEFINIR                 [(f6 )] [(shift f6 )] [(control f6 )])
       (copy-to-register        copy-rectangle-to-register open-rectangle          copy-rectangle-to-register    [(f7 )] [(shift f7 )] [(control f7 )])
       (yank                    yank-rectangle             insert-register         yank-rectangle                [(f8 )] [(shift f8 )] [(control f8 )])
       (query-replace           search-forward             replace-string          search-forward                [(f9 )] [(shift f9 )] [(control f9 )])
       (query-replace-regexp    search-forward-regexp      replace-regexp          search-forward-regexp         [(f10)] [(shift f10)] [(control f10)])
       (next-error              set-selective-display   add-change-log-entry-other-window set-selective-display  [(f11)] [(shift f11)] [(control f11)])
       (compile                 indent-region              repeat-complex-command  indent-region                 [(f12)] [(shift f12)] [(control f12)])
       )))
   (t
    (mapcar
     'cerca-map-keys
     ;;base                     shift                      ctrl                    gold                          4     5           6
     '(
       (next-file-buffer        next-buffer                cerca-show-keys         next-file-buffer              [(f1 )] [(shift f1 )] [(control f1 )])
       (find-file               dired                      insert-file             find-file                     [(f2 )] [(shift f2 )] [(control f2 )])
       (save-buffer             save-some-buffers          revert-buffer           save-buffer                   [(f3 )] [(shift f3 )] [(control f3 )])
       (kill-buffer             save-buffers-kill-emacs    save-buffers-kill-emacs kill-buffer                   [(f4 )] [(shift f4 )] [(control f4 )])
       (query-replace           isearch-forward-regexp     replace-regexp          query-replace                 [(f5 )] [(shift f5 )] [(control f5 )])
       (set-mark-command        exchange-point-and-mark    mark-whole-buffer       set-mark-command              [(f6 )] [(shift f6 )] [(control f6 )])
       (kill-line               kill-region                append-next-kill        kill-line                     [(f7 )] [(shift f7 )] [(control f7 )])
       (yank                    yank-pop                   insert-register         yank                          [(f8 )] [(shift f8 )] [(control f8 )])
       (copy-region-as-kill     yank-pop                   shell-command           copy-region-as-kill           [(f9 )] [(shift f9 )] [(control f9 )])
       (kill-rectangle          clear-rectangle            shell-command-on-region kill-rectangle                [(f10)] [(shift f10)] [(control f10)])
       (yank-rectangle          open-rectangle             add-change-log-entry    yank-rectangle                [(f11)] [(shift f11)] [(control f11)])
       (repeat-complex-command  indent-region              set-selective-display   repeat-complex-command        [(f12)] [(shift f12)] [(control f12)])
       )))
   ))
 ((equal cles-pfs 'vt)
  (define-key global-map [(f6 )]    'rlogin)
  (define-key GOLD-map   [(f6 )]    'dired)
  (define-key global-map [(f7 )]    'next-buffer)
  (define-key GOLD-map   [(f7 )]    'revert-buffer)
  (define-key global-map [(f8 )]    'find-file)
  (define-key GOLD-map   [(f8 )]    'find-alternate-file)
  (define-key global-map [(f9 )]    'save-buffer)
  (define-key GOLD-map   [(f9 )]    'revert-buffer)
  (define-key global-map [(f10)]    'save-buffers-kill-emacs)
  (define-key GOLD-map   [(f10)]    'save-some-buffer)
  (define-key global-map [(f12)]    'overwrite-mode)
  (define-key GOLD-map   [(f12)]    nil)
  (define-key global-map [(f13)]    'delete-previous-word)
  (define-key GOLD-map   [(f13)]    'backward-kill-sentence)
  (define-key global-map [(f14)]    'dabbrev-expand)
  (define-key GOLD-map   [(f14)]    'unexpand-abbrev)
  ;;(define-key global-map [(help)]   'describe-key-briefly)
  (define-key GOLD-map   [(help)]   'eval-expression)
  (define-key global-map [(menu)]   'execute-extended-command)
  (define-key GOLD-map   [(menu)]   'repeat-complex-command)
  (define-key global-map [(f17)]    'undo)
  (define-key GOLD-map   [(f17)]    'cerca-toggle-screen)
  (define-key global-map [(f18)]    'beginning-of-buffer)
  (define-key GOLD-map   [(f18)]    'line-to-top-of-window)
  (define-key global-map [(f19)]    'end-of-buffer)
  (define-key GOLD-map   [(f19)]    'line-to-bottom-of-window)
  (define-key global-map [(f20)]    'next-file-buffer)
  (define-key GOLD-map   [(f20)]    'next-buffer)
  ))

;; --------------------
;;  le GOLD-map
;; --------------------
(define-key GOLD-map "\et"  'transpose-paragraphs)
(define-key GOLD-map [(meta x)] 'clipboard-kill-region)
(define-key GOLD-map [(meta c)] 'clipboard-kill-ring-save)
(define-key GOLD-map [(meta v)] 'clipboard-yank)
(define-key GOLD-map "\C-a" 'append-to-file)
(define-key GOLD-map "\C-a" 'append-to-file)
(define-key GOLD-map "\C-c" '(lambda nil (interactive) (switch-to-buffer compilation-last-buffer)))
(define-key GOLD-map "\C-f" 'font-lock-fontify-buffer)
(define-key GOLD-map "\C-g" 'keyboard-quit)
(define-key GOLD-map "\C-i" (if (featurep 'xemacs) 'whitespace-visual-mode 'whitespace-mode))
(define-key GOLD-map "\C-j" 'sort-numeric-fields)
(define-key GOLD-map "\C-k" 'kill-paragraph)
(define-key GOLD-map "\C-l" 'downcase-region)
(define-key GOLD-map "\C-m" 'open-line)
(define-key GOLD-map "\C-o" 'sort-fields)
(define-key GOLD-map "\C-p" 'sort-columns)
(define-key GOLD-map "\C-r" 'reverse-region)
(define-key GOLD-map "\C-s" 'sort-lines)
(define-key GOLD-map "\C-t" 'transpose-sentences)
(define-key GOLD-map "\C-u" 'upcase-region)
(define-key GOLD-map "\C-v" 'compare-windows)
(define-key GOLD-map "\C-w" 'write-region)
(define-key GOLD-map "a" 'abbrev-mode)
(define-key GOLD-map "A" 'abbrev-mode)
(define-key GOLD-map "b" 'next-buffer)
(define-key GOLD-map "B" 'next-buffer)
(define-key GOLD-map "c" 'comment-region)
(define-key GOLD-map "C" 'uncomment-region)
(define-key GOLD-map "d" 'picture-duplicate-line)
(define-key GOLD-map "D" 'picture-duplicate-line)
(define-key GOLD-map "ee" '(lambda nil (interactive) (flyspell-english) (flyspell-buffer)))
(define-key GOLD-map "ef" '(lambda nil (interactive) (flyspell-francais) (flyspell-buffer)))
(define-key GOLD-map "es" '(lambda nil (interactive) (flyspell-espanol) (flyspell-buffer)))
(define-key GOLD-map "ec" '(lambda nil (interactive) (flyspell-auto-correct-word)))
(define-key GOLD-map "ep" '(lambda nil (interactive) (flyspell-auto-correct-previous-word)))
(define-key GOLD-map "E" 'flyspell-mode)
(define-key GOLD-map "f" 'find-file)
(define-key GOLD-map "F" 'find-alternate-file)
(define-key GOLD-map "g" 'grep)
(define-key GOLD-map "G" 'cerca-grep)
(define-key GOLD-map "h" 'command-apropos)
(define-key GOLD-map "H" 'apropos)
(define-key GOLD-map "i" 'insert-file)
(define-key GOLD-map "I" 'insert-buffer)
(define-key GOLD-map "j" nil)
(define-key GOLD-map "J" nil)
(define-key GOLD-map "k" 'kill-buffer)
(define-key GOLD-map "K" 'kill-buffer)
(define-key GOLD-map "l" 'goto-line)
(define-key GOLD-map "L" (if (<= emacs-major-version 25) 'linum-mode 'display-line-numbers-mode))
(define-key GOLD-map "m" 'vm)
(define-key GOLD-map "M" 'vm-mail)
(define-key GOLD-map "n" 'vm-visit-folder)
(define-key GOLD-map "N" 'vm-visit-virtual-folder)
(define-key GOLD-map "o" 'occur)
(define-key GOLD-map "O" 'occur)
(define-key GOLD-map "p" 'lpr-region)
(define-key GOLD-map "P" 'lpr-buffer)
(define-key GOLD-map "q" nil)
(define-key GOLD-map "Q" nil)
(define-key GOLD-map "r" 'revert-buffer)
(define-key GOLD-map "R" 'rename-uniquely)
(define-key GOLD-map "s" 'shell-command)
(define-key GOLD-map "S" 'shell)
(define-key GOLD-map "t" 'line-to-top-of-window)
(define-key GOLD-map "T" 'line-to-bottom-of-window)
(define-key GOLD-map "u" 'cvs-update)
(define-key GOLD-map "U" 'cvs-update-other-window)
;;(define-key GOLD-map "v" 'view-file)
;;(define-key GOLD-map "v" '(lambda nil (interactive) (vm-visit-folder (concat "/-:ailleurs:" (substitute-in-file-name "${MAIL}")))))
(define-key GOLD-map "v" '(lambda nil (interactive) (vm-visit-folder (substitute-in-file-name "${MAIL}"))))
(define-key GOLD-map "V" 'set-visited-file-name)
(define-key GOLD-map "w" 'save-buffer)
(define-key GOLD-map "W" 'write-file)
(define-key GOLD-map "x" 'fume-prompt-function-goto)
(define-key GOLD-map "X" 'fume-rescan-buffer)
(define-key GOLD-map "y" 'copy-region-as-kill)
(define-key GOLD-map "Y" 'copy-region-as-kill)
(define-key GOLD-map "z" nil)
(define-key GOLD-map "Z" nil)
(define-key GOLD-map "1" '(lambda nil (interactive) (jump-to-register ?1 nil)))
(define-key GOLD-map "2" '(lambda nil (interactive) (jump-to-register ?2 nil)))
(define-key GOLD-map "3" '(lambda nil (interactive) (jump-to-register ?3 nil)))
(define-key GOLD-map "4" '(lambda nil (interactive) (jump-to-register ?4 nil)))
(define-key GOLD-map "5" '(lambda nil (interactive) (jump-to-register ?5 nil)))
(define-key GOLD-map "6" '(lambda nil (interactive) (jump-to-register ?6 nil)))
(define-key GOLD-map "7" '(lambda nil (interactive) (jump-to-register ?7 nil)))
(define-key GOLD-map "8" '(lambda nil (interactive) (jump-to-register ?8 nil)))
(define-key GOLD-map "9" '(lambda nil (interactive) (jump-to-register ?9 nil)))
(define-key GOLD-map "0" '(lambda nil (interactive) (jump-to-register ?0 nil)))
(define-key GOLD-map " " 'undo)
(define-key GOLD-map "~" 'auto-save-mode)
(define-key GOLD-map "!" 'auto-fill-mode)
(define-key GOLD-map "@" '(lambda nil (interactive) (setq case-fold-search (not case-fold-search)) (setq-default case-fold-search case-fold-search)))
(define-key GOLD-map "#" 'toggle-newline-and-indent)
(define-key GOLD-map "$" 'tpu-add-at-eol)
(define-key GOLD-map "%" 'goto-percent)
(define-key GOLD-map "^" 'tpu-add-at-bol)
(define-key GOLD-map "&" 'toggle-truncate-lines)
(define-key GOLD-map "*" 'isearch-toggle-regexp)
(define-key GOLD-map "(" 'insert-parentheses)
(define-key GOLD-map ")" 'insert-parentheses)
(define-key GOLD-map "-" 'font-lock-mode)
(define-key GOLD-map "=" 'cerca-toolbar-spec)
(define-key GOLD-map "+" 'cerca-scrollbar-spec)
(define-key GOLD-map "," 'tags-search)
(define-key GOLD-map "/" 'visit-tags-table)
(define-key GOLD-map "." 'tags-query-replace)
(define-key GOLD-map "'" 'query-replace)
(define-key GOLD-map "\"" 'replace-string)
(define-key GOLD-map ";" 'query-replace-regexp)
(define-key GOLD-map ":" 'replace-regexp)
(define-key GOLD-map "?" 'where-are-other-keys)
(define-key GOLD-map "\\" nil)
(define-key GOLD-map "<" 'recover-context)
(define-key GOLD-map ">" 'sauver-contexte)
(define-key GOLD-map "[" 'point-to-register)
(define-key GOLD-map "]" 'jump-to-register)
(define-key GOLD-map "_" 'split-window-vertically)
(define-key GOLD-map "`" 'elimine-whitespaces)
(define-key GOLD-map "|" 'split-window-horizontally)
(define-key GOLD-map "{" 'pwd)
(define-key GOLD-map "}" 'cd)

;; --------------------
;;  le ctl-x-map
;; --------------------
(define-key global-map "\C-xw" 'count-words-region)
(define-key global-map "\C-xI" 'insert-buffer)
(define-key global-map "\C-x\C-i" 'compile)
(define-key global-map [(control x)(control left)] 'switch-to-previous-buffer)
(define-key global-map [(control x)(control right)] 'switch-to-next-buffer)

(if (keymapp (lookup-key global-map "\C-xa"))
    (progn
      (define-key global-map "\C-xa." 'abbrev-mode)
      (define-key global-map "\C-xau" 'unexpand-abbrev)
      (define-key global-map "\C-xaA" 'edit-abbrevs)
      (define-key global-map "\C-xa?" 'list-abbrevs)
      (define-key global-map "\C-xaR" 'read-abbrev-file)
      (define-key global-map "\C-xaW" 'write-abbrev-file)
      (define-key global-map "\C-xaK" 'kill-all-abbrevs)
      (define-key global-map "\C-xaL" 'define-mode-abbrev)
      (define-key global-map "\C-xaG" 'define-global-abbrev)
      (define-key global-map "\C-xaR" 'expand-region-abbrevs)))

(if (keymapp (lookup-key global-map "\C-xr"))
    (progn
      (define-key global-map "\C-xra" 'append-to-register)
      (define-key global-map "\C-xrA" 'prepend-to-register)
      (define-key global-map "\C-xrY" 'picture-yank-rectangle-from-register)
      (define-key global-map "\C-xrC" 'picture-clear-rectangle-to-register)
      (define-key global-map "\C-xrd" 'delete-rectangle)
      (define-key global-map "\C-xrv" 'view-register)
      (define-key global-map "\C-xrV" 'view-all-registers)
      (define-key global-map "\C-xrS" 'save-registers)))

;; --------------------
;;  divers
;; --------------------
(define-key global-map    [(meta \?)]  'display-tag-info)
(define-key global-map    [(meta *)]   'pop-tag-mark)
(define-key GOLD-map      [(meta /)]   'tag-complete-symbol)
;;(define-key global-map    [(execute)]  'undo)
;;(define-key GOLD-map      [(execute)]  'undo)
(define-key global-map    [(menu)]     'undo)
(define-key GOLD-map      [(menu)]     'undo)
(if (string-match "vt100" (or (getenv "TERM") "xterm"))
    (define-key global-map   "\C-h"   'delete-backward-char))
(define-key global-map   [(control delete)] 'unexpand-abbrev)
;BO(define-key global-map   [(meta delete)]    'delete-previous-word)
(define-key global-map   [(meta delete)]    'undo)
(define-key global-map   [(super delete)]    'undo)
(define-key global-map   [(backspace)]      'delete-backward-char)
(define-key global-map   [(meta backspace)] 'undo)
(define-key global-map   [(super backspace)] 'undo)
(define-key global-map "\C-u"     'delete-to-bol)
(define-key global-map "\C-c\C-k" 'kill-compilation)
(define-key global-map "\e#"      'calc-dispatch)
(define-key global-map "\es"      'center-line)
(define-key global-map "\eS"      'center-paragraph)
;; multiple-cursors line editing
(global-set-key (kbd "C-c m c") 'mc/edit-lines)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;; --------------------
;;  le mode-line-format
;; --------------------
(or (assq 'tpu-rectangular-p minor-mode-alist)
    (setq minor-mode-alist (cons '(tpu-rectangular-p tpu-rectangle-string)
                                 minor-mode-alist)))
(or (assq 'tpu-direction-string minor-mode-alist)
    (setq minor-mode-alist (cons '(tpu-direction-string tpu-direction-string)
                                 minor-mode-alist)))
(or (assq 'newline-and-indent-p minor-mode-alist)
    (setq minor-mode-alist (cons '(newline-and-indent-p newline-and-indent-p-string)
                                 minor-mode-alist)))
;;(advance-direction)

;; --------------------
;;  le minibuffer
;; --------------------
(if (boundp 'minibuffer-local-map)
    (progn
      (define-key minibuffer-local-map "\e[A" 'previous-history-element)
      (define-key minibuffer-local-map "\e[B" 'next-history-element)
      (define-key minibuffer-local-map "\eOA" 'previous-history-element)
      (define-key minibuffer-local-map "\eOB" 'next-history-element)
      (define-key minibuffer-local-map "\C-p" 'previous-history-element)
      (define-key minibuffer-local-map "\C-n" 'next-history-element)
      (define-key minibuffer-local-map "\eOM" 'exit-minibuffer)
      ))
;;(define-key minibuffer-local-completion-map "\eOM" 'exit-minibuffer)
;;(define-key minibuffer-local-must-match-map "\eOM" 'minibuffer-complete-and-exit)
;;(if (boundp 'minibuffer-local-ns-map)
;;    (define-key minibuffer-local-ns-map     "\eOM" 'exit-minibuffer))

;; --------------------
;;  la souris
;; --------------------
;;(define-key global-map [(shift   meta          button1)] 'mouse-kill-line)



(if (featurep 'xemacs)
    (progn
      ;;                                         'mouse-1 'mouse-track)
      ;;                     [(shift              mouse-1)] 'mouse-track-adjust)
      ;;                     [(      control      mouse-1)] 'mouse-track-insert)
      ;;                     [(shift control      mouse-1)] 'mouse-track-delete-and-insert)
      ;;                     [(              meta mouse-1)] nil)
      (define-key global-map [(shift         meta mouse-1)] 'mouse-track-do-rectangle)
      (define-key global-map [(      control meta mouse-1)] 'x-mouse-kill)
      (define-key global-map [(shift control meta mouse-1)] 'x-kill-primary-selection)

      ;;                                         'mouse-2 'mouse-yank))
      (define-key global-map [(shift              mouse-2)] 'mouse-yank)
      (define-key global-map [(      control      mouse-2)] 'x-set-point-and-move-selection)
      (define-key global-map [(shift control      mouse-2)] 'mouse-kill-line)
      ;;                     [(              meta mouse-2)] nil)
      (define-key global-map [(shift         meta mouse-2)] 'narrow-window-to-region)
      (define-key global-map [(      control meta mouse-2)] 'mouse-window-to-region)
      (define-key global-map [(shift control meta mouse-2)] 'mouse-scroll)

      ;;                                         'mouse-3 'popup-mode-menu))
      (define-key global-map [(shift              mouse-3)] 'mouse-function-menu)
      (define-key global-map [(      control      mouse-3)] 'popup-buffer-menu)
      (define-key global-map [(shift control      mouse-3)] 'popup-enriched-menu)
      ;;                     [(              meta mouse-3)] nil)
      (define-key global-map [(shift         meta mouse-3)] 'mouse-select-and-split)
      (define-key global-map [(      control meta mouse-3)] 'mouse-delete-window)
      (define-key global-map [(shift control meta mouse-3)] 'mouse-keep-one-window)
      )

;;; emacs

  (require 'mouse-copy)
  (global-set-key [S-down-mouse-2] 'mouse-drag-secondary-pasting)
  (global-set-key [S-down-mouse-3] 'mouse-drag-secondary-pasting)
  (global-set-key [C-S-down-mouse-2] 'mouse-drag-secondary-moving)
  (global-set-key [C-S-down-mouse-3] 'mouse-drag-secondary-moving)

  ;(global-set-key [mouse-3]              'mouse-save-then-kill)

  ;(global-unset-key [mouse-3])
  ;(global-set-key [mouse-3] 'mouse-buffer-menu)
  ;(global-set-key [C-down-mouse-3] 'mouse-buffer-menu)
  ;(global-set-key [S-down-mouse-1] 'mouse-appearance-menu)

  ;(global-set-key [C-down-mouse-3]
  ; '(menu-item "Menu Bar" ignore
 ;     :filter (lambda (_)
 ;               (if (zerop (or (frame-parameter nil 'menu-bar-lines) 0))
 ;                   (mouse-menu-bar-map)
 ;                 (mouse-menu-major-mode-map)))))

  )

  ;;(global-set-key [mouse-1]	     'mouse-set-point)
  ;;(global-set-key [drag-mouse-1]   'mouse-set-region)
  ;;(global-set-key [double-mouse-1] 'mouse-set-point)
  ;;(global-set-key [triple-mouse-1] 'mouse-set-point)
  ;;(global-set-key [mouse-2]	     'mouse-yank-at-click)
  ;;(global-set-key [mouse-3]        'mouse-save-then-kill)
  ;;(global-set-key [C-down-mouse-1] 'mouse-buffer-menu)
  ;;(global-set-key [S-down-mouse-1] 'mouse-appearance-menu))
  ;;(global-set-key [C-down-mouse-3]

    ;;<mouse-1>		mouse-set-point
    ;;<mouse-2>		mouse-yank-at-click
    ;;<mouse-3>		mouse-save-then-kill
    ;;<C-down-mouse-1>	mouse-buffer-menu
    ;;<C-down-mouse-2>	facemenu-menu
    ;;<C-mouse-3>	mouse-buffer-menu
    ;;<C-mouse-4>	mwheel-scroll
    ;;<C-mouse-5>	mwheel-scroll
    ;;<S-down-mouse-1>	mouse-appearance-menu
    ;;<S-mouse-4>	mwheel-scroll
    ;;<S-mouse-5>	mwheel-scroll
    ;;<double-mouse-1>	mouse-set-point
    ;;<down-mouse-1>	mouse-drag-region
    ;;<drag-mouse-1>	mouse-set-region
    ;;<mouse-movement>	ignore
    ;;<triple-mouse-1>	mouse-set-point
