;;; Creation d'un fichier .tex donnant la config. des cles pour le mode courant.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

(defun layout-insert-def-at-eol (line) ""
  (save-excursion
    (end-of-line)
    (if (string-match "-" line)
        (progn
          (insert (substring line 0 (match-beginning 0)) "&")
          (end-of-line 2)
          (setq line (substring line (match-end 0)))
          (if (string-match "-" line)
              (progn
                (insert (substring line 0 (match-beginning 0)) "&")
                (end-of-line 2)
                (insert (substring line (match-end 0)) "&"))
            (progn
              (insert line "&")
              (end-of-line 2)
              (insert "&"))))
      (progn
        (insert line "&")
        (end-of-line 2)
        (insert "&")
        (end-of-line 2)
        (insert "&")))
    ))


(defun layout-insert-break-at-eol () ""
  (end-of-line)
  (delete-backward-char 1)
  (insert "\\\\")
  (end-of-line 2)
  (delete-backward-char 1)
  (insert "\\\\")
  (end-of-line 2)
  (delete-backward-char 1)
  (insert "\\\\")
  (end-of-line 2))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun layout-print-list (hline prefixe liste) ""
  (if hline
      (insert "\\hline\n\n\n\n"))
  (beginning-of-line -2)
  (while liste
    (let ((defn (key-binding (concat prefixe (car liste)))))
      (cond
       ((null defn)
        (layout-insert-def-at-eol ""))
       ((integerp defn)
        (layout-insert-def-at-eol ""))
       ((keymapp defn)
        (layout-insert-def-at-eol "GOLD"))
       (t
        (layout-insert-def-at-eol (prin1-to-string defn)))))
    (setq liste (cdr liste)))
  (layout-insert-break-at-eol))
                                        ; (mapcar 'layout-print-def liste)


(defun layout-wk-pf (titre pref) ""
  (insert "\\mbox{\\begin{tabular}{r||c|c|c|c||c|c|c|c||c|c|c|c||}\n")
  (insert "\\multicolumn{13}{c}{\\normalsize\\bf " titre "}\\\\\n")
  (insert "\\cline{2-13}\n&~~~~~1~~~~&~~~~~2~~~~&~~~~~3~~~~&~~~~~4~~~~&~~~~~5~~~~&~~~~~6~~~~&~~~~~7~~~~&~~~~~8~~~~&~~~~~9~~~~&~~~~10~~~~&~~~~11~~~~&~~~~12~~~~\\\\\n")
  (insert "\\cline{2-13}\n&\n{\\em Ctrl}&\n&\n")
  (layout-print-list nil pref (list "\e[025q" "\e[026q" "\e[027q" "\e[028q" "\e[029q" "\e[030q" "\e[031q" "\e[032q" "\e[033q" "\e[034q" "\e[035q" "\e[036q"))
  (insert "\\cline{2-13}\n&\n{\\em Shft}&\n&\n")
  (layout-print-list nil pref (list "\e[013q" "\e[014q" "\e[015q" "\e[016q" "\e[017q" "\e[018q" "\e[019q" "\e[020q" "\e[021q" "\e[022q" "\e[023q" "\e[024q"))
  (insert "\\cline{2-13}\n&\n&\n&\n")
  (layout-print-list nil pref (list "\e[001q" "\e[002q" "\e[003q" "\e[004q" "\e[005q" "\e[006q" "\e[007q" "\e[008q" "\e[009q" "\e[010q" "\e[011q" "\e[012q"))
  (insert "\\cline{2-13}\n\\end{tabular}}\n"))


(defun layout-vt-pf (titre pref) ""
  (insert "\\mbox{\\begin{tabular}{||c|c|c|c|c||c|c|c|c||c|c||c|c|c|c||}\n")
  (insert "\\multicolumn{15}{c}{\\normalsize\\bf " titre "}\\\\\n")
  (insert "\\hline\n6&7&8&9&10&11&12&13&14&help&do&17&18&19&20\\\\\n")
  (layout-print-list t pref (list "\e[17~" "\e[18~" "\e[19~" "\e[20~" "\e[21~" "\e[23~" "\e[24~" "\e[25~" "\e[26~" "\e[28~" "\e[29~" "\e[31~" "\e[32~" "\e[33~" "\e[34~"))
  (insert "\\hline\n\\end{tabular}}\n"))


(defun layout-kpf (titre pref) ""
  (insert "\\mbox{\\begin{tabular}{|c|c|c|c|}\n")
  (insert "\\multicolumn{4}{c}{\\normalsize\\bf " titre "}\\\\\n")
  (layout-print-list t pref (list "\eOP" "\eOQ" "\eOR" "\eOS"))
  (layout-print-list t pref (list "\eOw" "\eOx" "\eOy" "\eOm"))
  (layout-print-list t pref (list "\eOt" "\eOu" "\eOv" "\eOl"))
  (layout-print-list t pref (list "\eOq" "\eOr" "\eOs" "\eOM"))
  (layout-print-list t pref (list "\eOp" "\eOp" "\eOn" "\eOM"))
  (insert "\\hline\n\\end{tabular}}\n"))


(defun layout-wk-keypad (titre pref) ""
  (insert "\\mbox{\\begin{tabular}{|c|c|c|}\n")
  (insert "\\multicolumn{3}{c}{\\normalsize\\bf " titre "}\\\\\n")
  ;;(layout-print-list nil pref (list "\e[209q" "\e[213q" "\e[217q"))
  (layout-print-list t pref (list "\e[4~" "\e[3~" "\e[2~"))
  (insert "\\hline\n")
  (layout-print-list t pref (list "\e[139q" "\e[H" "\e[150q"))
  (layout-print-list t pref (list "\177" "\e[146q" "\e[154q"))
  (insert "\\hline\n")
  (layout-print-list t pref (list "XXX" "\e[A" "XXX"))
  (layout-print-list t pref (list "\e[D" "\e[B" "\e[C"))
  (insert "\\hline\n\\end{tabular}}\n"))


(defun layout-wk-keypad-shift (titre pref) ""
  (insert "\\mbox{\\begin{tabular}{|c|c|c|}\n")
  (insert "\\multicolumn{3}{c}{\\normalsize\\bf " titre "}\\\\\n")
  (layout-print-list t pref (list "\e[210q" "\e[214q" "\e[218q"))
  (insert "\\hline\n")
  (layout-print-list t pref (list "\e[139q" "\e[143q" "\e[151q"))
  (layout-print-list t pref (list "\177" "\e[147q" "\e[155q"))
  (insert "\\hline\n")
  (layout-print-list t pref (list "XXX" "\e[A" "XXX"))
  (layout-print-list t pref (list "\e[D" "\e[B" "\e[C"))
  (insert "\\hline\n\\end{tabular}}\n"))


(defun layout-wk-keypad-ctrl (titre pref) ""
  (insert "\\mbox{\\begin{tabular}{|c|c|c|}\n")
  (insert "\\multicolumn{3}{c}{\\normalsize\\bf " titre "}\\\\\n")
  (layout-print-list t pref (list "\e[211q" "\e[215q" "\e[219q"))
  (insert "\\hline\n")
  (layout-print-list t pref (list "\e[140q" "\e[144q" "\e[152q"))
  (layout-print-list t pref (list "\177" "\e[148q" "\e[156q"))
  (insert "\\hline\n")
  (layout-print-list t pref (list "XXX" "\e[162q" "XXX"))
  (layout-print-list t pref (list "\e[159q" "\e[165q" "\e[168q"))
  (insert "\\hline\n\\end{tabular}}\n"))

(defun layout-wk-keypad-alt (titre pref) ""
  (insert "\\mbox{\\begin{tabular}{|c|c|c|}\n")
  (insert "\\multicolumn{3}{c}{\\normalsize\\bf " titre "}\\\\\n")
  (layout-print-list t pref (list "\e[212q" "\e[216q" "\e[220q"))
  (insert "\\hline\n")
  (layout-print-list t pref (list "\e[141q" "\e[145q" "\e[153q"))
  (layout-print-list t pref (list "\177" "\e[149q" "\e[157q"))
  (insert "\\hline\n")
  (layout-print-list t pref (list "XXX" "\e[163q" "XXX"))
  (layout-print-list t pref (list "\e[160q" "\e[166q" "\e[169q"))
  (insert "\\hline\n\\end{tabular}}\n"))


(defun layout-vt-keypad (titre pref) ""
  (insert "\\mbox{\\begin{tabular}{|c|c|c|}\n")
  (insert "\\multicolumn{3}{c}{\\normalsize\\bf " titre "}\\\\\n")
  (layout-print-list t pref (list "\e[1~" "\e[2~" "\e[3~"))
  (layout-print-list t pref (list "\e[4~" "\e[5~" "\e[6~"))
  (insert "\\hline\n")
  (layout-print-list t pref (list "XXX" "\e[A" "XXX"))
  (layout-print-list t pref (list "\e[D" "\e[B" "\e[C"))
  (insert "\\hline\n\\end{tabular}}\n"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun layout-print-mouse-list (header1 header2 liste) ""
  (insert "\\cline{2-4}\n{\\em " header1 "}&\n{\\em " header2 "}&\n&\n")
  (beginning-of-line -2)
  (while liste
    (layout-insert-def-at-eol (prin1-to-string (elt mouse::global-map (car liste))))
    (setq liste (cdr liste)))
  (layout-insert-break-at-eol))


(defun layout-mouse (titre pref) ""
  (insert "\\mbox{\\begin{tabular}{r||c|c|c|}\n")
  (insert "\\multicolumn{4}{c}{\\normalsize\\bf ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" titre "}\\\\\n")
  (insert "\\cline{2-4}\n&select&paste&modify\\\\\n")
  (layout-print-mouse-list "" "" (list 0 16 32))
  (layout-print-mouse-list "Shft" "(Delete)" (list 2 18 34))
  (layout-print-mouse-list "Ctrl" "(Thing)" (list 4 20 36))
  (layout-print-mouse-list "Shft+Ctrl" "(Delete+Thing)" (list 6 22 38))
  ;; (layout-print-mouse-list "Alt" "(Window)" (list 8 24 40))
  (layout-print-mouse-list "Alt+Shft" "(Window+Delete)" (list 10 26 42))
  (layout-print-mouse-list "Alt+Ctrl" "(Window+Thing)" (list 12 28 44))
  (layout-print-mouse-list "Alt+Shft+Ctrl" "(Window+Delete+Thing)" (list 14 30 46))
  (layout-print-mouse-list "Modeline" "(Modeline)" (list 48 64 80))
  (layout-print-mouse-list "Modeline+Shft" "(Modeline+Delete)" (list 50 66 82))
  (insert "\\cline{2-4}\n\\end{tabular}}\n"))

(defun layout-help-mouse ()
  "Obtenir une liste des cles de la souris."
  (interactive)
  (let ((i 0)
        (len (length mouse::global-map)))
    (with-output-to-temp-buffer "*MouseMap*"
      (while (< i len)
        (princ (format "%3d  %s\n" i (elt mouse::global-map i)))
        (setq i (+ i 1))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconst layout-file-name "/tmp/layout"
  "*Nom du fichier temporaire (sans extension) contenant la description des cles.")

(defconst layout-latex-command "latex"
  "*Commande pour creer le fichier .dvi contenant la description des cles.")

(defconst layout-dvips-command "dvips -t landscape"
  "*Commande pour creer le fichier .ps contenant la description des cles.")


(defun layout-latex ()
  "Preparer un fichier .tex contenant la description courante des cles.
Ce fichier est ensuite traite par latex, puis dvips.

Variables:
  layout-file-name:	Nom du fichier cree (sans extension)  [/tmp/layout]
  layout-latex-command:	Commande pour creer le .dvi  [latex]
  layout-dvips-command:	Commande pour creer le .ps  [dvips -t landscape]"
  (interactive)
  (save-excursion
    (find-file (concat layout-file-name ".tex"))
    (erase-buffer)
    (insert "\\documentstyle{article}\n"
            "\\pagestyle{empty}\n"
            "\\textheight 8.5in\n"
            "\\textwidth 11in\n"
            "\\headheight 0in\n"
            "\\headsep 0in\n"
            "\\oddsidemargin -.9in\n"
            "\\topmargin -.8in\n"
            "\\begin{document}\n"
            "Pour avoir cette configuration, prendre une copie du fichier ``.emacs''.\n\n"
            "\\vspace{10mm}\n\n"
            "\\footnotesize\n"
            )
    (layout-wk-pf "pf" "")
    (insert "\\vspace{10mm}\n\n")
    (layout-wk-pf "GOLD pf" "\eOP")
    (insert "\\vspace{8mm}\n\n")
    (layout-kpf "kpf" "")
    (insert "\\hspace{5mm}\n")
    (layout-kpf "GOLD kpf" "\eOP")
    (insert "\\hspace{5mm}\n")
    (if mouse-map (layout-mouse "mouse" ""))
    (insert "\\vspace{5mm}\n\n")
    (layout-wk-keypad "keypad" "")
    (insert "\\hspace{2mm}\n")
    (layout-wk-keypad "GOLD keypad" "\eOP")
    (insert "\\hspace{2mm}\n")
    (layout-wk-keypad-shift "SHIFT keypad" "")
    ;; (layout-wk-keypad-shift "GOLD SHIFT keypad" "\eOP")
    (insert "\\hspace{2mm}\n")
    (layout-wk-keypad-ctrl "CTRL keypad" "")
    ;; (layout-wk-keypad-ctrl "GOLD CTRL keypad" "\eOP")
    (insert "\\hspace{2mm}\n")
    (layout-wk-keypad-alt "ALT keypad" "")
    ;; (layout-wk-keypad-alt "GOLD ALT keypad" "\eOP")
    (insert "\\vspace{20mm}\n\n")
    (layout-vt-keypad "keypad (vt)" "")
    (insert "\\hspace{5mm}\n")
    (layout-vt-keypad "GOLD keypad (vt)" "\eOP")
    (insert "\\vspace{20mm}\n\n")
    (layout-vt-pf "pf sur (vt)" "")
    (insert "\\vspace{8mm}\n\n")
    (layout-vt-pf "GOLD pf sur (vt)" "\eOP")
    (insert "\\end{document}\n")
    (write-file (concat layout-file-name ".tex"))
    (message "Latexing %s.tex..." layout-file-name)
    (shell-command (concat layout-latex-command " " layout-file-name " && "
                           layout-dvips-command " " layout-file-name))
    (message "Fichier \"%s.ps\" cree." layout-file-name)
    ))


(if mouse-map
    (progn
      (layout-latex)
      (copy-file (concat layout-file-name ".ps") "./cerca-layout.ps" t t)))
