;;; Definition des menus specifiques a xemacs.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

(if (featurep 'xemacs)
    (progn
      (load "big-menubar" nil t)
      (delete-menu-item '("Top"))
      (delete-menu-item '("<<"))
      (delete-menu-item '(" | "))
      (delete-menu-item '(">>"))
      (delete-menu-item '("Bot"))
      (add-menu-button '("Tools") ["Term" term t])
      ))

(defvar cerca-spell-menu
  '("Spell Check (local)"
    ["FlySpell mode"     flyspell-mode t]
    ["FlySpell Francais" flyspell-francais]
    ["FlySpell English"  flyspell-english]
    ["FlySpell Espanol"  flyspell-espanol]
    ["Change dictionary" ispell-change-dictionary t]
    ["----" nil t]
    ["FlySpell Buffer" flyspell-buffer t]
    ["FlySpell Region" flyspell-region (mark)]
    ["FlySpell Word"   flyspell-word t]
    ["-----" nil t]
    ["Epelle -latex Buffer"    epelle-latex-buffer t]
    ["Epelle -latex Region"    epelle-latex-region (mark)]
    ["Epelle -latex Paragraph" epelle-latex-paragraph (mark)]
    ["Epelle -latex Word"      epelle-latex-word t]
    ["------" nil t]
    ["Epelle -8bit Buffer"     epelle-8bit-buffer t]
    ["Epelle -8bit Region"     epelle-8bit-region (mark)]
    ["Epelle -8bit Paragraph"  epelle-8bit-paragraph (mark)]
    ["Epelle -8bit Word"       epelle-8bit-word t]
    ["-------" nil t]
    ["Epelle -latex -8bit Buffer"    epelle-latex-8bit-buffer t]
    ["Epelle -latex -8bit Paragraph" epelle-latex-8bit-paragraph t]
    ["Epelle -latex -8bit Region"    epelle-latex-8bit-region (mark)]
    ["Epelle -latex -8bit Word"      epelle-latex-8bit-word t]
    ["-------" nil t]
    ["Spell Buffer"    spell-buffer t]
    ["Spell Region"    spell-region (mark)]
    ["Spell Paragraph" spell-paragraph (mark)]
    ["Spell Word"      spell-word t]
    ))
(if (featurep 'xemacs)
    (add-submenu '("Cmds") cerca-spell-menu)
  (add-submenu '("Tools") cerca-spell-menu "Spell Checking"))

;;(defvar ps-number-of-columns (if ps-landscape-mode 2 1) "*Specifies the number of columns")
;;(defvar ps-landscape-mode 'nil "*Non-nil means print in landscape mode.")
;;(defvar ps-print-header t nil)
;;(defvar ps-print-header-frame t nil)
(require 'ps-print) ;; easier than the above defvar's
(defvar cerca-prettyprint-menu
  '("Pretty-Print ..."
    ["Pretty-Print Buffer" ps-print-buffer-with-faces t]
    ["Pretty-Print Region" ps-print-region-with-faces t]
    ["----" nil t]
    ["In Color" (setq ps-print-color-p (not ps-print-color-p)) :style toggle :selected ps-print-color-p]
    ["Printer Command"  (setq ps-lpr-command  (read-string "ps-lpr-command: "
                                                           (prin1 ps-lpr-command))) t]
    ["Printer Switches" (setq ps-lpr-switches (read-string "ps-lpr-switches: "
                                                           (prin1 ps-lpr-switches))) t]
    ("Default Orientation"
     ["Landscape" (setq ps-landscape-mode t) :style radio :selected ps-landscape-mode]
     ["Portrait"  (setq ps-landscape-mode nil) :style radio :selected (not ps-landscape-mode)]
     )
    ("Default Number of columns"
     ["1"   (setq ps-number-of-columns 1) :style radio :selected (eq ps-number-of-columns 1)]
     ["2"   (setq ps-number-of-columns 2) :style radio :selected (eq ps-number-of-columns 2)]
     ["3"   (setq ps-number-of-columns 3) :style radio :selected (eq ps-number-of-columns 3)]
     ["4"   (setq ps-number-of-columns 4) :style radio :selected (eq ps-number-of-columns 4)]
     ["..." (setq ps-number-of-columns (read-expression "Number of columns: "
                                                        (format "%d" ps-number-of-columns))) t]
     )
    ("Default Header"
     ["With headers"    (setq ps-print-header t)   :style radio :selected      ps-print-header ]
     ["Without headers" (setq ps-print-header nil) :style radio :selected (not ps-print-header)]
     )
    ("Default Border Frame"
     ["With frame"    (setq ps-print-header-frame t)   :style radio :selected      ps-print-header-frame ]
     ["Without frame" (setq ps-print-header-frame nil) :style radio :selected (not ps-print-header-frame)]
     )
    ["----" nil t]
    ["Pretty-Print Buffer /tmp/... (Portrait,  1 col)"
     (let ((ps-landscape-mode nil)
           (ps-number-of-columns 1))
       (ps-print-buffer-with-faces (concat "/tmp/" (ps-get-buffer-name) ".ps"))) t]
    ["Pretty-Print Buffer /tmp/... (Landscape, 1 col)"
     (let ((ps-landscape-mode t)
           (ps-number-of-columns 1))
       (ps-print-buffer-with-faces (concat "/tmp/" (ps-get-buffer-name) ".ps"))) t]
    ["Pretty-Print Buffer /tmp/... (Landscape, 2 col)"
     (let ((ps-landscape-mode t)
           (ps-number-of-columns 2))
       (ps-print-buffer-with-faces (concat "/tmp/" (ps-get-buffer-name) ".ps"))) t]
    ["----" nil t]
    ["Pretty-Print Buffer /tmp/... (Portrait,  1 col, no decoration)"
     (let ((ps-landscape-mode nil)
           (ps-number-of-columns 1)
           (ps-print-header nil)
           (ps-print-header-frame nil))
       (ps-print-buffer-with-faces (concat "/tmp/" (ps-get-buffer-name) ".ps"))) t]
    ["Pretty-Print Buffer /tmp/... (Landscape, 1 col, no decoration)"
     (let ((ps-landscape-mode t)
           (ps-number-of-columns 1)
           (ps-print-header nil)
           (ps-print-header-frame nil))
       (ps-print-buffer-with-faces (concat "/tmp/" (ps-get-buffer-name) ".ps"))) t]
    ["Pretty-Print Buffer /tmp/... (Landscape, 2 col, no decoration)"
     (let ((ps-landscape-mode t)
           (ps-number-of-columns 2)
           (ps-print-header nil)
           (ps-print-header-frame nil))
       (ps-print-buffer-with-faces (concat "/tmp/" (ps-get-buffer-name) ".ps"))) t]
    ))
(if (featurep 'xemacs)
    (add-submenu '("File") cerca-prettyprint-menu)
  (add-submenu '("File") cerca-prettyprint-menu "Print Buffer"))

(defvar cerca-abbrevs-menu
  '("Abbrevs"
    ["Edit" edit-abbrevs t]
    ["List" list-abbrevs t]
    ["--" nil t]
    ["Add (mode)" add-mode-abbrev t]
    ["Inverse Add (mode)" inverse-add-mode-abbrev t]
    ["Define (mode)" define-mode-abbrev t]
    ["---" nil t]
    ["Add (global)" add-global-abbrev t]
    ["Inverse Add (global)" inverse-add-global-abbrev t]
    ["Define (global)" define-global-abbrev t]
    ["----" nil t]
    ["Expand" expand-abbrev t]
    ["Expand in Region" expand-region-abbrevs t]
    ["Unexpand" unexpand-abbrev t]
    ))
(if (featurep 'xemacs)
    (add-submenu '("Edit") cerca-abbrevs-menu)
  (add-submenu '("Edit") cerca-abbrevs-menu))

(defvar cerca-accents-menu
  '("Accents"
    ["Accents mode" accents-mode t]
    ["        fran�ais"         iso-accents-francais         iso-accents-mode]
    ["        us-international" iso-accents-us-international iso-accents-mode]
    ["        espa�ol"          iso-accents-espanol          iso-accents-mode]
    ["        portuguesa"       iso-accents-portuguesa       iso-accents-mode]
    ["        irlandais"        iso-accents-irlandais        iso-accents-mode]
    ["        latin-1"          iso-accents-latin-1          iso-accents-mode]
    ["        latin-2"          iso-accents-latin-2          iso-accents-mode]
    ["        latin-3"          iso-accents-latin-3          iso-accents-mode]
    "-----"
    ["Iso  -> Aucun" accents-get-aucun-from-iso (fboundp 'accents-replace)]
    "---"
    ["TeX  -> Iso" accents-get-iso-from-tex (fboundp 'accents-replace)]
    ["Iso  -> TeX" accents-get-tex-from-iso (fboundp 'accents-replace)]
    ["Html -> Iso" accents-get-iso-from-html (fboundp 'accents-replace)]
    ["Iso  -> Html" accents-get-html-from-iso (fboundp 'accents-replace)]
    "----"
    ["Quoted-p -> Iso" iso-quoted-printable (fboundp 'accents-replace)]
    ["Microsoft-> Iso" iso-microsoft (fboundp 'accents-replace)]
    ["utf      -> Iso" iso-utf (fboundp 'accents-replace)]
    ["utf8     -> Iso" iso-utf8 (fboundp 'accents-replace)]
    ))
(if (featurep 'xemacs)
    (add-submenu nil cerca-accents-menu)
  (add-submenu nil cerca-accents-menu))
;;(easy-menu-add-item nil nil cerca-accents-menu))

;;(add-menu-button nil ["(re)Load .emacs" (load-user-init-file (user-login-name)) t])
;;(add-menu-button nil
;;                 ["Load .emacs"
;;                  (progn (delete-menu-item '("Load .emacs"))
;;                         (load-user-init-file (user-login-name)))
;;                  t])

;;(load-library "hideshow")
;;(defun hs-minor-mode (&optional arg)
;;  "Toggle hideshow minor mode.
;;With ARG, turn hideshow minor mode on if ARG is positive, off otherwise.
;;When hideshow minor mode is on, the menu bar is augmented with hideshow
;;commands and the hideshow commands are enabled.  The variables\n
;;\tselective-display\n\tselective-display-ellipses\n
;;are set to t.  Lastly, the hooks set in hs-minor-mode-hook are called.
;;See documentation for `run-hooks'.\n
;;Turning hideshow minor mode off reverts the menu bar and the
;;variables to default values and disables the hideshow commands."
;;  (interactive "P")
;;  (setq hs-minor-mode
;;        (if (null arg)
;;            (not hs-minor-mode)
;;          (> (prefix-numeric-value arg) 0)))
;;  (if hs-minor-mode
;;      (progn
;;        (setq selective-display t
;;              selective-display-ellipses t)
;;        (hs-grok-mode-type)
;;        (run-hooks 'hs-minor-mode-hook))
;;    (kill-local-variable 'selective-display)
;;    (kill-local-variable 'selective-display-ellipses)))

;;(setq hs-menu
;;      '("HideShow"
;;        ["Minor Mode toggle" hs-minor-mode t]
;;        ["---"         hs-minor-mode t]
;;        ["Hide Block"  hs-hide-block  hs-minor-mode]
;;        ["Show Block"  hs-show-block  hs-minor-mode]
;;        ["Hide All"    hs-hide-all    hs-minor-mode]
;;        ["Show All"    hs-show-all    hs-minor-mode]
;;        ["Show Region" hs-show-region (mark)]
;;        ))
;;(add-submenu '("Utilities") hs-menu "Grep...")
;;(defun popup-hideshow-menu (e)
;;  "Pop up a copy of the HideShow menu (from the menubar) where the mouse is clicked."
;;  (interactive "@e")
;;  (popup-menu hs-menu)
;;  )
;;  (define-key hs-minor-mode-map "\C-czh" 'hs-hide-block)
;;  (define-key hs-minor-mode-map "\C-czs" 'hs-show-block)
;;  (define-key hs-minor-mode-map "\C-czH" 'hs-hide-all)
;;  (define-key hs-minor-mode-map "\C-czS" 'hs-show-all)
;;  (define-key hs-minor-mode-map "\C-czR" 'hs-show-region))
;; `hs-special-modes-alist' if you'd like to use hideshow w/ other modes.
