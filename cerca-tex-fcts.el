;;; Fonctions diverses pour le mode TeX.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

(defun TeX-autodvi ()
  ""
  (interactive)
  (save-excursion
    (mark-paragraph)
    (tex-region (region-beginning) (region-end))
    ))

(defun tex-alt-print ()
  (interactive)
  (tex-print t))

(defconst TeX-open-parentheses "{"
  "Open parenthesis characters for TeX.")

(defconst TeX-close-parentheses "}"
  "Close parenthesis characters for TeX.  These should match up with
open-parenthesis.")

(defun TeX-envelop-word (string count &optional surround)
  "Surround current word with TeX construct \\STRING{...}.  COUNT
specifies how many words to surround.  A negative count means to skip
backward."
  (let ((spos (point))
        (epos (point))
        (ccoun 0) noparens)
    (if (not (zerop count))
        (progn (if (= (char-syntax (preceding-char)) ?w)
                   (forward-sexp (min -1 count)))
               (setq spos (point))
               (if (looking-at (concat "{\\w" TeX-open-parentheses "}"))
                   (forward-char 2)
                 (goto-char epos)
                 (skip-chars-backward "\\W")
                 (forward-char -1))
               (forward-sexp (max count 1))
               (setq epos (point))))
    (goto-char spos)
    (while (and (< ccoun (length TeX-open-parentheses))
                (save-excursion
                  (or (search-forward (char-to-string
                                       (aref TeX-open-parentheses ccoun))
                                      epos t)
                      (search-forward (char-to-string
                                       (aref TeX-close-parentheses ccoun))
                                      epos t)))
                (setq ccoun (1+ ccoun))))
    (if (>= ccoun (length TeX-open-parentheses))
        (progn (goto-char epos)
               (insert "@end(" string ")")
               (goto-char spos)
               (insert "@begin(" string ")"))
      (goto-char epos)
      (insert (aref TeX-close-parentheses ccoun))
      (goto-char spos)
      (if surround
          (insert "\\" string (aref TeX-open-parentheses ccoun))
        (insert (aref TeX-open-parentheses ccoun) "\\" string " "))
      (goto-char epos)
      (forward-char 3)
      (skip-chars-forward TeX-close-parentheses))))

(defun TeX-kill-construct nil
  "Kills TeX construct {\\STRING ...}"
  (interactive)
  (search-forward TeX-close-parentheses)
  (delete-char -1)
  (search-backward TeX-open-parentheses)
  (delete-char 1)
  (let ((pos (point)))
    (if (looking-at "\\\\")
        (progn
          (search-forward " ")
          (delete-region pos (match-end 0)))
      (search-backward "\\")
      (delete-region pos (match-beginning 0)))
    ))

(defun TeX-underline-word (count)
  "Emphasize COUNT words around point by means of TeX constructs."
  (interactive "p")
  (TeX-envelop-word "underline" count t))

(defun TeX-overline-word (count)
  "Emphasize COUNT words around point by means of TeX constructs."
  (interactive "p")
  (TeX-envelop-word "overline" count t))

(defun TeX-emphasize-word (count)
  "Emphasize COUNT words around point by means of TeX constructs."
  (interactive "p")
  (TeX-envelop-word "emph" count t))

(defun TeX-slant-word (count)
  "Slant COUNT words around point by means of TeX constructs."
  (interactive "p")
  (TeX-envelop-word "textsl" count t))

(defun TeX-listing-word (count)
  "Lstinline COUNT words around point by means of TeX constructs."
  (interactive "p")
  (let ((TeX-open-parentheses "!")
        (TeX-close-parentheses "!"))
    (TeX-envelop-word "lstinline" count t)))

(defun TeX-code-word (count)
  "Codeface COUNT words around point by means of TeX constructs."
  (interactive "p")
  (TeX-envelop-word "code" count t))

(defun TeX-bold-word (count)
  "Boldface COUNT words around point by means of TeX constructs."
  (interactive "p")
  (TeX-envelop-word "textbf" count t))

(defun TeX-italicize-word (count)
  "Italicize COUNT words around point by means of TeX constructs."
  (interactive "p")
  (TeX-envelop-word "textit" count t))

(defun TeX-typewrite-word (count)
  "Typewrite COUNT words around point by means of TeX constructs."
  (interactive "p")
  (TeX-envelop-word "texttt" count t))

(defun TeX-chapter ()
  (interactive)
  (insert "\n")
  (forward-char -1)
  (TeX-envelop-word "Chapter" 0)
  (re-search-forward (concat "{" TeX-open-parentheses "}")))

(defun TeX-section ()
  (interactive)
  (insert "\n")
  (forward-char -1)
  (TeX-envelop-word "Section" 0)
  (re-search-forward (concat "{" TeX-open-parentheses "}")))

(defun TeX-subsection ()
  (interactive)
  (insert "\n")
  (forward-char -1)
  (TeX-envelop-word "SubSection" 0)
  (re-search-forward (concat "{" TeX-open-parentheses "}")))

(defun TeX-bracket-region-be (env min max)
  (interactive "sEnvironment: \nr")
  (save-excursion
    (goto-char max)
    (insert "@end(" env ")\n")
    (goto-char min)
    (insert "@begin(" env ")\n")))

(defun TeX-insert-environment (env)
  (interactive "sEnvironment: ")
  (TeX-bracket-region-be env (point) (point))
  (forward-line 1)
  (insert ?\n)
  (forward-char -1))

(defun TeX-split-paragraph nil
  (interactive)
  (save-excursion
    (mark-paragraph)
    (while (not (= (point) (region-end)))
      (forward-sentence 1)
      (goto-char (match-end 0))
      (if (not (looking-at "$"))
          (open-line 1))
      (save-excursion
        (fill-region (region-beginning) (region-end))
        (delete-char 1))
      )
    (open-line 2)))

(defvar TeX-look-for-section-regexp
   "^\\\\\\(chapter\\|\\(sub\\)*section\\)+")
(defun TeX-look-for-section (&optional arg)
  "Cherche le prochain chapter, section ou subsection.  Si un argument est
donne, ne pas initialise la direction de recherche."
  (interactive)
  (if (not arg) (setq searching-forward (if tpu-advance t nil)))
  (cond (searching-forward
         (re-search-forward TeX-look-for-section-regexp))
        (t
         (re-search-backward TeX-look-for-section-regexp))
        ))

;; correction...
(defun tex-bibtex-file ()
  "Run BibTeX on the current buffer's file."
  (interactive)
  (if (tex-shell-running)
      (tex-kill-job)
    (tex-start-shell))
  (let ((tex-out-file
         (tex-append (file-name-nondirectory (buffer-file-name)) ""))
        (file-dir (file-name-directory (buffer-file-name))))
    (tex-send-command tex-shell-cd-command file-dir)
    (tex-send-command tex-bibtex-command tex-out-file)))

(defun tex-reformat ()
  "Mettre les { sur des lignes séparées et ajouter des espaces dans les ()."
  (interactive)
  (save-excursion
    ;; mettre les { sur des lignes séparées
    (goto-char (point-min))
    (while (re-search-forward "\\([^ \t\n]+\\)[ \t]*{" nil t)
      (replace-match "\\1\n{") nil nil)
    ;; ajouter un espace après les (
    (goto-char (point-min))
    (while (re-search-forward "(\\([^ \t\n]+\\)" nil t)
      (replace-match "( \\1") nil nil)
    ;; ajouter un espace avant les )
    (goto-char (point-min))
    (while (re-search-forward "\\([^ \t\n]+\\))" nil t)
      (replace-match "\\1 )") nil nil)
    ;; indenter tout ça
    (indent-buffer)))

(provide 'cerca-tex-fcts)
