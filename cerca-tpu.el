;;;
;;;
;;;  G L O B A L
;;;  V A R I A B L E S
;;;
;;;
(defvar tpu-global-key-plist nil
  "Original emacs definitions of global keys, so they may be restored.")
(defvar tpu-last-replaced-text ""
  "Last text deleted by a TPU-edt replace command.")
(defvar tpu-last-deleted-region ""
  "Last text deleted by a TPU-edt remove command.")
(defvar tpu-last-deleted-lines ""
  "Last text deleted by a TPU-edt line-delete command.")
(defvar tpu-last-deleted-words ""
  "Last text deleted by a TPU-edt word-delete command.")
(defvar tpu-last-deleted-char ""
  "Last character deleted by a TPU-edt character-delete command.")
(defvar tpu-search-last-string ""
  "Last text searched for by the TPU-edt search commands.")
(defvar tpu-regexp-p nil
  "If non-nil, TPU-edt uses regexp search and replace routines.")
(defvar tpu-rectangular-p nil
  "If non-nil, TPU-edt removes and inserts rectangles.")
(defvar tpu-advance t
  "True when TPU-edt is operating in the forward direction.")
(defvar tpu-rectangle-string nil
  "Mode line string to identify rectangular mode.")


;;;
;;;
;;;  L O C A L
;;;  V A R I A B L E S
;;;
;;;
(defvar newline-and-indent-p nil
  "If non-nil, Return produces a newline and indents.")
(make-variable-buffer-local 'newline-and-indent-p)

(defvar tpu-saved-delete-func nil
  "Saved value of the delete key.")
(make-variable-buffer-local 'tpu-saved-delete-func)

(defvar tpu-buffer-local-map nil
  "TPU-edt buffer local key map.")
(make-variable-buffer-local 'tpu-buffer-local-map)


;;;
;;;
;;;  Modify the the mode line to show if the mark is set.
;;;
;;;
(defvar tpu-mark-flag " " " ")
(make-variable-buffer-local 'tpu-mark-flag)

;;;
;;;
;;;  Match markers -
;;;
;;;     Set in:  Search
;;;
;;;     Used in: Replace, Substitute, Store-Text, Cut/Remove,
;;;              Append, and Change-Case
;;;
;;;
(setq tpu-match-beginning-mark (make-marker))
(setq tpu-match-end-mark (make-marker))

(defun tpu-set-match nil
  "Set markers at match beginning and end."
  ;; Add one to beginning mark so it stays with the first character of
  ;;   the string even if characters are added just before the string.
  (setq tpu-match-beginning-mark (copy-marker (1+ (match-beginning 0))))
  (setq tpu-match-end-mark (copy-marker (match-end 0))))

(defun tpu-unset-match nil
  "Unset match beginning and end markers."
  (set-marker tpu-match-beginning-mark nil)
  (set-marker tpu-match-end-mark nil))

(defun tpu-match-beginning nil
  "Returns the location of the last match beginning."
  (1- (marker-position tpu-match-beginning-mark)))

(defun tpu-match-end nil
  "Returns the location of the last match end."
  (marker-position tpu-match-end-mark))

(defun tpu-check-match nil
  "Returns t if point is between tpu-match markers.
Otherwise sets the tpu-match markers to nil and returns nil."
  ;; make sure 1- marker is in this buffer
  ;;           2- point is at or after beginning marker
  ;;           3- point is before ending marker, or in the case of
  ;;              zero length regions (like bol, or eol) that the
  ;;              beginning, end, and point are equal.
  (cond ((and
	  (equal (marker-buffer tpu-match-beginning-mark) (current-buffer))
	  (>= (point) (1- (marker-position tpu-match-beginning-mark)))
	  (or
	   (< (point) (marker-position tpu-match-end-mark))
	   (and (= (1- (marker-position tpu-match-beginning-mark))
		   (marker-position tpu-match-end-mark))
		(= (marker-position tpu-match-end-mark) (point))))) t)
	(t
	 (tpu-unset-match) nil)))

(defun tpu-show-match-markers nil
  "Show the values of the match markers."
  (interactive)
  (if (markerp tpu-match-beginning-mark)
      (let ((beg (marker-position tpu-match-beginning-mark)))
	(message "(%s, %s) in %s -- current %s in %s"
		 (if beg (1- beg) nil)
		 (marker-position tpu-match-end-mark)
		 (marker-buffer tpu-match-end-mark)
		 (point) (current-buffer)))))


;;;
;;;
;;;  U T I L I T I E S
;;;
;;;
(defun caar (thingy) (car (car thingy)))
(defun cadr (thingy) (car (cdr thingy)))
(defun cadar (thingy) (car (cdr (car thingy))))
(defun caddar (thingy) (car (cdr (cdr (car thingy)))))

(defvar tpu-last-answer "" "Last answer en TPU.")
(defun tpu-y-or-n-p (prompt &optional not-yes)
  "Prompt for a y or n answer with positive default.
Optional second argument NOT-YES changes default to negative.
Like emacs y-or-n-p, also accepts space as y and DEL as n."
  (message (format "%s[%s]" prompt (if not-yes "n" "y")))
  (let ((doit t))
    (while doit
      (setq doit nil)
      (let ((ans (read-char)))
	(cond ((or (= ans ?y) (= ans ?Y) (= ans ?\ ))
	       (setq tpu-last-answer t))
	      ((or (= ans ?n) (= ans ?N) (= ans ?\C-?))
	       (setq tpu-last-answer nil))
	      ((= ans ?\r) (setq tpu-last-answer (not not-yes)))
	      (t
	       (setq doit t)
	       (message (format "Please answer y or n.  %s[%s]"
				prompt (if not-yes "n" "y"))))))))
  tpu-last-answer)

;;;
;;;  miscellaneous
;;;
(defun tpu-local-set-key (key func)
  "Replace a key in the TPU-edt local key map.
Create the key map if necessary."
  (cond ((not (keymapp tpu-buffer-local-map))
	 (setq tpu-buffer-local-map (if (current-local-map)
					(copy-keymap (current-local-map))
				      (make-sparse-keymap)))
	 (use-local-map tpu-buffer-local-map)))
  (local-set-key key func))

(defun tpu-change-case (num)
  "Change the case of the character under the cursor or region.
Accepts a prefix argument of the number of characters to invert."
  (interactive "p")
  (cond ((mark)
	 (let ((beg (region-beginning)) (end (region-end)))
	   (while (> end beg)
	     (funcall (if (<= ?a (char-after beg))
			  'upcase-region 'downcase-region)
		      beg (1+ beg))
	     (setq beg (1+ beg)))
	   (tpu-unselect t)))
	((tpu-check-match)
	 (let ((beg (tpu-match-beginning)) (end (tpu-match-end)))
	   (while (> end beg)
	     (funcall (if (<= ?a (char-after beg))
			  'upcase-region 'downcase-region)
		      beg (1+ beg))
	     (setq beg (1+ beg)))
	   (tpu-unset-match)))
	(t
	 (while (> num 0)
	   (funcall (if (<= ?a (following-char))
			'upcase-region 'downcase-region)
		    (point) (1+ (point)))
	   (forward-char (if (not tpu-advance) -1 1))
	   (setq num (1- num))))))

(defun update-mode-line nil
  "Make sure mode-line in the current buffer reflects all changes."
  (setq tpu-mark-flag (if (mark) "M" " "))
  (set-buffer-modified-p (buffer-modified-p))
  (sit-for 0))

(defvar newline-and-indent-p-string "")
(defun toggle-newline-and-indent nil
  "Toggle between 'newline and indent' and 'simple newline'."
  (interactive)
  (cond (newline-and-indent-p
         (setq newline-and-indent-p-string "")
         (setq newline-and-indent-p nil)
         (tpu-local-set-key "\C-m" 'newline))
        (t
         (setq newline-and-indent-p-string " AutoIndent")
         (setq newline-and-indent-p t)
         (tpu-local-set-key "\C-m" 'newline-and-indent)))
  (update-mode-line))

(defun tpu-select (&optional quiet)
  "Sets the mark to define one end of a region."
  (interactive "P")
  (cond ((mark)
	 (tpu-unselect quiet))
	(t
	 (set-mark (point))
	 (update-mode-line)
	 (if (not quiet) (message "Move the text cursor to select text.")))))

(defun tpu-unselect (&optional quiet)
  "Removes the mark to unselect the current region."
  (interactive "P")
  (setq mark-ring nil)
  (set-mark nil)
  (update-mode-line)
  (if (not quiet) (message "Selection canceled.")))

;;;
;;;  TPU-alike commands
;;;
(defun quit nil
  "Quit the way TPU does, ask to make sure changes should be abandoned."
  (interactive)
  (let ((list (buffer-list))
	(working t))
    (while (and list working)
      (let ((buffer (car list)))
	(if (and (buffer-file-name buffer) (buffer-modified-p buffer))
	    (if (tpu-y-or-n-p
		 (format "Modifications will not be saved, continue quitting? " ))
		(kill-emacs t) (setq working nil)))
	(setq list (cdr list))))
    (if working (kill-emacs t))))


;;;
;;;  Help
;;;
(defun tpu-help nil
  "Display TPU-edt help."
  (interactive)
  ;; Save current window configuration
  (save-window-excursion
    ;; Create and fill help buffer if necessary
    (if (not (get-buffer "*TPU-edt Help*"))
	(progn (generate-new-buffer "*TPU-edt Help*")
	       (switch-to-buffer "*TPU-edt Help*")
	       (insert "\f
          _______________________    _______________________________
         | HELP  |      Do       |  |       |       |       |       |
         |KeyDefs|               |  |       |       |       |       |
         |_______|_______________|  |_______|_______|_______|_______|
          _______________________    _______________________________
         | Find  |Insert |Remove |  | Gold  | HELP  |FndNxt | Del L |
         |       |       |Sto Tex|  |  key  |E-Help | Find  |Undel L|
         |_______|_______|_______|  |_______|_______|_______|_______|
         |Select |Pre Scr|Nex Scr|  | Page  | Sect  |Append | Del W |
         | Reset |Pre Win|Nex Win|  |  Do   | Fill  |Replace|Undel W|
         |_______|_______|_______|  |_______|_______|_______|_______|
                 |Move up|          |Forward|Reverse|Remove | Del C |
                 |  Top  |          |Bottom |  Top  |Insert |Undel C|
          _______|_______|_______   |_______|_______|_______|_______|
         |Mov Lef|Mov Dow|Mov Rig|  | Word  |  EOL  | Char  |       |
         |StaOfLi|Bottom |EndOfLi|  |ChngCas|Del EOL|SpecIns| Enter |
         |_______|_______|_______|  |_______|_______|_______|       |
                                    |     Line      |Select | Subs  |
                                    |   Open Line   | Reset |       |
                                    |_______________|_______|_______|


\f

      Control Characters

      ^A  toggle insert and overwrite
      ^B  recall
      ^E  end of line

      ^G  Cancel current operation
      ^H  beginning of line
      ^J  delete previous word

      ^K  learn
      ^L  insert page break
      ^R  remember (during learn), re-center

      ^U  delete to beginning of line
      ^V  quote
      ^W  refresh

      ^Z  exit
    ^X^X  exchange point and mark - useful for checking region boundaries

\f
       Gold-<key> Functions

       B     Next Buffer - display the next buffer (all buffers)
       C     Recall - edit and possibly repeat previous commands
       E     Exit - save current buffer and ask about others

       G     Get - load a file into a new edit buffer
       I     Include - include a file in this buffer
       K     Kill Buffer - abandon edits and delete buffer

       M     Buffer Menu - display a list of all buffers
       N     Next File Buffer - display next buffer containing a file
       O     Occur - show following lines containing REGEXP

       Q     Quit - exit without saving anything
       R     Toggle rectangular mode for remove and insert
       S     Search and substitute - line mode REPLACE command

       U     Undo - undo the last edit
       W     Write - save current buffer
       X     Exit - save all modified buffers and exit

\f

   *** No more help, use P to view previous screen")
      (setq buffer-read-only t)))

    ;; Display the help buffer
    (switch-to-buffer "*TPU-edt Help*")
    (delete-other-windows)
    (beginning-of-buffer)
    (forward-line 1)
    (line-to-top-of-window)

    ;; Prompt for keys to describe, based on screen state (split/not split)
    (let ((key nil) (split nil))
      (while (not (equal key "\r"))
	(if split
	    (setq key
		  (read-key-sequence
		   "Press the key you want help on (RET=exit, ENTER=redisplay, N=next, P=prev): "))
	  (setq key
		(read-key-sequence
		 "Press the key you want help on (RET to exit, N next screen, P prev screen): ")))

	;; Process the read key
	;;
	;;    ENTER    Display just the help window
	;;    N or n   Next help or describe-key screen
	;;    P or p   Previous help or describe-key screen
	;;    RETURN   Exit from TPU-help
	;;    default  describe the key
	;;
	(cond ((equal "\eOM" key)
	       (setq split nil)
	       (delete-other-windows))
	      ((or (equal "N" key) (equal "n" key))
	       (cond (split
	              (condition-case nil
	        	  (scroll-other-window 8)
	        	(error nil)))
	             (t
	              (forward-page)
	              (forward-line 1)
	              (line-to-top-of-window))))
	      ((or (equal "P" key) (equal "p" key))
	       (cond (split
	              (condition-case nil
	        	  (scroll-other-window -8)
	        	(error nil)))
	             (t
	              (backward-page 2)
	              (forward-line 1)
	              (line-to-top-of-window))))
	      ((not (equal "\r" key))
	       (setq split t)
	       (describe-key key)
	       ;; If the key is undefined, leave the
	       ;;   message in the mini-buffer for 3 seconds
	       (if (not (key-binding key)) (sit-for 3))))))))

;;;
;;;  Buffer
;;;
;;(defun next-buffer nil
;;  "Go to next buffer in ring."
;;  (interactive)
;;  (switch-to-buffer (car (reverse (buffer-list)))))

(defun next-buffer nil
  "Go to next buffer in ring."
  (interactive)
  (let ((starting-buffer (buffer-name)))
    (switch-to-buffer (car (reverse (buffer-list))))
    (while (and (not (equal (buffer-name) starting-buffer))
                (equal (aref (buffer-name) 0) ?\ )
                )
      (switch-to-buffer (car (reverse (buffer-list)))))
    (if (equal (buffer-name) starting-buffer)
        (error "%s  (No other buffers.)" buffer-file-name)
      (message buffer-file-name)
      )))

(defun next-file-buffer nil
  "Go to next buffer in ring that is visiting a file."
  (interactive)
  (let ((starting-buffer (buffer-name)))
    (switch-to-buffer (car (reverse (buffer-list))))
    (while (and (not (equal (buffer-name) starting-buffer))
		(or (not (buffer-file-name))
                    (equal (aref (buffer-name) 0) ?\ )
                ))
      (switch-to-buffer (car (reverse (buffer-list)))))
    (if (equal (buffer-name) starting-buffer)
        (error "%s  (No other file buffers.)" buffer-file-name)
      (message buffer-file-name)
      )))

;;;
;;;  Repeat count
;;;
;; (defun repeat-command-0 nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [48] nil nil))
;;
;; (defun repeat-command-1 nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [49] nil nil))
;;
;; (defun repeat-command-2 nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [50] nil nil))
;;
;; (defun repeat-command-3 nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [51] nil nil))
;;
;; (defun repeat-command-4 nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [52] nil nil))
;;
;; (defun repeat-command-5 nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [53] nil nil))
;;
;; (defun repeat-command-6 nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [54] nil nil))
;;
;; (defun repeat-command-7 nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [55] nil nil))
;;
;; (defun repeat-command-8 nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [56] nil nil))
;;
;; (defun repeat-command-9 nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [57] nil nil))
;;
;; (defun repeat-command-- nil
;;   "Repeats the following keystroke."
;;   (interactive)
;;   (prefix-arg-internal [45] nil nil))


;;;
;;;  Search
;;;
(defvar searching-forward "" "Search direction en TPU.")
(defun tpu-toggle-regexp nil
   "Switches in and out of regular expression search and replace mode."
   (interactive)
   (setq tpu-regexp-p (not tpu-regexp-p))
   (tpu-set-search)
   (message "Regular expression search and substitute %sabled."
	    (if tpu-regexp-p "en" "dis")))

(defun tpu-regexp-prompt (prompt)
  "Read a string, adding 'RE' to the prompt if tpu-regexp-p is set."
  (read-string (concat (if tpu-regexp-p "RE ") prompt)))

(defun tpu-search nil
  "Search for a string or regular expression.
The search is performed in the current direction."
  (interactive)
  (tpu-set-search)
  (tpu-search-internal ""))

(defun tpu-search-again nil
  "Search for the same string or regular expression as last time.
The search is performed in the current direction."
  (interactive)
  (tpu-search-internal tpu-search-last-string))

;;  tpu-set-search defines the search functions used by the TPU-edt internal
;;  search function.  It should be called whenever the direction changes, or
;;  the regular expression mode is turned on or off.  It can also be called
;;  to ensure that the next search will be in the current direction.  It is
;;  called from:

;;       tpu-advance              tpu-backup
;;       tpu-toggle-regexp        tpu-toggle-search-direction (t)
;;       tpu-search               tpu-lm-replace

(defun tpu-set-search (&optional arg)
  "Set the search functions and set the search direction to the current
direction.  If an argument is specified, don't set the search direction."
  (if (not arg) (setq searching-forward (if tpu-advance t nil)))
  (cond (searching-forward
	 (cond (tpu-regexp-p
		(fset 'tpu-emacs-search 're-search-forward)
		(fset 'tpu-emacs-rev-search 're-search-backward))
	       (t
		(fset 'tpu-emacs-search 'search-forward)
		(fset 'tpu-emacs-rev-search 'search-backward))))
	(t
	 (cond (tpu-regexp-p
		(fset 'tpu-emacs-search 're-search-backward)
		(fset 'tpu-emacs-rev-search 're-search-forward))
	       (t
		(fset 'tpu-emacs-search 'search-backward)
		(fset 'tpu-emacs-rev-search 'search-forward))))))

(defun tpu-search-internal (pat &optional quiet)
  "Search for a string or regular expression."
  (setq tpu-search-last-string
	(if (not (string= "" pat)) pat (tpu-regexp-prompt "Search: ")))

  (tpu-unset-match)
  (tpu-adjust-search)

  (cond ((tpu-emacs-search tpu-search-last-string nil t)
	 (tpu-set-match) (goto-char (tpu-match-beginning)))

	(t
	 (tpu-adjust-search t)
	 (let ((found nil) (pos nil))
	   (save-excursion
	     (let ((searching-forward (not searching-forward)))
	       (tpu-adjust-search)
	       (setq found (tpu-emacs-rev-search tpu-search-last-string nil t))
	       (setq pos (match-beginning 0))))

	   (cond (found
		  (cond ((tpu-y-or-n-p
			  (format "Found in %s direction.  Go there? "
				  (if searching-forward "reverse" "forward")))
			 (goto-char pos) (tpu-set-match)
			 (tpu-toggle-search-direction))))

		 (t
		  (if (not quiet)
		      (message
		       "%sSearch failed: \"%s\""
		       (if tpu-regexp-p "RE " "") tpu-search-last-string))))))))

(defun tpu-adjust-search (&optional arg)
  "For forward searches, move forward a character before searching,
and backward a character after a failed search.  Arg means end of search."
  (if searching-forward
      (cond (arg (if (not (bobp)) (forward-char -1)))
	    (t (if (not (eobp)) (forward-char 1))))))

(defun tpu-toggle-search-direction nil
  "Toggle the TPU-edt search direction.
Used for reversing a search in progress."
  (interactive)
  (setq searching-forward (not searching-forward))
  (tpu-set-search t))


;;;
;;;  Delete
;;;
(defun tpu-toggle-rectangle nil
  "Toggle rectangular mode for remove and insert."
  (interactive)
  (setq tpu-rectangular-p (not tpu-rectangular-p))
  (setq tpu-rectangle-string (if tpu-rectangular-p " Rect" ""))
  (update-mode-line)
  (message "Rectangular cut and paste %sabled."
	   (if tpu-rectangular-p "en" "dis")))

(defun tpu-arrange-rectangle nil
  "Adjust point and mark to mark upper left and lower right
corners of a rectangle."
  (let ((mc (current-column))
	(pc (progn (exchange-point-and-mark) (current-column))))

    (cond ((> (point) (mark))                      ; point on lower line
	   (cond ((> pc mc)                        ; point @  lower-right
		  (exchange-point-and-mark))       ; point -> upper-left

		 (t	                           ; point @  lower-left
		  (move-to-column-force mc)        ; point -> lower-right
		  (exchange-point-and-mark)        ; point -> upper-right
		  (move-to-column-force pc))))     ; point -> upper-left

	  (t                                       ; point on upper line
	   (cond ((> pc mc)                        ; point @  upper-right
		  (move-to-column-force mc)        ; point -> upper-left
		  (exchange-point-and-mark)        ; point -> lower-left
		  (move-to-column-force pc)        ; point -> lower-right
		  (exchange-point-and-mark)))))))  ; point -> upper-left

(defun tpu-cut nil
  "Delete the selected region.
The text is saved for the tpu-paste command."
  (interactive)
    (cond ((mark)
	   (cond (tpu-rectangular-p
		  (tpu-arrange-rectangle)
		  (picture-clear-rectangle (point) (mark) (not overwrite-mode))
                  (tpu-unselect t))
                 (t
		  (setq tpu-last-deleted-region
			(buffer-substring (mark) (point)))
		  (delete-region (mark) (point))
		  (tpu-unselect t))))
	   ((tpu-check-match)
	    (let ((beg (tpu-match-beginning)) (end (tpu-match-end)))
	      (setq tpu-last-deleted-region (buffer-substring beg end))
	      (delete-region beg end)
	      (tpu-unset-match)))
	   (t
	    (message "No selection active."))))

(defun tpu-store-text nil
  "Copy the selected region to the cut buffer without deleting it.
The text is saved for the tpu-paste command."
  (interactive)
    (cond ((mark)
	   (cond (tpu-rectangular-p
		  (save-excursion
		    (tpu-arrange-rectangle)
		    (setq picture-killed-rectangle
			  (extract-rectangle (point) (mark))))
		  (tpu-unselect t))
		 (t
		  (setq tpu-last-deleted-region
			(buffer-substring (mark) (point)))
		  (tpu-unselect t))))
	   ((tpu-check-match)
	    (setq tpu-last-deleted-region
		  (buffer-substring (tpu-match-beginning) (tpu-match-end)))
	    (tpu-unset-match))
	   (t
	    (message "No selection active."))))

(defun tpu-append-region nil
  "Delete the selected region and append it to the tpu-cut buffer."
  (interactive)
  (cond ((mark)
	 (let ((beg (region-beginning)) (end (region-end)))
	   (setq tpu-last-deleted-region
		 (concat tpu-last-deleted-region
			 (buffer-substring beg end)))
	   (delete-region beg end)
	   (tpu-unselect t)))
	((tpu-check-match)
	 (let ((beg (tpu-match-beginning)) (end (tpu-match-end)))
	   (setq tpu-last-deleted-region
		 (concat tpu-last-deleted-region
			 (buffer-substring beg end)))
	   (delete-region beg end)
	   (tpu-unset-match)))
	(t
	 (message "No selection active."))))

(defun delete-current-line (num)
  "Delete one or specified number of lines after point.
This includes the newline character at the end of each line.
They are saved for the TPU-edt undelete-lines command."
  (interactive "p")
  (let ((beg (point)))
    (forward-line num)
    (if (not (eq (preceding-char) ?\n))
        (insert "\n"))
    (setq tpu-last-deleted-lines
          (buffer-substring beg (point)))
    (delete-region beg (point))))

(defun delete-to-eol (num)
  "Delete text up to end of line.
With argument, delete up to to Nth line-end past point.
They are saved for the TPU-edt undelete-lines command."
  (interactive "p")
  (let ((beg (point)))
    (forward-char 1)
    (end-of-line num)
    (setq tpu-last-deleted-lines
          (buffer-substring beg (point)))
    (delete-region beg (point))))

(defun delete-to-bol (num)
  "Delete text back to beginning of line.
With argument, delete up to to Nth line-end past point.
They are saved for the TPU-edt undelete-lines command."
  (interactive "p")
  (let ((beg (point)))
    (backward-char 1)
    (beginning-of-line num)
    (setq tpu-last-deleted-lines
          (buffer-substring (point) beg))
    (delete-region (point) beg)))

(defun delete-current-word (num)
  "Delete one or specified number of words after point.
They are saved for the TPU-edt undelete-words command."
  (interactive "p")
  (let ((beg (point)))
    (forward-to-word num)
    (setq tpu-last-deleted-words
          (buffer-substring beg (point)))
    (delete-region beg (point))))

(defun delete-previous-word (num)
  "Delete one or specified number of words before point.
They are saved for the TPU-edt undelete-words command."
  (interactive "p")
  (let ((beg (point)))
    (backward-to-word num)
    (setq tpu-last-deleted-words
          (buffer-substring (point) beg))
    (delete-region beg (point))))

(defun delete-current-char (num)
  "Delete one or specified number of characters after point.  The last
character deleted is saved for the TPU-edt undelete-char command."
  (interactive "p")
  (while (and (> num 0) (not (eobp)))
    (setq tpu-last-deleted-char (char-after (point)))
    (cond (overwrite-mode
	   (picture-clear-column 1)
	   (forward-char 1))
	  (t
	   (delete-char 1)))
    (setq num (1- num))))


;;;
;;;  Undelete
;;;
(defun tpu-paste (num)
  "Insert the last region or rectangle of killed text.
With argument reinserts the text that many times."
  (interactive "p")
  (while (> num 0)
    (cond (tpu-rectangular-p
	   (let ((beg (point)))
	     (save-excursion
	       (picture-yank-rectangle (not overwrite-mode))
	       (message ""))
	     (goto-char beg)))
	  (t
	   (insert tpu-last-deleted-region)))
    (setq num (1- num))))

(defun undelete-lines (num)
  "Insert lines deleted by last TPU-edt line-deletion command.
With argument reinserts lines that many times."
  (interactive "p")
  (let ((beg (point)))
    (while (> num 0)
      (insert tpu-last-deleted-lines)
      (setq num (1- num)))
    (goto-char beg)))

(defun undelete-words (num)
  "Insert words deleted by last TPU-edt word-deletion command.
With argument reinserts words that many times."
  (interactive "p")
  (let ((beg (point)))
    (while (> num 0)
      (insert tpu-last-deleted-words)
      (setq num (1- num)))
    (goto-char beg)))

(defun undelete-char (num)
  "Insert character deleted by last TPU-edt character-deletion command.
With argument reinserts the character that many times."
  (interactive "p")
  (while (> num 0)
    (if overwrite-mode (prog1 (forward-char -1) (delete-char 1)))
    (insert tpu-last-deleted-char)
    (forward-char -1)
    (setq num (1- num))))


;;;
;;;  Replace and Substitute
;;;
(defun tpu-replace nil
  "Replace the selected region with the contents of the cut buffer."
  (interactive)
  (cond ((mark)
	 (let ((beg (region-beginning)) (end (region-end)))
	   (setq tpu-last-replaced-text (buffer-substring beg end))
	   (delete-region beg end)
	   (insert tpu-last-deleted-region)
	   (tpu-unselect t)))
	((tpu-check-match)
	 (let ((beg (tpu-match-beginning)) (end (tpu-match-end)))
	   (setq tpu-last-replaced-text (buffer-substring beg end))
	   (replace-match tpu-last-deleted-region
			  (not case-replace) (not tpu-regexp-p))
	   (tpu-unset-match)))
	(t
	 (message "No selection active."))))

(defun tpu-substitute (num)
  "Replace the selected region with the contents of the cut buffer, and
repeat most recent search.  A numeric argument serves as a repeat count.
A negative argument means replace all occurrences of the search string."
  (interactive "p")
  (cond ((or (mark) (tpu-check-match))
	 (while (and (not (= num 0)) (or (mark) (tpu-check-match)))
	   (let ((beg (point)))
	     (tpu-replace)
	     (if searching-forward (forward-char -1) (goto-char beg))
	     (tpu-search-again))
	   (setq num (1- num))))
	(t
	 (message "No selection active."))))

(defun tpu-add-at-bol (text)
  "Add text to the beginning of each line."
  (interactive "sString to add: ")
  (save-excursion
    (goto-char (point-min))
    (replace-regexp "^" text)))

(defun tpu-add-at-eol (text)
  "Add text to the end of each line."
  (interactive "sString to add: ")
  (save-excursion
    (goto-char (point-min))
    (replace-regexp "$" text)))


;;;
;;;  Position
;;;
(defun next-end-of-line (num)
  "Move to end of line; if at end, move to end of next line.
Accepts a prefix argument for the number of lines to move."
  (interactive "p")
  (forward-char)
  (end-of-line num))

(defun previous-end-of-line (num)
  "Move EOL upward.
Accepts a prefix argument for the number of lines to move."
  (interactive "p")
  (end-of-line (- 1 num)))

(defun tpu-line (num)
  "Move to the beginning of the next line in the current direction.
A repeat count means move that many lines."
  (interactive "p")
  (if tpu-advance (tpu-forward-line num) (tpu-backward-line num)))

(defun tpu-forward-line (num)
  "Move to beginning of next line.
Prefix argument serves as a repeat count."
  (interactive "p")
  (forward-line num))

(defun tpu-backward-line (num)
  "Move to beginning of previous line.
Prefix argument serves as repeat count."
  (interactive "p")
  (forward-line (- num)))

(defun tpu-paragraph (num)
  "Move to the next paragraph in the current direction.
A repeat count means move that many paragraphs."
  (interactive "p")
  (if tpu-advance
      (tpu-next-paragraph num) (tpu-previous-paragraph num)))

(defun tpu-next-paragraph (num)
  "Move to beginning of the next paragraph.
Accepts a prefix argument for the number of paragraphs."
  (interactive "p")
  (beginning-of-line)
  (while (and (not (eobp)) (> num 0))
    (if (re-search-forward "^[ \t]*$" nil t)
	(if (re-search-forward "[^ \t\n]" nil t)
	    (goto-char (match-beginning 0))
	  (goto-char (point-max))))
    (setq num (1- num))))

(defun tpu-previous-paragraph (num)
  "Move to beginning of previous paragraph.
Accepts a prefix argument for the number of paragraphs."
  (interactive "p")
  (end-of-line)
  (while (and (not (bobp)) (> num 0))
    (if (not (and (re-search-backward "^[ \t]*$" nil t)
		  (re-search-backward "[^ \t\n]" nil t)
		  (re-search-backward "^[ \t]*$" nil t)
		  (progn (re-search-forward "[^ \t\n]" nil t)
			 (goto-char (match-beginning 0)))))
	(goto-char (point-min)))
    (setq num (1- num))))

(defun goto-percent (perc)
  "Move point to ARG percentage of the buffer."
  (interactive "NGoto-percentage: ")
  (if (or (> perc 100) (< perc 0))
      (error "Percentage %d out of range 0 < percent < 100" perc)
    (goto-char (/ (* (point-max) perc) 100))))


;;;
;;;  Movement by word
;;;
(defconst tpu-word-separator-list '()
  "List of additional word separators.")
(defconst tpu-skip-chars "^ \t"
  "Characters to skip when moving by word.
Additional word separators are added to this string.")

(defun tpu-word (num)
  "Move to the beginning of the next word in the current direction.
A repeat count means move that many words."
  (interactive "p")
  (if tpu-advance (forward-to-word num) (backward-to-word num)))

(defun forward-to-word (num)
  "Move forward until encountering the beginning of a word.
With argument, do this that many times."
  (interactive "p")
  (while (and (> num 0) (not (eobp)))
    (let* ((beg (point))
	   (end (prog2 (end-of-line) (point) (goto-char beg))))
      (cond ((eolp)
	     (forward-char 1))
	    ((memq (char-after (point)) tpu-word-separator-list)
	     (forward-char 1)
	     (skip-chars-forward " \t" end))
	    (t
	     (skip-chars-forward tpu-skip-chars end)
	     (skip-chars-forward " \t" end))))
    (setq num (1- num))))

(defun backward-to-word (num)
  "Move backward until encountering the beginning of a word.
With argument, do this that many times."
  (interactive "p")
  (while (and (> num 0) (not (bobp)))
    (let* ((beg (point))
	   (end (prog2 (beginning-of-line) (point) (goto-char beg))))
      (cond ((bolp)
	     ( forward-char -1))
	    ((memq (char-after (1- (point)))  tpu-word-separator-list)
	     (forward-char -1))
	    (t
	     (skip-chars-backward " \t" end)
	     (skip-chars-backward tpu-skip-chars end)
	     (if (and (not (bolp)) (= ?  (char-syntax (char-after (point)))))
		 (forward-char -1)))))
    (setq num (1- num))))

(defun tpu-add-word-separators (separators)
  "Add new word separators for TPU-edt word commands."
  (interactive "sSeparators: ")
  (let* ((n 0) (length (length separators)))
    (while (< n length)
      (let ((char (aref separators n))
	    (ss (substring separators n (1+ n))))
	(cond ((not (memq char tpu-word-separator-list))
	       (setq tpu-word-separator-list
		     (append ss tpu-word-separator-list))
	       (cond ((= char ?-)
		      (setq tpu-skip-chars (concat tpu-skip-chars "\\-")))
		     ((= char ?\\)
		      (setq tpu-skip-chars (concat tpu-skip-chars "\\\\")))
		     ((= char ?^)
		      (setq tpu-skip-chars (concat tpu-skip-chars "\\^")))
		     (t
		      (setq tpu-skip-chars (concat tpu-skip-chars ss))))))
	(setq n (1+ n))))))

(defun tpu-reset-word-separators nil
  "Reset word separators to default value."
  (interactive)
  (setq tpu-word-separator-list nil)
  (setq tpu-skip-chars "^ \t"))

(defun tpu-set-word-separators (separators)
  "Set new word separators for TPU-edt word commands."
  (interactive "sSeparators: ")
  (tpu-reset-word-separators)
  (tpu-add-word-separators separators))


;;;
;;;  Movement by sentence
;;;
(defun tpu-sentence (num)
  "Move to the next sentence in the current direction.
A repeat count means move that many sentences."
  (interactive "p")
  ;; (if tpu-advance (forward-sentence num) (backward-sentence num)))
  (if tpu-advance
      (progn
        (forward-sentence num)
        (goto-char (match-end 0)))
    (backward-sentence num)))


;;;
;;;  Window
;;;
(defun beginning-of-window nil
  "Move cursor to top of window."
  (interactive)
  (move-to-window-line 0))

(defun end-of-window nil
  "Move cursor to bottom of window."
  (interactive)
  (move-to-window-line -1))

(defun line-to-bottom-of-window nil
  "Move the current line to the bottom of the window."
  (interactive)
  (recenter -1))

(defun line-to-top-of-window nil
  "Move the current line to the top of the window."
  (interactive)
  (recenter 0))

(defun tpu-next-window nil
  "Move to the next window."
  (interactive)
  (other-window 1))

(defun tpu-previous-window nil
  "Move to the previous window."
  (interactive)
  (select-window (previous-window)))


;;;
;;;  Direction
;;;
(defvar tpu-direction-string "" "Direction en TPU.")
(defun advance-direction nil
  "Set TPU Advance mode so keypad commands move forward."
  (interactive)
  (setq tpu-direction-string " Adv")
  (setq tpu-advance t)
  (tpu-set-search)
  (update-mode-line))

(defun backup-direction nil
  "Set TPU Backup mode so keypad commands move backward."
  (interactive)
  (setq tpu-direction-string " Rev")
  (setq tpu-advance nil)
  (tpu-set-search)
  (update-mode-line))

(require 'picture)
(provide 'cerca-tpu)
