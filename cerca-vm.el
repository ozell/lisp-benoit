;; avant vm
;; (defun vm-menu-create-author-or-recipient-virtual-folder ()
;;   (interactive)
;;   (if (string-match "8.1.2" vm-version) ;; vieille version
;;       (vm-select-folder-buffer)
;;     (vm-select-folder-buffer-and-validate 0 (vm-interactive-p)))
;;   (setq this-command 'vm-create-virtual-folder)
;;   (vm-create-virtual-folder 'author-or-recipient (regexp-quote
;;                                                   (vm-su-from (car vm-message-pointer)))))
;; (defvar vm-menu-author-menu
;;   (let ((title (list "Take Action on Author...")))
;;     `(,@title
;;       ["Mark Messages, Same Author" vm-mark-messages-same-author
;;        vm-message-list]
;;       ["Unmark Messages, Same Author" vm-unmark-messages-same-author
;;        vm-message-list]
;;       ["Virtual Folder, Matching Author" vm-menu-create-author-virtual-folder
;;        vm-message-list]
;;       ["Virtual Folder, Matching Author or Recipient" vm-menu-create-author-or-recipient-virtual-folder
;;        vm-message-list]
;;       ["Send a message" vm-menu-mail-to
;;        vm-message-list]
;;       )))

;; enlever le vm déjà présent s'il y a lieu
(if (featurep 'vm-vars) (unload-feature 'vm-vars t))
(if (featurep 'vm-version) (unload-feature 'vm-version t))
;; obtenir le path du répertoire lisp-benoit
(setq lisp-benoit-dir (file-name-directory load-file-name))
;; chercher le nouveau vm
(let ((path (file-name-directory load-file-name)))
  (add-to-list 'load-path (concat lisp-benoit-dir "vm-8.1.2/lisp"))
  (add-to-list 'load-path (concat lisp-benoit-dir "vm-8.2.0b/lisp"))
  (add-to-list 'load-path (concat lisp-benoit-dir "vm-8.2.0fedora"))
  (add-to-list 'load-path (concat lisp-benoit-dir "vm-8.3/share/emacs/site-lisp/vm"))
  )
(load "vm-autoloads")

;;(require 'bbdb)
;;(add-hook 'vm-mail-mode-hook 'bbdb-define-all-aliases)
;;(bbdb-initialize 'vm)
;;(bbdb-insinuate-vm)

(if (not (featurep 'xemacs))
    (load-library "mailabbrev"))
(add-hook 'mail-setup-hook 'mail-abbrevs-setup)

;;(if (not (and (boundp 'emacs-major-version) (<= emacs-major-version 21)))
;;    (require 'vm-autoloads))
;;(require 'cl "cl" t) ;; pour la fonction "defun*" dont a besoin vm
(defalias 'defun* 'cl-defun)
(if (require 'vm "vm" t)
    (progn
(require 'vm-version "vm-version" t)
(if (and (boundp 'avec-u-vm-color) avec-u-vm-color)
    (progn
      (require 'u-vm-color "u-vm-color" t)
      (add-hook 'vm-summary-mode-hook 'u-vm-color-summary-mode) ;; obsolete since 8.2.0 ; remplacé par vm-summary-enable-faces ci-dessous
      (add-hook 'vm-select-message-hook 'u-vm-color-fontify-buffer)
      ;; (defadvice vm-fill-paragraphs-containing-long-lines
      ;;     (after u-vm-color activate)
      ;;   (u-vm-color-fontify-buffer))
      ;;(vm-summary-faces-mode t)
      )
  (setq-default vm-summary-enable-faces t
                ;;vm-use-lucid-highlighting t
                )
  )
(setq-default vm-folder-directory "~/Mail/"
              vm-crash-box "~/Mail/INBOX.CRASH"
              vm-primary-inbox "~/Mail/in"
              ;;vm-postponed-folder "postponed"
              vm-configure-pixmapdir (concat lisp-benoit-dir "vm-pixmaps")
              vm-toolbar-pixmap-directory (concat lisp-benoit-dir "vm-pixmaps")
              vm-image-directory (concat lisp-benoit-dir "vm-pixmaps")
              send-mail-function 'sendmail-send-it
              mail-header-separator "----texte suit cette ligne----"
              vm-toolbar-orientation 'top
              toolbar-mail-reader 'vm
              tool-bar-style 'image
              vm-auto-next-message nil
              vm-auto-get-new-mail nil
              vm-preview-lines t
              vm-keep-sent-messages t
              vm-tale-is-an-idiot t
              vm-folders-summary-database nil
              vm-url-regexp
              (concat
               "<URL:\\([^>\n]+\\)>"
               "\\|"
               "\\(\\(file\\|sftp\\|ftp\\|http\\|https\\|news\\|www\\)://[^ \t\n\f\r\"<>()]*[^ \t\n\f\r\"<>.!?(){}]\\)"
               "\\|"
               "\\(mailto:[^ \t\n\f\r\"<>()]*[^ \t\n\f\r\"<>.!?(){}]\\)"
               "\\|"
               "\\(file:/[^ \t\n\f\r\"<>()]*[^ \t\n\f\r\"<>.!?(){}]\\)"
               ;; appending...
               "\\|"
               "\\(//www\\.[^ \t\n\f\r\"<>()]*[^ \t\n\f\r\"<>.!?(){}]\\)"
               "\\|"
               "\\(www\\.[^ \t\n\f\r\"<>()]*[^ \t\n\f\r\"<>.!?(){}]\\)"
               )
              vm-url-browser (if (string-match "darwin" system-configuration) "open" "firefox")
              ;; vm-summary-format "%n %*%a %-17.17F %-3.3m %2d %4l/%-5c %I\"%s\"\n"
              ;; vm-summary-format "%n %*%A %-17.17F (%-10.10t) %w %2d %-3.3m %H %4l/%-5c %I\"%s\"\n"
              ;; vm-summary-format "%n %*%A %-17.17F (%-9.9t) %2d %-3.3m %y %H %z %4l/%-5c %I\"%s\"\n"
              ;; vm-summary-format "%n %*%A %-17.17F %2d %-3.3m %2.-2y %H %3.3z%4l/%-5c%I\"%s\"\n"
              ;; vm-summary-format "%n %*%A %-20.20F %2d/%02M/%2.-2y %H %3.3z%4l/%-5c%I\"%s\"\n"
              ;; vm-summary-format "%n %*%a %-20.20F %2d/%02M/%y %H %3.3z %4l/%-5c%I\"%s\"\n"
              vm-summary-format
              (if (string-match "8.1.2" vm-version) ;; vieille version
                  "%n %*%a %-20.20F %2d/%02M/%y %H %3.3z %P %4l/%-5c%I\"%s\"\n"
                "%n %*%b %-20.20F %2d/%02M/%y %H %3.3z %P %4l/%-5c%I\"%s\"\n"
                )
              ;;T vm-summary-arrow ""
              ;;vm-summary-attachment-indicator "+"
              vm-included-text-attribution-format "De %F: (le %y/%M/%d à %H %z)\n"
              vm-included-text-prefix "> "
              vm-forwarding-digest-type nil
              vm-forwarding-subject-format "message `forwarded' de %F"
              vm-mime-8bit-text-transfer-encoding '8bit
              ;; vm-mime-8bit-composition-charset "iso-8859-1" ;; This variable is unused in XEmacs/MULE and FSF Emacs starting with version 20.
              vm-display-xfaces t
              vm-berkeley-mail-compatibility t
              ;; vm-mime-composition-armor-from-lines t
              ;; vm-print-command "lpr"
              ;; vm-print-command-switches nil
              vm-frame-per-completion nil
              vm-mime-default-face-charsets
              '("iso8859-1" "iso-8859-1" "iso-8859-2" "iso-8859-3" "iso-8859-7" "iso-8859-15"
                "windows-1251" "windows-1252" "windows-1256" "windows-1258" "apple roman")
              ;!!  "us-ascii" "ascii" "gb2312" "Cp1252" "x-unknown" "x-user-defined")
              ;!!vm-mime-charset-converter-alist
              ;!!'(
              ;!!;;  ;;("cp-1252"      "iso-8859-1" "iconv -c -f CP1252       -t iso-8859-1")
              ;!!;;  ;;("windows-1252" "iso-8859-1" "iconv -c -f windows-1252 -t iso-8859-1")
              ;!!;;  ;;("windows-1256" "iso-8859-1" "iconv -c -f windows-1256 -t iso-8859-1")
              ;!!;;  ;;("windows-1258" "iso-8859-1" "iconv -c -f windows-1258 -t iso-8859-1")
              ;!!  ("UTF-8"        "iso-8859-1" "/usr/bin/iconv -c -f utf-8        -t iso-8859-1 | sed \"s/~/'/g\"")
              ;!!;;  ;;("text/html" "text/plain" "w3m -T text/html -dump")
              ;!!  )
              ;;vm-mime-charset-font-alist
              ;;'(
              ;;  ("windows-1252" . "-*-*-*-r-*-*-*-*-*-*-*-*-microsoft-cp1252")
              ;;  ("iso-8859-3" . "-*-*-medium-r-normal-*-14-*-100-100-c-*-iso8859-3")
              ;;  ("iso-8859-7" . "-*-*-medium-r-normal-*-14-*-100-100-c-*-iso8859-7")
              ;;  )
              ;; vm-coding-system-priorities '(iso-8859-1 iso-8859-15 iso-latin-1 iso-latin-9 mule-utf-8)
              vm-coding-system-priorities '(utf-8 iso-8859-1 iso-8859-15 iso-latin-1 iso-latin-9 mule-utf-8)
              ;;'(iso-8859-1 iso-8859-15 iso-latin-1 iso-latin-9 mule-utf-8 mac-roman)
              mm-coding-system-priorities vm-coding-system-priorities
              vm-mime-alternative-show-method 'all
              ;;vm-mime-alternative-show-method '(favorite "text/plain" "text/html")
              vm-mime-alternative-yank-method '(favorite "text/plain" "text/html")
              ;;vm-mime-internal-content-type-exceptions '("text/html")
              vm-mime-auto-displayed-content-type-exceptions '("text/html")
              ;; vm-mime-button-format-alist
              ;; '(("text" . "%-35.35(%d, %c%) [%k to %a]")
              ;;   ("multipart/alternative" . "%-35.35(%d%) [%k to %a]")
              ;;   ("multipart/digest" . "%-35.35(%d, %n message%s%) [%k to %a]")
              ;;   ("multipart" . "%-35.35(%d, %n part%s%) [%k to %a]")
              ;;   ("message/partial" . "%-35.35(%d, part %N (of %T)%) [%k to %a]")
              ;;   ("message/external-body" . "%-35.35(%d%) [%k to %a (%x)]")
              ;;   ("message" . "%-35.35(%d%) [%k to %a]")
              ;;   ("audio" . "%-35.35(%d%) [%k to %a]")
              ;;   ("video" . "%-35.35(%d%) [%k to %a]")
              ;;   ("image" . "%-35.35(%d%) [%k to %a]")
              ;;   ("application/octet-stream" . "%-35.35(%d, %f%) [%k to %a]")
              ;;   ("application/pdf" . "%-35.35(%d, %f%) [%k pour %a]")
              ;;   )
              ;; vm-mime-button-format-alist
              ;; '(("text" . "%-60.60(%t (%c): %f, %d%) %10.10([%a]%)")
              ;;   ("multipart/alternative" . "%-50.50(%d%) %20.20([%a]%)")
              ;;   ("multipart/digest" . "%-50.50(%d, %n message%s%) %20.20([%a]%)")
              ;;   ("multipart" . "%-50.50(%d, %n part%s%) %20.20([%a]%)")
              ;;   ("message/partial" . "%-50.50(%d, part %N (of %T)%) %20.20([%a]%)")
              ;;   ("message/external-body" . "%-55.55(%d%) [%a (%x)]")
              ;;   ("message" . "%-50.50(%d%) %20.20([%a]%)")
              ;;   ("audio" . "%-55.55(%t: %f, %d%) %10.10([%a]%)")
              ;;   ("video" . "%-55.55(%t: %f, %d%) %10.10([%a]%)")
              ;;   ("image" . "%-55.55(%t: %f, %d%) %10.10([%a]%)")
              ;;   ("application/pdf" . "%-75.75(%d, %f%) [%k pour %a]")
              ;;   ("application" . "%-55.55(%t: %f, %d%) %10.10([%a]%)")
              ;;   )
              vm-infer-mime-types t
              vm-mime-type-completion-alist
              (append
               vm-mime-type-completion-alist
               '(
                 ("text/calendar")
                 ("application/msword")
                 ))
              vm-mime-attachment-auto-type-alist
              (append
               vm-mime-attachment-auto-type-alist
               '(
                 ("\\.tex" . "application/x-tex")
                 ("\\.dvi" . "application/x-dvi")
                 ("\\.rtf" . "application/rtf")
                 ("\\.doc" . "application/msword")
                 ("\\.xls" . "application/msexcel")
                 ("\\.sxw" . "application/soffice")
                 ("\\.vu"  . "application/x-vu")
                 ("\\.odt" . "application/vnd.oasis.opendocument.text")
                 ("\\.ott" . "application/vnd.oasis.opendocument.text-template")
                 ("\\.oth" . "application/vnd.oasis.opendocument.text-web")
                 ("\\.odm" . "application/vnd.oasis.opendocument.text-master")
                 ("\\.odg" . "application/vnd.oasis.opendocument.graphics")
                 ("\\.otg" . "application/vnd.oasis.opendocument.graphics-template")
                 ("\\.odp" . "application/vnd.oasis.opendocument.presentation")
                 ("\\.otp" . "application/vnd.oasis.opendocument.presentation-template")
                 ("\\.ods" . "application/vnd.oasis.opendocument.spreadsheet")
                 ("\\.ots" . "application/vnd.oasis.opendocument.spreadsheet-template")
                 ("\\.odc" . "application/vnd.oasis.opendocument.chart")
                 ("\\.odf" . "application/vnd.oasis.opendocument.formula")
                 ("\\.odb" . "application/vnd.oasis.opendocument.database")
                 ("\\.odi" . "application/vnd.oasis.opendocument.image")
                 ("\\.oxt" . "application/vnd.openofficeorg.extension")

                 ))
              vm-mime-external-content-types-alist
              (cond ((string-match "darwin" system-configuration)
                     '(
                       ("application/ms-tnef" "tnef -v -C /tmp %f")
                       ("application/pdf" "open %f")
                       ("application/postscript" "open %f")
                       ("application/x-dvi" "xdvi %f")
                       ("application/X-MS-Excel" "open %f")
                       ("application/msexcel" "open %f")
                       ("application/msexcell" "open %f")
                       ("application/x-msexcel" "open %f")
                       ("application/msword" "open %f")
                       ("application/rtf" "open %f")
                       ("application/vnd.openxmlformats-officedocument.wordprocessingml.document" "open %f")
                       ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" "open %f")
                       ("application/vnd.openxmlformats-officedocument.presentationml.presentation" "open %f")
                       ("application/vnd.msword" "open %f")
                       ("application/vnd.ms-excel" "open %f")
                       ("application/vnd.ms-powerpoint" "open %f")
                       ("application/vnd.oasis.opendocument.text" "open %f")
                       ("application/vnd.oasis.opendocument.spreadsheet" "open %f")
                       ("application/vnd.sun.xml.calc" "open %f")
                       ("application/vnd.sun.xml.draw" "open %f")
                       ("application/vnd.sun.xml.impress" "open %f")
                       ("application/vnd.sun.xml.math" "open %f")
                       ("application/vnd.sun.xml.writer" "open %f")
                       ("application/vnd.ms-xpsdocument" "open %f")
                       ("application/x-vu" "Vu -web %f")
                       ("image/gif"  "display %f")
                       ("image/jpeg" "display %f")
                       ("image/jpg"  "display %f")
                       ("image/png"  "display %f")
                       ("image/tiff" "display %f")
                       ("image"      "display %f")
                       ("text/richtext" "open %f")
                       ("text/calendar" "ln %f %f.ics && open %f.ics")
                       ("audio/wav"   "mplayer %f")
                       ("audio/x-wav" "mplayer %f")
                       ("audio/m4a"   "mplayer %f")
                       ("audio/mpeg"  "mplayer %f")
                       ("video"       "mplayer %f")
                       ("video/mpeg" "plaympeg %f")
                       ("message/external-body" "open %f")
                       ("text/html" "open %f")
                       ))
                      (t
                     '(
                       ("application/ms-tnef" "tnef -v -C /tmp %f")
                       ("application/pdf" "okular %f")
                       ("application/postscript" "ghostview %f")
                       ("application/x-dvi" "xdvi %f")
                       ("application/X-MS-Excel" "soffice %f")
                       ("application/msexcel" "soffice %f")
                       ("application/msexcell" "soffice %f")
                       ("application/x-msexcel" "soffice %f")
                       ("application/msword" "soffice %f")
                       ("application/rtf" "soffice %f")
                       ("application/soffice" "soffice %f")
                       ("application/vnd.openxmlformats-officedocument.wordprocessingml.document" "soffice %f")
                       ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" "soffice %f")
                       ("application/vnd.openxmlformats-officedocument.presentationml.presentation" "soffice %f")
                       ("application/vnd.msword" "soffice %f")
                       ("application/vnd.ms-excel" "soffice %f")
                       ("application/vnd.ms-powerpoint" "soffice %f")
                       ("application/vnd.oasis.opendocument.text" "soffice %f")
                       ("application/vnd.oasis.opendocument.spreadsheet" "soffice %f")
                       ("application/vnd.sun.xml.calc" "soffice %f")
                       ("application/vnd.sun.xml.draw" "soffice %f")
                       ("application/vnd.sun.xml.impress" "soffice %f")
                       ("application/vnd.sun.xml.math" "soffice %f")
                       ("application/vnd.sun.xml.writer" "soffice %f")
                       ("application/vnd.ms-xpsdocument" "okular %f")
                       ("application/x-vu" "Vu -web %f")
                       ("image/gif"  "display %f")
                       ("image/jpeg" "display %f")
                       ("image/jpg"  "display %f")
                       ("image/png"  "display %f")
                       ("image/tiff" "display %f")
                       ("image"      "display %f")
                       ("text/richtext" "soffice %f")
                       ;;("text/calendar" "korganizer %f")
                       ("audio/wav"   "mplayer %f")
                       ("audio/x-wav" "mplayer %f")
                       ("audio/m4a"   "mplayer %f")
                       ("audio/mpeg"  "mplayer %f")
                       ("video"       "mplayer %f")
                       ("video/mpeg" "plaympeg %f")
                       ("message/external-body" "firefox %f")
                       ("text/html" "firefox %f")
                       ("application/zip" "file-roller %f")
                       ))
                   )
              ;;vm-mime-type-converter-alist
              ;;'(
              ;;  ("image/jpeg" "image/gif"  "jpeg2gif")
              ;;  ;;("text/html"  "text/plain" "striptags")
              ;;  ;;("text/html"  "text/plain" "cat")
              ;;  ;;("application/msword" "text/plain" "catdoc -")
              ;;  )
              vm-stunnel-configuration-file (format "%s/%s%d/vm-stunnel.conf"
                                                    (or (getenv "TMPDIR") "/tmp")
                                                    (if (featurep 'xemacs) "gsrvdir" "emacs")
                                                    (user-uid))
              )
(if (string-match "8.1.2" vm-version) ;; vieille version
    (progn
      (setq-default
       vm-include-text-from-presentation t
       vm-auto-displayed-mime-content-type-exceptions '("text/html" "text/richtext")
       vm-mime-alternative-select-method 'favorite-internal
       ;; vm-mime-alternative-select-method 'best-internal ;; la valeur de défaut
       )))
)) ;; require vm

(defun vm-mime-display-internal-image/jpg (layout)
  (vm-mime-display-internal-image-xxxx layout 'jpeg "JPEG"))

(defun vm-revisit-folder ()
  (interactive)
  ;; Select the folder buffer corresponding to the current buffer
  (vm-select-folder-buffer)
  ;; recharger le fichier (probablement modifié en sauvegardant un courriel envoyé)
  (vm-visit-folder (buffer-file-name))
  )

;;T  ;; 2016-24-29
;;T  ;; De https://bugs.launchpad.net/vm/+bug/878471
;;T  (defconst vm-mime-charset-completion-alist
;;T    (mapcar (lambda (a) (list (car a)))
;;T            vm-mime-mule-charset-to-coding-alist))

;;T  ;; http://www.pietvanoostrum.com/emacs/sending-non-ascii-text-in-emails-with-emacs-and-vm/en/
;;T  (defun vm-sort-coding-systems-predicate (a b)
;;T    (> (length (memq a vm-coding-system-priorities))
;;T       (length (memq b vm-coding-system-priorities))))

;;T  ;; De http://www.emacswiki.org/emacs/ViewMailOtherCustomizations
;;T  ;; First, don't display iso-8859-1 as-is in default face
;;T  (delete "iso-8859-1" vm-mime-default-face-charsets)
;;T  ;; Then substitute windows-1252 for iso-8859-1
;;T  (add-to-list 'vm-mime-mule-charset-to-coding-alist
;;T               '("iso-8859-1" windows-1252))

;;T  ;; The next line is for a noautoload vm.elc. Otherwise use "vm-mime".
;;T  ;;(eval-after-load "vm"
;;T  ;; The next line is for an autoload (default) vm.elc. Otherwise use "vm".
;;T  (eval-after-load "vm-mime"
;;T    '(defun vm-determine-proper-charset (beg end)
;;T       (save-excursion
;;T         (save-restriction
;;T           (narrow-to-region beg end)
;;T           (catch 'done
;;T             (goto-char (point-min))
;;T             (if (or vm-xemacs-mule-p
;;T                     (and vm-fsfemacs-mule-p enable-multibyte-characters))
;;T                 (let ((charsets (delq 'compound-text (find-coding-systems-region
;;T                                                       (point-min) (point-max)))))
;;T                   (cond ((equal charsets '(undecided))
;;T                          ;;"us-ascii")
;;T                          "iso-8859-1")
;;T                         (t
;;T                          (setq charsets
;;T                                (sort charsets 'vm-sort-coding-systems-predicate))
;;T                          (while charsets
;;T                            (let ((cs (coding-system-get (pop charsets) 'mime-charset)))
;;T                              (if cs
;;T                                  (throw 'done (symbol-name cs))))))))
;;T               (and (re-search-forward "[^\000-\177]" nil t)
;;T                    (throw 'done (or vm-mime-8bit-composition-charset
;;T                                     "iso-8859-1")))
;;T               (throw 'done vm-mime-7bit-composition-charset)))))))


;; Redéfinir vm-save-folder en oubliant le ''modtime'' du folder qu'on visite
;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Modification-Time.html#Modification-Time

(defun vm-save-folderNOT (&optional prefix)
  "Save current folder to disk.
Deleted messages are not expunged.
Prefix arg is handled the same as for the command `save-buffer'.

When applied to a virtual folder, this command runs itself on
each of the underlying real folders associated with the virtual
folder."
  (interactive (list current-prefix-arg))
  (vm-select-folder-buffer)
  (vm-check-for-killed-summary)
  (vm-display nil nil '(vm-save-folder) '(vm-save-folder))
  (if (eq major-mode 'vm-virtual-mode)
      (vm-virtual-save-folder prefix)
    (if (buffer-modified-p)
	(let (mp (newlist nil) (buffer-undo-list t))
	  (cond ((eq vm-folder-access-method 'pop)
		 (vm-pop-synchronize-folder t t t nil))
		((eq vm-folder-access-method 'imap)
		 (vm-imap-synchronize-folder t t t nil t)))
          ;; remove the message summary file of Thunderbird and force
	  ;; it to rebuild it.  Expect error if Thunderbird is active.
          (let ((msf (concat buffer-file-name ".msf")))
            (if (and (eq vm-sync-thunderbird-status t)
		     (file-exists-p msf))
                (delete-file msf)))
          ;; ajout pour encfs BO
          (clear-visited-file-modtime) ;; ajout pour encfs BO
	  ;; stuff the attributes of messages that need it.
	  (message "Stuffing attributes...")
	  (vm-stuff-folder-attributes nil)
	  (message "Stuffing attributes... done")
	  ;; stuff bookmark and header variable values
	  (if vm-message-list
	      (progn
		;; get summary cache up-to-date
		(vm-update-summary-and-mode-line)
		(vm-stuff-bookmark)
		(vm-stuff-pop-retrieved)
		(vm-stuff-imap-retrieved)
		(vm-stuff-last-modified)
		(vm-stuff-header-variables)
		(vm-stuff-labels)
		(vm-stuff-summary)
		(and vm-message-order-changed
		     (vm-stuff-message-order))))
	  (message "Saving...")
	  (let ((vm-inhibit-write-file-hook t)
		(oldmodebits (and (fboundp 'default-file-modes)
				  (default-file-modes))))
	    (unwind-protect
		(progn
		  (and oldmodebits (set-default-file-modes
				    vm-default-folder-permission-bits))
		  (save-buffer prefix))
	      (and oldmodebits (set-default-file-modes oldmodebits))))
	  (vm-set-buffer-modified-p nil)
	  ;; clear the modified flag in virtual folders if all the
	  ;; real buffers associated with them are unmodified.
	  (let ((b-list vm-virtual-buffers) rb-list one-modified)
	    (save-excursion
	      (while b-list
		(if (null (cdr (vm-buffer-variable-value (car b-list)
							 'vm-real-buffers)))
		    (vm-set-buffer-modified-p nil (car b-list))
		  (set-buffer (car b-list))
		  (setq rb-list vm-real-buffers one-modified nil)
		  (while rb-list
		    (if (buffer-modified-p (car rb-list))
			(setq one-modified t rb-list nil)
		      (setq rb-list (cdr rb-list))))
		  (if (not one-modified)
		      (vm-set-buffer-modified-p nil (car b-list))))
		(setq b-list (cdr b-list)))))
	  (vm-clear-modification-flag-undos)
	  (setq vm-messages-not-on-disk 0)
	  (setq vm-block-new-mail nil)
	  (vm-write-index-file-maybe)
	  (if (and vm-folders-summary-database buffer-file-name)
	      (progn
		(vm-compute-totals)
		(vm-store-folder-totals buffer-file-name (cdr vm-totals))))
	  (vm-update-summary-and-mode-line)
	  (and (zerop (buffer-size))
	       vm-delete-empty-folders
	       buffer-file-name
	       (or (eq vm-delete-empty-folders t)
		   (y-or-n-p (format "%s is empty, remove it? "
				     (or buffer-file-name (buffer-name)))))
	       (condition-case ()
		   (progn
		     (delete-file buffer-file-name)
		     (vm-delete-index-file)
		     (clear-visited-file-modtime)
		     (message "%s removed" buffer-file-name))
		 ;; no can do, oh well.
		 (error nil)))
	  )
      (message "No changes need to be saved"))))

;; ;;http://stackoverflow.com/questions/2284703/emacs-how-to-disable-file-changed-on-disk-checking

;; (defun ask-user-about-supersession-threat (fn)
;;   "Ask a user who is about to modify an obsolete buffer what to do.
;; This function has two choices: it can return, in which case the modification
;; of the buffer will proceed, or it can (signal 'file-supersession (file)),
;; in which case the proposed buffer modification will not be made.
;;
;; You can rewrite this to use any criterion you like to choose which one to do.
;; The buffer in question is current when this function is called."
;;   (discard-input)
;;   (save-window-excursion
;;     (let ((prompt
;; 	   (format "%s changed on disk; \
;; really edit the buffer? (y, n, r or C-h) "
;; 		   (file-name-nondirectory fn)))
;; 	  (choices '(?y ?n ?r ?? ?\C-h))
;; 	  answer)
;;       (while (null answer)
;; 	(setq answer (read-char-choice prompt choices))
;; 	(cond ((memq answer '(?? ?\C-h))
;; 	       (ask-user-about-supersession-help)
;; 	       (setq answer nil))
;; 	      ((eq answer ?r)
;; 	       ;; Ask for confirmation if buffer modified
;; 	       (revert-buffer nil (not (buffer-modified-p)))
;; 	       (signal 'file-supersession
;; 		       (list "File reverted" fn)))
;; 	      ((eq answer ?n)
;; 	       (signal 'file-supersession
;; 		       (list "File changed on disk" fn)))))
;;       (message
;;        "File on disk now will become a backup file if you save these changes.")
;;       (setq buffer-backed-up nil))))
;;
;;

;; 'e' <= '$-e'
(define-key vm-mime-reader-map "e" 'vm-mime-reader-map-display-using-external-viewer)

(provide 'cerca-vm)
