;;; Fonctions reliees a la toolbar.
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

;; (defun cerca-toolbar-add-button nil
;;   "Ajoute des boutons a la \"toolbar\"."
;;   (interactive)
;;   (if (equal (device-type) 'x)
;;       (progn
;; ;        (toolbar-add-item '[:style 2D :size 2] 4)
;; ;        (toolbar-add-item '[:style 2D :size 2] 8)
;; ;        (toolbar-add-item '[toolbar-last-win-icon
;; ;                            pop-window-configuration
;; ;                            t "Back to last window configuration"] 10)
;; ;        (toolbar-add-item '[toolbar-next-win-icon
;; ;                            unpop-window-configuration
;; ;                            t "Forward to next window configuration"] 11)
;; ;        (toolbar-add-item '[:style 2D :size 2] 12)
;;         )))

;; (toolbar-make-button-list
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-up.xpm"
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-dn.xbm"
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-dn.xbm"
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-up.xpm"
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-dn.xbm"
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-xx.xpm")
;;
;; (toolbar-make-button-list
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/next-win-up.xpm"
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/next-win-dn.xbm"
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/next-win-xx.xpm"
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/next-win-cap-up.xpm"
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/next-win-cap-xx.xpm"
;;  "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/next-win-xx.xpm")
;;
;; (setq edit-toolbar-added-buttons-alist
;;       '((edit-toolbar-button49600
;;          "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-up.xpm"
;;          "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-dn.xbm"
;;          "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-dn.xbm"
;;          "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-up.xpm"
;;          "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-dn.xbm"
;;          "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-xx.xpm")
;;         (edit-toolbar-button49601 "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-cap-up.xpm"
;;                                   "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-cap-up.xpm"
;;                                   "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-cap-up.xpm"
;;                                   "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-cap-up.xpm"
;;                                   "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-cap-up.xpm"
;;                                   "~/xemacs-20.4/lib/xemacs-20.4/etc/toolbar/last-win-cap-up.xpm")))
;;
;; (mapcar
;;  (lambda (cons)
;;    (setf (symbol-value (car cons)) (toolbar-make-button-list (cdr cons)))
;;    )
;;  edit-toolbar-added-buttons-alist)
;;
;; (set-specifier default-toolbar
;;                '([edit-toolbar-button49601 vm t "new"]
;;                  [toolbar-file-icon toolbar-open t "Open a file"]
;;                  [toolbar-folder-icon toolbar-dired t "View directory"]
;;                  [toolbar-disk-icon toolbar-save t "Save buffer"]
;;                  [toolbar-printer-icon toolbar-print t "Print buffer"]
;;                  [toolbar-cut-icon toolbar-cut t "Kill region"]
;;                  [toolbar-copy-icon toolbar-copy t "Copy region"]
;;                  [toolbar-paste-icon toolbar-paste t "Paste from clipboard"]
;;                  [toolbar-undo-icon toolbar-undo t "Undo edit"]
;;                  [toolbar-spell-icon toolbar-ispell t "Spellcheck"]
;;                  [toolbar-replace-icon toolbar-replace t "Replace text"]
;;                  [toolbar-mail-icon toolbar-mail t "Mail"]
;;                  [toolbar-info-icon toolbar-info t "Information"]
;;                  [toolbar-compile-icon toolbar-compile t "Compile"]
;;                  [toolbar-debug-icon toolbar-debug t "Debug"]
;;                  [toolbar-news-icon toolbar-news t "News"]))

(defun cerca-toolbar-spec nil
  "Enlever et remettre la \"toolbar\"."
  (interactive)
  (if (equal (device-type) 'x)
      (if (specifier-instance default-toolbar)
          (remove-specifier default-toolbar)
        (set-specifier default-toolbar initial-toolbar-spec)
        (cerca-toolbar-add-button))
    ))

(defun cerca-scrollbar-spec nil
  "Enlever et remettre les \"scrollbars\"."
  (interactive)
  (if (= (specifier-instance scrollbar-width) 0)
      (progn
        (set-specifier scrollbar-width 15)
        (set-specifier scrollbar-height 15))
    (set-specifier scrollbar-width 0)
    (set-specifier scrollbar-height 0)))

(defun toolbar-print ()
  "Print without having to touch the keyboard."
  (interactive)
  (popup-dialog-box
   `(,(concat "Print:\n        " lpr-command lpr-switches)
     ["pretty print Buffer" (ps-print-buffer-with-faces) t]
     ["lpr Buffer" (lpr-buffer) t]
     ["lpr Region" (lpr-region) (mark)]
     nil
     ["Cancel" (message "Quit") t])))

;(defun cerca-vm-mouse-send-url-to-netscape (url &optional new-netscape new-window)
;  ;; Change commas to %2C to avoid confusing Netscape -remote.
;  (while (string-match "," url)
;    (setq url (replace-match "%2C" nil t url)))
;  (cond
;   ((string-match "^//" url) (setq url (concat "http:" url)))
;   ((not (string-match ":" url)) (setq url (concat "http://" url))))
;  (message "Sending %s to %s..." url vm-netscape-program)
;  (if new-netscape
;      (apply 'vm-run-background-command vm-netscape-program
;            (append vm-netscape-program-switches (list url)))
;    (or (equal 0 (apply 'vm-run-command vm-netscape-program
;                       (append vm-netscape-program-switches
;                               (list "-remote"
;                                     (concat "openURL(" url
;                                             (if new-window ",new-window" "")
;                                             ")")))))
;       (vm-mouse-send-url-to-netscape url t new-window)))
;  (message "Sending %s to %s... done" url vm-netscape-program))

;;; enriched.el
(defun popup-enriched-menu (event)
  "Pop up a copy of the Text Properties menu (from the menubar) where the mouse is clicked."
  (interactive "e")
  (let ((window (and (event-over-text-area-p event) (event-window event)))
        (bmenu nil))
    (or window
        (error "Pointer must be in a normal window"))
    (select-window window)
    (if current-menubar
        (setq bmenu (assoc "Text Properties" current-menubar)))
    (if (null bmenu)
        (setq bmenu (assoc "Text Properties" default-menubar)))
    (if (null bmenu)
        (setq bmenu (assoc "Text Properties" (assoc "Edit" default-menubar))))
    (if (null bmenu)
        (setq bmenu (assoc "Text Properties" (assoc "%_Edit" default-menubar))))
    (if (null bmenu)
        (error "Can't find the Text Properties menu"))
    (popup-menu bmenu)))

(provide 'cerca-x11)
