;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; (c-add-style "cerca"
;;              '(
;;                (c-basic-offset . 3)
;;                (c-comment-only-line-offset . (0 . 0))
;;                (c-offsets-alist . ((statement-block-intro . +)
;;                                    (knr-argdecl-intro . 5)
;;                                    (substatement-open . 0)
;;                                    (label . 0)
;;                                    (statement-case-open . +)
;;                                    (statement-cont . 0)
;;                                    (inline-open . 0)
;;                                    (arglist-intro . c-lineup-arglist-intro-after-paren)
;;                                    (arglist-close . c-lineup-arglist)
;;                                    ))
;;                (c-special-indent-hook . c-gnu-impose-minimum)
;;                ))

(c-add-style "envcan"
             '(
               (c-basic-offset . 4)
               (c-comment-only-line-offset . (0 . 0))
               (c-offsets-alist . ((statement-block-intro . +)
                                   (knr-argdecl-intro . 5)
                                   (substatement-open . 0)
                                   (label . 0)
                                   (statement-case-open . +)
                                   (statement-cont . 0)
                                   (inline-open . 0)
                                   (arglist-intro . c-lineup-arglist-intro-after-paren)
                                   (arglist-close . c-lineup-arglist)
                                   ))
               (c-special-indent-hook . c-gnu-impose-minimum)
               ))

(
 (c-mode . ((c-file-style . "envcan")
            ;;(subdirs . nil)      ; if nil => only for this directory
	    ))
 (c++-mode . ((c-file-style . "envcan")
              ;;(subdirs . nil)      ; if nil => only for this directory
	      ))

 ;; Pour tous les fichiers ...
 ;; (nil . ((indent-tabs-mode . t) ; Indentation can insert tabs if this is non-nil.
 ;;         (fill-column . 80)     ; Column beyond which automatic line-wrapping should happen.
 ;;         ))

 ;; ex: https://www.gnu.org/software/emacs/manual/html_node/emacs/Directory-Variables.html
 ;; ("src/imported"
 ;;  . ((nil . ((change-log-default-name
 ;;              . "ChangeLog.local")))))

 ;; voir aussi "editorconfig"
 ;; https://stackoverflow.com/questions/45706513/emacs-directory-variables-excluding-particular-subdir
 )
