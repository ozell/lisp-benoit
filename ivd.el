(defvar variable-list nil "List of variables used for ivd-insert-variable-dump")

(defun ivd-select-variable-list()
  "\nSelects the *variable-list* buffer in which C variables may be entered,
one per line, optionally followed by space and a printf type specifier
(default \"%d\").  Upon invocation of the function ivd-insert-variable-dump,
a printf statement with the variables listed in the *variable-list* window
will be generated and inserted after point in the current buffer.  It
will have the form:  printf(\"@var1=%d var2=%s etc.\\n\", var1, var2, etc.);
\nA sample *variable-list* buffer might look like:
\nfoo\nvar 0x%08x\n(char)bar %c
\nSince spaces separate the variable name from its type specifier, no spaces
may exist in either the variable name or type specifier.  The function
ivd-insert-in-variable-list can be used to insert the variable around point in
the current buffer into the variable list.  Also, leading space effectively
comments out a variable in the list (which can be useful for big lists)."
  (interactive)
  (if (not (equal (buffer-name) "*variable-list*"))
      (switch-to-buffer-other-window "*variable-list*")
    )
  )

(defun build-variable-list()
  "Build the variable-list from the contents of the *variable-list* buffer."
  (save-excursion
    (set-buffer (get-buffer-create "*variable-list*"))
    (goto-char (point-min))
    (let
        (
         (lines (count-lines (point-min) (point-max)))
         (i 0)
         possible-variable
         possible-format
         )
      (while (< i lines)
        (beginning-of-line)
        ;; Get the variable name.
        (setq possible-variable (re-around-point "[^ \t\n]" "[ \t\n]"))
        (if possible-variable
            ;; We found a variable.
            (progn
              ;; Get any format string.
              (if (re-search-forward "[ \t][ \t]*" (eol-location) t)
                  ;; We found space.
                  (progn
                    (setq possible-format
                          (re-around-point "[^ \t\n]" "[ \t\n]")
                          )
                    (if (not possible-format)
                        (setq possible-format "%d")
                      )
                    )
                ;; No space, so no format string
                (setq possible-format "%d")
                )
              ;; Add variable/format list to the master list.
              (setq variable-list
                    (append
                     variable-list
                     (list (list possible-variable possible-format))
                     )
                    )
              )
          )
        ;; Check the next line for variables.
        (next-line 1)
        (beginning-of-line)
        (setq i (1+ i))
        )
      )
    ;; Reset the state of the buffer to not modified
    (set-buffer-modified-p nil)
    ))

(defun ivd-insert-variable-dump (&optional arg)
  "Insert a printf statement composed of variables in the *variable-list*
buffer.  See ivd-select-variable-list for more details."
  (interactive "P")
  (let*
      (
       (variable-list-buffer (get-buffer "*variable-list*"))
       (variable-list-modified (buffer-modified-p variable-list-buffer))
       )
;;;    (if variable-list-buffer
    ;; The buffer exists
    (if (not variable-list-modified)
        ;; The buffer exists and is not modified so there is no
        ;; reason to rebuild the list.  Just dump the old list.
        (ivd-insert-printf-statement arg)
      ;; The buffer exists but is modified so build and dump.
      (setq variable-list nil)
      (build-variable-list)
      (ivd-insert-printf-statement arg)
;;;          )
      ;; The buffer doesn't exist
;;;      (message "(no variable list)")
      )
    )
  )

(defun ivd-insert-printf-statement (&optional arg)
  "Does the actual insertion of the printf statement into current buffer."
  ;;NOT IN EMACS (fume-rescan-buffer)
  (let ((mode (if arg (if (eq arg 2) 'fundamental-mode 'c-mode) major-mode))
        (current-list (car variable-list))
        (rest-of-variable-list (cdr variable-list))
        (nom (buffer-name))
        )
    ;;(setq nom (or (fume-function-before-point) (buffer-name)))
    (setq nom (concat
               (file-name-sans-extension (buffer-name))
               ;;NOT IN EMACS ":" (fume-function-before-point)
               ))
;;;    (if (not variable-list)
    ;; variable list is empty
;;;        (message "(empty variable list)")
    ;; else, print away.
    (cond
     ;; inserer un write
     ((or (eq mode 'fortran-mode)
          (eq mode 'f90-mode))
      (insert "      WRITE(0,*) '@" nom ";")
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert (car current-list) ",")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (delete-backward-char 1)
      (insert "', ")
      (setq current-list (car variable-list))
      (setq rest-of-variable-list (cdr variable-list))
      (while current-list
        ;; Output each variable for the argument list.
        (insert (car current-list) ",")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (delete-backward-char 1)
      )
     ;; inserer un print
     ((eq mode 'python-mode)
      (insert "print(\"//@" nom ";\"" )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert ", \" " (car current-list) "=\", " (car current-list))
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (insert ")")
      )
     ;; inserer un println
     ((or (eq mode 'jde-mode)
          (eq mode 'java-mode))
      (insert "System.out.println(\"//@" nom ";\" " )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert "+ \" " (car current-list) "=\" + " (car current-list) " ")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (delete-backward-char 1)
      (insert ");")
      )
     ;; inserer un alert
     ((or (eq mode 'javascript-mode)
          (eq mode 'js-mode))
      (insert "console.log(\"//@" nom ";\"" )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert ", \" " (car current-list) "=\", " (car current-list))
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (insert ");")
      )
     ;; inserer un cerr
     ((eq mode 'c++-mode)
      (insert "std::cerr << \"//@" nom ",\" << __LINE__ << \";\" " )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert "<< \" " (car current-list) "=\" << " (car current-list) " ")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (delete-backward-char 1)
      (insert " << std::endl;")
      )
     ;; inserer un warning
     ((eq mode 'makefile-mode)
      (insert "$(warning \"//@" nom "; " )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert " " (car current-list) "=$(" (car current-list) ") ")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (delete-backward-char 1)
      (insert " \")")
      )
     ;; inserer un DBG
     ((eq mode 'fundamental-mode)
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert "DBG(\"" (car current-list) "=" (car (cdr current-list)) "\"," (car current-list) ");")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      ;;(delete-backward-char 1)
      ;;(insert " << std::endl;")
      )
     ;; inserer un echo
     ((or (eq mode 'sh-mode)
          (eq mode 'ksh-mode))
      (insert "echo \"//@" )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert " " (car current-list) "=\\\"${" (car current-list) "}\\\"")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (insert "\"" )
      ;;(delete-backward-char 1)
      ;;(insert " << std::endl;")
      )
     ;; inserer un echo
     ((or (eq mode 'php-mode)
          (eq mode 'web-mode))
      (insert "echo \"@" )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert " \\" (car current-list) " = \" . print_r(" (car current-list) ") . \"<br>")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (insert "\";" )
      ;;(delete-backward-char 1)
      ;;(insert " << std::endl;")
      )
     ;; inserer un message
     ((eq mode 'emacs-lisp-mode)
      (insert "(message (concat \"@ \" " )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert "\", " (car current-list) "=\" (prin1-to-string " (car current-list) ") ")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (insert "))" )
      ;;(delete-backward-char 1)
      ;;(insert " << std::endl;")
      )
     ;; inserer un print
     ((eq mode 'perl-mode)
      (insert "print STDERR \"#\\@" )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert " \\" (car current-list) "=\\\"" (car current-list) "\\\"")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (insert "\\n\";" )
      ;;(delete-backward-char 1)
      ;;(insert " << std::endl;")
      )
     ;; inserer un print
     ((eq mode 'awk-mode)
      (insert "print \"@" )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert " " (car current-list) "=\"" (car current-list) "\"")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (insert "\";" )
      ;;(delete-backward-char 1)
      ;;(insert " << std::endl;")
      )
     ;; inserer un printf
     (t
      (insert "fprintf(stderr,\"//@" nom "; " )
      (while current-list
        ;; Output each variable/format pair for the printf string.
        (insert (car current-list) "=" (car (cdr current-list)) " ")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (delete-backward-char 1)
      (insert "\\n\", ")
      (setq current-list (car variable-list))
      (setq rest-of-variable-list (cdr variable-list))
      (while current-list
        ;; Output each variable for the argument list.
        (insert (car current-list) ", ")
        (setq current-list (car rest-of-variable-list))
        (setq rest-of-variable-list (cdr rest-of-variable-list))
        )
      (delete-backward-char 2)
      (insert ");")
      )
     )
    )
;;;    )
  )

(defun ivd-insert-in-variable-list()
  "Find the variable name around point and insert into *variable-list*.
A variable name here is defined as that which is composed of only
letters, digits, and underscores.  See ivd-select-variable-list."
  (interactive)
  (let
      (
                                        ; "A-Za-z0-9_]" "[*A-Za-z0-9_]"
       (possible-variable (re-around-point "[^ =;:(),&\t\n]" "[ =;:(),&\t\n]"))
       )
    (if possible-variable
        ;; We found a variable, insert it into the *variable-list* buffer.
        ;;    (with-output-to-temp-buffer "*MouseMap*"
        (save-excursion
          (set-buffer (get-buffer-create "*variable-list*"))
          (goto-char (point-max))
          (insert possible-variable)
          (newline)
          (message (concat "\"" possible-variable "\"")))
      ;; Otherwise, complain
      (message "(variable not found)"))))


(defun ivd-insert-FILELINE-in-variable-list()
  "Find the variable name around point and insert into *variable-list*.
A variable name here is defined as that which is composed of only
letters, digits, and underscores.  See ivd-select-variable-list."
  (interactive)
  (save-excursion
    (set-buffer (get-buffer-create "*variable-list*"))
    (goto-char (point-max))
    (insert "__FILE__ %s") (newline)
    (insert "__LINE__ %d") (newline)
    (message (concat "\"" "__FILE__" "__LINE" "\""))))


(defun ivd-insert-float-variable-list()
  "Find the variable name around point and insert into *variable-list*.
A variable name here is defined as that which is composed of only
letters, digits, and underscores.  See ivd-select-variable-list."
  (interactive)
  (let
      (
                                        ; "A-Za-z0-9_]" "[*A-Za-z0-9_]"
       (possible-variable (re-around-point "[^ =;:(),&\t\n]" "[ =;:(),&\t\n]"))
       )
    (if possible-variable
        ;; We found a variable, insert it into the *variable-list* buffer.
        ;;    (with-output-to-temp-buffer "*MouseMap*"
        (save-excursion
          (set-buffer (get-buffer-create "*variable-list*"))
          (goto-char (point-max))
          (insert (concat possible-variable " %g"))
          (newline)
          (message (concat "\"" possible-variable "\"")))
      ;; Otherwise, complain
      (message "(variable not found)"))))


(defun ivd-insert-string-in-variable-list()
  "Find the variable name around point and insert into *variable-list*.
A variable name here is defined as that which is composed of only
letters, digits, and underscores.  See ivd-select-variable-list."
  (interactive)
  (let
      (
                                        ; "A-Za-z0-9_]" "[*A-Za-z0-9_]"
       (possible-variable (re-around-point "[^ =;(),&\t\n]" "[ =;(),&\t\n]"))
       )
    (if possible-variable
        ;; We found a variable, insert it into the *variable-list* buffer.
        ;;    (with-output-to-temp-buffer "*MouseMap*"
        (save-excursion
          (set-buffer (get-buffer-create "*variable-list*"))
          (goto-char (point-max))
          (insert (concat possible-variable " <%s>"))
          (newline)
          (message (concat "\"" possible-variable "\"")))
      ;; Otherwise, complain
      (message "(variable not found)"))))

(defun re-around-point(containing-re delimiting-re)
  "Return a symbol composed of CONTAINING-RE delimited by DELIMITING-RE."
  (save-excursion
    (if
        ;;         (and
        ;;            (not (looking-at containing-re))
        ;;            (not (looking-back containing-re))
        ;;         )
        (not (looking-at containing-re))
        ;; Point is not on a word, so return nil.
        nil
      ;; Otherwise, get the word
      (if (re-search-backward delimiting-re (bol-location) 1)
          (forward-char 1))
      (let (variable-start variable-end)
        (setq variable-start (point))
        (if (re-search-forward delimiting-re (eol-location) 1)
            (backward-char 1))
        (setq variable-end (point))
        ;; Return the re-string found
        (buffer-substring variable-start variable-end)
        ))
    )
  )

(defun bol-location()
  (save-excursion
    (let ((beg (beginning-of-line))))
    ))
(defun eol-location()
  (save-excursion
    (let ((beg (end-of-line))))
    ))

(defun ivd-reset-variable-list()
  "Remove all variables currently in the *variable-list* buffer"
  (interactive)
  (save-excursion
    (set-buffer (get-buffer-create "*variable-list*"))
    (erase-buffer)
    (message "(variable list reset)")
    ))
