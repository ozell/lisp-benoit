;;; Definition du mode Pie (from tcl.el)
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

;; HOW TO INSTALL:
;; Put the following forms in your .emacs to enable autoloading of Pie
;; mode, and auto-recognition of ".pie" files.
;;
;;   (autoload 'pie-mode "pie" "Pie mode." t)
;;   (autoload 'inferior-pie "pie" "Run inferior Pie process." t)
;;   (setq auto-mode-alist (append '(("\\.pie$" . pie-mode)) auto-mode-alist))
;;
;; If you plan to use the interface to the PieX help files, you must
;; set the variable pie-help-directory-list to point to the topmost
;; directories containing the PieX help files.  Eg:
;;
;;   (setq pie-help-directory-list '("/usr/local/lib/piex/help"))
;;
;; Also you will want to add the following to your .emacs:
;;
;;   (autoload 'pie-help-on-word "pie" "Help on Pie commands" t)
;;
;; FYI a *very* useful thing to do is nroff all the Tk man pages and
;; put them in a subdir of the help system.
;;

;;; Commentary:

;; CUSTOMIZATION NOTES:
;; * pie-proc-list can be used to customize a list of things that
;; "define" other things.  Eg in my project I put "defvar" in this
;; list.
;; * pie-typeword-list is similar, but uses font-lock-type-face.
;; * pie-keyword-list is a list of keywords.  I've generally used this
;; for flow-control words.  Eg I add "unwind_protect" to this list.
;; * pie-type-alist can be used to minimally customize indentation
;; according to context.

;; KNOWN BUGS:
;; * indent-region should skip blank lines.  (It does in v19, so I'm
;;   not motivated to fix it here).
;; * In Pie "#" is not always a comment character.  This can confuse
;;   pie.el in certain circumstances.  For now the only workaround is
;;   to enclose offending hash characters in quotes or precede it with
;;   a backslash.  Note that using braces won't work -- quotes change
;;   the syntax class of characters between them, while braces do not.
;;   The electric-# mode helps alleviate this problem somewhat.
;; * indent-pie-exp is untested.
;; * Doesn't work under Emacs 18 yet.
;; * There's been a report that font-lock does strange things under
;;   Lucid Emacs 19.6.  For instance in "proc foobar", the space
;;   before "foobar" is highlighted.

;; TODO:
;; * make add-log-pie-defun smarter.  should notice if we are in the
;;   middle of a defun, or between defuns.  should notice if point is
;;   on first line of defun (or maybe even in comments before defun).
;; * Allow continuation lines to be indented under the first argument
;;   of the preceeding line, like this:
;;      [list something \
;;            something-else]
;; * There is a request that indentation work like this:
;;        button .fred -label Fred \
;;                     -command {puts fred}
;; * Should have pie-complete-symbol that queries the inferior process.
;; * Should have describe-symbol that works by sending the magic
;;   command to a pieX process.
;; * Need C-x C-e binding (pie-eval-last-exp).
;; * Write indent-region function that is faster than indenting each
;;   line individually.
;; * pie-figure-type should stop at "beginning of line" (only ws
;;   before point, and no "\" on previous line).  (see pie-real-command-p).
;; * overrides some comint keybindings; fix.
;; * Trailing \ will eat blank lines.  Should deal with this.
;;   (this would help catch some potential bugs).
;; * Inferior should display in half the screen, not the whole screen.
;; * Indentation should deal with "switch".
;; * Consider writing code to find help files automatically (for
;;   common cases).
;; * `#' shouldn't insert `\#' when point is in string.



;;; Code:

;; I sure wish Emacs had a package that made it easy to extract this
;; sort of information.
(defconst pie-using-emacs-19 (string-match "19\\." emacs-version)
  "Nil unless using Emacs 19 (XEmacs or FSF).")

;; FIXME this will break on Emacs 19.100.
(defconst pie-using-emacs-19-23
  (string-match "19\\.\\(2[3-9]\\|[3-9][0-9]\\)" emacs-version)
  "Nil unless using Emacs 19-23 or later.")

(defconst pie-using-xemacs-19 (string-match "XEmacs" emacs-version)
  "Nil unless using XEmacs).")

(require 'comint)

;; When compiling under GNU Emacs, load imenu during compilation.  If
;; you have 19.22 or earlier, comment this out, or get imenu.
(and (fboundp 'eval-when-compile)
     (eval-when-compile
       (if (and (string-match "19\\." emacs-version)
		(not (string-match "XEmacs" emacs-version)))
	   (require 'imenu))
       ()))

(defconst pie-version "1.43")
(defconst pie-maintainer "Benoit Ozell <benoit.ozell@polymtl.ca>") ;;;BO

;;
;; User variables.
;;

(defvar pie-indent-level 3 ;;;BO
  "*Indentation of Pie statements with respect to containing block.")

(defvar pie-continued-indent-level 3 ;;;BO
  "*Indentation of continuation line relative to first line of command.")

(defvar pie-auto-newline nil
  "*Non-nil means automatically newline before and after braces
inserted in Pie code.")

(defvar pie-tab-always-indent t
  "*Control effect of TAB key.
If t (the default), always indent current line.
If nil and point is not in the indentation area at the beginning of
the line, a TAB is inserted.
Other values cause the first possible action from the following list
to take place:

  1. Move from beginning of line to correct indentation.
  2. Delete an empty comment.
  3. Move forward to start of comment, indenting if necessary.
  4. Move forward to end of line, indenting if necessary.
  5. Create an empty comment.
  6. Move backward to start of comment, indenting if necessary.")

(defvar pie-use-hairy-comment-detector t
  "*If not `nil', the the more complicated, but slower, comment
detecting function is used.  This variable is only used in GNU Emacs
19 (the fast function is always used elsewhere).")

(defvar pie-electric-hash-style 'smart
  "*Style of electric hash insertion to use.
Possible values are 'backslash, meaning that `\\' quoting should be
done; `quote, meaning that `\"' quoting should be done; 'smart,
meaning that the choice between 'backslash and 'quote should be
made depending on the number of hashes inserted; or nil, meaning that
no quoting should be done.  Any other value for this variable is
taken to mean 'smart.  The default is 'smart.")

(defvar pie-help-directory-list nil
  "*List of topmost directories containing PieX help files")

(defvar pie-use-smart-word-finder t
  "*If not nil, use a better way of finding the current word when
looking up help on a Pie command.")
;;;BO
(defvar pie-application "Vu"
  "*Name of Pie application to run in inferior Pie mode.")

(defvar pie-command-switches '("-pc")
  "*Switches to supply to `pie-application'.")

(defvar pie-maille-command "maille.sh"
  "Nom du mailleur.")

;;;BO
;;(defvar pie-prompt-regexp "^\\(% \\|\\)"
(defvar pie-prompt-regexp "^\\(([0-9]*) Vu >\\)"
  "*If not nil, a regexp that will match the prompt in the inferior process.
If nil, the prompt is the name of the application with \">\" appended.

The default is \"^\\(% \\|\\)\", which will match the default primary
and secondary prompts for piesh and wish.")

;;;BO
(defvar inferior-pie-source-command "open \"%s\"\n"
  "*Format-string for building a Pie command to load a file.
This format string should use `%s' to substitute a file name
and should result in a Pie expression that will command the
inferior Pie to load that file.  The filename will be appropriately
quoted for Pie.")

;;
;; Keymaps, abbrevs, syntax tables.
;;

(defvar pie-mode-abbrev-table nil
  "Abbrev table in use in Pie-mode buffers.")
(if pie-mode-abbrev-table
    ()
  (define-abbrev-table 'pie-mode-abbrev-table ()))

(defvar pie-mode-map ()
  "Keymap used in Pie mode.")

(defvar pie-mode-syntax-table nil
  "Syntax table in use in Pie-mode buffers.")
(if pie-mode-syntax-table
    ()
  (setq pie-mode-syntax-table (make-syntax-table))
;;;BO
  ;;(modify-syntax-entry ?%  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?@  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?&  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?*  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?+  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?-  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?.  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?:  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?!  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?$  "_" pie-mode-syntax-table) ; FIXME use "'"?
  ;;(modify-syntax-entry ?/  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?~  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?<  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?=  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?>  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?|  "_" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?\(  "()" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?\)  ")(" pie-mode-syntax-table)
  ;;(modify-syntax-entry ?\;  "." pie-mode-syntax-table)
  ;;(modify-syntax-entry ?\n ">   " pie-mode-syntax-table)
  ;;(modify-syntax-entry ?\f ">   " pie-mode-syntax-table)
  ;;(modify-syntax-entry ?# "<   " pie-mode-syntax-table)
  ;;     ma version:
  (modify-syntax-entry ?%  "."     pie-mode-syntax-table)
  (modify-syntax-entry ?+  "."     pie-mode-syntax-table)
  (modify-syntax-entry ?-  "."     pie-mode-syntax-table)
  (modify-syntax-entry ?=  "."     pie-mode-syntax-table)
  (modify-syntax-entry ?<  "."     pie-mode-syntax-table)
  (modify-syntax-entry ?>  "."     pie-mode-syntax-table)
  (modify-syntax-entry ?&  "."     pie-mode-syntax-table)
  (modify-syntax-entry ?|  "."     pie-mode-syntax-table)
  (modify-syntax-entry ?\;  "."    pie-mode-syntax-table)
  (modify-syntax-entry ?\(  "()"   pie-mode-syntax-table)
  (modify-syntax-entry ?\)  ")("   pie-mode-syntax-table)
  (modify-syntax-entry ?\\ "\\"    pie-mode-syntax-table)
  (modify-syntax-entry ?\' "\""    pie-mode-syntax-table)
  (modify-syntax-entry ?/  ". 1456" pie-mode-syntax-table)
  (modify-syntax-entry ?*  ". 23"   pie-mode-syntax-table)
  (modify-syntax-entry ?\n "> b" pie-mode-syntax-table)
  (modify-syntax-entry ?\f "> b" pie-mode-syntax-table)
  ;; Give CR the same syntax as newline, for selective-display
  ;;(modify-syntax-entry ?\^m "> b"    pie-mode-syntax-table)
  )

(defvar inferior-pie-mode-map nil
  "Keymap used in Inferior Pie mode.")

;; XEmacs menu.
(defvar pie-xemacs-menu
  '("Pie" ;;;BO
    ["Beginning of function" pie-beginning-of-defun t]
    ["End of function" pie-end-of-defun t]
    ["Mark function" pie-mark-defun t]
    ["Indent region" indent-region (pie-mark)]
    ["Comment region" comment-region (pie-mark)]
    ["Uncomment region" pie-uncomment-region (pie-mark)]
    "----"
    ["Show Pie process buffer" inferior-pie t]
    ["Send function to Pie process" pie-eval-defun
     (and inferior-pie-buffer (get-buffer inferior-pie-buffer))]
    ["Send region to Pie process" pie-eval-region
     (and inferior-pie-buffer (get-buffer inferior-pie-buffer))]
    ["Send file to Pie process" pie-load-file
     (and inferior-pie-buffer (get-buffer inferior-pie-buffer))]
    ["Restart Pie process with file" pie-restart-with-file t]
    "----"
    ["Maille region" pie-maille-region (pie-mark)]
    ["Maille buffer" pie-maille-buffer t]
    "----"
    ["Pie help" pie-help-on-word pie-help-directory-list]
    ["Send bug report" pie-submit-bug-report t])
  "XEmacs menu for Pie mode.")

;; GNU Emacs does menus via keymaps.  Do it in a function in case we
;; later decide to add it to inferior Pie mode as well.
(defun pie-add-fsf-menu (map)
  (define-key map [menu-bar] (make-sparse-keymap))
  ;; This fails in Emacs 19.22 and earlier.
  (require 'lmenu)
  (let ((menu (make-lucid-menu-keymap "Pie" pie-xemacs-menu)))
    (define-key map [menu-bar pie] (cons "Pie" menu))
    ;; The following is intended to compute the key sequence
    ;; information for the menu.  It doesn't work.
    (x-popup-menu nil menu)))

(defun pie-fill-mode-map ()
  (define-key pie-mode-map "{" 'pie-electric-char)
  (define-key pie-mode-map "}" 'pie-electric-brace)
  ;;(define-key pie-mode-map "[" 'pie-electric-char) ;;;BO
  ;;(define-key pie-mode-map "]" 'pie-electric-char) ;;;BO
  (define-key pie-mode-map ";" 'pie-electric-char)
  ;;(define-key pie-mode-map "#" 'pie-electric-hash) ;;;BO
  ;; FIXME.
  (define-key pie-mode-map "\e\C-a" 'pie-beginning-of-defun)
  ;; FIXME.
  (define-key pie-mode-map "\e\C-e" 'pie-end-of-defun)
  ;; FIXME.
  ;;(define-key pie-mode-map "\e\C-h" 'pie-mark-defun) ;;;BO
  (define-key pie-mode-map "\e\C-q" 'indent-pie-exp)
  ;;(define-key pie-mode-map "\177" 'backward-delete-char-untabify) ;;;BO
  (define-key pie-mode-map "\t" 'pie-indent-command)
  (define-key pie-mode-map "\M-;" 'pie-indent-for-comment)
  (define-key pie-mode-map "\M-\C-x" 'pie-eval-defun)
  (define-key pie-mode-map "\C-c\C-b" 'pie-submit-bug-report)
  (and (fboundp 'comment-region)
       (define-key pie-mode-map "\C-c\C-c" 'comment-region))
  (define-key pie-mode-map "\C-c\C-i" 'pie-help-on-word)
  (define-key pie-mode-map "\C-c\C-v" 'pie-eval-defun)
  (define-key pie-mode-map "\C-c\C-f" 'pie-load-file)
  ;;(define-key pie-mode-map "\C-c\C-t" 'inferior-pie) ;;;BO
  (define-key pie-mode-map "\C-c\C-p" 'inferior-pie) ;;;BO
  (define-key pie-mode-map "\C-c\C-x" 'pie-eval-region)
  (define-key pie-mode-map "\C-cm" 'pie-maille-buffer)
  (define-key pie-mode-map "\C-cM" 'pie-maille-region)
  (define-key pie-mode-map "\C-c0" 'cerca-cpp-if)

  ;; Make menus.
 ;;;BO
  ;;(if (and pie-using-emacs-19 (not pie-using-xemacs-19))
  ;;    (progn
  ;;	(pie-add-fsf-menu pie-mode-map))))
  (if pie-using-emacs-19
      (if pie-using-xemacs-19
	  ;; In XEmacs, button 3 seems to be the standard for this.
	  (define-key pie-mode-map 'button3 'pie-popup-menu)
	;; In FSF 19, there is no standard, so I use shift-button2.
	(pie-add-fsf-menu pie-mode-map)
	(define-key pie-mode-map [S-down-mouse-2] 'pie-popup-menu))))

(defun pie-fill-inferior-map ()
  (define-key inferior-pie-mode-map "\t" 'comint-dynamic-complete)
  (define-key inferior-pie-mode-map "\M-?"
    'comint-dynamic-list-filename-completions)
  (define-key inferior-pie-mode-map "\e\C-a" 'pie-beginning-of-defun)
  (define-key inferior-pie-mode-map "\e\C-e" 'pie-end-of-defun)
  (define-key inferior-pie-mode-map "\177" 'backward-delete-char-untabify)
  (define-key inferior-pie-mode-map "\M-\C-x" 'pie-eval-defun)
  (define-key inferior-pie-mode-map "\C-c\C-b" 'pie-submit-bug-report)
  (define-key inferior-pie-mode-map "\C-c\C-i" 'pie-help-on-word)
  (define-key inferior-pie-mode-map "\C-c\C-v" 'pie-eval-defun)
  (define-key inferior-pie-mode-map "\C-c\C-f" 'pie-load-file)
  (define-key inferior-pie-mode-map "\C-c\C-t" 'inferior-pie)
  (define-key inferior-pie-mode-map "\C-c\C-x" 'pie-eval-region)
  (define-key inferior-pie-mode-map "\C-c\C-s" 'switch-to-pie))

(if pie-mode-map
    ()
  (setq pie-mode-map (make-sparse-keymap))
  (pie-fill-mode-map))

(if inferior-pie-mode-map
    ()
  ;; FIXME Use keymap inheritance here?  FIXME we override comint
  ;; keybindings here.  Maybe someone has a better set?
  (setq inferior-pie-mode-map (copy-keymap comint-mode-map))
  (pie-fill-inferior-map))


(defvar inferior-pie-buffer nil
  "*The current inferior-pie process buffer.

MULTIPLE PROCESS SUPPORT
===========================================================================
To run multiple Pie processes, you start the first up with
\\[inferior-pie].  It will be in a buffer named `*inferior-pie*'.
Rename this buffer with \\[rename-buffer].  You may now start up a new
process with another \\[inferior-pie].  It will be in a new buffer,
named `*inferior-pie*'.  You can switch between the different process
buffers with \\[switch-to-buffer].

Commands that send text from source buffers to Pie processes -- like
`pie-eval-defun' or `pie-load-file' -- have to choose a process to
send to, when you have more than one Pie process around.  This is
determined by the global variable `inferior-pie-buffer'.  Suppose you
have three inferior Lisps running:
    Buffer              Process
    foo                 inferior-pie
    bar                 inferior-pie<2>
    *inferior-pie*      inferior-pie<3>
If you do a \\[pie-eval-defun] command on some Lisp source code, what
process do you send it to?

- If you're in a process buffer (foo, bar, or *inferior-pie*),
  you send it to that process.
- If you're in some other buffer (e.g., a source file), you
  send it to the process attached to buffer `inferior-pie-buffer'.
This process selection is performed by function `inferior-pie-proc'.

Whenever \\[inferior-pie] fires up a new process, it resets
`inferior-pie-buffer' to be the new process's buffer.  If you only run
one process, this does the right thing.  If you run multiple
processes, you can change `inferior-pie-buffer' to another process
buffer with \\[set-variable].")

;;
;; Hooks and other customization.
;;

(defvar pie-mode-hook nil
  "Hook run on entry to Pie mode.

Several functions exist which are useful to run from your
`pie-mode-hook' (see each function's documentation for more
information):

  pie-guess-application
    Guesses a default setting for `pie-application' based on any
    \"#!\" line at the top of the file.
  pie-hashify-buffer
    Quotes all \"#\" characters that don't correspond to actual
    Pie comments.  (Useful when editing code not originally created
    with this mode).
  pie-auto-fill-mode
    Auto-filling of Pie comments.

Emacs 19 users can add functions to the hook with `add-hook':

   (add-hook 'pie-mode-hook 'pie-guess-application)

Emacs 18 users must use `setq':

   (setq pie-mode-hook (cons 'pie-guess-application pie-mode-hook))")


(defvar inferior-pie-mode-hook nil
  "Hook for customizing Inferior Pie mode.")

(defvar pie-proc-list
  '("proc" "method" "ipie_class")
  "List of commands whose first argument defines something.
This exists because some people (eg, me) use \"defvar\" et al.
Call `pie-set-proc-regexp' and `pie-set-font-lock-keywords'
after changing this list.")

(defvar pie-proc-regexp nil
  "Regexp to use when matching proc headers.")

 ;;;BO
;;(defvar pie-typeword-list
;;  '("global" "upvar" "inherit" "public" "protected" "common")
;;  "List of Pie keywords denoting \"type\".  Used only for highlighting.
;;Call `pie-set-font-lock-keywords' after changing this list.")
(defvar pie-typeword-list
  '("[Tt][Ee][Xx][Tt][Ee]"
    "[Mm][Aa][Ii][Ll][Ll][Aa][Gg][Ee]"
    "[Gg][Rr][Ii][Dd]"
    "[Zz][Oo][Nn][Ee]"
    "[Ss][Oo][Ll][Uu][Tt][Ii][Oo][Nn]"
    "[Vv][Aa][Rr][Ii][Aa][Bb][Ll][Ee]"
    "[Cc][Hh][Aa][Mm][Pp]"
    "[Cc][Hh][Aa][Mm][Pp]<int>"
    "[Cc][Hh][Aa][Mm][Pp]<float>"
    "[Cc][Hh][Aa][Mm][Pp]<double>"
    "[Ff][Ii][Ee][Ll][Dd]"
    "[Ff][Ii][Ee][Ll][Dd]<int>"
    "[Ff][Ii][Ee][Ll][Dd]<float>"
    "[Ff][Ii][Ee][Ll][Dd]<double>"
    "[Gg][Ee][Oo][Mm][Ee][Tt][Rr][Ii][Ee]"
    "[Zz][Oo][Nn][Aa][Gg][Ee]"
    "[Pp][Oo][Ii][Nn][Tt]"
    "[Cc][Oo][Uu][Rr][Bb][Ee]"
    "[Cc][Uu][Rr][Vv][Ee]"
    "[Ss][Uu][Rr][Ff][Aa][Cc][Ee]"
    "[Ss][Oo][Mm][Mm][Ee][Tt]"
    "[Vv][Ee][Rr][Tt][Ee][Xx]"
    "[Aa][Rr][Ee][Tt][Ee]"
    "[Ee][Dd][Gg][Ee]"
    "[Ff][Aa][Cc][Ee]"
    "[Vv][Oo][Ll][Uu][Mm][Ee]"
    "[Aa][Nn][Nn][Oo][Tt][Aa][Tt][Ii][Oo][Nn]"
    "[Ff][Oo][Nn][Cc][Tt][Ii][Oo][Nn]"
    "[Ss][Pp][Hh][Ee][Rr][Ee]"
    "[Cc][Yy][Ll][Ii][Nn][Dd][Rr][Ee]"
    "[Cc][Yy][Ll][Ii][Nn][Dd][Ee][Rr]"
    "[Aa][Tt][Oo][Mm][Ee]"
    "[Aa][Tt][Oo][Mm]"
    "[Ll][Ii][Ee][Nn]"
    "[Bb][Oo][Nn][Dd]"
    "FENETRE"
    "SUPPORT"
    "ENTITE"
    "IMAGE"
    "VUE"
    )
  "List of Pie keywords denoting \"type\".  Used only for highlighting.
Call `pie-set-font-lock-keywords' after changing this list.")

;; Generally I've picked control operators to be keywords.
;;;BO
;;(defvar pie-keyword-list
;;  '("if" "then" "else" "elseif" "for" "foreach" "break" "continue" "while"
;;    "eval" "case" "in" "switch" "default" "exit" "error" "proc" "return"
;;    "uplevel" "constructor" "destructor" "ipie_class" "loop" "for_array_keys"
;;    "for_recursive_glob" "for_file")
;;  "List of Pie keywords.  Used only for highlighting.
;;Default list includes some PieX keywords.
;;Call `pie-set-font-lock-keywords' after changing this list.")
(defvar pie-keyword-list
  '("if" "then" "else")
  "List of Pie keywords.  Used only for highlighting.
Default list includes some PieX keywords.
Call `pie-set-font-lock-keywords' after changing this list.")

(defvar pie-font-lock-keywords nil
  "Keywords to highlight for Pie.  See variable `font-lock-keywords'.
This variable is generally set from `pie-proc-regexp',
`pie-typeword-list', and `pie-keyword-list' by the function
`pie-set-font-lock-keywords'.")

;; FIXME need some way to recognize variables because array refs look
;; like 2 sexps.
(defvar pie-type-alist
  '(
    ("proc" nil pie-expr pie-commands)
    ("expr" pie-expr)
    ("catch" pie-commands)
    ("if" pie-expr "then" pie-commands)
    ("elseif" pie-expr "then" pie-commands)
    ("elseif" pie-expr pie-commands)
    ("if" pie-expr pie-commands)
    ("while" pie-expr pie-commands)
    ("for" pie-commands pie-expr pie-commands pie-commands)
    ("foreach" nil nil pie-commands)
    ;; Loop handling is not perfect, because the third argument can be
    ;; either a command or an expr, and there is no real way to look
    ;; forward.
    ("loop" nil pie-expr pie-expr pie-commands)
    ("loop" nil pie-expr pie-commands)
    )
  "Alist that controls indentation.
\(Actually, this really only controls what happens on continuation lines).
Each entry looks like `(KEYWORD TYPE ...)'.
Each type entry describes a sexp after the keyword, and can be one of:
* nil, meaning that this sexp has no particular type.
* pie-expr, meaning that this sexp is an arithmetic expression.
* pie-commands, meaning that this sexp holds Pie commands.
* a string, which must exactly match the string at the corresponding
  position for a match to be made.

For example, the entry for the \"loop\" command is:

   (\"loop\" nil pie-expr pie-commands)

This means that the \"loop\" command has three arguments.  The first
argument is ignored (for indentation purposes).  The second argument
is a Pie expression, and the last argument is Pie commands.")

(defvar pie-explain-indentation nil
  "If not `nil', debugging message will be printed during indentation.")



;;
;; Work around differences between various versions of Emacs.
;;

;; We use this because Lemacs 19.9 has what we need.
(defconst pie-pps-has-arg-6
  (or pie-using-emacs-19
      (and pie-using-xemacs-19
	   (condition-case nil
	       (progn
		 (parse-partial-sexp (point) (point) nil nil nil t)
		 t)
	     (error nil))))
  "t if using an emacs which supports sixth (\"commentstop\") argument
to parse-partial-sexp.")

;; Its pretty bogus to have to do this, but there is no easier way to
;; say "match not syntax-1 and not syntax-2".  Too bad you can't put
;; \s in [...].  This sickness is used in Emacs 19 to match a defun
;; starter.  (It is used for this in v18 as well).
;;(defconst pie-omit-ws-regexp
;;  (concat "^\\(\\s"
;;	  (mapconcat 'char-to-string "w_.()\"\\$'/" "\\|\\s")
;;	  "\\)\\S(*")
;;  "Regular expression that matches everything except space, comment
;;starter, and comment ender syntax codes.")

;; FIXME?  Instead of using the hairy regexp above, we just use a
;; simple one.
;;(defconst pie-omit-ws-regexp "^[^] \t\n#}]\\S(*"
;;  "Regular expression used in locating function definitions.")

;; Here's another stab.  I think this one actually works.  Now the
;; problem seems to be that there is a bug in Emacs 19.22 where
;; end-of-defun doesn't really use the brace matching the one that
;; trails defun-prompt-regexp.
(defconst pie-omit-ws-regexp "^[^ \t\n#}][^\n}]+}*[ \t]+")

(defun pie-internal-beginning-of-defun (&optional arg)
  "Move backward to next beginning-of-defun.
With argument, do this that many times.
Returns t unless search stops due to end of buffer."
  (interactive "p")
  (if (or (null arg) (= arg 0))
      (setq arg 1))
  (let (success)
    (while (progn
	     (setq arg (1- arg))
	     (and (>= arg 0)
		  (setq success
			(re-search-backward pie-omit-ws-regexp nil 'move 1))))
      (while (and (looking-at "[]#}]")
		  (setq success
			(re-search-backward pie-omit-ws-regexp nil 'move 1)))))
    (beginning-of-line)
    (not (null success))))

(defun pie-internal-end-of-defun (&optional arg)
  "Move forward to next end of defun.
An end of a defun is found by moving forward from the beginning of one."
  (interactive "p")
  (if (or (null arg) (= arg 0)) (setq arg 1))
  (let ((start (point)))
    ;; Was forward-char.  I think this works a little better.
    (forward-line)
    (pie-beginning-of-defun)
    (while (> arg 0)
      (while (and (re-search-forward pie-omit-ws-regexp nil 'move 1)
		  (progn (beginning-of-line) t)
		  (looking-at "[]#}]")
		  (progn (forward-line) t)))
      (let ((next-line (save-excursion
			 (forward-line)
			 (point))))
	(while (< (point) next-line)
	  (forward-sexp)))
      (forward-line)
      (if (> (point) start) (setq arg (1- arg))))))

;; In Emacs 19, we can use begining-of-defun as long as we set up a
;; certain regexp.  In Emacs 18, we need our own function.
(fset 'pie-beginning-of-defun
      (if pie-using-emacs-19
	  'beginning-of-defun
	'pie-internal-beginning-of-defun))

;; Ditto end-of-defun.
(fset 'pie-end-of-defun
      (if (and pie-using-emacs-19 (not pie-using-xemacs-19))
	  'end-of-defun
	'pie-internal-end-of-defun))

;; Internal mark-defun that is used for losing Emacsen.
(defun pie-internal-mark-defun ()
  "Put mark at end of Pie function, point at beginning."
  (interactive)
  (push-mark (point))
  (pie-end-of-defun)
  (if pie-using-emacs-19
      (push-mark (point) nil t)
    (push-mark (point)))
  (pie-beginning-of-defun)
  (backward-paragraph))

;; In GNU Emacs 19-23 and later, mark-defun works as advertised.  I
;; don't know about XEmacs, so for now it and Emacs 18 just lose.
(fset 'pie-mark-defun
      (if pie-using-emacs-19-23
	  'mark-defun
	'pie-internal-mark-defun))

;; In GNU Emacs 19, mark takes an additional "force" argument.  I
;; don't know about XEmacs, so I'm just assuming it is the same.
;; Emacs 18 doesn't have this argument.
(defun pie-mark ()
  "Return mark, or nil if none."
  (if pie-using-emacs-19
      (mark t)
    (mark)))



;;
;; Some helper functions.
;;

(defun pie-set-proc-regexp ()
  "Set `pie-proc-regexp' from variable `pie-proc-list'."
  (setq pie-proc-regexp (concat "^\\s-*\\("
				(mapconcat 'identity pie-proc-list "\\|")
				"\\)[ \t]+")))

(defun pie-set-font-lock-keywords ()
  "Set `pie-font-lock-keywords'.
Uses variables `pie-proc-regexp' and `pie-keyword-list'."
  (setq pie-font-lock-keywords
	(list
	 ;; Names of functions (and other "defining things").
	 (list (concat pie-proc-regexp "\\([^ \t\n]+\\)")
	       2 'font-lock-function-name-face)

	 ;; Names of type-defining things.
	 (list (concat "\\(\\s-\\|^\\)\\("
		       ;; FIXME Use 'regexp-quote?
		       (mapconcat 'identity pie-typeword-list "\\|")
		       "\\)\\(\\s-\\|$\\)")
	       2 'font-lock-type-face)

	 ;; Keywords.  Only recognized if surrounded by whitespace.
	 ;; FIXME consider using "not word or symbol", not
	 ;; "whitespace".
	 (cons (concat "\\(\\s-\\|^\\)\\("
		       ;; FIXME Use regexp-quote?
		       (mapconcat 'identity pie-keyword-list "\\|")
		       "\\)\\(\\s-\\|$\\)")
	       2)
	 )))

(if pie-proc-regexp
    ()
  (pie-set-proc-regexp))

(if pie-font-lock-keywords
    ()
  (pie-set-font-lock-keywords))



;;
;; The mode itself.
;;

;;;###autoload
(defun pie-mode ()
  "Major mode for editing Pie code.
Expression and list commands understand all Pie brackets.
Tab indents for Pie code.
Paragraphs are separated by blank lines only.
Delete converts tabs to spaces as it moves back.

Variables controlling indentation style:
  pie-indent-level
    Indentation of Pie statements within surrounding block.
  pie-continued-indent-level
    Indentation of continuation line relative to first line of command.

Variables controlling user interaction with mode (see variable
documentation for details):
  pie-tab-always-indent
    Controls action of TAB key.
  pie-auto-newline
    Non-nil means automatically newline before and after braces, brackets,
    and semicolons inserted in Pie code.
  pie-electric-hash-style
    Controls action of `#' key.
  pie-use-hairy-comment-detector
    If t, use more complicated, but slower, comment detector.
    This variable is only used in GNU Emacs 19.

Turning on Pie mode calls the value of the variable `pie-mode-hook'
with no args, if that value is non-nil.  Read the documentation for
`pie-mode-hook' to see what kinds of interesting hook functions
already exist.

Commands:
\\{pie-mode-map}"
  (interactive)
  (kill-all-local-variables)
  (use-local-map pie-mode-map)
  (setq major-mode 'pie-mode)
  (setq mode-name "Pie")
  (setq local-abbrev-table pie-mode-abbrev-table)
  (set-syntax-table pie-mode-syntax-table)

  (make-local-variable 'paragraph-start)
  (make-local-variable 'paragraph-separate)
  (if (and pie-using-emacs-19-23
	   (>= emacs-minor-version 29))
      (progn
	;; In Emacs 19.29, you aren't supposed to start these with a
	;; ^.
	(setq paragraph-start "$\\|")
	(setq paragraph-separate paragraph-start))
    (setq paragraph-start (concat "^$\\|" page-delimiter))
    (setq paragraph-separate paragraph-start))
  (make-local-variable 'paragraph-ignore-fill-prefix)
  (setq paragraph-ignore-fill-prefix t)
  (make-local-variable 'fill-paragraph-function)
  (setq fill-paragraph-function 'pie-do-fill-paragraph)

  (make-local-variable 'indent-line-function)
  (setq indent-line-function 'pie-indent-line)
  ;; Pie doesn't require a final newline.
  ;; (make-local-variable 'require-final-newline)
  ;; (setq require-final-newline t)

  (make-local-variable 'comment-start)
  ;;(setq comment-start "# ") ;;;BO
  (setq comment-start "// ") ;;;BO
  (make-local-variable 'comment-start-skip)
  ;;(setq comment-start-skip "#+ *") ;;;BO
  (setq comment-start-skip "//+ *") ;;;BO
  (make-local-variable 'comment-column)
  (setq comment-column 40)
  (make-local-variable 'comment-end)
  (setq comment-end "")

  (make-local-variable 'outline-regexp)
  (setq outline-regexp "[^\n\^M]")
  (make-local-variable 'outline-level)
  (setq outline-level 'pie-outline-level)

  (make-local-variable 'font-lock-keywords)
  (setq font-lock-keywords pie-font-lock-keywords)

  ;; The following only really makes sense under GNU Emacs 19.
  (make-local-variable 'imenu-create-index-function)
  (setq imenu-create-index-function 'pie-imenu-create-index-function)
  (make-local-variable 'parse-sexp-ignore-comments)

  ;; Settings for new dabbrev code.
  (make-local-variable 'dabbrev-case-fold-search)
  (setq dabbrev-case-fold-search nil)
  (make-local-variable 'dabbrev-case-replace)
  (setq dabbrev-case-replace nil)
  (make-local-variable 'dabbrev-abbrev-skip-leading-regexp)
  (setq dabbrev-abbrev-skip-leading-regexp "[$!]")
  (make-local-variable 'dabbrev-abbrev-char-regexp)
  (setq dabbrev-abbrev-char-regexp "\\sw\\|\\s_")

  (if pie-using-emacs-19
      (progn
	;; This can only be set to t in Emacs 19 and XEmacs.
	;; Emacs 18 and Epoch lose.
	(setq parse-sexp-ignore-comments t)
	;; XEmacs has defun-prompt-regexp, but I don't believe
	;; that it works for end-of-defun -- only for
	;; beginning-of-defun.
	(make-local-variable 'defun-prompt-regexp)
	(setq defun-prompt-regexp pie-omit-ws-regexp)
	;; The following doesn't work in Lucid Emacs 19.6, but maybe
	;; it will appear in later versions.
	(make-local-variable 'add-log-current-defun-function)
	(setq add-log-current-defun-function 'add-log-pie-defun))
    (setq parse-sexp-ignore-comments nil))

  ;; Put Pie menu into menubar for XEmacs.  This happens
  ;; automatically for GNU Emacs.
  (if (and pie-using-xemacs-19
	   current-menubar
	   (not (assoc "Pie" current-menubar)))
      (progn
	(set-buffer-menubar (copy-sequence current-menubar))
	(add-submenu nil pie-xemacs-menu)))
  ;; Append Pie menu to popup menu for XEmacs.
  (if (and pie-using-xemacs-19 (boundp 'mode-popup-menu))
      (setq mode-popup-menu
	    (cons (concat mode-name " Mode Commands") pie-xemacs-menu)))

  (run-hooks 'pie-mode-hook))



;; This is used for braces, brackets, and semi (except for closing
;; braces, which are handled specially).
(defun pie-electric-char (arg)
  "Insert character and correct line's indentation."
  (interactive "p")
  ;; Indent line first; this looks better if parens blink.
  (pie-indent-line)
  (self-insert-command arg)
  (if (and pie-auto-newline (= last-command-char ?\;))
      (progn
	(newline)
	(pie-indent-line))))

;; This is used for closing braces.  If pie-auto-newline is set, can
;; insert a newline both before and after the brace, depending on
;; context.  FIXME should this be configurable?  Does anyone use this?
(defun pie-electric-brace (arg)
  "Insert character and correct line's indentation."
  (interactive "p")
  ;; If auto-newlining and there is stuff on the same line, insert a
  ;; newline first.
  (if pie-auto-newline
      (progn
	(if (save-excursion
	      (skip-chars-backward " \t")
	      (bolp))
	    ()
	  (pie-indent-line)
	  (newline))
	;; In auto-newline case, must insert a newline after each
	;; brace.  So an explicit loop is needed.
	(while (> arg 0)
	  (insert last-command-char)
	  (pie-indent-line)
	  (newline)
	  (setq arg (1- arg))))
    (self-insert-command arg))
  (pie-indent-line))



(defun pie-indent-command (&optional arg)
  "Indent current line as Pie code, or in some cases insert a tab character.
If pie-tab-always-indent is t (the default), always indent current line.
If pie-tab-always-indent is nil and point is not in the indentation
area at the beginning of the line, a TAB is inserted.
Other values of pie-tab-always-indent cause the first possible action
from the following list to take place:

  1. Move from beginning of line to correct indentation.
  2. Delete an empty comment.
  3. Move forward to start of comment, indenting if necessary.
  4. Move forward to end of line, indenting if necessary.
  5. Create an empty comment.
  6. Move backward to start of comment, indenting if necessary."
  (interactive "p")
  (cond
   ((not pie-tab-always-indent)
    ;; Indent if in indentation area, otherwise insert TAB.
    (if (<= (current-column) (current-indentation))
	(pie-indent-line)
      (self-insert-command arg)))
   ((eq pie-tab-always-indent t)
    ;; Always indent.
    (pie-indent-line))
   (t
    ;; "Perl-mode" style TAB command.
    (let* ((ipoint (point))
	   (eolpoint (progn
		       (end-of-line)
		       (point)))
	   (comment-p (pie-in-comment)))
      (cond
       ((= ipoint (save-excursion
		    (beginning-of-line)
		    (point)))
	(beginning-of-line)
	(pie-indent-line)
	;; If indenting didn't leave us in column 0, go to the
	;; indentation.  Otherwise leave point at end of line.  This
	;; is a hack.
	(if (= (point) (save-excursion
			 (beginning-of-line)
			 (point)))
	    (end-of-line)
	  (back-to-indentation)))
       ((and comment-p (looking-at "[ \t]*$"))
	;; Empty comment, so delete it.  We also delete any ";"
	;; characters at the end of the line.  I think this is
	;; friendlier, but I don't know how other people will feel.
	(backward-char)
	(skip-chars-backward " \t;")
	(delete-region (point) eolpoint))
       ((and comment-p (< ipoint (point)))
	;; Before comment, so skip to it.
	(pie-indent-line)
	(indent-for-comment))
       ((/= ipoint eolpoint)
	;; Go to end of line (since we're not there yet).
	(goto-char eolpoint)
	(pie-indent-line))
       ((not comment-p)
	(pie-indent-line)
	(pie-indent-for-comment))
       (t
	;; Go to start of comment.  We don't leave point where it is
	;; because we want to skip comment-start-skip.
	(pie-indent-line)
	(indent-for-comment)))))))

(defun pie-indent-line ()
  "Indent current line as Pie code.
Return the amount the indentation changed by."
  (let ((indent (calculate-pie-indent nil))
	beg shift-amt
	(case-fold-search nil)
	(pos (- (point-max) (point))))
    (beginning-of-line)
    (setq beg (point))
    (cond ((eq indent nil)
	   (setq indent (current-indentation)))
          (t
	   (skip-chars-forward " \t")
	   (if (listp indent) (setq indent (car indent)))
	   (cond ((= (following-char) ?})
		  (setq indent (- indent pie-indent-level)))
		 ((= (following-char) ?\])
		  (setq indent (- indent 1))))))
    (skip-chars-forward " \t")
    (setq shift-amt (- indent (current-column)))
    (if (zerop shift-amt)
	(if (> (- (point-max) pos) (point))
	    (goto-char (- (point-max) pos)))
      (delete-region beg (point))
      (indent-to indent)
      ;; If initial point was within line's indentation,
      ;; position after the indentation.  Else stay at same point in text.
      (if (> (- (point-max) pos) (point))
	  (goto-char (- (point-max) pos))))
    shift-amt))

(defun pie-figure-type ()
  "Determine type of sexp at point.
This is either 'pie-expr, 'pie-commands, or nil.  Puts point at start
of sexp that indicates types.

See documentation for variable `pie-type-alist' for more information."
  (let ((count 0)
	result
	word-stack)
    (while (and (< count 5)
		(not result))
      (condition-case nil
	  (progn
	    ;; FIXME should use "pie-backward-sexp", which would skip
	    ;; over entire variables, etc.
	    (backward-sexp)
	    (if (looking-at "[a-zA-Z_]+")
		(let ((list pie-type-alist)
		      entry)
		  (setq word-stack (cons (current-word) word-stack))
		  (while (and list (not result))
		    (setq entry (car list))
		    (setq list (cdr list))
		    (let ((index 0))
		      (while (and entry (<= index count))
			;; Abort loop if string does not match word on
			;; stack.
			(and (stringp (car entry))
			     (not (string= (car entry)
					   (nth index word-stack)))
			     (setq entry nil))
			(setq entry (cdr entry))
			(setq index (1+ index)))
		      (and (> index count)
			   (not (stringp (car entry)))
			   (setq result (car entry)))
		      )))
	      (setq word-stack (cons nil word-stack))))
	(error nil))
      (setq count (1+ count)))
    (and pie-explain-indentation
	 (message "Indentation type %s" result))
    result))

(defun calculate-pie-indent (&optional parse-start)
  "Return appropriate indentation for current line as Pie code.
In usual case returns an integer: the column to indent to.
Returns nil if line starts inside a string, t if in a comment."
  (save-excursion
    (beginning-of-line)
    (let* ((indent-point (point))
	   (case-fold-search nil)
	   (continued-line
	    (save-excursion
	      (if (bobp)
		  nil
		(backward-char)
		(= ?\\ (preceding-char)))))
	   (continued-indent-value (if continued-line
				       pie-continued-indent-level
				     0))
	   state
	   containing-sexp
	   found-next-line)
      (if parse-start
	  (goto-char parse-start)
	(pie-beginning-of-defun))
      (while (< (point) indent-point)
	(setq parse-start (point))
	(setq state (parse-partial-sexp (point) indent-point 0))
	(setq containing-sexp (car (cdr state))))
      ;;(cond ((or (nth 3 state) (nth 4 state)) ;;;BO
      (cond ((nth 3 state) ;;;BO
	     ;; Inside comment or string.  Return nil or t if should
	     ;; not change this line
	     ;;(nth 4 state)) ;;;BO
	     nil) ;;;BO
	    ((null containing-sexp)
	     ;; Line is at top level.
	     continued-indent-value)
	    (t
	     ;; Set expr-p if we are looking at the expression part of
	     ;; an "if", "expr", etc statement.  Set commands-p if we
	     ;; are looking at the body part of an if, while, etc
	     ;; statement.  FIXME Should check for "for" loops here.
	     (goto-char containing-sexp)
	     (let* ((sexpr-type (pie-figure-type))
		    (expr-p (eq sexpr-type 'pie-expr))
		    (commands-p (eq sexpr-type 'pie-commands))
		    (expr-start (point)))
	       ;; Find the first statement in the block and indent
	       ;; like it.  The first statement in the block might be
	       ;; on the same line, so what we do is skip all
	       ;; "virtually blank" lines, looking for a non-blank
	       ;; one.  A line is virtually blank if it only contains
	       ;; a comment and whitespace.  FIXME continued comments
	       ;; aren't supported.  They are a wart on Pie anyway.
	       ;; We do it this funky way because we want to know if
	       ;; we've found a statement on some line _after_ the
	       ;; line holding the sexp opener.
	       (goto-char containing-sexp)
	       (forward-char)
	       (if (and (< (point) indent-point)
			(looking-at "[ \t]*\\(#.*\\)?$"))
		   (progn
		     (forward-line)
		     (while (and (< (point) indent-point)
				 (looking-at "[ \t]*\\(#.*\\)?$"))
		       (setq found-next-line t)
		       (forward-line))))
	       (if (or continued-line
		       (/= (char-after containing-sexp) ?{)
		       expr-p)
		   (progn
		     ;; Line is continuation line, or the sexp opener
		     ;; is not a curly brace, or we are are looking at
		     ;; an `expr' expression (which must be split
		     ;; specially).  So indentation is column of first
		     ;; good spot after sexp opener (with some added
		     ;; in the continued-line case).  If there is no
		     ;; nonempty line before the indentation point, we
		     ;; use the column of the character after the sexp
		     ;; opener.
		     (if (>= (point) indent-point)
			 (progn
			   (goto-char containing-sexp)
			   (forward-char))
		       (skip-chars-forward " \t"))
		     (+ (current-column) continued-indent-value))
		 ;; After a curly brace, and not a continuation line.
		 ;; So take indentation from first good line after
		 ;; start of block, unless that line is on the same
		 ;; line as the opening brace.  In this case use the
		 ;; indentation of the opening brace's line, plus
		 ;; another indent step.  If we are in the body part
		 ;; of an "if" or "while" then the indentation is
		 ;; taken from the line holding the start of the
		 ;; statement.
		 (if (and (< (point) indent-point)
			  found-next-line)
		     (current-indentation)
		   (if commands-p
		       (goto-char expr-start)
		     (goto-char containing-sexp))
		   (+ (current-indentation) pie-indent-level)))))))))



(defun indent-pie-exp ()
  "Indent each line of the Pie grouping following point."
  (interactive)
  (let ((indent-stack (list nil))
	(contain-stack (list (point)))
	(case-fold-search nil)
	outer-loop-done inner-loop-done state ostate
	this-indent last-sexp continued-line
	(next-depth 0)
	last-depth)
    (save-excursion
      (forward-sexp 1))
    (save-excursion
      (setq outer-loop-done nil)
      (while (and (not (eobp)) (not outer-loop-done))
	(setq last-depth next-depth)
	;; Compute how depth changes over this line
	;; plus enough other lines to get to one that
	;; does not end inside a comment or string.
	;; Meanwhile, do appropriate indentation on comment lines.
	(setq inner-loop-done nil)
	(while (and (not inner-loop-done)
		    (not (and (eobp) (setq outer-loop-done t))))
	  (setq ostate state)
	  (setq state (parse-partial-sexp (point) (progn (end-of-line) (point))
					  nil nil state))
	  (setq next-depth (car state))
	  (if (and (car (cdr (cdr state)))
		   (>= (car (cdr (cdr state))) 0))
	      (setq last-sexp (car (cdr (cdr state)))))
	  (if (or (nth 4 ostate))
	      (pie-indent-line))
	  (if (or (nth 3 state))
	      (forward-line 1)
	    (setq inner-loop-done t)))
	(if (<= next-depth 0)
	    (setq outer-loop-done t))
	(if outer-loop-done
	    nil
	  ;; If this line had ..))) (((.. in it, pop out of the levels
	  ;; that ended anywhere in this line, even if the final depth
	  ;; doesn't indicate that they ended.
	  (while (> last-depth (nth 6 state))
	    (setq indent-stack (cdr indent-stack)
		  contain-stack (cdr contain-stack)
		  last-depth (1- last-depth)))
	  (if (/= last-depth next-depth)
	      (setq last-sexp nil))
	  ;; Add levels for any parens that were started in this line.
	  (while (< last-depth next-depth)
	    (setq indent-stack (cons nil indent-stack)
		  contain-stack (cons nil contain-stack)
		  last-depth (1+ last-depth)))
	  (if (null (car contain-stack))
	      (setcar contain-stack
		      (or (car (cdr state))
			  (save-excursion
			    (forward-sexp -1)
			    (point)))))
	  (forward-line 1)
	  (setq continued-line
		(save-excursion
		  (backward-char)
		  (= (preceding-char) ?\\)))
	  (skip-chars-forward " \t")
	  (if (eolp)
	      nil
	    (if (and (car indent-stack)
		     (>= (car indent-stack) 0))
		;; Line is on an existing nesting level.
		(setq this-indent (car indent-stack))
	      ;; Just started a new nesting level.
	      ;; Compute the standard indent for this level.
	      (let ((val (calculate-pie-indent
			  (if (car indent-stack)
			      (- (car indent-stack))))))
		(setcar indent-stack
			(setq this-indent val))
		(setq continued-line nil)))
	    (cond ((not (numberp this-indent)))
		  ((= (following-char) ?})
		   (setq this-indent (- this-indent pie-indent-level)))
		  ((= (following-char) ?\])
		   (setq this-indent (- this-indent 1))))
	    ;; Put chosen indentation into effect.
	    (or (null this-indent)
		(= (current-column)
		   (if continued-line
		       (+ this-indent pie-indent-level)
		     this-indent))
		(progn
		  (delete-region (point) (progn (beginning-of-line) (point)))
		  (indent-to
		   (if continued-line
		       (+ this-indent pie-indent-level)
		     this-indent)))))))))
  )



;;
;; Interfaces to other packages.
;;

(defun pie-imenu-create-index-function ()
  "Generate alist of indices for imenu."
  (let ((re (concat pie-proc-regexp "\\([^ \t\n{]+\\)"))
	alist prev-pos)
    (goto-char (point-min))
    (imenu-progress-message prev-pos 0)
    (save-match-data
      (while (re-search-forward re nil t)
	(imenu-progress-message prev-pos)
	;; Position on start of proc name, not beginning of line.
	(setq alist (cons
		     (cons (buffer-substring (match-beginning 2) (match-end 2))
			   (match-beginning 2))
		     alist))))
    (imenu-progress-message prev-pos 100)
    (nreverse alist)))

;; FIXME Definition of function is very ad-hoc.  Should use
;; pie-beginning-of-defun.  Also has incestuous knowledge about the
;; format of pie-proc-regexp.
(defun add-log-pie-defun ()
  "Return name of Pie function point is in, or nil."
  (save-excursion
    (if (re-search-backward
	 (concat pie-proc-regexp "\\([^ \t\n{]+\\)") nil t)
	(buffer-substring (match-beginning 2)
			  (match-end 2)))))

(defun pie-outline-level ()
  (save-excursion
    (skip-chars-forward " \t")
    (current-column)))



;;
;; Helper functions for inferior Pie mode.
;;

;; This exists to let us delete the prompt when commands are sent
;; directly to the inferior Pie.  See gud.el for an explanation of how
;; it all works (I took it from there).  This stuff doesn't really
;; work as well as I'd like it to.  But I don't believe there is
;; anything useful that can be done.
(defvar inferior-pie-delete-prompt-marker nil)

(defun pie-filter (proc string)
  (let ((inhibit-quit t))
    (save-excursion
      (set-buffer (process-buffer proc))
      (goto-char (process-mark proc))
      ;; Delete prompt if requested.
      (if (marker-buffer inferior-pie-delete-prompt-marker)
	  (progn
	    (delete-region (point) inferior-pie-delete-prompt-marker)
	    (set-marker inferior-pie-delete-prompt-marker nil)))))
  (if pie-using-emacs-19
      (comint-output-filter proc string)
    (funcall comint-output-filter string)))

(defun pie-send-string (proc string)
  (save-excursion
    (set-buffer (process-buffer proc))
    (goto-char (process-mark proc))
    (beginning-of-line)
    (if (looking-at comint-prompt-regexp)
	(set-marker inferior-pie-delete-prompt-marker (point))))
  (comint-send-string proc string))

(defun pie-send-region (proc start end)
  (save-excursion
    (set-buffer (process-buffer proc))
    (goto-char (process-mark proc))
    (beginning-of-line)
    (if (looking-at comint-prompt-regexp)
	(set-marker inferior-pie-delete-prompt-marker (point))))
  (comint-send-region proc start end))

(defun switch-to-pie (eob-p)
  "Switch to inferior Pie process buffer.
With argument, positions cursor at end of buffer."
  (interactive "P")
  (if (get-buffer inferior-pie-buffer)
      (pop-to-buffer inferior-pie-buffer)
    (error "No current inferior Pie buffer"))
  (cond (eob-p
	 (push-mark)
	 (goto-char (point-max)))))

(defun inferior-pie-proc ()
  "Return current inferior Pie process.
See variable `inferior-pie-buffer'."
  (let ((proc (get-buffer-process (if (eq major-mode 'inferior-pie-mode)
				      (current-buffer)
				    inferior-pie-buffer))))
    (or proc
	(error "No Pie process; see variable `inferior-pie-buffer'"))))

(defun pie-eval-region (start end &optional and-go)
  "Send the current region to the inferior Pie process.
Prefix argument means switch to the Pie buffer afterwards."
  (interactive "r\nP")
  (let ((proc (inferior-pie-proc)))
    (pie-send-region proc start end)
    (pie-send-string proc "\n")
    (if and-go (switch-to-pie t))))

(defun pie-eval-defun (&optional and-go)
  "Send the current defun to the inferior Pie process.
Prefix argument means switch to the Pie buffer afterwards."
  (interactive "P")
  (save-excursion
    (pie-end-of-defun)
    (let ((end (point)))
      (pie-beginning-of-defun)
      (pie-eval-region (point) end)))
  (if and-go (switch-to-pie t)))



;;
;; Inferior Pie mode itself.
;;

(defun inferior-pie-mode ()
  "Major mode for interacting with Pie interpreter.

A Pie process can be started with M-x inferior-pie.

Entry to this mode runs the hooks comint-mode-hook and
inferior-pie-mode-hook, in that order.

You can send text to the inferior Pie process from other buffers
containing Pie source.

Variables controlling Inferior Pie mode:
  pie-application
    Name of program to run.
  pie-command-switches
    Command line arguments to `pie-application'.
  pie-prompt-regexp
    Matches prompt.
  inferior-pie-source-command
    Command to use to read Pie file in running application.
  inferior-pie-buffer
    The current inferior Pie process buffer.  See variable
    documentation for details on multiple-process support.

The following commands are available:
\\{inferior-pie-mode-map}"
  (interactive)
  (comint-mode)
  (setq comint-prompt-regexp (or pie-prompt-regexp
				 (concat "^"
					 (regexp-quote pie-application)
					 ">")))
  (setq major-mode 'inferior-pie-mode)
  (setq mode-name "Inferior Pie")
  (if (boundp 'modeline-process)
      (setq modeline-process '(": %s"))	; For XEmacs.
    (setq mode-line-process '(": %s")))
  (use-local-map inferior-pie-mode-map)
  (setq local-abbrev-table pie-mode-abbrev-table)
  (set-syntax-table pie-mode-syntax-table)
  (if pie-using-emacs-19
      (progn
	(make-local-variable 'defun-prompt-regexp)
	(setq defun-prompt-regexp pie-omit-ws-regexp)))
  (make-local-variable 'inferior-pie-delete-prompt-marker)
  (setq inferior-pie-delete-prompt-marker (make-marker))
  (set-process-filter (get-buffer-process (current-buffer)) 'pie-filter)
  (run-hooks 'inferior-pie-mode-hook))

;;;###autoload
(defun inferior-pie (cmd)
  "Run inferior Pie process.
Prefix arg means enter program name interactively.
See documentation for function `inferior-pie-mode' for more information."
  (interactive
   (list (if current-prefix-arg
	     (read-string "Run Pie: " pie-application)
	   pie-application)))
  (if (not (comint-check-proc "*inferior-pie*"))
      (progn
	(set-buffer (apply (function make-comint) "inferior-pie" cmd nil
			   pie-command-switches))
	(inferior-pie-mode)))
  (make-local-variable 'pie-application)
  (setq pie-application cmd)
  (setq inferior-pie-buffer "*inferior-pie*")
  ;; (switch-to-buffer "*inferior-pie*")) ;;; BO
  (switch-to-buffer-other-window "*inferior-pie*"))

(and (fboundp 'defalias)
     (defalias 'run-pie 'inferior-pie))



;;
;; Auto-fill support.
;;

(defun pie-real-command-p ()
  "Return nil if point is not at the beginning of a command.
A command is the first word on an otherwise empty line, or the
first word following a semicolon, opening brace, or opening bracket."
  (save-excursion
    (skip-chars-backward " \t")
    (cond
     ((bobp) t)
     ((bolp)
      (backward-char)
      ;; Note -- continued comments are not supported here.  I
      ;; consider those to be a wart on the language.
      (not (eq ?\\ (preceding-char))))
     (t
      (memq (preceding-char) '(?\; ?{ ?\[))))))

;; FIXME doesn't actually return t.  See last case.
(defun pie-real-comment-p ()
  "Return t if point is just after the `#' beginning a real comment.
Does not check to see if previous char is actually `#'.
A real comment is either at the beginning of the buffer,
preceeded only by whitespace on the line, or has a preceeding
semicolon, opening brace, or opening bracket on the same line."
  (save-excursion
    (backward-char)
    (pie-real-command-p)))

(defun pie-hairy-scan-for-comment (state end always-stop)
  "Determine if point is in a comment.
Returns a list of the form `(FLAG . STATE)'.  STATE can be used
as input to future invocations.  FLAG is nil if not in comment,
t otherwise.  If in comment, leaves point at beginning of comment.
Only works in Emacs 19.  See also `pie-simple-scan-for-comment', a
simpler version that is often right, and works in Emacs 18."
  (let ((bol (save-excursion
	       (goto-char end)
	       (beginning-of-line)
	       (point)))
	real-comment
	last-cstart)
    (while (and (not last-cstart) (< (point) end))
      (setq real-comment nil)		;In case we've looped around and it is
                                        ;set.
      (setq state (parse-partial-sexp (point) end nil nil state t))
      (if (nth 4 state)
	  (progn
	    ;; If ALWAYS-STOP is set, stop even if we don't have a
	    ;; real comment, or if the comment isn't on the same line
	    ;; as the end.
	    (if always-stop (setq last-cstart (point)))
	    ;; If we have a real comment, then set the comment
	    ;; starting point if we are on the same line as the ending
	    ;; location.
	    (setq real-comment (pie-real-comment-p))
	    (if real-comment
		(progn
		  (and (> (point) bol) (setq last-cstart (point)))
		  ;; NOTE Emacs 19 has a misfeature whereby calling
		  ;; parse-partial-sexp with COMMENTSTOP set and with
		  ;; an initial list that says point is in a comment
		  ;; will cause an immediate return.  So we must skip
		  ;; over the comment ourselves.
		  (beginning-of-line 2)))
	    ;; Frob the state to make it look like we aren't in a
	    ;; comment.
	    (setcar (nthcdr 4 state) nil))))
    (and last-cstart
	 (goto-char last-cstart))
    (cons real-comment state)))

(defun pie-hairy-in-comment ()
  "Return t if point is in a comment, and leave point at beginning
of comment."
  (let ((save (point)))
    (pie-beginning-of-defun)
    (car (pie-hairy-scan-for-comment nil save nil))))

(defun pie-simple-in-comment ()
  "Return t if point is in comment, and leave point at beginning
of comment.  This is faster that `pie-hairy-in-comment', but is
correct less often."
  (let ((save (point))
	comment)
    (beginning-of-line)
    (while (and (< (point) save) (not comment))
      (search-forward "#" save 'move)
      (setq comment (pie-real-comment-p)))
    comment))

(defun pie-in-comment ()
  "Return t if point is in comment, and leave point at beginning
of comment."
  (if (and pie-pps-has-arg-6
	   pie-use-hairy-comment-detector)
      (pie-hairy-in-comment)
    (pie-simple-in-comment)))

(defun pie-do-fill-paragraph (ignore)
  "fill-paragraph function for Pie mode.  Only fills in a comment."
  (let (in-comment col where)
    (save-excursion
      (end-of-line)
      (setq in-comment (pie-in-comment))
      (if in-comment
	  (progn
	    (setq where (1+ (point)))
	    (setq col (1- (current-column))))))
    (and in-comment
	 (save-excursion
	   (back-to-indentation)
	   (= col (current-column)))
	 ;; In a comment.  Set the fill prefix, and find the paragraph
	 ;; boundaries by searching for lines that look like
	 ;; comment-only lines.
	 (let ((fill-prefix (buffer-substring (progn
						(beginning-of-line)
						(point))
					      where))
	       p-start p-end)
	   ;; Search backwards.
	   (save-excursion
	     (while (looking-at "^[ \t]*#")
	       (forward-line -1))
	     (forward-line)
	     (setq p-start (point)))

	   ;; Search forwards.
	   (save-excursion
	     (while (looking-at "^[ \t]*#")
	       (forward-line))
	     (setq p-end (point)))

	   ;; Narrow and do the fill.
	   (save-restriction
	     (narrow-to-region p-start p-end)
	     (fill-paragraph ignore)))))
  t)

(defun pie-do-auto-fill ()
  "Auto-fill function for Pie mode.  Only auto-fills in a comment."
  (let ((fill-prefix "# ")
	in-comment col)
    (save-excursion
      (setq in-comment (pie-in-comment))
      (if in-comment
	  (setq col (1- (current-column)))))
    (if in-comment
	(progn
	  (do-auto-fill)
	  (save-excursion
	    (back-to-indentation)
	    (delete-region (point) (save-excursion
				     (beginning-of-line)
				     (point)))
	    (indent-to-column col))))))



;;
;; Help-related code.
;;

(defvar pie-help-saved-dirs nil
  "Saved help directories.
If `pie-help-directory-list' changes, this allows `pie-help-on-word'
to update the alist.")

(defvar pie-help-alist nil
  "Alist with command names as keys and filenames as values.")

(defun pie-help-snarf-commands (dirlist)
  "Build alist of commands and filenames."
  (while dirlist
    (let ((files (directory-files (car dirlist) t)))
      (while files
	(if (and (file-directory-p (car files))
		 (not
		  (let ((fpart (file-name-nondirectory (car files))))
		    (or (equal fpart ".")
			(equal fpart "..")))))
	    (let ((matches (directory-files (car files) t)))
	      (while matches
		(or (file-directory-p (car matches))
		    (setq pie-help-alist
			  (cons
			   (cons (file-name-nondirectory (car matches))
				 (car matches))
			   pie-help-alist)))
		(setq matches (cdr matches)))))
	(setq files (cdr files))))
    (setq dirlist (cdr dirlist))))

(defun pie-reread-help-files ()
  "Set up to re-read files, and then do it."
  (interactive)
  (message "Building Pie help file index...")
  (setq pie-help-saved-dirs pie-help-directory-list)
  (setq pie-help-alist nil)
  (pie-help-snarf-commands pie-help-directory-list)
  (message "Building Pie help file index...done"))

(defun pie-current-word (flag)
  "Return current command word, or nil.
If FLAG is nil, just uses `current-word'.
Otherwise scans backward for most likely Pie command word."
  (if (and flag
	   (memq major-mode '(pie-mode inferior-pie-mode)))
      (condition-case nil
	  (save-excursion
	    ;; Look backward for first word actually in alist.
	    (if (bobp)
		()
	      (while (and (not (bobp))
			  (not (pie-real-command-p)))
		(backward-sexp)))
	    (if (assoc (current-word) pie-help-alist)
		(current-word)))
	(error nil))
    (current-word)))

;;;###autoload
(defun pie-help-on-word (command &optional arg)
  "Get help on Pie command.  Default is word at point.
Prefix argument means invert sense of `pie-use-smart-word-finder'."
  (interactive
   (list
    (progn
      (if (not (equal pie-help-directory-list pie-help-saved-dirs))
	  (pie-reread-help-files))
      (let ((word (pie-current-word
		   (if current-prefix-arg
		       (not pie-use-smart-word-finder)
		     pie-use-smart-word-finder))))
	(completing-read
	 (if (or (null word) (string= word ""))
	     "Help on Pie command: "
	   (format "Help on Pie command (default %s): " word))
	 pie-help-alist nil t)))
    current-prefix-arg))
  (if (not (equal pie-help-directory-list pie-help-saved-dirs))
      (pie-reread-help-files))
  (if (string= command "")
      (setq command (pie-current-word
		     (if arg
			 (not pie-use-smart-word-finder)
		       pie-use-smart-word-finder))))
  (let* ((help (get-buffer-create "*Pie help*"))
	 (cell (assoc command pie-help-alist))
	 (file (and cell (cdr cell))))
    (set-buffer help)
    (delete-region (point-min) (point-max))
    (if file
	(progn
	  (insert "*** " command "\n\n")
	  (insert-file-contents file))
      (if (string= command "")
	  (insert "Magical Pig!")
	(insert "Pie command " command " not in help\n")))
    (set-buffer-modified-p nil)
    (goto-char (point-min))
    (display-buffer help)))



;;
;; Other interactive stuff.
;;

(defun pie-maille-region nil
  "Maille le contenu du buffer."
  (interactive)
  (message "On maille! ...")
  (save-excursion
    (shell-command-on-region (region-beginning) (region-end) pie-maille-command nil)
    (set-buffer "*Shell Command Output*")
    (pie-mode)
    ))

(defun pie-maille-buffer nil
  "Maille le contenu du buffer."
  (interactive)
  (save-excursion
    (mark-whole-buffer)
    (pie-maille-region)))

(defvar pie-previous-dir/file nil
  "Record last directory and file used in loading.
This holds a cons cell of the form `(DIRECTORY . FILE)'
describing the last `pie-load-file' command.")

(defun pie-load-file (file &optional and-go)
  "Load a Pie file into the inferior Pie process.
Prefix argument means switch to the Pie buffer afterwards."
  (interactive
   (list
    ;; car because comint-get-source returns a list holding the
    ;; filename.
    (car (comint-get-source "Load Pie file: "
			    (or (and
				 (eq major-mode 'pie-mode)
				 (buffer-file-name))
				pie-previous-dir/file)
			    '(pie-mode) t))
    current-prefix-arg))
  (comint-check-source file)
  (setq pie-previous-dir/file (cons (file-name-directory file)
				    (file-name-nondirectory file)))
  (pie-send-string (inferior-pie-proc)
		   (format inferior-pie-source-command (pie-quote file)))
  (if and-go (switch-to-pie t)))

(defun pie-restart-with-file (file &optional and-go)
  "Restart inferior Pie with file.
If an inferior Pie process exists, it is killed first.
Prefix argument means switch to the Pie buffer afterwards."
  (interactive
   (list
    (car (comint-get-source "Restart with Pie file: "
			    (or (and
				 (eq major-mode 'pie-mode)
				 (buffer-file-name))
				pie-previous-dir/file)
			    '(pie-mode) t))
    current-prefix-arg))
  (let* ((buf (if (eq major-mode 'inferior-pie-mode)
		  (current-buffer)
		inferior-pie-buffer))
	 (proc (and buf (get-process buf))))
    (cond
     ((not (and buf (get-buffer buf)))
      ;; I think this will be ok.
      (inferior-pie pie-application)
      (pie-load-file file and-go))
     ((or
       (not (comint-check-proc buf))
       (yes-or-no-p
	"A Pie process is running, are you sure you want to reset it? "))
      (save-excursion
	(comint-check-source file)
	(setq pie-previous-dir/file (cons (file-name-directory file)
					  (file-name-nondirectory file)))
	(comint-exec (get-buffer-create buf)
		     (if proc
			 (process-name proc)
		       "inferior-pie")
		     pie-application file pie-command-switches)
	(if and-go (switch-to-pie t)))))))

;; FIXME I imagine you can do this under Emacs 18.  I just don't know
;; how.
(defun pie-auto-fill-mode (&optional arg)
  "Like `auto-fill-mode', but controls filling of Pie comments."
  (interactive "P")
  (and (not pie-using-emacs-19)
       (error "You must use Emacs 19 to get this feature."))
  ;; Following code taken from "auto-fill-mode" (simple.el).
  (prog1
      (setq auto-fill-function
	    (if (if (null arg)
		    (not auto-fill-function)
		  (> (prefix-numeric-value arg) 0))
		'pie-do-auto-fill
	      nil))
    ;; Update mode line.  FIXME I'd use force-mode-line-update, but I
    ;; don't know if it exists in v18.
    (set-buffer-modified-p (buffer-modified-p))))

(defun pie-electric-hash (&optional count)
  "Insert a `#' and quote if it does not start a real comment.
Prefix arg is number of `#'s to insert.
See variable `pie-electric-hash-style' for description of quoting
styles."
  (interactive "p")
  (or count (setq count 1))
  (if (> count 0)
      (let ((type
	     (if (eq pie-electric-hash-style 'smart)
		 (if (> count 3)	; FIXME what is "smart"?
		     'quote
		   'backslash)
	       pie-electric-hash-style))
	    comment)
	(if type
	    (progn
	      (save-excursion
		(insert "#")
		(setq comment (pie-in-comment)))
	      (delete-char 1)
	      (and pie-explain-indentation (message "comment: %s" comment))
	      (cond
	       ((eq type 'quote)
		(if (not comment)
		    (insert "\"")))
	       ((eq type 'backslash)
		;; The following will set count to 0, so the
		;; insert-char can still be run.
		(if (not comment)
		    (while (> count 0)
		      (insert "\\#")
		      (setq count (1- count)))))
	       (t nil))))
	(insert-char ?# count))))

(defun pie-hashify-buffer ()
  "Quote all `#'s in current buffer that aren't Pie comments."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (if (and pie-pps-has-arg-6 pie-use-hairy-comment-detector)
	(let (state
	      result)
	  (while (< (point) (point-max))
	    (setq result (pie-hairy-scan-for-comment state (point-max) t))
	    (if (car result)
		(beginning-of-line 2)
	      (backward-char)
	      (if (eq ?# (following-char))
		  (insert "\\"))
	      (forward-char))
	    (setq state (cdr result))))
      (while (and (< (point) (point-max))
		  (search-forward "#" nil 'move))
	(if (pie-real-comment-p)
	    (beginning-of-line 2)
	  ;; There's really no good way for the simple converter to
	  ;; work.  So we just quote # if it isn't already quoted.
	  ;; Bogus, but it works.
	  (backward-char)
	  (if (not (eq ?\\ (preceding-char)))
	      (insert "\\"))
	  (forward-char))))))

(defun pie-indent-for-comment ()
  "Indent this line's comment to comment column, or insert an empty comment.
Is smart about syntax of Pie comments.
Parts of this were taken from indent-for-comment (simple.el)."
  (interactive "*")
  (end-of-line)
  (or (pie-in-comment)
      (progn
	;; Not in a comment, so we have to insert one.  Create an
	;; empty comment (since there isn't one on this line).  If
	;; line is not blank, make sure we insert a ";" first.
	(skip-chars-backward " \t")
	(let ((eolpoint (point)))
	  (beginning-of-line)
	  (if (/= (point) eolpoint)
	      (progn
		(goto-char eolpoint)
		(insert
		 (if (pie-real-command-p) "" ";")
		 "# ")
		(backward-char))))))
  ;; Point is just after the "#" starting a comment.  Move it as
  ;; appropriate.
  (let* ((indent (if comment-indent-hook
		     (funcall comment-indent-hook)
		   (funcall comment-indent-function)))
	 (begpos (progn
		   (backward-char)
		   (point))))
    (if (/= begpos indent)
	(progn
	  (skip-chars-backward " \t" (save-excursion
				       (beginning-of-line)
				       (point)))
	  (delete-region (point) begpos)
	  (indent-to indent)))
    (looking-at comment-start-skip)	; Always true.
    (goto-char (match-end 0))
    ;; I don't like the effect of the next two.
    ;;(skip-chars-backward " \t" (match-beginning 0))
    ;;(skip-chars-backward "^ \t" (match-beginning 0))
    ))

;; The following was inspired by the Pie editing mode written by
;; Gregor Schmid <schmid@fb3-s7.math.TU-Berlin.DE>.  His version also
;; attempts to snarf the command line options from the command line,
;; but I didn't think that would really be that helpful (doesn't seem
;; like it owould be right enough.  His version also looks for the
;; "#!/bin/csh ... exec" hack, but that seemed even less useful.
;; FIXME should make sure that the application mentioned actually
;; exists.
(defun pie-guess-application ()
  "Attempt to guess Pie application by looking at first line.
The first line is assumed to look like \"#!.../program ...\"."
  (save-excursion
    (goto-char (point-min))
    (if (looking-at "#![^ \t]*/\\([^ \t\n/]+\\)\\([ \t]\\|$\\)")
	(progn
	  (make-local-variable 'pie-application)
	  (setq pie-application (buffer-substring (match-beginning 1)
						  (match-end 1)))))))

;; This only exists to put on the menubar.  I couldn't figure out any
;; other way to do it.  FIXME should take "number of #-marks"
;; argument.
(defun pie-uncomment-region (beg end)
  "Uncomment region."
  (interactive "r")
  (comment-region beg end -1))



;;
;; XEmacs menu support.
;; Taken from schmid@fb3-s7.math.TU-Berlin.DE (Gregor Schmid),
;; who wrote a different Pie mode.
;; We also have support for menus in FSF.  We do this by
;; loading the XEmacs menu emulation code.
;;

(defun pie-popup-menu (e)
  (interactive "@e")
  (and pie-using-emacs-19
       (not pie-using-xemacs-19)
       (if pie-using-emacs-19-23
	   (require 'lmenu)
	 ;; CAVEATS:
	 ;; * lmenu.el provides 'menubar, which is bogus.
	 ;; * lmenu.el causes menubars to be turned on everywhere.
	 ;;   Doubly bogus!
	 ;; Both of these problems are fixed in Emacs 19.23.  People
	 ;; using an Emacs before that just suffer.
	 (require 'menubar "lmenu")))  ;; This is annoying
  ;; IMHO popup-menu should be autoloaded in FSF Emacs.  Oh well.
  (popup-menu pie-xemacs-menu))



;;
;; Quoting and unquoting functions.
;;

;; This quoting is sufficient to protect eg a filename from any sort
;; of expansion or splitting.  Pie quoting sure sucks.
(defun pie-quote (string)
  "Quote STRING according to Pie rules."
  (mapconcat (function (lambda (char)
			 (if (memq char '(?[ ?] ?{ ?} ?\\ ?\" ?$ ?  ?\;))
			     (concat "\\" (char-to-string char))
			   (char-to-string char))))
	     string ""))



;;
;; Bug reporting.
;;

(and (fboundp 'eval-when-compile)
     (eval-when-compile
       (require 'reporter)))

(defun pie-submit-bug-report ()
  "Submit via mail a bug report on Pie mode."
  (interactive)
  (require 'reporter)
  (and
   (y-or-n-p "Do you really want to submit a bug report on Pie mode? ")
   (reporter-submit-bug-report
    pie-maintainer
    (concat "Pie mode " pie-version)
    '(pie-indent-level
      pie-continued-indent-level
      pie-auto-newline
      pie-tab-always-indent
      pie-use-hairy-comment-detector
      pie-electric-hash-style
      pie-help-directory-list
      pie-use-smart-word-finder
      pie-application
      pie-command-switches
      pie-prompt-regexp
      inferior-pie-source-command
      pie-using-emacs-19
      pie-using-emacs-19-23
      pie-using-xemacs-19
      pie-proc-list
      pie-proc-regexp
      pie-typeword-list
      pie-keyword-list
      pie-font-lock-keywords
      pie-pps-has-arg-6))))



(provide 'pie)

;;; pie.el ends here
