(setq ps-left-margin 60 ps-right-margin 20
      ps-bottom-margin 80 ps-top-margin 60
      ;; ps-font-size 10
      ps-font-size 11
      ;; Font size, in points, for generating Postscript.
      ;; ps-avg-char-width (if (fboundp 'float) 5.6 6)
      ps-avg-char-width (if (fboundp 'float) 4.2 4)
      ;; The average width, in points, of a character, for generating Postscript.
      ;; This is the value that ps-print uses to determine the length, x-dimension,
      ;; of the text it has printed, and thus affects the point at which long lines
      ;; wrap around.  If you change the font or font size, you will probably have
      ;; to adjust this value to match.
      ;; ps-space-width (if (fboundp 'float) 5.6 6)
      ps-space-width ps-avg-char-width
      ;; The width of a space character, for generating Postscript.
      ;; This value is used in expanding tab characters.
      ;; ps-line-height (if (fboundp 'float) 11.29 11)
      ps-line-height (if (fboundp 'float) 7.8 8)
      ;; The height of a line, for generating Postscript.  This is the value that
      ;; ps-print uses to determine the height, y-dimension, of the lines of text
      ;; it has printed, and thus affects the point at which page-breaks are
      ;; placed.  If you change the font or font size, you will probably have to
      ;; adjust this value to match.  The line-height is *not* the same as the
      ;; point size of the font.
      ;; ps-lpr-command "ghostview"            ps-lpr-switches '("-")
      ;; ps-always-build-face-reference t
      )
