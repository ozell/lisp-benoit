;;; Definition du mode Vu (from tcl.el)
;;; Copyright (C) Benoit Ozell, benoit.ozell@polymtl.ca.

;; HOW TO INSTALL:
;; Put the following forms in your .emacs to enable autoloading of Vu
;; mode, and auto-recognition of ".vu" files.
;;
;;   (autoload 'vu-mode "vu" "Vu mode." t)
;;   (autoload 'inferior-vu "vu" "Run inferior Vu process." t)
;;   (setq auto-mode-alist (append '(("\\.vu$" . vu-mode)) auto-mode-alist))
;;
;; If you plan to use the interface to the VuX help files, you must
;; set the variable vu-help-directory-list to point to the topmost
;; directories containing the VuX help files.  Eg:
;;
;;   (setq vu-help-directory-list '("/usr/local/lib/vux/help"))
;;
;; Also you will want to add the following to your .emacs:
;;
;;   (autoload 'vu-help-on-word "vu" "Help on Vu commands" t)
;;

;;; Commentary:

;; CUSTOMIZATION NOTES:
;; * vu-proc-list can be used to customize a list of things that
;; "define" other things.  Eg in my project I put "defvar" in this
;; list.
;; * vu-typeword-list is similar, but uses font-lock-type-face.
;; * vu-keyword-list is a list of keywords.  I've generally used this
;; for flow-control words.  Eg I add "unwind_protect" to this list.
;; * vu-type-alist can be used to minimally customize indentation
;; according to context.




;;; Code:

;; I sure wish Emacs had a package that made it easy to extract this
;; sort of information.
(defconst vu-using-emacs-19 (featurep 'xemacs)
  "Nil unless using Emacs 19 (XEmacs or FSF).")

;; FIXME this will break on Emacs 19.100.
(defconst vu-using-emacs-19-23
  (string-match "19\\.\\(2[3-9]\\|[3-9][0-9]\\)" emacs-version)
  "Nil unless using Emacs 19-23 or later.")

(defconst vu-using-xemacs-19 (string-match "XEmacs" emacs-version)
  "Nil unless using XEmacs).")

(require 'comint)

;; When compiling under GNU Emacs, load imenu during compilation.  If
;; you have 19.22 or earlier, comment this out, or get imenu.
(and (fboundp 'eval-when-compile)
     (eval-when-compile
       (if (and (string-match "19\\." emacs-version)
		(not (string-match "XEmacs" emacs-version)))
	   (require 'imenu))
       ()))

(defconst vu-version "1.43")
(defconst vu-maintainer "Benoit Ozell <benoit.ozell@polymtl.ca>") ;;;BO

;;
;; User variables.
;;

(defvar vu-indent-level 3 ;;;BO
  "*Indentation of Vu statements with respect to containing block.")

(defvar vu-continued-indent-level 3 ;;;BO
  "*Indentation of continuation line relative to first line of command.")

(defvar vu-auto-newline nil
  "*Non-nil means automatically newline before and after braces
inserted in Vu code.")

(defvar vu-tab-always-indent t
  "*Control effect of TAB key.
If t (the default), always indent current line.
If nil and point is not in the indentation area at the beginning of
the line, a TAB is inserted.
Other values cause the first possible action from the following list
to take place:

  1. Move from beginning of line to correct indentation.
  2. Delete an empty comment.
  3. Move forward to start of comment, indenting if necessary.
  4. Move forward to end of line, indenting if necessary.
  5. Create an empty comment.
  6. Move backward to start of comment, indenting if necessary.")

(defvar vu-use-hairy-comment-detector t
  "*If not `nil', the the more complicated, but slower, comment
detecting function is used.  This variable is only used in GNU Emacs
19 (the fast function is always used elsewhere).")

(defvar vu-electric-hash-style 'smart
  "*Style of electric hash insertion to use.
Possible values are 'backslash, meaning that `\\' quoting should be
done; `quote, meaning that `\"' quoting should be done; 'smart,
meaning that the choice between 'backslash and 'quote should be
made depending on the number of hashes inserted; or nil, meaning that
no quoting should be done.  Any other value for this variable is
taken to mean 'smart.  The default is 'smart.")

(defvar vu-help-directory-list nil
  "*List of topmost directories containing VuX help files")

(defvar vu-use-smart-word-finder t
  "*If not nil, use a better way of finding the current word when
looking up help on a Vu command.")
;;;BO
(defvar vu-application "Vu"
  "*Name of Vu application to run in inferior Vu mode.")

(defvar vu-command-switches '("-pc")
  "*Switches to supply to `vu-application'.")

(defvar vu-maille-command "maille.sh"
  "Nom du mailleur.")

;;;BO
;;(defvar vu-prompt-regexp "^\\(% \\|\\)"
(defvar vu-prompt-regexp "^\\(([0-9]*) Vu >\\)"
  "*If not nil, a regexp that will match the prompt in the inferior process.
If nil, the prompt is the name of the application with \">\" appended.

The default is \"^\\(% \\|\\)\", which will match the default primary
and secondary prompts for vush and wish.")

;;;BO
(defvar inferior-vu-source-command "open \"%s\"\n"
  "*Format-string for building a Vu command to load a file.
This format string should use `%s' to substitute a file name
and should result in a Vu expression that will command the
inferior Vu to load that file.  The filename will be appropriately
quoted for Vu.")

;;
;; Keymaps, abbrevs, syntax tables.
;;

(defvar vu-mode-abbrev-table nil
  "Abbrev table in use in Vu-mode buffers.")
(if vu-mode-abbrev-table
    ()
  (define-abbrev-table 'vu-mode-abbrev-table ()))

(defvar vu-mode-map ()
  "Keymap used in Vu mode.")

(defvar vu-mode-syntax-table nil
  "Syntax table in use in Vu-mode buffers.")
(if vu-mode-syntax-table
    ()
  (setq vu-mode-syntax-table (make-syntax-table))
;;;BO
  ;;(modify-syntax-entry ?%  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?@  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?&  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?*  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?+  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?-  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?.  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?:  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?!  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?$  "_" vu-mode-syntax-table) ; FIXME use "'"?
  ;;(modify-syntax-entry ?/  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?~  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?<  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?=  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?>  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?|  "_" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?\(  "()" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?\)  ")(" vu-mode-syntax-table)
  ;;(modify-syntax-entry ?\;  "." vu-mode-syntax-table)
  ;;(modify-syntax-entry ?\n ">   " vu-mode-syntax-table)
  ;;(modify-syntax-entry ?\f ">   " vu-mode-syntax-table)
  ;;(modify-syntax-entry ?# "<   " vu-mode-syntax-table)
  ;;     ma version:
  (modify-syntax-entry ?%  "."     vu-mode-syntax-table)
  (modify-syntax-entry ?+  "."     vu-mode-syntax-table)
  (modify-syntax-entry ?-  "."     vu-mode-syntax-table)
  (modify-syntax-entry ?=  "."     vu-mode-syntax-table)
  (modify-syntax-entry ?<  "."     vu-mode-syntax-table)
  (modify-syntax-entry ?>  "."     vu-mode-syntax-table)
  (modify-syntax-entry ?&  "."     vu-mode-syntax-table)
  (modify-syntax-entry ?|  "."     vu-mode-syntax-table)
  (modify-syntax-entry ?\;  "."    vu-mode-syntax-table)
  (modify-syntax-entry ?\(  "()"   vu-mode-syntax-table)
  (modify-syntax-entry ?\)  ")("   vu-mode-syntax-table)
  (modify-syntax-entry ?\\ "\\"    vu-mode-syntax-table)
  (modify-syntax-entry ?\' "\""    vu-mode-syntax-table)
  (modify-syntax-entry ?/  ". 1456" vu-mode-syntax-table)
  (modify-syntax-entry ?*  ". 23"   vu-mode-syntax-table)
  (modify-syntax-entry ?\n "> b" vu-mode-syntax-table)
  (modify-syntax-entry ?\f "> b" vu-mode-syntax-table)
  ;; Give CR the same syntax as newline, for selective-display
  ;;(modify-syntax-entry ?\^m "> b"    vu-mode-syntax-table)
  )

(defvar inferior-vu-mode-map nil
  "Keymap used in Inferior Vu mode.")

;; XEmacs menu.
(defvar vu-xemacs-menu
  '("Vu" ;;;BO
    ["Beginning of function" vu-beginning-of-defun t]
    ["End of function" vu-end-of-defun t]
    ["Mark function" vu-mark-defun t]
    ["Indent region" indent-region (vu-mark)]
    ["Comment region" comment-region (vu-mark)]
    ["Uncomment region" vu-uncomment-region (vu-mark)]
    "----"
    ["Show Vu process buffer" inferior-vu t]
    ["Send function to Vu process" vu-eval-defun
     (and inferior-vu-buffer (get-buffer inferior-vu-buffer))]
    ["Send region to Vu process" vu-eval-region
     (and inferior-vu-buffer (get-buffer inferior-vu-buffer))]
    ["Send file to Vu process" vu-load-file
     (and inferior-vu-buffer (get-buffer inferior-vu-buffer))]
    ["Restart Vu process with file" vu-restart-with-file t]
    "----"
    ["Maille region" vu-maille-region (vu-mark)]
    ["Maille buffer" vu-maille-buffer t]
    "----"
    ["Vu help" vu-help-on-word vu-help-directory-list]
    ["Send bug report" vu-submit-bug-report t])
  "XEmacs menu for Vu mode.")

;; GNU Emacs does menus via keymaps.  Do it in a function in case we
;; later decide to add it to inferior Vu mode as well.
(defun vu-add-fsf-menu (map)
  (define-key map [menu-bar] (make-sparse-keymap))
  ;; This fails in Emacs 19.22 and earlier.
  (require 'lmenu)
  (let ((menu (make-lucid-menu-keymap "Vu" vu-xemacs-menu)))
    (define-key map [menu-bar vu] (cons "Vu" menu))
    ;; The following is intended to compute the key sequence
    ;; information for the menu.  It doesn't work.
    (x-popup-menu nil menu)))

(defun vu-fill-mode-map ()
  (define-key vu-mode-map "{" 'vu-electric-char)
  (define-key vu-mode-map "}" 'vu-electric-brace)
  ;;(define-key vu-mode-map "[" 'vu-electric-char) ;;;BO
  ;;(define-key vu-mode-map "]" 'vu-electric-char) ;;;BO
  (define-key vu-mode-map ";" 'vu-electric-char)
  ;;(define-key vu-mode-map "#" 'vu-electric-hash) ;;;BO
  ;; FIXME.
  (define-key vu-mode-map "\e\C-a" 'vu-beginning-of-defun)
  ;; FIXME.
  (define-key vu-mode-map "\e\C-e" 'vu-end-of-defun)
  ;; FIXME.
  ;;(define-key vu-mode-map "\e\C-h" 'vu-mark-defun) ;;;BO
  (define-key vu-mode-map "\e\C-q" 'indent-vu-exp)
  ;;(define-key vu-mode-map "\177" 'backward-delete-char-untabify) ;;;BO
  (define-key vu-mode-map "\t" 'vu-indent-command)
  (define-key vu-mode-map "\M-;" 'vu-indent-for-comment)
  (define-key vu-mode-map "\M-\C-x" 'vu-eval-defun)
  (define-key vu-mode-map "\C-c\C-b" 'vu-submit-bug-report)
  (and (fboundp 'comment-region)
       (define-key vu-mode-map "\C-c\C-c" 'comment-region))
  (define-key vu-mode-map "\C-c\C-i" 'vu-help-on-word)
  (define-key vu-mode-map "\C-c\C-v" 'vu-eval-defun)
  (define-key vu-mode-map "\C-c\C-f" 'vu-load-file)
  ;;(define-key vu-mode-map "\C-c\C-t" 'inferior-vu) ;;;BO
  (define-key vu-mode-map "\C-c\C-p" 'inferior-vu) ;;;BO
  (define-key vu-mode-map "\C-c\C-x" 'vu-eval-region)
  (define-key vu-mode-map "\C-cm" 'vu-maille-buffer)
  (define-key vu-mode-map "\C-cM" 'vu-maille-region)
  (define-key vu-mode-map "\C-c0" 'cerca-cpp-if)

  ;; Make menus.
 ;;;BO
  ;;(if (and vu-using-emacs-19 (not vu-using-xemacs-19))
  ;;    (progn
  ;;	(vu-add-fsf-menu vu-mode-map))))
  (if vu-using-emacs-19
      (if vu-using-xemacs-19
	  ;; In XEmacs, button 3 seems to be the standard for this.
	  (define-key vu-mode-map 'button3 'vu-popup-menu)
	;; In FSF 19, there is no standard, so I use shift-button2.
	(vu-add-fsf-menu vu-mode-map)
	(define-key vu-mode-map [S-down-mouse-2] 'vu-popup-menu))))

(defun vu-fill-inferior-map ()
  (define-key inferior-vu-mode-map "\t" 'comint-dynamic-complete)
  (define-key inferior-vu-mode-map "\M-?"
    'comint-dynamic-list-filename-completions)
  (define-key inferior-vu-mode-map "\e\C-a" 'vu-beginning-of-defun)
  (define-key inferior-vu-mode-map "\e\C-e" 'vu-end-of-defun)
  (define-key inferior-vu-mode-map "\177" 'backward-delete-char-untabify)
  (define-key inferior-vu-mode-map "\M-\C-x" 'vu-eval-defun)
  (define-key inferior-vu-mode-map "\C-c\C-b" 'vu-submit-bug-report)
  (define-key inferior-vu-mode-map "\C-c\C-i" 'vu-help-on-word)
  (define-key inferior-vu-mode-map "\C-c\C-v" 'vu-eval-defun)
  (define-key inferior-vu-mode-map "\C-c\C-f" 'vu-load-file)
  (define-key inferior-vu-mode-map "\C-c\C-t" 'inferior-vu)
  (define-key inferior-vu-mode-map "\C-c\C-x" 'vu-eval-region)
  (define-key inferior-vu-mode-map "\C-c\C-s" 'switch-to-vu))

(if vu-mode-map
    ()
  (setq vu-mode-map (make-sparse-keymap))
  (vu-fill-mode-map))

(if inferior-vu-mode-map
    ()
  ;; FIXME Use keymap inheritance here?  FIXME we override comint
  ;; keybindings here.  Maybe someone has a better set?
  (setq inferior-vu-mode-map (copy-keymap comint-mode-map))
  (vu-fill-inferior-map))


(defvar inferior-vu-buffer nil
  "*The current inferior-vu process buffer.

MULTIPLE PROCESS SUPPORT
===========================================================================
To run multiple Vu processes, you start the first up with
\\[inferior-vu].  It will be in a buffer named `*inferior-vu*'.
Rename this buffer with \\[rename-buffer].  You may now start up a new
process with another \\[inferior-vu].  It will be in a new buffer,
named `*inferior-vu*'.  You can switch between the different process
buffers with \\[switch-to-buffer].

Commands that send text from source buffers to Vu processes -- like
`vu-eval-defun' or `vu-load-file' -- have to choose a process to
send to, when you have more than one Vu process around.  This is
determined by the global variable `inferior-vu-buffer'.  Suppose you
have three inferior Lisps running:
    Buffer              Process
    foo                 inferior-vu
    bar                 inferior-vu<2>
    *inferior-vu*      inferior-vu<3>
If you do a \\[vu-eval-defun] command on some Lisp source code, what
process do you send it to?

- If you're in a process buffer (foo, bar, or *inferior-vu*),
  you send it to that process.
- If you're in some other buffer (e.g., a source file), you
  send it to the process attached to buffer `inferior-vu-buffer'.
This process selection is performed by function `inferior-vu-proc'.

Whenever \\[inferior-vu] fires up a new process, it resets
`inferior-vu-buffer' to be the new process's buffer.  If you only run
one process, this does the right thing.  If you run multiple
processes, you can change `inferior-vu-buffer' to another process
buffer with \\[set-variable].")

;;
;; Hooks and other customization.
;;

(defvar vu-mode-hook nil
  "Hook run on entry to Vu mode.

Several functions exist which are useful to run from your
`vu-mode-hook' (see each function's documentation for more
information):

  vu-guess-application
    Guesses a default setting for `vu-application' based on any
    \"#!\" line at the top of the file.
  vu-hashify-buffer
    Quotes all \"#\" characters that don't correspond to actual
    Vu comments.  (Useful when editing code not originally created
    with this mode).
  vu-auto-fill-mode
    Auto-filling of Vu comments.

Emacs 19 users can add functions to the hook with `add-hook':

   (add-hook 'vu-mode-hook 'vu-guess-application)

Emacs 18 users must use `setq':

   (setq vu-mode-hook (cons 'vu-guess-application vu-mode-hook))")


(defvar inferior-vu-mode-hook nil
  "Hook for customizing Inferior Vu mode.")

(defvar vu-proc-list
  '("proc" "method" "ivu_class")
  "List of commands whose first argument defines something.
This exists because some people (eg, me) use \"defvar\" et al.
Call `vu-set-proc-regexp' and `vu-set-font-lock-keywords'
after changing this list.")

(defvar vu-proc-regexp nil
  "Regexp to use when matching proc headers.")

 ;;;BO
;;(defvar vu-typeword-list
;;  '("global" "upvar" "inherit" "public" "protected" "common")
;;  "List of Vu keywords denoting \"type\".  Used only for highlighting.
;;Call `vu-set-font-lock-keywords' after changing this list.")
(defvar vu-typeword-list
  '("[Tt][Ee][Xx][Tt][Ee]"
    "[Mm][Aa][Ii][Ll][Ll][Aa][Gg][Ee]"
    "[Gg][Rr][Ii][Dd]"
    "[Mm][Ee][Ss][Hh]"
    "[Zz][Oo][Nn][Ee]"
    "[Ss][Oo][Ll][Uu][Tt][Ii][Oo][Nn]"
    "[Vv][Aa][Rr][Ii][Aa][Bb][Ll][Ee]"
    "[Bb][Ii][Tt][Mm][Aa][Pp]"
    "[Cc][Hh][Aa][Mm][Pp]"
    "[Cc][Hh][Aa][Mm][Pp]<char>"
    "[Cc][Hh][Aa][Mm][Pp]<unsigned char>"
    "[Cc][Hh][Aa][Mm][Pp]<short>"
    "[Cc][Hh][Aa][Mm][Pp]<unsigned short>"
    "[Cc][Hh][Aa][Mm][Pp]<int>"
    "[Cc][Hh][Aa][Mm][Pp]<unsigned int>"
    "[Cc][Hh][Aa][Mm][Pp]<float>"
    "[Cc][Hh][Aa][Mm][Pp]<double>"
    "[Ff][Ii][Ee][Ll][Dd]"
    "[Ff][Ii][Ee][Ll][Dd]<char>"
    "[Ff][Ii][Ee][Ll][Dd]<unsigned char>"
    "[Ff][Ii][Ee][Ll][Dd]<short>"
    "[Ff][Ii][Ee][Ll][Dd]<unsigned short>"
    "[Ff][Ii][Ee][Ll][Dd]<int>"
    "[Ff][Ii][Ee][Ll][Dd]<unsigned int>"
    "[Ff][Ii][Ee][Ll][Dd]<float>"
    "[Ff][Ii][Ee][Ll][Dd]<double>"
    "[Gg][Ee][Oo][Mm][Ee][Tt][Rr][Ii][Ee]"
    "[Zz][Oo][Nn][Aa][Gg][Ee]"
    "[Pp][Oo][Ii][Nn][Tt]"
    "[Cc][Oo][Uu][Rr][Bb][Ee]"
    "[Cc][Uu][Rr][Vv][Ee]"
    "[Ss][Uu][Rr][Ff][Aa][Cc][Ee]"
    "[Ss][Oo][Mm][Mm][Ee][Tt]"
    "[Vv][Ee][Rr][Tt][Ee][Xx]"
    "[Aa][Rr][Ee][Tt][Ee]"
    "[Ee][Dd][Gg][Ee]"
    "[Ff][Aa][Cc][Ee]"
    "[Vv][Oo][Ll][Uu][Mm][Ee]"
    "[Aa][Nn][Nn][Oo][Tt][Aa][Tt][Ii][Oo][Nn]"
    "[Ff][Oo][Nn][Cc][Tt][Ii][Oo][Nn]"
    "[Tt][Ee][Aa][Pp][Oo][Tt]"
    "[Ss][Pp][Hh][Ee][Rr][Ee]"
    "[Cc][Yy][Ll][Ii][Nn][Dd][Rr][Ee]"
    "[Cc][Yy][Ll][Ii][Nn][Dd][Ee][Rr]"
    "[Aa][Tt][Oo][Mm][Ee]"
    "[Aa][Tt][Oo][Mm]"
    "[Ll][Ii][Ee][Nn]"
    "[Bb][Oo][Nn][Dd]"
    "FENETRE"
    "SUPPORT"
    "MATERIAU"
    "PALETTE"
    "LUMIERE"
    "GEOMETRIE"
    "ENTITE"
    "IMAGE"
    "VUE"
    "SEQUENCE"
    )
  "List of Vu keywords denoting \"type\".  Used only for highlighting.
Call `vu-set-font-lock-keywords' after changing this list.")

;; Generally I've picked control operators to be keywords.
;;;BO
;;(defvar vu-keyword-list
;;  '("if" "then" "else" "elseif" "for" "foreach" "break" "continue" "while"
;;    "eval" "case" "in" "switch" "default" "exit" "error" "proc" "return"
;;    "uplevel" "constructor" "destructor" "ivu_class" "loop" "for_array_keys"
;;    "for_recursive_glob" "for_file")
;;  "List of Vu keywords.  Used only for highlighting.
;;Default list includes some VuX keywords.
;;Call `vu-set-font-lock-keywords' after changing this list.")
(defvar vu-keyword-list
  '("if" "then" "else")
  "List of Vu keywords.  Used only for highlighting.
Default list includes some VuX keywords.
Call `vu-set-font-lock-keywords' after changing this list.")

(defvar vu-font-lock-keywords nil
  "Keywords to highlight for Vu.  See variable `font-lock-keywords'.
This variable is generally set from `vu-proc-regexp',
`vu-typeword-list', and `vu-keyword-list' by the function
`vu-set-font-lock-keywords'.")

;; FIXME need some way to recognize variables because array refs look
;; like 2 sexps.
(defvar vu-type-alist
  '(
    ("proc" nil vu-expr vu-commands)
    ("expr" vu-expr)
    ("catch" vu-commands)
    ("if" vu-expr "then" vu-commands)
    ("elseif" vu-expr "then" vu-commands)
    ("elseif" vu-expr vu-commands)
    ("if" vu-expr vu-commands)
    ("while" vu-expr vu-commands)
    ("for" vu-commands vu-expr vu-commands vu-commands)
    ("foreach" nil nil vu-commands)
    ;; Loop handling is not perfect, because the third argument can be
    ;; either a command or an expr, and there is no real way to look
    ;; forward.
    ("loop" nil vu-expr vu-expr vu-commands)
    ("loop" nil vu-expr vu-commands)
    )
  "Alist that controls indentation.
\(Actually, this really only controls what happens on continuation lines).
Each entry looks like `(KEYWORD TYPE ...)'.
Each type entry describes a sexp after the keyword, and can be one of:
* nil, meaning that this sexp has no particular type.
* vu-expr, meaning that this sexp is an arithmetic expression.
* vu-commands, meaning that this sexp holds Vu commands.
* a string, which must exactly match the string at the corresponding
  position for a match to be made.

For example, the entry for the \"loop\" command is:

   (\"loop\" nil vu-expr vu-commands)

This means that the \"loop\" command has three arguments.  The first
argument is ignored (for indentation purposes).  The second argument
is a Vu expression, and the last argument is Vu commands.")

(defvar vu-explain-indentation nil
  "If not `nil', debugging message will be printed during indentation.")



;;
;; Work around differences between various versions of Emacs.
;;

;; We use this because Lemacs 19.9 has what we need.
(defconst vu-pps-has-arg-6
  (or vu-using-emacs-19
      (and vu-using-xemacs-19
	   (condition-case nil
	       (progn
		 (parse-partial-sexp (point) (point) nil nil nil t)
		 t)
	     (error nil))))
  "t if using an emacs which supports sixth (\"commentstop\") argument
to parse-partial-sexp.")

;; Its pretty bogus to have to do this, but there is no easier way to
;; say "match not syntax-1 and not syntax-2".  Too bad you can't put
;; \s in [...].  This sickness is used in Emacs 19 to match a defun
;; starter.  (It is used for this in v18 as well).
;;(defconst vu-omit-ws-regexp
;;  (concat "^\\(\\s"
;;	  (mapconcat 'char-to-string "w_.()\"\\$'/" "\\|\\s")
;;	  "\\)\\S(*")
;;  "Regular expression that matches everything except space, comment
;;starter, and comment ender syntax codes.")

;; FIXME?  Instead of using the hairy regexp above, we just use a
;; simple one.
;;(defconst vu-omit-ws-regexp "^[^] \t\n#}]\\S(*"
;;  "Regular expression used in locating function definitions.")

;; Here's another stab.  I think this one actually works.  Now the
;; problem seems to be that there is a bug in Emacs 19.22 where
;; end-of-defun doesn't really use the brace matching the one that
;; trails defun-prompt-regexp.
(defconst vu-omit-ws-regexp "^[^ \t\n#}][^\n}]+}*[ \t]+")

(defun vu-internal-beginning-of-defun (&optional arg)
  "Move backward to next beginning-of-defun.
With argument, do this that many times.
Returns t unless search stops due to end of buffer."
  (interactive "p")
  (if (or (null arg) (= arg 0))
      (setq arg 1))
  (let (success)
    (while (progn
	     (setq arg (1- arg))
	     (and (>= arg 0)
		  (setq success
			(re-search-backward vu-omit-ws-regexp nil 'move 1))))
      (while (and (looking-at "[]#}]")
		  (setq success
			(re-search-backward vu-omit-ws-regexp nil 'move 1)))))
    (beginning-of-line)
    (not (null success))))

(defun vu-internal-end-of-defun (&optional arg)
  "Move forward to next end of defun.
An end of a defun is found by moving forward from the beginning of one."
  (interactive "p")
  (if (or (null arg) (= arg 0)) (setq arg 1))
  (let ((start (point)))
    ;; Was forward-char.  I think this works a little better.
    (forward-line)
    (vu-beginning-of-defun)
    (while (> arg 0)
      (while (and (re-search-forward vu-omit-ws-regexp nil 'move 1)
		  (progn (beginning-of-line) t)
		  (looking-at "[]#}]")
		  (progn (forward-line) t)))
      (let ((next-line (save-excursion
			 (forward-line)
			 (point))))
	(while (< (point) next-line)
	  (forward-sexp)))
      (forward-line)
      (if (> (point) start) (setq arg (1- arg))))))

;; In Emacs 19, we can use begining-of-defun as long as we set up a
;; certain regexp.  In Emacs 18, we need our own function.
(fset 'vu-beginning-of-defun
      (if vu-using-emacs-19
	  'beginning-of-defun
	'vu-internal-beginning-of-defun))

;; Ditto end-of-defun.
(fset 'vu-end-of-defun
      (if (and vu-using-emacs-19 (not vu-using-xemacs-19))
	  'end-of-defun
	'vu-internal-end-of-defun))

;; Internal mark-defun that is used for losing Emacsen.
(defun vu-internal-mark-defun ()
  "Put mark at end of Vu function, point at beginning."
  (interactive)
  (push-mark (point))
  (vu-end-of-defun)
  (if vu-using-emacs-19
      (push-mark (point) nil t)
    (push-mark (point)))
  (vu-beginning-of-defun)
  (backward-paragraph))

;; In GNU Emacs 19-23 and later, mark-defun works as advertised.  I
;; don't know about XEmacs, so for now it and Emacs 18 just lose.
(fset 'vu-mark-defun
      (if vu-using-emacs-19-23
	  'mark-defun
	'vu-internal-mark-defun))

;; In GNU Emacs 19, mark takes an additional "force" argument.  I
;; don't know about XEmacs, so I'm just assuming it is the same.
;; Emacs 18 doesn't have this argument.
(defun vu-mark ()
  "Return mark, or nil if none."
  (if vu-using-emacs-19
      (mark t)
    (mark)))



;;
;; Some helper functions.
;;

(defun vu-set-proc-regexp ()
  "Set `vu-proc-regexp' from variable `vu-proc-list'."
  (setq vu-proc-regexp (concat "^\\s-*\\("
				(mapconcat 'identity vu-proc-list "\\|")
				"\\)[ \t]+")))

(defun vu-set-font-lock-keywords ()
  "Set `vu-font-lock-keywords'.
Uses variables `vu-proc-regexp' and `vu-keyword-list'."
  (setq vu-font-lock-keywords
	(list
	 ;; Names of functions (and other "defining things").
	 (list (concat vu-proc-regexp "\\([^ \t\n]+\\)")
	       2 'font-lock-function-name-face)

	 ;; Names of type-defining things.
	 (list (concat "\\(\\s-\\|^\\)\\("
		       ;; FIXME Use 'regexp-quote?
		       (mapconcat 'identity vu-typeword-list "\\|")
		       "\\)\\(\\s-\\|$\\)")
	       2 'font-lock-type-face)

	 ;; Keywords.  Only recognized if surrounded by whitespace.
	 ;; FIXME consider using "not word or symbol", not
	 ;; "whitespace".
	 (cons (concat "\\(\\s-\\|^\\)\\("
		       ;; FIXME Use regexp-quote?
		       (mapconcat 'identity vu-keyword-list "\\|")
		       "\\)\\(\\s-\\|$\\)")
	       2)
	 )))

(if vu-proc-regexp
    ()
  (vu-set-proc-regexp))

(if vu-font-lock-keywords
    ()
  (vu-set-font-lock-keywords))



;;
;; The mode itself.
;;

;;;###autoload
(defun vu-mode ()
  "Major mode for editing Vu code.
Expression and list commands understand all Vu brackets.
Tab indents for Vu code.
Paragraphs are separated by blank lines only.
Delete converts tabs to spaces as it moves back.

Variables controlling indentation style:
  vu-indent-level
    Indentation of Vu statements within surrounding block.
  vu-continued-indent-level
    Indentation of continuation line relative to first line of command.

Variables controlling user interaction with mode (see variable
documentation for details):
  vu-tab-always-indent
    Controls action of TAB key.
  vu-auto-newline
    Non-nil means automatically newline before and after braces, brackets,
    and semicolons inserted in Vu code.
  vu-electric-hash-style
    Controls action of `#' key.
  vu-use-hairy-comment-detector
    If t, use more complicated, but slower, comment detector.
    This variable is only used in GNU Emacs 19.

Turning on Vu mode calls the value of the variable `vu-mode-hook'
with no args, if that value is non-nil.  Read the documentation for
`vu-mode-hook' to see what kinds of interesting hook functions
already exist.

Commands:
\\{vu-mode-map}"
  (interactive)
  (kill-all-local-variables)
  (use-local-map vu-mode-map)
  (setq major-mode 'vu-mode)
  (setq mode-name "Vu")
  (setq local-abbrev-table vu-mode-abbrev-table)
  (set-syntax-table vu-mode-syntax-table)

  (make-local-variable 'paragraph-start)
  (make-local-variable 'paragraph-separate)
  (if (and vu-using-emacs-19-23
	   (>= emacs-minor-version 29))
      (progn
	;; In Emacs 19.29, you aren't supposed to start these with a
	;; ^.
	(setq paragraph-start "$\\|")
	(setq paragraph-separate paragraph-start))
    (setq paragraph-start (concat "^$\\|" page-delimiter))
    (setq paragraph-separate paragraph-start))
  (make-local-variable 'paragraph-ignore-fill-prefix)
  (setq paragraph-ignore-fill-prefix t)
  (make-local-variable 'fill-paragraph-function)
  (setq fill-paragraph-function 'vu-do-fill-paragraph)

  (make-local-variable 'indent-line-function)
  (setq indent-line-function 'vu-indent-line)
  ;; Vu doesn't require a final newline.
  ;; (make-local-variable 'require-final-newline)
  ;; (setq require-final-newline t)

  (make-local-variable 'comment-start)
  ;;(setq comment-start "# ") ;;;BO
  (setq comment-start "// ") ;;;BO
  (make-local-variable 'comment-start-skip)
  ;;(setq comment-start-skip "#+ *") ;;;BO
  (setq comment-start-skip "//+ *") ;;;BO
  (make-local-variable 'comment-column)
  (setq comment-column 40)
  (make-local-variable 'comment-end)
  (setq comment-end "")

  (make-local-variable 'outline-regexp)
  (setq outline-regexp "[^\n\^M]")
  (make-local-variable 'outline-level)
  (setq outline-level 'vu-outline-level)

  (make-local-variable 'font-lock-keywords)
  (setq font-lock-keywords vu-font-lock-keywords)

  ;; The following only really makes sense under GNU Emacs 19.
  (make-local-variable 'imenu-create-index-function)
  (setq imenu-create-index-function 'vu-imenu-create-index-function)
  (make-local-variable 'parse-sexp-ignore-comments)

  ;; Settings for new dabbrev code.
  (make-local-variable 'dabbrev-case-fold-search)
  (setq dabbrev-case-fold-search nil)
  (make-local-variable 'dabbrev-case-replace)
  (setq dabbrev-case-replace nil)
  (make-local-variable 'dabbrev-abbrev-skip-leading-regexp)
  (setq dabbrev-abbrev-skip-leading-regexp "[$!]")
  (make-local-variable 'dabbrev-abbrev-char-regexp)
  (setq dabbrev-abbrev-char-regexp "\\sw\\|\\s_")

  (if vu-using-emacs-19
      (progn
	;; This can only be set to t in Emacs 19 and XEmacs.
	;; Emacs 18 and Epoch lose.
	(setq parse-sexp-ignore-comments t)
	;; XEmacs has defun-prompt-regexp, but I don't believe
	;; that it works for end-of-defun -- only for
	;; beginning-of-defun.
	(make-local-variable 'defun-prompt-regexp)
	(setq defun-prompt-regexp vu-omit-ws-regexp)
	;; The following doesn't work in Lucid Emacs 19.6, but maybe
	;; it will appear in later versions.
	(make-local-variable 'add-log-current-defun-function)
	(setq add-log-current-defun-function 'add-log-vu-defun))
    (setq parse-sexp-ignore-comments nil))

  ;; Put Vu menu into menubar for XEmacs.  This happens
  ;; automatically for GNU Emacs.
  (if (and vu-using-xemacs-19
	   current-menubar
	   (not (assoc "Vu" current-menubar)))
      (progn
	(set-buffer-menubar (copy-sequence current-menubar))
	(add-submenu nil vu-xemacs-menu)))
  ;; Append Vu menu to popup menu for XEmacs.
  (if (and vu-using-xemacs-19 (boundp 'mode-popup-menu))
      (setq mode-popup-menu
	    (cons (concat mode-name " Mode Commands") vu-xemacs-menu)))

  (run-hooks 'vu-mode-hook))



;; This is used for braces, brackets, and semi (except for closing
;; braces, which are handled specially).
(defun vu-electric-char (arg)
  "Insert character and correct line's indentation."
  (interactive "p")
  ;; Indent line first; this looks better if parens blink.
  (vu-indent-line)
  (self-insert-command arg)
  (if (and vu-auto-newline (= last-command-char ?\;))
      (progn
	(newline)
	(vu-indent-line))))

;; This is used for closing braces.  If vu-auto-newline is set, can
;; insert a newline both before and after the brace, depending on
;; context.  FIXME should this be configurable?  Does anyone use this?
(defun vu-electric-brace (arg)
  "Insert character and correct line's indentation."
  (interactive "p")
  ;; If auto-newlining and there is stuff on the same line, insert a
  ;; newline first.
  (if vu-auto-newline
      (progn
	(if (save-excursion
	      (skip-chars-backward " \t")
	      (bolp))
	    ()
	  (vu-indent-line)
	  (newline))
	;; In auto-newline case, must insert a newline after each
	;; brace.  So an explicit loop is needed.
	(while (> arg 0)
	  (insert last-command-char)
	  (vu-indent-line)
	  (newline)
	  (setq arg (1- arg))))
    (self-insert-command arg))
  (vu-indent-line))



(defun vu-indent-command (&optional arg)
  "Indent current line as Vu code, or in some cases insert a tab character.
If vu-tab-always-indent is t (the default), always indent current line.
If vu-tab-always-indent is nil and point is not in the indentation
area at the beginning of the line, a TAB is inserted.
Other values of vu-tab-always-indent cause the first possible action
from the following list to take place:

  1. Move from beginning of line to correct indentation.
  2. Delete an empty comment.
  3. Move forward to start of comment, indenting if necessary.
  4. Move forward to end of line, indenting if necessary.
  5. Create an empty comment.
  6. Move backward to start of comment, indenting if necessary."
  (interactive "p")
  (cond
   ((not vu-tab-always-indent)
    ;; Indent if in indentation area, otherwise insert TAB.
    (if (<= (current-column) (current-indentation))
	(vu-indent-line)
      (self-insert-command arg)))
   ((eq vu-tab-always-indent t)
    ;; Always indent.
    (vu-indent-line))
   (t
    ;; "Perl-mode" style TAB command.
    (let* ((ipoint (point))
	   (eolpoint (progn
		       (end-of-line)
		       (point)))
	   (comment-p (vu-in-comment)))
      (cond
       ((= ipoint (save-excursion
		    (beginning-of-line)
		    (point)))
	(beginning-of-line)
	(vu-indent-line)
	;; If indenting didn't leave us in column 0, go to the
	;; indentation.  Otherwise leave point at end of line.  This
	;; is a hack.
	(if (= (point) (save-excursion
			 (beginning-of-line)
			 (point)))
	    (end-of-line)
	  (back-to-indentation)))
       ((and comment-p (looking-at "[ \t]*$"))
	;; Empty comment, so delete it.  We also delete any ";"
	;; characters at the end of the line.  I think this is
	;; friendlier, but I don't know how other people will feel.
	(backward-char)
	(skip-chars-backward " \t;")
	(delete-region (point) eolpoint))
       ((and comment-p (< ipoint (point)))
	;; Before comment, so skip to it.
	(vu-indent-line)
	(indent-for-comment))
       ((/= ipoint eolpoint)
	;; Go to end of line (since we're not there yet).
	(goto-char eolpoint)
	(vu-indent-line))
       ((not comment-p)
	(vu-indent-line)
	(vu-indent-for-comment))
       (t
	;; Go to start of comment.  We don't leave point where it is
	;; because we want to skip comment-start-skip.
	(vu-indent-line)
	(indent-for-comment)))))))

(defun vu-indent-line ()
  "Indent current line as Vu code.
Return the amount the indentation changed by."
  (let ((indent (calculate-vu-indent nil))
	beg shift-amt
	(case-fold-search nil)
	(pos (- (point-max) (point))))
    (beginning-of-line)
    (setq beg (point))
    (cond ((eq indent nil)
	   (setq indent (current-indentation)))
          (t
	   (skip-chars-forward " \t")
	   (if (listp indent) (setq indent (car indent)))
	   (cond ((= (following-char) ?})
		  (setq indent (- indent vu-indent-level)))
		 ((= (following-char) ?\])
		  (setq indent (- indent 1))))))
    (skip-chars-forward " \t")
    (setq shift-amt (- indent (current-column)))
    (if (zerop shift-amt)
	(if (> (- (point-max) pos) (point))
	    (goto-char (- (point-max) pos)))
      (delete-region beg (point))
      (indent-to indent)
      ;; If initial point was within line's indentation,
      ;; position after the indentation.  Else stay at same point in text.
      (if (> (- (point-max) pos) (point))
	  (goto-char (- (point-max) pos))))
    shift-amt))

(defun vu-figure-type ()
  "Determine type of sexp at point.
This is either 'vu-expr, 'vu-commands, or nil.  Puts point at start
of sexp that indicates types.

See documentation for variable `vu-type-alist' for more information."
  (let ((count 0)
	result
	word-stack)
    (while (and (< count 5)
		(not result))
      (condition-case nil
	  (progn
	    ;; FIXME should use "vu-backward-sexp", which would skip
	    ;; over entire variables, etc.
	    (backward-sexp)
	    (if (looking-at "[a-zA-Z_]+")
		(let ((list vu-type-alist)
		      entry)
		  (setq word-stack (cons (current-word) word-stack))
		  (while (and list (not result))
		    (setq entry (car list))
		    (setq list (cdr list))
		    (let ((index 0))
		      (while (and entry (<= index count))
			;; Abort loop if string does not match word on
			;; stack.
			(and (stringp (car entry))
			     (not (string= (car entry)
					   (nth index word-stack)))
			     (setq entry nil))
			(setq entry (cdr entry))
			(setq index (1+ index)))
		      (and (> index count)
			   (not (stringp (car entry)))
			   (setq result (car entry)))
		      )))
	      (setq word-stack (cons nil word-stack))))
	(error nil))
      (setq count (1+ count)))
    (and vu-explain-indentation
	 (message "Indentation type %s" result))
    result))

(defun calculate-vu-indent (&optional parse-start)
  "Return appropriate indentation for current line as Vu code.
In usual case returns an integer: the column to indent to.
Returns nil if line starts inside a string, t if in a comment."
  (save-excursion
    (beginning-of-line)
    (let* ((indent-point (point))
	   (case-fold-search nil)
	   (continued-line
	    (save-excursion
	      (if (bobp)
		  nil
		(backward-char)
		(= ?\\ (preceding-char)))))
	   (continued-indent-value (if continued-line
				       vu-continued-indent-level
				     0))
	   state
	   containing-sexp
	   found-next-line)
      (if parse-start
	  (goto-char parse-start)
	(vu-beginning-of-defun))
      (while (< (point) indent-point)
	(setq parse-start (point))
	(setq state (parse-partial-sexp (point) indent-point 0))
	(setq containing-sexp (car (cdr state))))
      ;;(cond ((or (nth 3 state) (nth 4 state)) ;;;BO
      (cond ((nth 3 state) ;;;BO
	     ;; Inside comment or string.  Return nil or t if should
	     ;; not change this line
	     ;;(nth 4 state)) ;;;BO
	     nil) ;;;BO
	    ((null containing-sexp)
	     ;; Line is at top level.
	     continued-indent-value)
	    (t
	     ;; Set expr-p if we are looking at the expression part of
	     ;; an "if", "expr", etc statement.  Set commands-p if we
	     ;; are looking at the body part of an if, while, etc
	     ;; statement.  FIXME Should check for "for" loops here.
	     (goto-char containing-sexp)
	     (let* ((sexpr-type (vu-figure-type))
		    (expr-p (eq sexpr-type 'vu-expr))
		    (commands-p (eq sexpr-type 'vu-commands))
		    (expr-start (point)))
	       ;; Find the first statement in the block and indent
	       ;; like it.  The first statement in the block might be
	       ;; on the same line, so what we do is skip all
	       ;; "virtually blank" lines, looking for a non-blank
	       ;; one.  A line is virtually blank if it only contains
	       ;; a comment and whitespace.  FIXME continued comments
	       ;; aren't supported.  They are a wart on Vu anyway.
	       ;; We do it this funky way because we want to know if
	       ;; we've found a statement on some line _after_ the
	       ;; line holding the sexp opener.
	       (goto-char containing-sexp)
	       (forward-char)
	       (if (and (< (point) indent-point)
			(looking-at "[ \t]*\\(#.*\\)?$"))
		   (progn
		     (forward-line)
		     (while (and (< (point) indent-point)
				 (looking-at "[ \t]*\\(#.*\\)?$"))
		       (setq found-next-line t)
		       (forward-line))))
	       (if (or continued-line
		       (/= (char-after containing-sexp) ?{)
		       expr-p)
		   (progn
		     ;; Line is continuation line, or the sexp opener
		     ;; is not a curly brace, or we are are looking at
		     ;; an `expr' expression (which must be split
		     ;; specially).  So indentation is column of first
		     ;; good spot after sexp opener (with some added
		     ;; in the continued-line case).  If there is no
		     ;; nonempty line before the indentation point, we
		     ;; use the column of the character after the sexp
		     ;; opener.
		     (if (>= (point) indent-point)
			 (progn
			   (goto-char containing-sexp)
			   (forward-char))
		       (skip-chars-forward " \t"))
		     (+ (current-column) continued-indent-value))
		 ;; After a curly brace, and not a continuation line.
		 ;; So take indentation from first good line after
		 ;; start of block, unless that line is on the same
		 ;; line as the opening brace.  In this case use the
		 ;; indentation of the opening brace's line, plus
		 ;; another indent step.  If we are in the body part
		 ;; of an "if" or "while" then the indentation is
		 ;; taken from the line holding the start of the
		 ;; statement.
		 (if (and (< (point) indent-point)
			  found-next-line)
		     (current-indentation)
		   (if commands-p
		       (goto-char expr-start)
		     (goto-char containing-sexp))
		   (+ (current-indentation) vu-indent-level)))))))))



(defun indent-vu-exp ()
  "Indent each line of the Vu grouping following point."
  (interactive)
  (let ((indent-stack (list nil))
	(contain-stack (list (point)))
	(case-fold-search nil)
	outer-loop-done inner-loop-done state ostate
	this-indent last-sexp continued-line
	(next-depth 0)
	last-depth)
    (save-excursion
      (forward-sexp 1))
    (save-excursion
      (setq outer-loop-done nil)
      (while (and (not (eobp)) (not outer-loop-done))
	(setq last-depth next-depth)
	;; Compute how depth changes over this line
	;; plus enough other lines to get to one that
	;; does not end inside a comment or string.
	;; Meanwhile, do appropriate indentation on comment lines.
	(setq inner-loop-done nil)
	(while (and (not inner-loop-done)
		    (not (and (eobp) (setq outer-loop-done t))))
	  (setq ostate state)
	  (setq state (parse-partial-sexp (point) (progn (end-of-line) (point))
					  nil nil state))
	  (setq next-depth (car state))
	  (if (and (car (cdr (cdr state)))
		   (>= (car (cdr (cdr state))) 0))
	      (setq last-sexp (car (cdr (cdr state)))))
	  (if (or (nth 4 ostate))
	      (vu-indent-line))
	  (if (or (nth 3 state))
	      (forward-line 1)
	    (setq inner-loop-done t)))
	(if (<= next-depth 0)
	    (setq outer-loop-done t))
	(if outer-loop-done
	    nil
	  ;; If this line had ..))) (((.. in it, pop out of the levels
	  ;; that ended anywhere in this line, even if the final depth
	  ;; doesn't indicate that they ended.
	  (while (> last-depth (nth 6 state))
	    (setq indent-stack (cdr indent-stack)
		  contain-stack (cdr contain-stack)
		  last-depth (1- last-depth)))
	  (if (/= last-depth next-depth)
	      (setq last-sexp nil))
	  ;; Add levels for any parens that were started in this line.
	  (while (< last-depth next-depth)
	    (setq indent-stack (cons nil indent-stack)
		  contain-stack (cons nil contain-stack)
		  last-depth (1+ last-depth)))
	  (if (null (car contain-stack))
	      (setcar contain-stack
		      (or (car (cdr state))
			  (save-excursion
			    (forward-sexp -1)
			    (point)))))
	  (forward-line 1)
	  (setq continued-line
		(save-excursion
		  (backward-char)
		  (= (preceding-char) ?\\)))
	  (skip-chars-forward " \t")
	  (if (eolp)
	      nil
	    (if (and (car indent-stack)
		     (>= (car indent-stack) 0))
		;; Line is on an existing nesting level.
		(setq this-indent (car indent-stack))
	      ;; Just started a new nesting level.
	      ;; Compute the standard indent for this level.
	      (let ((val (calculate-vu-indent
			  (if (car indent-stack)
			      (- (car indent-stack))))))
		(setcar indent-stack
			(setq this-indent val))
		(setq continued-line nil)))
	    (cond ((not (numberp this-indent)))
		  ((= (following-char) ?})
		   (setq this-indent (- this-indent vu-indent-level)))
		  ((= (following-char) ?\])
		   (setq this-indent (- this-indent 1))))
	    ;; Put chosen indentation into effect.
	    (or (null this-indent)
		(= (current-column)
		   (if continued-line
		       (+ this-indent vu-indent-level)
		     this-indent))
		(progn
		  (delete-region (point) (progn (beginning-of-line) (point)))
		  (indent-to
		   (if continued-line
		       (+ this-indent vu-indent-level)
		     this-indent)))))))))
  )



;;
;; Interfaces to other packages.
;;

(defun vu-imenu-create-index-function ()
  "Generate alist of indices for imenu."
  (let ((re (concat vu-proc-regexp "\\([^ \t\n{]+\\)"))
	alist prev-pos)
    (goto-char (point-min))
    (imenu-progress-message prev-pos 0)
    (save-match-data
      (while (re-search-forward re nil t)
	(imenu-progress-message prev-pos)
	;; Position on start of proc name, not beginning of line.
	(setq alist (cons
		     (cons (buffer-substring (match-beginning 2) (match-end 2))
			   (match-beginning 2))
		     alist))))
    (imenu-progress-message prev-pos 100)
    (nreverse alist)))

;; FIXME Definition of function is very ad-hoc.  Should use
;; vu-beginning-of-defun.  Also has incestuous knowledge about the
;; format of vu-proc-regexp.
(defun add-log-vu-defun ()
  "Return name of Vu function point is in, or nil."
  (save-excursion
    (if (re-search-backward
	 (concat vu-proc-regexp "\\([^ \t\n{]+\\)") nil t)
	(buffer-substring (match-beginning 2)
			  (match-end 2)))))

(defun vu-outline-level ()
  (save-excursion
    (skip-chars-forward " \t")
    (current-column)))



;;
;; Helper functions for inferior Vu mode.
;;

;; This exists to let us delete the prompt when commands are sent
;; directly to the inferior Vu.  See gud.el for an explanation of how
;; it all works (I took it from there).  This stuff doesn't really
;; work as well as I'd like it to.  But I don't believe there is
;; anything useful that can be done.
(defvar inferior-vu-delete-prompt-marker nil)

(defun vu-filter (proc string)
  (let ((inhibit-quit t))
    (save-excursion
      (set-buffer (process-buffer proc))
      (goto-char (process-mark proc))
      ;; Delete prompt if requested.
      (if (marker-buffer inferior-vu-delete-prompt-marker)
	  (progn
	    (delete-region (point) inferior-vu-delete-prompt-marker)
	    (set-marker inferior-vu-delete-prompt-marker nil)))))
  (if vu-using-emacs-19
      (comint-output-filter proc string)
    (funcall comint-output-filter string)))

(defun vu-send-string (proc string)
  (save-excursion
    (set-buffer (process-buffer proc))
    (goto-char (process-mark proc))
    (beginning-of-line)
    (if (looking-at comint-prompt-regexp)
	(set-marker inferior-vu-delete-prompt-marker (point))))
  (comint-send-string proc string))

(defun vu-send-region (proc start end)
  (save-excursion
    (set-buffer (process-buffer proc))
    (goto-char (process-mark proc))
    (beginning-of-line)
    (if (looking-at comint-prompt-regexp)
	(set-marker inferior-vu-delete-prompt-marker (point))))
  (comint-send-region proc start end))

(defun switch-to-vu (eob-p)
  "Switch to inferior Vu process buffer.
With argument, positions cursor at end of buffer."
  (interactive "P")
  (if (get-buffer inferior-vu-buffer)
      (pop-to-buffer inferior-vu-buffer)
    (error "No current inferior Vu buffer"))
  (cond (eob-p
	 (push-mark)
	 (goto-char (point-max)))))

(defun inferior-vu-proc ()
  "Return current inferior Vu process.
See variable `inferior-vu-buffer'."
  (let ((proc (get-buffer-process (if (eq major-mode 'inferior-vu-mode)
				      (current-buffer)
				    inferior-vu-buffer))))
    (or proc
	(error "No Vu process; see variable `inferior-vu-buffer'"))))

(defun vu-eval-region (start end &optional and-go)
  "Send the current region to the inferior Vu process.
Prefix argument means switch to the Vu buffer afterwards."
  (interactive "r\nP")
  (let ((proc (inferior-vu-proc)))
    (vu-send-region proc start end)
    (vu-send-string proc "\n")
    (if and-go (switch-to-vu t))))

(defun vu-eval-defun (&optional and-go)
  "Send the current defun to the inferior Vu process.
Prefix argument means switch to the Vu buffer afterwards."
  (interactive "P")
  (save-excursion
    (vu-end-of-defun)
    (let ((end (point)))
      (vu-beginning-of-defun)
      (vu-eval-region (point) end)))
  (if and-go (switch-to-vu t)))



;;
;; Inferior Vu mode itself.
;;

(defun inferior-vu-mode ()
  "Major mode for interacting with Vu interpreter.

A Vu process can be started with M-x inferior-vu.

Entry to this mode runs the hooks comint-mode-hook and
inferior-vu-mode-hook, in that order.

You can send text to the inferior Vu process from other buffers
containing Vu source.

Variables controlling Inferior Vu mode:
  vu-application
    Name of program to run.
  vu-command-switches
    Command line arguments to `vu-application'.
  vu-prompt-regexp
    Matches prompt.
  inferior-vu-source-command
    Command to use to read Vu file in running application.
  inferior-vu-buffer
    The current inferior Vu process buffer.  See variable
    documentation for details on multiple-process support.

The following commands are available:
\\{inferior-vu-mode-map}"
  (interactive)
  (comint-mode)
  (setq comint-prompt-regexp (or vu-prompt-regexp
				 (concat "^"
					 (regexp-quote vu-application)
					 ">")))
  (setq major-mode 'inferior-vu-mode)
  (setq mode-name "Inferior Vu")
  (if (boundp 'modeline-process)
      (setq modeline-process '(": %s"))	; For XEmacs.
    (setq mode-line-process '(": %s")))
  (use-local-map inferior-vu-mode-map)
  (setq local-abbrev-table vu-mode-abbrev-table)
  (set-syntax-table vu-mode-syntax-table)
  (if vu-using-emacs-19
      (progn
	(make-local-variable 'defun-prompt-regexp)
	(setq defun-prompt-regexp vu-omit-ws-regexp)))
  (make-local-variable 'inferior-vu-delete-prompt-marker)
  (setq inferior-vu-delete-prompt-marker (make-marker))
  (set-process-filter (get-buffer-process (current-buffer)) 'vu-filter)
  (run-hooks 'inferior-vu-mode-hook))

;;;###autoload
(defun inferior-vu (cmd)
  "Run inferior Vu process.
Prefix arg means enter program name interactively.
See documentation for function `inferior-vu-mode' for more information."
  (interactive
   (list (if current-prefix-arg
	     (read-string "Run Vu: " vu-application)
	   vu-application)))
  (if (not (comint-check-proc "*inferior-vu*"))
      (progn
	(set-buffer (apply (function make-comint) "inferior-vu" cmd nil
			   vu-command-switches))
	(inferior-vu-mode)))
  (make-local-variable 'vu-application)
  (setq vu-application cmd)
  (setq inferior-vu-buffer "*inferior-vu*")
  ;; (switch-to-buffer "*inferior-vu*")) ;;; BO
  (switch-to-buffer-other-window "*inferior-vu*"))

(and (fboundp 'defalias)
     (defalias 'run-vu 'inferior-vu))



;;
;; Auto-fill support.
;;

(defun vu-real-command-p ()
  "Return nil if point is not at the beginning of a command.
A command is the first word on an otherwise empty line, or the
first word following a semicolon, opening brace, or opening bracket."
  (save-excursion
    (skip-chars-backward " \t")
    (cond
     ((bobp) t)
     ((bolp)
      (backward-char)
      ;; Note -- continued comments are not supported here.  I
      ;; consider those to be a wart on the language.
      (not (eq ?\\ (preceding-char))))
     (t
      (memq (preceding-char) '(?\; ?{ ?\[))))))

;; FIXME doesn't actually return t.  See last case.
(defun vu-real-comment-p ()
  "Return t if point is just after the `#' beginning a real comment.
Does not check to see if previous char is actually `#'.
A real comment is either at the beginning of the buffer,
preceeded only by whitespace on the line, or has a preceeding
semicolon, opening brace, or opening bracket on the same line."
  (save-excursion
    (backward-char)
    (vu-real-command-p)))

(defun vu-hairy-scan-for-comment (state end always-stop)
  "Determine if point is in a comment.
Returns a list of the form `(FLAG . STATE)'.  STATE can be used
as input to future invocations.  FLAG is nil if not in comment,
t otherwise.  If in comment, leaves point at beginning of comment.
Only works in Emacs 19.  See also `vu-simple-scan-for-comment', a
simpler version that is often right, and works in Emacs 18."
  (let ((bol (save-excursion
	       (goto-char end)
	       (beginning-of-line)
	       (point)))
	real-comment
	last-cstart)
    (while (and (not last-cstart) (< (point) end))
      (setq real-comment nil)		;In case we've looped around and it is
                                        ;set.
      (setq state (parse-partial-sexp (point) end nil nil state t))
      (if (nth 4 state)
	  (progn
	    ;; If ALWAYS-STOP is set, stop even if we don't have a
	    ;; real comment, or if the comment isn't on the same line
	    ;; as the end.
	    (if always-stop (setq last-cstart (point)))
	    ;; If we have a real comment, then set the comment
	    ;; starting point if we are on the same line as the ending
	    ;; location.
	    (setq real-comment (vu-real-comment-p))
	    (if real-comment
		(progn
		  (and (> (point) bol) (setq last-cstart (point)))
		  ;; NOTE Emacs 19 has a misfeature whereby calling
		  ;; parse-partial-sexp with COMMENTSTOP set and with
		  ;; an initial list that says point is in a comment
		  ;; will cause an immediate return.  So we must skip
		  ;; over the comment ourselves.
		  (beginning-of-line 2)))
	    ;; Frob the state to make it look like we aren't in a
	    ;; comment.
	    (setcar (nthcdr 4 state) nil))))
    (and last-cstart
	 (goto-char last-cstart))
    (cons real-comment state)))

(defun vu-hairy-in-comment ()
  "Return t if point is in a comment, and leave point at beginning
of comment."
  (let ((save (point)))
    (vu-beginning-of-defun)
    (car (vu-hairy-scan-for-comment nil save nil))))

(defun vu-simple-in-comment ()
  "Return t if point is in comment, and leave point at beginning
of comment.  This is faster that `vu-hairy-in-comment', but is
correct less often."
  (let ((save (point))
	comment)
    (beginning-of-line)
    (while (and (< (point) save) (not comment))
      (search-forward "#" save 'move)
      (setq comment (vu-real-comment-p)))
    comment))

(defun vu-in-comment ()
  "Return t if point is in comment, and leave point at beginning
of comment."
  (if (and vu-pps-has-arg-6
	   vu-use-hairy-comment-detector)
      (vu-hairy-in-comment)
    (vu-simple-in-comment)))

(defun vu-do-fill-paragraph (ignore)
  "fill-paragraph function for Vu mode.  Only fills in a comment."
  (let (in-comment col where)
    (save-excursion
      (end-of-line)
      (setq in-comment (vu-in-comment))
      (if in-comment
	  (progn
	    (setq where (1+ (point)))
	    (setq col (1- (current-column))))))
    (and in-comment
	 (save-excursion
	   (back-to-indentation)
	   (= col (current-column)))
	 ;; In a comment.  Set the fill prefix, and find the paragraph
	 ;; boundaries by searching for lines that look like
	 ;; comment-only lines.
	 (let ((fill-prefix (buffer-substring (progn
						(beginning-of-line)
						(point))
					      where))
	       p-start p-end)
	   ;; Search backwards.
	   (save-excursion
	     (while (looking-at "^[ \t]*#")
	       (forward-line -1))
	     (forward-line)
	     (setq p-start (point)))

	   ;; Search forwards.
	   (save-excursion
	     (while (looking-at "^[ \t]*#")
	       (forward-line))
	     (setq p-end (point)))

	   ;; Narrow and do the fill.
	   (save-restriction
	     (narrow-to-region p-start p-end)
	     (fill-paragraph ignore)))))
  t)

(defun vu-do-auto-fill ()
  "Auto-fill function for Vu mode.  Only auto-fills in a comment."
  (let ((fill-prefix "# ")
	in-comment col)
    (save-excursion
      (setq in-comment (vu-in-comment))
      (if in-comment
	  (setq col (1- (current-column)))))
    (if in-comment
	(progn
	  (do-auto-fill)
	  (save-excursion
	    (back-to-indentation)
	    (delete-region (point) (save-excursion
				     (beginning-of-line)
				     (point)))
	    (indent-to-column col))))))



;;
;; Help-related code.
;;

(defvar vu-help-saved-dirs nil
  "Saved help directories.
If `vu-help-directory-list' changes, this allows `vu-help-on-word'
to update the alist.")

(defvar vu-help-alist nil
  "Alist with command names as keys and filenames as values.")

(defun vu-help-snarf-commands (dirlist)
  "Build alist of commands and filenames."
  (while dirlist
    (let ((files (directory-files (car dirlist) t)))
      (while files
	(if (and (file-directory-p (car files))
		 (not
		  (let ((fpart (file-name-nondirectory (car files))))
		    (or (equal fpart ".")
			(equal fpart "..")))))
	    (let ((matches (directory-files (car files) t)))
	      (while matches
		(or (file-directory-p (car matches))
		    (setq vu-help-alist
			  (cons
			   (cons (file-name-nondirectory (car matches))
				 (car matches))
			   vu-help-alist)))
		(setq matches (cdr matches)))))
	(setq files (cdr files))))
    (setq dirlist (cdr dirlist))))

(defun vu-reread-help-files ()
  "Set up to re-read files, and then do it."
  (interactive)
  (message "Building Vu help file index...")
  (setq vu-help-saved-dirs vu-help-directory-list)
  (setq vu-help-alist nil)
  (vu-help-snarf-commands vu-help-directory-list)
  (message "Building Vu help file index...done"))

(defun vu-current-word (flag)
  "Return current command word, or nil.
If FLAG is nil, just uses `current-word'.
Otherwise scans backward for most likely Vu command word."
  (if (and flag
	   (memq major-mode '(vu-mode inferior-vu-mode)))
      (condition-case nil
	  (save-excursion
	    ;; Look backward for first word actually in alist.
	    (if (bobp)
		()
	      (while (and (not (bobp))
			  (not (vu-real-command-p)))
		(backward-sexp)))
	    (if (assoc (current-word) vu-help-alist)
		(current-word)))
	(error nil))
    (current-word)))

;;;###autoload
(defun vu-help-on-word (command &optional arg)
  "Get help on Vu command.  Default is word at point.
Prefix argument means invert sense of `vu-use-smart-word-finder'."
  (interactive
   (list
    (progn
      (if (not (equal vu-help-directory-list vu-help-saved-dirs))
	  (vu-reread-help-files))
      (let ((word (vu-current-word
		   (if current-prefix-arg
		       (not vu-use-smart-word-finder)
		     vu-use-smart-word-finder))))
	(completing-read
	 (if (or (null word) (string= word ""))
	     "Help on Vu command: "
	   (format "Help on Vu command (default %s): " word))
	 vu-help-alist nil t)))
    current-prefix-arg))
  (if (not (equal vu-help-directory-list vu-help-saved-dirs))
      (vu-reread-help-files))
  (if (string= command "")
      (setq command (vu-current-word
		     (if arg
			 (not vu-use-smart-word-finder)
		       vu-use-smart-word-finder))))
  (let* ((help (get-buffer-create "*Vu help*"))
	 (cell (assoc command vu-help-alist))
	 (file (and cell (cdr cell))))
    (set-buffer help)
    (delete-region (point-min) (point-max))
    (if file
	(progn
	  (insert "*** " command "\n\n")
	  (insert-file-contents file))
      (if (string= command "")
	  (insert "Magical Pig!")
	(insert "Vu command " command " not in help\n")))
    (set-buffer-modified-p nil)
    (goto-char (point-min))
    (display-buffer help)))



;;
;; Other interactive stuff.
;;

(defun vu-maille-region nil
  "Maille le contenu du buffer."
  (interactive)
  (message "On maille! ...")
  (save-excursion
    (shell-command-on-region (region-beginning) (region-end) vu-maille-command nil)
    (set-buffer "*Shell Command Output*")
    (vu-mode)
    ))

(defun vu-maille-buffer nil
  "Maille le contenu du buffer."
  (interactive)
  (save-excursion
    (mark-whole-buffer)
    (vu-maille-region)))

(defvar vu-previous-dir/file nil
  "Record last directory and file used in loading.
This holds a cons cell of the form `(DIRECTORY . FILE)'
describing the last `vu-load-file' command.")

(defun vu-load-file (file &optional and-go)
  "Load a Vu file into the inferior Vu process.
Prefix argument means switch to the Vu buffer afterwards."
  (interactive
   (list
    ;; car because comint-get-source returns a list holding the
    ;; filename.
    (car (comint-get-source "Load Vu file: "
			    (or (and
				 (eq major-mode 'vu-mode)
				 (buffer-file-name))
				vu-previous-dir/file)
			    '(vu-mode) t))
    current-prefix-arg))
  (comint-check-source file)
  (setq vu-previous-dir/file (cons (file-name-directory file)
				    (file-name-nondirectory file)))
  (vu-send-string (inferior-vu-proc)
		   (format inferior-vu-source-command (vu-quote file)))
  (if and-go (switch-to-vu t)))

(defun vu-restart-with-file (file &optional and-go)
  "Restart inferior Vu with file.
If an inferior Vu process exists, it is killed first.
Prefix argument means switch to the Vu buffer afterwards."
  (interactive
   (list
    (car (comint-get-source "Restart with Vu file: "
			    (or (and
				 (eq major-mode 'vu-mode)
				 (buffer-file-name))
				vu-previous-dir/file)
			    '(vu-mode) t))
    current-prefix-arg))
  (let* ((buf (if (eq major-mode 'inferior-vu-mode)
		  (current-buffer)
		inferior-vu-buffer))
	 (proc (and buf (get-process buf))))
    (cond
     ((not (and buf (get-buffer buf)))
      ;; I think this will be ok.
      (inferior-vu vu-application)
      (vu-load-file file and-go))
     ((or
       (not (comint-check-proc buf))
       (yes-or-no-p
	"A Vu process is running, are you sure you want to reset it? "))
      (save-excursion
	(comint-check-source file)
	(setq vu-previous-dir/file (cons (file-name-directory file)
					  (file-name-nondirectory file)))
	(comint-exec (get-buffer-create buf)
		     (if proc
			 (process-name proc)
		       "inferior-vu")
		     vu-application file vu-command-switches)
	(if and-go (switch-to-vu t)))))))

;; FIXME I imagine you can do this under Emacs 18.  I just don't know
;; how.
(defun vu-auto-fill-mode (&optional arg)
  "Like `auto-fill-mode', but controls filling of Vu comments."
  (interactive "P")
  (and (not vu-using-emacs-19)
       (error "You must use Emacs 19 to get this feature."))
  ;; Following code taken from "auto-fill-mode" (simple.el).
  (prog1
      (setq auto-fill-function
	    (if (if (null arg)
		    (not auto-fill-function)
		  (> (prefix-numeric-value arg) 0))
		'vu-do-auto-fill
	      nil))
    ;; Update mode line.  FIXME I'd use force-mode-line-update, but I
    ;; don't know if it exists in v18.
    (set-buffer-modified-p (buffer-modified-p))))

(defun vu-electric-hash (&optional count)
  "Insert a `#' and quote if it does not start a real comment.
Prefix arg is number of `#'s to insert.
See variable `vu-electric-hash-style' for description of quoting
styles."
  (interactive "p")
  (or count (setq count 1))
  (if (> count 0)
      (let ((type
	     (if (eq vu-electric-hash-style 'smart)
		 (if (> count 3)	; FIXME what is "smart"?
		     'quote
		   'backslash)
	       vu-electric-hash-style))
	    comment)
	(if type
	    (progn
	      (save-excursion
		(insert "#")
		(setq comment (vu-in-comment)))
	      (delete-char 1)
	      (and vu-explain-indentation (message "comment: %s" comment))
	      (cond
	       ((eq type 'quote)
		(if (not comment)
		    (insert "\"")))
	       ((eq type 'backslash)
		;; The following will set count to 0, so the
		;; insert-char can still be run.
		(if (not comment)
		    (while (> count 0)
		      (insert "\\#")
		      (setq count (1- count)))))
	       (t nil))))
	(insert-char ?# count))))

(defun vu-hashify-buffer ()
  "Quote all `#'s in current buffer that aren't Vu comments."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (if (and vu-pps-has-arg-6 vu-use-hairy-comment-detector)
	(let (state
	      result)
	  (while (< (point) (point-max))
	    (setq result (vu-hairy-scan-for-comment state (point-max) t))
	    (if (car result)
		(beginning-of-line 2)
	      (backward-char)
	      (if (eq ?# (following-char))
		  (insert "\\"))
	      (forward-char))
	    (setq state (cdr result))))
      (while (and (< (point) (point-max))
		  (search-forward "#" nil 'move))
	(if (vu-real-comment-p)
	    (beginning-of-line 2)
	  ;; There's really no good way for the simple converter to
	  ;; work.  So we just quote # if it isn't already quoted.
	  ;; Bogus, but it works.
	  (backward-char)
	  (if (not (eq ?\\ (preceding-char)))
	      (insert "\\"))
	  (forward-char))))))

(defun vu-indent-for-comment ()
  "Indent this line's comment to comment column, or insert an empty comment.
Is smart about syntax of Vu comments.
Parts of this were taken from indent-for-comment (simple.el)."
  (interactive "*")
  (end-of-line)
  (or (vu-in-comment)
      (progn
	;; Not in a comment, so we have to insert one.  Create an
	;; empty comment (since there isn't one on this line).  If
	;; line is not blank, make sure we insert a ";" first.
	(skip-chars-backward " \t")
	(let ((eolpoint (point)))
	  (beginning-of-line)
	  (if (/= (point) eolpoint)
	      (progn
		(goto-char eolpoint)
		(insert
		 (if (vu-real-command-p) "" ";")
		 "# ")
		(backward-char))))))
  ;; Point is just after the "#" starting a comment.  Move it as
  ;; appropriate.
  (let* ((indent (if comment-indent-hook
		     (funcall comment-indent-hook)
		   (funcall comment-indent-function)))
	 (begpos (progn
		   (backward-char)
		   (point))))
    (if (/= begpos indent)
	(progn
	  (skip-chars-backward " \t" (save-excursion
				       (beginning-of-line)
				       (point)))
	  (delete-region (point) begpos)
	  (indent-to indent)))
    (looking-at comment-start-skip)	; Always true.
    (goto-char (match-end 0))
    ;; I don't like the effect of the next two.
    ;;(skip-chars-backward " \t" (match-beginning 0))
    ;;(skip-chars-backward "^ \t" (match-beginning 0))
    ))

;; The following was inspired by the Vu editing mode written by
;; Gregor Schmid <schmid@fb3-s7.math.TU-Berlin.DE>.  His version also
;; attempts to snarf the command line options from the command line,
;; but I didn't think that would really be that helpful (doesn't seem
;; like it owould be right enough.  His version also looks for the
;; "#!/bin/csh ... exec" hack, but that seemed even less useful.
;; FIXME should make sure that the application mentioned actually
;; exists.
(defun vu-guess-application ()
  "Attempt to guess Vu application by looking at first line.
The first line is assumed to look like \"#!.../program ...\"."
  (save-excursion
    (goto-char (point-min))
    (if (looking-at "#![^ \t]*/\\([^ \t\n/]+\\)\\([ \t]\\|$\\)")
	(progn
	  (make-local-variable 'vu-application)
	  (setq vu-application (buffer-substring (match-beginning 1)
						  (match-end 1)))))))

;; This only exists to put on the menubar.  I couldn't figure out any
;; other way to do it.  FIXME should take "number of #-marks"
;; argument.
(defun vu-uncomment-region (beg end)
  "Uncomment region."
  (interactive "r")
  (comment-region beg end -1))



;;
;; XEmacs menu support.
;; Taken from schmid@fb3-s7.math.TU-Berlin.DE (Gregor Schmid),
;; who wrote a different Vu mode.
;; We also have support for menus in FSF.  We do this by
;; loading the XEmacs menu emulation code.
;;

(defun vu-popup-menu (e)
  (interactive "@e")
  (and vu-using-emacs-19
       (not vu-using-xemacs-19)
       (if vu-using-emacs-19-23
	   (require 'lmenu)
	 ;; CAVEATS:
	 ;; * lmenu.el provides 'menubar, which is bogus.
	 ;; * lmenu.el causes menubars to be turned on everywhere.
	 ;;   Doubly bogus!
	 ;; Both of these problems are fixed in Emacs 19.23.  People
	 ;; using an Emacs before that just suffer.
	 (require 'menubar "lmenu")))  ;; This is annoying
  ;; IMHO popup-menu should be autoloaded in FSF Emacs.  Oh well.
  (popup-menu vu-xemacs-menu))



;;
;; Quoting and unquoting functions.
;;

;; This quoting is sufficient to protect eg a filename from any sort
;; of expansion or splitting.  Vu quoting sure sucks.
(defun vu-quote (string)
  "Quote STRING according to Vu rules."
  (mapconcat (function (lambda (char)
			 (if (memq char '(?[ ?] ?{ ?} ?\\ ?\" ?$ ?  ?\;))
			     (concat "\\" (char-to-string char))
			   (char-to-string char))))
	     string ""))



;;
;; Bug reporting.
;;

(and (fboundp 'eval-when-compile)
     (eval-when-compile
       (require 'reporter)))

(defun vu-submit-bug-report ()
  "Submit via mail a bug report on Vu mode."
  (interactive)
  (require 'reporter)
  (and
   (y-or-n-p "Do you really want to submit a bug report on Vu mode? ")
   (reporter-submit-bug-report
    vu-maintainer
    (concat "Vu mode " vu-version)
    '(vu-indent-level
      vu-continued-indent-level
      vu-auto-newline
      vu-tab-always-indent
      vu-use-hairy-comment-detector
      vu-electric-hash-style
      vu-help-directory-list
      vu-use-smart-word-finder
      vu-application
      vu-command-switches
      vu-prompt-regexp
      inferior-vu-source-command
      vu-using-emacs-19
      vu-using-emacs-19-23
      vu-using-xemacs-19
      vu-proc-list
      vu-proc-regexp
      vu-typeword-list
      vu-keyword-list
      vu-font-lock-keywords
      vu-pps-has-arg-6))))



(provide 'vu)

;;; vu.el ends here
